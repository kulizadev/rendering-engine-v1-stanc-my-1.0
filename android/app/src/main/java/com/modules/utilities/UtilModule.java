package com.modules.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
// import com.kuliza.lendin.GCMRegisterTask;
import com.kuliza.lendin.MainActivity;
// import com.kuliza.lendin.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by @raj on 12/04/18.
 */
public class UtilModule extends ReactContextBaseJavaModule implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ActivityEventListener {
    private Context mContext;
    private Callback mCallback;
    private String gcmToken;
    private GoogleApiClient googleApiClient;
    private static final int REQUEST_CHECK_SETTINGS = 100;
    private Promise mPromise;
    private static final String ERR_USER_DENIED_CODE = "ERR00";
    private static final String ERR_SETTINGS_CHANGE_UNAVAILABLE_CODE = "ERR01";
    private static final String ERR_FAILED_OPEN_DIALOG_CODE = "ERR02";
    private static final String ERR_PERMISSIONS_NOT_SATISFIED = "ERR03";
    private static final String ERR_SOMETHING_WENT_WRONG = "ERR04";
    private static final String ERR_REACT_INTERNAL_BUG = "ERR05";

    public UtilModule(ReactApplicationContext reactContext) {
        super(reactContext);
        mContext = reactContext;
        reactContext.addActivityEventListener(this);
    }

    @Override
    public String getName() {
        return "Utilities";
    }

//    @ReactMethod
//    public void getDeviceToken(Callback callback) {
//        mCallback = callback;
//        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
//
//        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
//        gcmToken = sp.getString(mContext.getString(R.string.gcm_token), "");
//
//        if (TextUtils.isEmpty(gcmToken)) {
//            new GCMRegisterTask(mContext, gcm, this).execute();
//        } else {
//            callback.invoke(gcmToken);
//        }
//    }
//
//    @ReactMethod
//    public void setLocalization(String languageTag) {
//        MainActivity activity = (MainActivity)getReactApplicationContext().getCurrentActivity();
//        if (activity!=null) {
//            activity.changeLanguage(languageTag);
//        }
//    }

    @ReactMethod
    public void showGpsPopup(final Promise promise) {
        this.mPromise = promise;
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            StringBuilder missingPermissions = new StringBuilder("");
            missingPermissions.append("ACCESS_FINE_LOCATION\n");
            promise.reject(ERR_PERMISSIONS_NOT_SATISFIED, "Following permissions are not granted " + missingPermissions);
        }
        if (googleApiClient == null) {
            Activity activity = getReactApplicationContext().getCurrentActivity();
            if (activity!=null) {
                googleApiClient = new GoogleApiClient.Builder(activity)
                        .addApi(LocationServices.API)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this).build();
                googleApiClient.connect();
            } else {
                promise.reject(ERR_REACT_INTERNAL_BUG, "getCurrentActivity is null");
            }
        }
    }

    // @Override
    // public void gcmRegistered() {
    //     mCallback.invoke(gcmToken);
    // }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        mPromise.resolve("GPS Already Activated");
                        disconnectGoogleApiClient();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(((ReactApplicationContext) mContext).getCurrentActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            mPromise.reject(ERR_FAILED_OPEN_DIALOG_CODE, "Cannot open the dialog");
                            disconnectGoogleApiClient();
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        mPromise.reject(ERR_SETTINGS_CHANGE_UNAVAILABLE_CODE, "Location settings change is not allowed");
                        disconnectGoogleApiClient();
                        break;
                }
            }
        });
    }

    private void disconnectGoogleApiClient() {
        if (googleApiClient != null) {
            googleApiClient.disconnect();
            googleApiClient = null;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPromise.reject(ERR_SOMETHING_WENT_WRONG, "Google Client connection suspended");
        disconnectGoogleApiClient();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mPromise.reject(ERR_SOMETHING_WENT_WRONG, "Google Client connection failed");
        disconnectGoogleApiClient();
    }

    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHECK_SETTINGS && mPromise != null) {
            if (resultCode == RESULT_OK) {
                mPromise.resolve("enabled");
                disconnectGoogleApiClient();
            } else {
                mPromise.reject(ERR_USER_DENIED_CODE, "GPS setting denied by the user");
                disconnectGoogleApiClient();
            }
            this.mPromise = null;
        }
    }

    @Override
    public void onNewIntent(Intent intent) {

    }
}
