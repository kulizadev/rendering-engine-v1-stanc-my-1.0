# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /usr/local/Cellar/android-sdk/24.3.3/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Disabling obfuscation is useful if you collect stack traces from production crashes
# (unless you are using a system that supports de-obfuscate the stack traces).
#-dontobfuscate
#
## React Native
#
# Keep our interfaces so they can be used by other ProGuard rules.
# See http://sourceforge.net/p/proguard/bugs/466/
-keep,allowobfuscation @interface com.facebook.proguard.annotations.DoNotStrip
-keep,allowobfuscation @interface com.facebook.proguard.annotations.KeepGettersAndSetters
-keep,allowobfuscation @interface com.facebook.common.internal.DoNotStrip

# Do not strip any method/class that is annotated with @DoNotStrip
-keep @com.facebook.proguard.annotations.DoNotStrip class *
-keep @com.facebook.common.internal.DoNotStrip class *
-keepclassmembers class * {
    @com.facebook.proguard.annotations.DoNotStrip *;
    @com.facebook.common.internal.DoNotStrip *;
}

-keepclassmembers @com.facebook.proguard.annotations.KeepGettersAndSetters class * {
  void set*(***);
  *** get*();
}

-keep class * extends com.facebook.react.bridge.JavaScriptModule { *; }
-keep class * extends com.facebook.react.bridge.NativeModule { *; }
-keepclassmembers,includedescriptorclasses class * { native <methods>; }
-keepclassmembers class *  { @com.facebook.react.uimanager.UIProp <fields>; }
-keepclassmembers class *  { @com.facebook.react.uimanager.annotations.ReactProp <methods>; }
-keepclassmembers class *  { @com.facebook.react.uimanager.annotations.ReactPropGroup <methods>; }

-dontwarn com.facebook.react.**
#
## TextLayoutBuilder uses a non-public Android constructor within StaticLayout.
## See libs/proxy/src/main/java/com/facebook/fbui/textlayoutbuilder/proxy for details.
-dontwarn android.text.StaticLayout

-keep class com.facebook.react.devsupport.** { *; }
-dontwarn com.facebook.react.devsupport.**

-keep class com.facebook.react.cxxbridge.** { *; }
-keep class com.facebook.react.bridge.** { *; }

# SoLoader
-keep class com.facebook.soloader.** { *; }
-keepclassmembers class com.facebook.soloader.SoLoader {
   static <fields>;
}
-keep class com.facebook.react.devsupport.** { *; }
#
## okhttp
#
#-keepattributes Signature
#-keepattributes *Annotation*
#-keep class okhttp3.** { *; }
#-keep interface okhttp3.** { *; }
#-dontwarn okhttp3.**
#-dontwarn okio.**
#
##-keep public class * extends android.app.Activity
##
#-keep class com.google.gson.** { *; }
#-keep class com.google.inject.** { *; }
#-keep class org.apache.http.** { *; }
#-keep class org.apache.james.mime4j.** { *; }
#-keep class javax.inject.** { *; }
#-keep class retrofit.** { *; }
#-dontwarn org.apache.http.**
#-dontwarn android.net.http.AndroidHttpClient
#
#-dontwarn sun.misc.**
#
#-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
#   long producerIndex;
#   long consumerIndex;
#}
#
#-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
#   long producerNode;
#   long consumerNode;
#}

#####################################################################################
## RxJava
#####################################################################################
#-keep class rx.schedulers.Schedulers {
#    public static <methods>;
#}
#-keep class rx.schedulers.ImmediateScheduler {
#    public <methods>;
#}
#-keep class rx.schedulers.TestScheduler {
#    public <methods>;
#}
#-keep class rx.schedulers.Schedulers {
#    public static ** test();
#}
#
#-dontwarn com.google.**
#-dontwarn javax.annotation.*
#
#-dontwarn retrofit2.**
#-keep class retrofit2.** { *; }
#-keepattributes Signature
#-keepattributes Exceptions
#
#-keepclasseswithmembers class * {
#    @retrofit2.http.* <methods>;
#}
#
#####################################################################################
## Okio
#####################################################################################
#-keep class sun.misc.Unsafe { *; }
#-dontwarn java.nio.file.*
#-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
#-dontwarn okio.**
#
#####################################################################################
## Retrofit 2
#####################################################################################
## Platform calls Class.forName on types which do not exist on Android to determine platform.
#-dontnote retrofit2.Platform
## Platform used when running on RoboVM on iOS. Will not be used at runtime.
#-dontnote retrofit2.Platform$IOS$MainThreadExecutor
## Platform used when running on Java 8 VMs. Will not be used at runtime.
#-dontwarn retrofit2.Platform$Java8
## Retain generic type information for use by reflection by converters and adapters.
#-keepattributes Signature
## Retain declared checked exceptions for use by a Proxy instance.
#-keepattributes Exceptions
#
######################################################################################
### WebView
######################################################################################
#-keepclassmembers class * {
#    @android.webkit.JavascriptInterface <methods>;
#}
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
#-keep public class android.net.http.SslError
#-keep public class android.webkit.WebViewClient
#
#-dontwarn android.webkit.WebView
#-dontwarn android.net.http.SslError
#-dontwarn android.webkit.WebViewClient



#-dontobfuscate

# React Native

# Keep our interfaces so they can be used by other ProGuard rules.
# See http://sourceforge.net/p/proguard/bugs/466/
#-keep,allowobfuscation @interface com.facebook.proguard.annotations.DoNotStrip
#-keep,allowobfuscation @interface com.facebook.proguard.annotations.KeepGettersAndSetters
#-keep,allowobfuscation @interface com.facebook.common.internal.DoNotStrip

# Do not strip any method/class that is annotated with @DoNotStrip
#-keep @com.facebook.proguard.annotations.DoNotStrip class *
#-keep @com.facebook.common.internal.DoNotStrip class *
#-keepclassmembers class * {
# @com.facebook.proguard.annotations.DoNotStrip *;
# @com.facebook.common.internal.DoNotStrip *;
#}
#
#-keepclassmembers @com.facebook.proguard.annotations.KeepGettersAndSetters class * {
# void set*(***);
# *** get*();
#}

#-keep class * extends com.facebook.react.bridge.JavaScriptModule { *; }
#-keep class * extends com.facebook.react.bridge.NativeModule { *; }
#-keepclassmembers,includedescriptorclasses class * { native <methods>; }
#-keepclassmembers class *  { @com.facebook.react.uimanager.UIProp <fields>; }
#-keepclassmembers class *  { @com.facebook.react.uimanager.annotations.ReactProp <methods>; }
#-keepclassmembers class *  { @com.facebook.react.uimanager.annotations.ReactPropGroup <methods>; }

#-keep, includedescriptorclasses class com.facebook.** { *; }
#-dontwarn com.facebook.react.**

# okhttp

-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn javax.annotation.**
-dontwarn org.conscrypt.**
# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase

# okio

-keep, includedescriptorclasses class sun.misc.Unsafe { *; }
-keep, includedescriptorclasses class okio.AsyncTimeout { *; }
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn okio.**


#########################
# hyperverge
#########################
-keep, includedescriptorclasses class co.hyperverge.** { *; }


#########################
# GMS
#########################
-keep, includedescriptorclasses public class com.google.android.gms.** { public *; }
-dontwarn com.google.android.gms.**

-keep class com.google.protobuf.zzc
-keep class com.google.protobuf.zzd
-keep class com.google.protobuf.zze

-keep class com.google.android.gms.dynamic.IObjectWrapper
-dontwarn com.google.android.gms.internal.**
-dontnote com.google.android.gms.internal.**
-keep class com.google.android.gms.tagmanager.zzce
-keep class com.google.android.gms.tagmanager.zzcn
-keep class com.google.android.gms.plus.PlusOneButton$OnPlusOneClickListener
-keep class com.google.android.gms.measurement.AppMeasurement$zza
-keep class com.google.android.gms.measurement.AppMeasurement$OnEventListener
-keep class com.google.android.gms.measurement.AppMeasurement$EventInterceptor

-keep class com.google.android.gms.ads.mediation.MediationAdRequest
-keep class com.google.android.gms.ads.mediation.MediationBannerListener
-keep class com.google.android.gms.ads.AdSize
-keep class com.google.android.gms.ads.mediation.MediationInterstitialListener
-keep class com.google.android.gms.ads.mediation.MediationNativeListener
-keep class com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdListener
-keep class com.google.android.gms.ads.InterstitialAd
-keep class com.google.android.gms.ads.AdListener
-keep class com.google.android.gms.ads.Correlator
-keep class com.google.android.gms.ads.formats.NativeAd
-keep class com.google.android.gms.ads.mediation.NativeMediationAdRequest
-keep class com.google.android.gms.ads.formats.MediaView
-keep class com.google.android.gms.ads.formats.AdChoicesView
-keep class com.google.android.gms.ads.mediation.NativeMediationAdRequest
-keep class com.google.android.gms.ads.VideoOptions
-keep class com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener
-keep class com.google.android.gms.ads.mediation.customevent.CustomEventInterstitialListener
-keep class com.google.android.gms.ads.doubleclick.AppEventListener
-keep class com.google.android.gms.ads.mediation.customevent.CustomEventBannerListener
-keep class com.google.android.gms.ads.mediation.customevent.CustomEventNativeListener
-keep class com.google.android.gms.ads.mediation.customevent.CustomEventExtras

-keep class com.google.ads.mediation.MediationServerParameters
-keep class com.google.ads.mediation.NetworkExtras
-keep class com.google.ads.mediation.MediationInterstitialListener
-keep class com.google.ads.mediation.customevent.CustomEventServerParameters
-keep class com.google.ads.mediation.MediationBannerListener
-keep class com.google.ads.AdSize
-keep class com.google.ads.mediation.MediationAdRequest
-keep class com.google.ads.mediation.customevent.CustomEventBannerListener
-keep class com.google.ads.mediation.customevent.CustomEventInterstitialListener

-keep class com.google.firebase.FirebaseApp
-keep class com.google.firebase.database.connection.idl.zzah
-keep class com.google.firebase.database.connection.idl.zzq
-keep class com.google.firebase.database.connection.idl.zzw
-keep class com.google.firebase.database.connection.idl.zzk

-dontnote com.google.protobuf.zzc
-dontnote com.google.protobuf.zzd
-dontnote com.google.protobuf.zze
-dontnote com.google.android.gms.internal.q
-dontnote com.google.android.gms.internal.zzcem

-keep class com.google.android.gms.cast.framework.OptionsProvider

#########################
# gesturehandler
#########################
-keep, includedescriptorclasses class com.swmansion.gesturehandler.** { *; }
-keepclasseswithmembers, includedescriptorclasses class org.devio.** { *; }


-dontwarn com.amazonaws.**
-dontnote com.amazonaws.**
-dontwarn com.google.**
-dontnote com.google.**

-keep class com.kuliza.lendin.BuildConfig { *; }