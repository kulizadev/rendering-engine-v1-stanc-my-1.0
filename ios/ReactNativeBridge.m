//
//  ReactNativeBridge.m
//  NativeRenderingEngine
//
//  Created by Kuliza-282 on 11/04/18.
//  Copyright © 2018 Facebook. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTConvert.h>

@interface RCT_EXTERN_MODULE(CameraModule, NSObject)

RCT_EXTERN_METHOD(openCamera:(NSString *)type title:(NSString *)title instruction:(NSString *)instruction callback:(RCTResponseSenderBlock *)successCallback);

RCT_EXTERN_METHOD(initHyperverge:(NSString *)appId appKey:(NSString *)appKey region:(NSString *)region product:(NSString *)product);

RCT_EXTERN_METHOD(takeSelfie:(RCTResponseSenderBlock *)successCallback);

@end

@interface RCT_EXTERN_MODULE(LendingUtils, NSObject)


RCT_EXTERN_METHOD(openHVFBActivity:(NSString *)imageUri completionHook:(NSString *)completionHook appID:(NSString *)appId appKey:(NSString *)appKey callback:(RCTResponseSenderBlock *)successCallback);

@end

@interface RCT_EXTERN_MODULE(Utilities, NSObject)

RCT_EXTERN_METHOD(getDeviceToken:(RCTResponseSenderBlock *)successCallback);


@end

@interface RCT_EXTERN_MODULE(PayooModule, NSObject)

RCT_EXTERN_METHOD(openPayoo:(NSString *)locale email:(NSString *)email phoneNo:(NSString *)phoneNo loanId:(NSString *)loanId callback:(RCTResponseSenderBlock *)successCallback);

@end









