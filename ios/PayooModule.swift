//
//  PayooModule.swift
//  NativeRenderingEngine
//
//  Created by Vaibhav Sharma on 30/05/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

//import PayooServiceSDK
//import PayooSDK
import Foundation

private let merchantID = "746"
private let secretKeyString = "8a77de405da7967d5af5113c73fe51d8"

/***
 // Response : [["status": status, "data": data]]
 ***/

@objc(PayooModule)
class PayooModule: NSObject {

  @objc func openPayoo(_ locale: String,
                       email: String,
                       phoneNo: String,
                       loanId: String,
                       callback successCallback: @escaping RCTResponseSenderBlock) {
    successCallback([])
    /*
      if locale.lowercased() == "en-us" {
        Configuration.set(language: .english)
      } else {
        Configuration.set(language: .vietnamese)
      }
    
      let paymentConfig = PaymentConfig.makeBuilder()
        .defaultCustomerEmail(email)
        .defaultCustomerPhone(phoneNo)
        .build()
    
    PayooService.presentQueryBill(customerId: loanId, paymentConfig: paymentConfig) { (status, response) in
      var responseDataDictionary: [String: Any] = [:]
      var dataDictionary: [String: Any] = [:]
      responseDataDictionary["status"] = status.rawValue
      if let response = response {
        dataDictionary["code"] = response.code
        dataDictionary["message"] = response.message ?? ""
        
        var internalDataDictionary: [String: Any] = [:]
        if let internalData = response.data {
          internalDataDictionary["orderId"] = internalData.orderId ?? ""
          internalDataDictionary["customerEmail"] = internalData.customerEmail ?? ""
          internalDataDictionary["orderXml"] = internalData.orderXml ?? ""
          internalDataDictionary["paymentCode"] = internalData.paymentCode ?? ""
          internalDataDictionary["authToken"] = internalData.authToken ?? ""
        }
        dataDictionary["data"] = internalDataDictionary
      }
      responseDataDictionary["responseData"] = dataDictionary
      guard let jsonData = try? JSONSerialization.data(withJSONObject: responseDataDictionary),
        let jsonString = String(data: jsonData, encoding: String.Encoding.utf8) else {
          return
      }
      successCallback([jsonString])
    } */
  }
  
  /*private func setPayooSDKKeys() {
    PayooServiceSDK.Configuration.set(merchantId: merchantID, secretKey:secretKeyString)
    PayooServiceSDK.Configuration.set(environment: .development)
  }*/
}


