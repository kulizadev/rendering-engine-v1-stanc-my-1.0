//
//  Utilities.swift
//  NativeRenderingEngine
//
//  Created by Kuliza-282 on 18/04/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

import Foundation


@objc(Utilities)
class Utilities: NSObject {
  
  @objc func methodQueue() ->  DispatchQueue {
    return DispatchQueue.main
  }
  
  @objc func getDeviceToken(_ successCallback: @escaping RCTResponseSenderBlock){
       let defaults = UserDefaults.standard
      if let deviceToken = defaults.string(forKey: "deviceToken") {
          successCallback([deviceToken])
      }else {
         successCallback([""])
      }
  }
}

