//
//  CameraModule.swift
//  NativeRenderingEngine
//
//  Created by Kuliza-282 on 11/04/18.
//  Copyright © 2018 Facebook. All rights reserved.
//


import Foundation
import HyperSnapSDK
import UIKit

@objc(CameraModule)
class CameraModule: NSObject {

  private var presentedController: UIViewController?

  @objc func methodQueue() ->  DispatchQueue {
    return DispatchQueue.main
  }

  @objc func initHyperverge(_ appId:String, appKey:String,  region:String,  product:String) {

    HyperSnapSDK.initialize(appId: appId, appKey: appKey, region: .AsiaPacific, product: .faceID)
  }
  
  @objc func openCamera(_ type:String, title:String, instruction:String, callback successCallback: @escaping RCTResponseSenderBlock) {
    print("coming inside")
    var document:HyperSnapParams.Document
    switch (type){
    case "VIETNAM_NATIONAL_ID", "DRIVING_LICENCE", "MOTOR_REGISTRATION_CERTIFICATE":
      document = HyperSnapParams.Document(type: .card)
      break;
    case "BANK_STATEMENT", "INSURANCE_RECEIPT","ELECTRICITY_BILL":
      document = HyperSnapParams.Document(type: .a4)
      break;
    case "PASSPORT":
      document = HyperSnapParams.Document(type: .passport)
      break;
    default:
      document = HyperSnapParams.Document(type: .card);
      break;
    }

    let bundle = Bundle(for: HVFaceViewController.self)
    let vc = UIStoryboard(name: HyperSnapSDK.StoryBoardName, bundle:bundle).instantiateViewController(withIdentifier: "HVDocsViewController") as! HVDocsViewController

    //Set ViewController properties (described later)
    vc.document = document
    vc.topText = title
    vc.bottomText = instruction

    vc.completionHandler = { error,result in

      guard error == nil else{
        successCallback([error])
        self.dismissView()
        return
      }

      guard let result = result, let imageUri = result["imageUri"] as? String else{
        successCallback([error])
        self.dismissView()
        return
      }

      let pathURl = NSURL(fileURLWithPath: imageUri)
      let fileName = self.getFileName(pathURl: pathURl)
      let size = self.getFileSize(imageUri: imageUri)
      let pathExtension = self.getPathExtension(pathURl: pathURl)

      var data: [String: String] = [:]
      data["fileName"] = fileName
      data["fileSize"] = size
      data["type"] = pathExtension
      data["uri"] = imageUri


      do{
        let data = try JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        if let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) as? String {
          successCallback([jsonString])
          self.dismissView()
        }
      }catch {
        successCallback([error])
        self.dismissView()
      }
    }
    if let appDelegate = UIApplication.shared.delegate  {

      if let currentViewController = appDelegate.window!?.rootViewController {
        currentViewController.present(vc, animated: true, completion: nil)
      }

    }


  }

  private func dismissView(){
     
    let rootViewController:UIViewController = (UIApplication.shared.keyWindow?.rootViewController)!
    if (rootViewController.presentedViewController != nil) {
      print(rootViewController.presentedViewController)
      rootViewController.presentedViewController?.dismiss(animated: true, completion: nil)
      guard let controller = self.presentedController else { return }
      print(controller.self)
      rootViewController.present(controller, animated: true, completion: nil)
    }
  }


  @objc func takeSelfie(_ successCallback:@escaping RCTResponseSenderBlock){

    let bundle = Bundle(for: HyperSnapSDK.self)
    let vc = UIStoryboard(name: HyperSnapSDK.StoryBoardName, bundle:bundle).instantiateViewController(withIdentifier: "HVFaceViewController") as! HVFaceViewController
    //Set ViewController properties
    vc.setLivenessMode(HyperSnapParams.LivenessMode.textureLiveness)
    vc.completionHandler = {error, result,vcNew in
      guard error == nil,let result = result else{
        
        print("Error received - Code:\(error!.code), Description:\(error!.userInfo[NSLocalizedDescriptionKey] ?? "No Description")")
        successCallback([error!])
        self.dismissView()
        return
        
      }
      if let imageUri = result["imageUri"] as? String {
        let pathURl = NSURL(fileURLWithPath: imageUri)
        let fileName = self.getFileName(pathURl: pathURl)
        let size = self.getFileSize(imageUri: imageUri)
        let pathExtension = self.getPathExtension(pathURl: pathURl)
        
        var data: [String: String] = [:]
        data["fileName"] = fileName
        data["fileSize"] = size
        data["type"] = pathExtension
        data["uri"] = imageUri
        data["live"] = result["live"] as? String
        data["livenessScore"] = result["liveness-score"] as? String
        data["toBeReviewed"] = result["to-be-reviewed"] as? String
        
        do {
          let data = try JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
          if let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) as? String {
            successCallback([jsonString])
            self.dismissView()
          }
        } catch {
          successCallback([error])
          self.dismissView()
        }
      } else {
        print("No image Uri")
        successCallback([error])
        self.dismissView()
      }
    }
    
    if let appDelegate = UIApplication.shared.delegate  {

      if let currentViewController = appDelegate.window!?.rootViewController {
        print(currentViewController.self)
        print(currentViewController.childViewControllers)
        presentedController = currentViewController.presentedViewController
        presentedController?.dismiss(animated: true, completion: nil)
        //currentViewController.navigationController?.pushViewController(vc, animated: true)
        currentViewController.present(vc, animated: true, completion: nil)
      }

    }

  }

  private func getFileName(pathURl:NSURL) -> String {
    var fileName = ""
    if let path = pathURl.lastPathComponent {
      var myStringArr = path.components(separatedBy: ".")
      if myStringArr.count > 0 {
        fileName = myStringArr[0] + "." + myStringArr[1]
      }
    }
    return fileName
  }

  private func getFileSize(imageUri:String) -> String {
    var fileSize = ""
    do
    {
      let fileDictionary = try FileManager.default.attributesOfItem(atPath: imageUri)
      fileSize = String((fileDictionary[FileAttributeKey.size] as? Int)! / 1024)
      return fileSize
    }

    catch{
      return fileSize
    }
  }

  private func getPathExtension(pathURl:NSURL) ->String {
    if  let type = pathURl.pathExtension {
      return type
    }
    return ""
  }


}
