//
//  LendingUtils.swift
//  NativeRenderingEngine
//
//  Created by Kuliza-282 on 12/04/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

import Foundation
import HVLendingUtils
import HyperSnapSDK


@objc(LendingUtils)
class LendingUtils: NSObject {
  
  
  @objc func methodQueue() ->  DispatchQueue {
    return DispatchQueue.main
  }
  
  
 /***
   // Response : Error case : [null, ["error:"Error Recived"]]
   /// Success Case : [["success:"Success"], null]
 ***/
  
  @objc func openHVFBActivity(_ imageUri:String, completionHook completionHook:String, appID appId:String, appKey appKey:String, callback successCallback: @escaping RCTResponseSenderBlock) {
    
    
    let bundle = Bundle(for: HVLendingUtils.self)
    let vc = UIStoryboard(name: HVLendingUtils.StoryBoardName, bundle:bundle).instantiateViewController(withIdentifier: "HVFBManagerViewController") as! HVFBManagerViewController
    let completionHook1 = "https://requestbin.fullcontact.com/q026mtq0"
    
    vc.setProperties(imageUri, appId: appId, appKey: appKey, completionHook: completionHook1){
      error,results in
      guard error == nil else{
        var data: [String: String] = [:]
        data["error"] = "Error Recieved"
        do {
            let data = try JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
            if let errorString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) as? String {
            successCallback([NSNull(), errorString])
            self.dismissView()
            return
          }
          return
        }catch {
             print("error")
            return
        }
      }
      
      //Success scenario
      if let result  = results {
        var data: [String: String] = [:]
        data["success"] = "Success"
        do {
          let data = try JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
          if let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) as? String {
            successCallback([jsonString, NSNull()])
            self.dismissView()
          }
        }catch {
          print("error")
        }
        
      }
      
    }
    
    if let appDelegate = UIApplication.shared.delegate  {
      
      if let currentViewController = appDelegate.window!?.rootViewController {
        currentViewController.present(vc, animated: true, completion: nil)
      }
      
    }
    
  }
  
  private func dismissView(){
    
    let rootViewController:UIViewController = (UIApplication.shared.keyWindow?.rootViewController)!
    
    if (rootViewController.presentedViewController != nil) {
      rootViewController.dismiss(animated: true, completion: {
        //completion block.
      })
    }
  }
  
  
  
}
