## TODO: NEED TO UPDATE IN FUTURE COMMITS ##

#### install react-native-cli globally

#### install the simulators for ios and android (Xcode for ios - android to be done)

### Run
	npm install

#### For Web

	npm run start


#### For iOS

	### For First time
	sudo gem install cocoapods
	cd ios && pod install

	react-native run-ios


#### For Android


[Install JDK 7 or above.](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

[Install Android Studio](https://developer.android.com/studio/index.html)

Configure Android studio SDK Manager

1. In SDK Platform,

      * Check Show Package Details.
    
      * Check Google APIs Intel x86 Atom System Image for the required OS version.
    
      * Check Google Play Intel x86 Atom System Image for the required OS version.

2. In SDK Tools

      * Check NDK for installation

  
Open android folder in Android Studio and set up emulator for project to run.

##### Run
	### For First time
	react-native link react-native-vector-icons
	
	npm run android (for dev config)

	npm run android:prod (for prod config)

	npm run android:uat (for uat config)

##### Build

	npm run android:build

	npm run android:uat:build (for uat build config)