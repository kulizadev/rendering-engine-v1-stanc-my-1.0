/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import SplashScreen from 'react-native-splash-screen';
import store from './/app/utils/store';
import {
  Platform,
  StyleSheet,
  View,
} from 'react-native';

import AppDetails from './app/containers/AppDetails/';
import { colors } from './app/configs/styleVars';
import { appBreakPoints } from './app/configs/appConstants';

import './app/utils/configureLogger.native';

// Create redux store with history

type Props = {};

import { setBreakPoints } from 'react-native-responsive-grid';

export default class App extends Component<Props> {

  componentDidMount() {
    SplashScreen.hide();

    setBreakPoints({
      SMALL_Width: appBreakPoints.SMALL_Width,
      MEDIUM_Width: appBreakPoints.MEDIUM_Width,
      LARGE_Width: appBreakPoints.LARGE_Width,
    });
  }

  render() {
    const renderBlock = <AppDetails />;
    return (
      <Provider store={store}>
        <View style={styles.container}>
          {renderBlock}
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    backgroundColor: colors.secondaryBGColor,
    paddingTop: (Platform.OS === 'ios') ? 20 : 0,
    flex: 1,
  },
});
