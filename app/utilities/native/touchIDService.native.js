import TouchID from 'react-native-touch-id';
import { Platform } from 'react-native';

export const checkSupport = async () => {
  try {
    if (Platform.OS === 'android' && Platform.Version <= 23) {
      return false;
    }
    const biometryType = await TouchID.isSupported();
    if (biometryType === 'TouchID' || biometryType === true) {
      return true;
    }
    // biometryType === 'TouchID' for iOS & biometryType === true for android
    // biometryType === 'FaceID' also possible

    return false;
  } catch (err) {
    return false;
  }
};

export const authenticate = async () => {
  try {
    const authStatus = await TouchID.authenticate('Place your finger on the Home button to sign in to your account.');
    if (authStatus) {
      return true;
    }
  } catch (err) {
    return false;
  }
};
