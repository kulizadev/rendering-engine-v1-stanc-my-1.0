import * as permissionService from '../../utilities/native/permissionService';
import { permissionConst } from '../configs/appConstants';

const getSms = () => {
  NativeModules.CallList.getSms((smsList) => {
    console.log(smsList);
    return smsList;
  });
};

const getLocation = () => {
  NativeModules.CallList.getLocation((status, lat, long) => {
    if (status == true) {
      console.log(`lat ${lat}long${long}`);
      return {
        lat,
        long,
      };
    }
    console.log('error');
  });
};

const getCallDetails = () => {
  NativeModules.CallList.getCallDetails((callList) => {
    console.log(callList);
    return callList;
  });
};

export const scrapData = async () => {
  try {
    const granted = await permissionService.getPermission(['getLocation', 'readCallLog', 'readSms']);
    if (granted === permissionConst.GRANTED) {
      const smsList = getSms();
      const location = getLocation();
      const callList = getCallDetails();
      return {
        smsList,
        location,
        callList,
      };
    }
  } catch (err) {
    console.warn(err);
  }
};

export const scrapSms = async () => {
  try {
    const granted = await permissionService.getPermission('readSms');
    if (granted === permissionConst.GRANTED) {
      return getSms();
    }
  } catch (err) {
    console.warn(err);
  }
};

export const scrapCallLog = async () => {
  try {
    const granted = await permissionService.getPermission('readCallLog');
    if (granted === permissionConst.GRANTED) {
      return getCallDetails();
    }
  } catch (err) {
    console.warn(err);
  }
};

export const scrapLocation = async () => {
  try {
    const granted = await permissionService.getPermission('getLocation');
    if (granted === permissionConst.GRANTED) {
      return getLocation();
    }
  } catch (err) {
    console.warn(err);
  }
};
