import { Platform, Linking } from 'react-native';
import { permissionConst } from '../../configs/appConstants';
import RNFetchBlob from 'react-native-fetch-blob';
import * as common from '../../utilities/commonUtils';
import * as permissionService from './permissionService';


export const getPermission = async () => {
  try {
    const granted = await permissionService.getPermission(['storageWrite']);
    return granted;
  } catch (err) {
    console.warn(err);
    return false;
  }
};

export const download = async (url, docType) => {
  const permissionStatus = await getPermission();
  if (permissionStatus === permissionConst.GRANTED && url && docType) {
    if (Platform.OS === 'ios') {
      const date = new Date();
      const DownloadDir = Platform.OS === 'ios' ? RNFetchBlob.fs.dirs.DocumentDir : RNFetchBlob.fs.dirs.DownloadDir;
      const options = {
        fileCache: true,
        overwrite: false,
        path: `${DownloadDir}/FEC-doc.${docType}`,
        addAndroidDownloads: {
          useDownloadManager: true,
          notification: true,
          path: `${DownloadDir}/FEC-doc.${docType}`,
          mime: common.getMimetype(docType),
        },
      };
      RNFetchBlob
        .config(options)
        .fetch('GET', url)
        .then((res) => {
          if (Platform.OS === 'ios') {
            RNFetchBlob.ios.openDocument(res.path());
          } else {
            // TODO: need to handle the viewing part
            // RNFetchBlob.android.actionViewIntent(res.path(), common.getMimetype(docType));
            res.path();
          }
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      Linking.canOpenURL(url).then((supported) => {
        if (supported) {
          Linking.openURL(url);
        } else {
          console.log(`Don't know how to open URL: ${url}`);
        }
      });
    }
  }
};
