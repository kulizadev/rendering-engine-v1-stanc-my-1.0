import { NativeModules, PermissionsAndroid } from 'react-native';
import { permissionConst as constants } from '../../configs/appConstants';

const permissionsMap = {
  camera: PermissionsAndroid.PERMISSIONS.CAMERA,
  storageWrite: PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
  storageRead: PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
  getLocation: PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  readCallLog: PermissionsAndroid.PERMISSIONS.READ_CALL_LOG,
  readSms: PermissionsAndroid.PERMISSIONS.READ_SMS,
};

export const getPermission = async (permissions) => {
  const tempArr = Array.isArray(permissions) ? permissions : [permissions];
  const permissionsArr = tempArr.map((permission) => permissionsMap[permission]);
  let permissionResult = constants.GRANTED;
  try {
    const granted = await PermissionsAndroid.requestMultiple(permissionsArr);

    Object.keys(granted).forEach((permission) => {
      if (granted[permission] !== PermissionsAndroid.RESULTS.GRANTED) {
        permissionResult = constants.DENIED;
      }
    });

    return permissionResult;
  } catch (err) {
    console.warn(err);
  }
};
