// NavigationService.js

import { NavigationActions, DrawerActions } from 'react-navigation';

let navigator;

export const setTopLevelNavigator = (navigatorRef) => {
  navigator = navigatorRef;
};

export const navigate = (routeName, params) => {
  navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );
};

export const toggleDrawer = () => {
  navigator.dispatch(
    DrawerActions.toggleDrawer()
  );
};
