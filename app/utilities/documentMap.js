export const documentType = {
  vietnamNationalId: 'CARD',
  electricityBill: 'A4',
  bankStatement: 'A4',
  insuranceReceipt: 'A4',
  drivingLicence: 'CARD',
  motorRegistrationCertificate: 'CARD',
};

export const getDocumentType = (type) => {
  const docType = type.split('-');
  return documentType[docType[0]];
};
