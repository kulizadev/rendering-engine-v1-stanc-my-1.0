/* This is storage util file for native data
contains:
  getters-setters in AsyncStorage
*/

import { AsyncStorage } from 'react-native';

const sessionStorage = AsyncStorage;
// Get an item from the sessionStorage
export const getSessionKey = (key) => AsyncStorage.getItem(key);

// Set any key with the value
export const setSessionKey = async (key, value) => AsyncStorage.setItem(key, value);

// Remove an item from the sessionStorage
export const deleteSessionKey = async (key) => AsyncStorage.removeItem(key);
