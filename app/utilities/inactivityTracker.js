import { appConst } from '../configs/appConfig';
let tracker;
let defaultCallBack = null;

export const setDefaultCallback = (callback) => {
  defaultCallBack = callback;
};

export const clearTracker = () => {
  if (tracker) {
    window.clearTimeout(tracker);
  }
};

export const resetInactivity = (callback = defaultCallBack) => {

  // if (tracker) {
  //   window.clearTimeout(tracker);
  // }
  // if (callback) {
  //   tracker = window.setTimeout(() => {
  //     callback();
  //   }, 60000 * appConst.inactivityTime);
  // }
};
