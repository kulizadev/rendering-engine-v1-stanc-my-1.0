/* This is sesstion util file
contains:
  getters-setters in sessionStorage
*/

const sessionStorage = window && window.sessionStorage;
// Get an item from the sessionStorage
export const getSessionKey = async (key) => {
  const value = await (sessionStorage && sessionStorage.getItem(key));
  return value;
};

// Set any key with the value
export const setSessionKey = async (key, value) => await (sessionStorage && sessionStorage.setItem(key, value));

// Remove an item from the sessionStorage
export const deleteSessionKey = async (key) => await (sessionStorage && sessionStorage.removeItem(key));
