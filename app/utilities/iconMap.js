import en from '../images/en.png';
import vi from '../images/vi.png';
import ID from '../images/nationalId.png';
import down from '../images/down.png';
import rightArrow from '../images/rightArrow.png';
import radioSelect from '../images/radioSelect.png';
import radioDeselect from '../images/radioDeselect.png';
import checkBoxChecked from '../images/checked.png';
import checkBoxUnchecked from '../images/unchecked.png';
import minus from '../images/minus.png';
import plus from '../images/plus.png';

import selfie from '../images/selfie.png';
import fb from '../images/facebook.png';
import completed from '../images/completed.png';

import paymentCash from '../images/paymentCash.png';
import paymentBanking from '../images/paymentIBanking.png';
import payoo from '../images/payoo.png';
import vnp from '../images/vnp.png';
import vpb from '../images/vpb.png';
import bidv from '../images/bidv.png';
import agb from '../images/agb.png';
import faq from '../images/faq.png';
const iconMap = {
  en,
  vi,
  ID,
  down,
  rightArrow,
  radioSelect,
  radioDeselect,
  checkBoxChecked,
  checkBoxUnchecked,
  minus,
  plus,
  selfie,
  fb,
  completed,
  paymentBanking,
  paymentCash,
  payoo,
  vnp,
  vpb,
  bidv,
  agb,
  faq,
};

export default iconMap;
