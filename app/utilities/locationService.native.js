import { Platform, NativeModules } from 'react-native';
import { getPermission } from './native/permissionService';
const DEFAULT_OPTIONS = {
  maximumAge: 10000,
  timeout: 10000,
};

export const getLocation = (options = DEFAULT_OPTIONS) => new Promise(async (resolve, reject) => {
  if (Platform.OS === 'android') {
    try {
      const locationPermission = await getPermission('getLocation');
      if (locationPermission) {
        const isLocationOn = await NativeModules.Utilities.showGpsPopup();
        if (isLocationOn) {
          navigator.geolocation.getCurrentPosition(resolve, reject, options);
        }
      } else {
        reject('Permission Denied');
      }
    } catch (error) {
      reject('Permission Denied');
    }
  } else {
    navigator.geolocation.getCurrentPosition(resolve, reject, options);
  }
});
