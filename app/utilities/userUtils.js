/* This is a user utils file
Contains:
  All the user/token related information
  Getters and Setters User information
*/

import * as sessionUtils from './sessionUtils';
import { userConfig } from '../configs/appConfig';
const packagePrefix = userConfig.runAsPlugin ? 'u-ku-pkg-' : '';
const userKeys = ['mobileNumber', 'emailId', 'taskId', 'processInstanceId', 'bpm_token', 'refreshToken', 'django_token', 'otp', 'mpin_setup', 'mpin', 'mpinConfirmed', 'journeyStarted', 'useTouchID', 'introCompleted'];


// User is logged In || Any task is in session
export function isLoggedIn() {
  let logInFlag = true;
  userKeys.forEach(async (key) => {
    const value = await sessionUtils.getSessionKey(key);
    if (!value || value === '') {
      logInFlag = false;
    }
  });

  return logInFlag;
}

export const setUserDetails = async (data) => {
  if (data.mobileNumber) {
    await sessionUtils.setSessionKey(`${packagePrefix}mobileNumber`, data.mobileNumber);
  }
  if (data.emailId) {
    await sessionUtils.setSessionKey('emailId', data.emailId);
  }
  if (data.leadId) {
    await sessionUtils.setSessionKey('leadId', data.leadId);
  }
  if (data.bpm_token) {
    await sessionUtils.setSessionKey(`${packagePrefix}bpm_token`, data.bpm_token);
  }
  if (data.refreshToken) {
    await sessionUtils.setSessionKey(`${packagePrefix}refreshToken`, data.refreshToken);
  }
  if (data.django_token) {
    await sessionUtils.setSessionKey(`${packagePrefix}django_token`, data.django_token);
  }
  if (data.taskId) {
    await sessionUtils.setSessionKey(`${packagePrefix}taskId`, data.taskId);
  }
  if (data.processInstanceId) {
    await sessionUtils.setSessionKey(`${packagePrefix}processInstanceId`, data.processInstanceId);
  }
  if (data.otp) {
    await sessionUtils.setSessionKey(`${packagePrefix}otp`, data.otp);
  }
  if (data.mpin_setup) {
    await sessionUtils.setSessionKey(`${packagePrefix}mpin_setup`, data.mpin_setup);
  }
  if (data.mpin) {
    await sessionUtils.setSessionKey(`${packagePrefix}mpin`, data.mpin);
  }
  if (data.mpinConfirmed) {
    await sessionUtils.setSessionKey(`${packagePrefix}mpinConfirmed`, data.mpinConfirmed);
  }
  if (data.journeyStarted) {
    await sessionUtils.setSessionKey(`${packagePrefix}journeyStarted`, data.journeyStarted);
  }
  if (data.useTouchID) {
    await sessionUtils.setSessionKey(`${packagePrefix}useTouchID`, data.useTouchID);
  }
  if (data.introCompleted) {
    await sessionUtils.setSessionKey(`${packagePrefix}introCompleted`, data.introCompleted);
  }
};

export const getSessionUserDetails = async () => {
  const obj = {};
  await Promise.all(userKeys.map(async (key) => {
    obj[key] = await sessionUtils.getSessionKey(packagePrefix + key);
  }));

  return obj;
};

export const removeUserKeys = async (keys, userObj) => {
  const obj = { ...userObj };
  if (Array.isArray(keys)) {
    await Promise.all(keys.map(async (key) => {
      obj[key] = await sessionUtils.deleteSessionKey(key);
    }));
  } else {
    obj[keys] = await sessionUtils.deleteSessionKey(keys);
  }

  return obj;
};

export const logout = async () => {
  const obj = {};
  await Promise.all(userKeys.map(async (key) => {
    key !== `${packagePrefix}introCompleted` ? obj[key] = await sessionUtils.deleteSessionKey(packagePrefix + key) : obj[key] = packagePrefix + key;
  }));

  return obj;
};
