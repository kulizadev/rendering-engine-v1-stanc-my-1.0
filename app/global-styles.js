import { injectGlobal } from 'styled-components';
import styleVars from 'configs/styleVars';

import LatoRegular from './fonts/Lato-Regular.ttf';
import LatoSemibold from './fonts/Lato-Regular.ttf';

const fonts = styleVars.fonts,
  colors = styleVars.colors;

/* eslint no-unused-expressions: 0 */
injectGlobal`
  @font-face {
    font-family: 'Lato-Regular';
    src: url('${LatoRegular}') format('truetype');
  }

  @font-face {
    font-family: 'Lato-Semibold';
    src: url('${LatoSemibold}') format('truetype');
  }

  html,
  body {
    height: 100%;
    width: 100%;
    color: ${colors.basicFontColor};
    margin: 0;
    padding: 0;
  }


  #ku-lendin-journey-container {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
    font-family: ${fonts.primaryFontFamily};

    p,
    label,
    div,
    span {
      font-family: ${fonts.primaryFontFamily};
      //line-height: 1.5em;
      //color: ${colors.basicFontColor};
    }

    input[type=text]:focus,
    input[type=number]:focus,
    input[type=email]:focus,
    input[type=password]:focus {
      box-shadow: none !important;
      -webkit-box-shadow: none !important;
      -moz-box-shadow: none !important;
    }
  }

`;
