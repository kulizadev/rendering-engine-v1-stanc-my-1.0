/**
 *
 * CustomerProfile
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { View, Image } from 'react-native';
import { connect } from 'react-redux';
import { compose } from 'redux';

import RenderSubHeader from '../../components/RenderSubHeader';
import RenderDetailList from '../../components/RenderDetailList';
import * as inactivityTracker from '../../utilities/inactivityTracker';

import { imageStyle, imageWrapperStyle, profileListStyle } from './style';

export class CustomerProfile extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      profileInfo: {},
    };
    this.resetInactivity = this.resetInactivity.bind(this);
  }

  componentDidMount() {
    const reqObj = {
      key: 'userData',
      data: {
        successCb: (response) => {
          this.setState({ profileInfo: response.data });
        },
        userDetails: this.props.screenProps.userDetails,
      },
    };
    this.props.screenProps.getRequest(reqObj);
    this.resetInactivity();
  }

  componentWillUnmount() {
    inactivityTracker.clearTracker();
  }

  toggleMpin = () => {
    this.props.screenProps.toggleMpin(false);
  }

  // logs out user in case of inactivity for x min
  resetInactivity() {
    const self = this;
    inactivityTracker.resetInactivity(this.toggleMpin);
  }

  getProfileInfo = () => {
    const { profileInfo } = this.state;
    const profileInfoList = [
      {
        id: 'name',
        label: 'Name',
        value: profileInfo.name,
      },
      {
        id: 'age',
        label: 'Age',
        value: profileInfo.age,
      },
      {
        id: 'sex',
        label: 'Sex',
        value: profileInfo.sex,
      },
      {
        id: 'nationalId',
        label: 'National Id',
        value: profileInfo.nationalId,
      },
      {
        id: 'contactNumber',
        label: 'Contact Number',
        value: profileInfo.contactNumber,
      },
      {
        id: 'emailId',
        label: 'Email ID',
        value: profileInfo.emailId,
      },
      {
        id: 'customerSince',
        label: 'Customer Since',
        value: profileInfo.customerSince,
      },
    ];
    return profileInfoList;
  }
  render() {
    const profileInfo = this.getProfileInfo();
    return (
      <View>
        {this.props.screenProps.appHeader}
        <RenderSubHeader>{'Customer Profile'}</RenderSubHeader>
        <View style={imageWrapperStyle}>
          <Image
            style={imageStyle}
            source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }}
          />
        </View>
        <View style={profileListStyle}>
          <RenderDetailList detailList={profileInfo} />
        </View>
      </View>
    );
  }
}

CustomerProfile.propTypes = {
  dispatch: PropTypes.func.isRequired,
};


function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(null, mapDispatchToProps);

export default compose(
  withConnect,
)(CustomerProfile);
