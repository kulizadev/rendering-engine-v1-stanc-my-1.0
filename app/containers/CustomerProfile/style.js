export const imageWrapperStyle = { alignItems: 'center', paddingTop: 15, paddingBottom: 15 };

export const imageStyle = { width: 150, height: 150, borderRadius: 75 };

export const profileListStyle = { paddingLeft: 15, paddingRight: 15 };
