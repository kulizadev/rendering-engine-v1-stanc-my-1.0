/**
 *
 * RenderStep
 *
 */

import React from 'react';

import * as renderService from '../../utilities/renderDataService';
import { behaveConfig } from '../../configs/appConfig';

export default class RenderStepAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.fieldMap = {};
    this.state = {

    };
    this.onBlurHandler = this.onBlurHandler.bind(this);
    this.onChangeHandler = this.onChangeHandler.bind(this);
  }

  clearFieldMap = () => {
    this.fieldMap = {};
  }
  // On blur handler for elements
  onBlurHandler(value, elmId) {
    // Trigger validation if trigger event is blurred for input elements
    if (behaveConfig.validationTrigger === 'blur') {
      this.props.validateAndUpdateRender(value, elmId);
    }
  }

  /*
  On change may do multiple things - depending on the config setting
    --> Set form data
    --> Validate the current value
  */
  onChangeHandler(value, elmId) {
    // updateFormData
    this.props.updateFormData(elmId, value);
    // Validate things over if validation trigger is change
    if (behaveConfig.validationTrigger === 'change') {
      this.props.validateAndUpdateRender(value, elmId);
    }
  }


  render() {
    return null;
  }
}

/*
Recieved props are:
formData
renderData
fieldOrder
screenId
validateAndUpdateRender - Validates the single element and update the valid state + error messages
updateFormData -  updates the formData for a particular key

This component will render the step's form elements in the order prescribed
-> Loop over fieldOrder
-> map the respective component to the form_type or meta.type
-> Render the unit component with the respective props for that elmId
->

*/
