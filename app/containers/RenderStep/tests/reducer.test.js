
import { fromJS } from 'immutable';
import renderStepReducer from '../reducer';

describe('renderStepReducer', () => {
  it('returns the initial state', () => {
    expect(renderStepReducer(undefined, {})).toEqual(fromJS({}));
  });
});
