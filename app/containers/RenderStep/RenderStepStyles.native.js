import { Dimensions } from 'react-native';
import styleVars from '../../configs/styleVars';
const { colors, fonts } = styleVars;


export const colWrapper = {
  paddingLeft: 15,
  paddingRight: 15,
};
