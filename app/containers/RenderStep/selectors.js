import { createSelector } from 'reselect';

/**
 * Direct selector to the renderStep state domain
 */
const selectRenderStepDomain = (state) => state.get('renderStep');

/**
 * Other specific selectors
 */


/**
 * Default selector used by RenderStep
 */

const makeSelectRenderStep = () => createSelector(
  selectRenderStepDomain,
  (substate) => substate.toJS()
);

export default makeSelectRenderStep;
export {
  selectRenderStepDomain,
};
