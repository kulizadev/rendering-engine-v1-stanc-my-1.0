/**
 *
 * RenderStep
 *
 */

import React from 'react';
import { View } from 'react-native';
import Text from '../../components/RenderText';
import componentProps from './componentProps';
import * as renderService from '../../utilities/renderDataService';
import { layoutConfig, behaveConfig } from '../../configs/appConfig';
import RenderStepAbstract from './RenderStepAbstract';
import { strings } from '../../../locales/i18n';

import * as styles from './RenderStepStyles';
import { Row, Column as Col, Grid } from 'react-native-responsive-grid';

export default class RenderStepContainer extends RenderStepAbstract { // eslint-disable-line react/prefer-stateless-function

  renderComponent = (fieldOrder) => {
    const renderData = { ...this.props.renderData },
      renderRows = [],
      hiddenFields = [];

    let currentColWidth = 0,
      renderElms = [];

    const fieldOrderArr = fieldOrder.filter((fieldId) => (renderData[fieldId] && !renderData[fieldId].isHidden));

    fieldOrder.forEach((elmId, index) => {
      if (!renderData[elmId]) { console.debug('fieldId wrong ', elmId); }
    });

    fieldOrderArr.forEach((elmId, index) => {
      if (renderData[elmId] && !renderData[elmId].isHidden && !this.fieldMap[elmId]) {
        const meta = renderData[elmId] && renderData[elmId].metaData;
        const isPrebuiltStaticComponent = renderService.getPrebuiltStaticFlag(elmId);
        let formType = meta && meta.form_type;
        if (meta.type === 'rest') {
          formType = meta.type;
        }
        if (meta.expression) {
          formType = 'expression';
        }
        if (isPrebuiltStaticComponent) {
          formType = 'prebuilt';
        }
        const UnitComponent = renderService.getComponentByFormType(formType),
          colWidth = meta.colWidth || layoutConfig.defaultColumnWidth,
          hasChildren = meta.children && meta.children.length,
          offset = meta.offset || '',
          colSize = colWidth * layoutConfig.defaultColumnSizeForMobile;
        const defaultValue = this.props.formData[elmId];

        if (hasChildren) {
          colWidth = 12;
          colSize = 100;
        }
        const compProps = componentProps[formType] && componentProps[formType](renderData[elmId], elmId, defaultValue, {...this.props.formData});
        this.fieldMap[elmId] = true;
        if (UnitComponent) {
          const childrenBlock = hasChildren && this.renderComponent(meta.children);
          let rowsBlock = childrenBlock && childrenBlock.renderRows ? childrenBlock.renderRows : null,
            hiddenBlock = childrenBlock && childrenBlock.hiddenFields ? childrenBlock.hiddenFields : null;

          const componentSection = (<UnitComponent
            compProps={compProps}
            key={elmId}
            elmId={elmId}
            renderData={renderData[elmId]}
            formData={this.props.formData}
            updateFormData={this.props.updateFormData}
            setRenderData={this.props.setRenderData}
            onChangeHandler={this.onChangeHandler}
            onBlurHandler={(e) => this.onBlurHandler(e, elmId)}
            toggleScroll={this.props.toggleScroll}
          >
            {rowsBlock}
            {hiddenBlock}
          </UnitComponent>);

          const compWrapper = (<Col style={{ ...styles.colWrapper }} size={colSize} smSize={100} key={`compwrapper-${elmId}`}>{componentSection}</Col>);

          // Console Logs commented below can help in debugging layout.
          if (meta.form_type !== 'hidden' && meta.form_type !== 'popup') {
            // console.debug(
            //   'Current Col Width :', currentColWidth,
            //   ', Adding "', elmId, '" of Form type "', formType, '" with ColWidth :', colWidth,
            // );
            currentColWidth += colWidth;
            if (currentColWidth > 12 || meta.newRow) {
              // console.debug('Because of', (meta.newRow ? 'newRow flag "' : 'Current Col Width overflow "'), elmId, '" will wait for the next row');
              if (renderElms.length) {
                renderRows.push(<Row key={`row-${index}`}>{[...renderElms]}</Row>); //`
                // console.debug('Row -- ', renderRows.length, ' created');
              }
              renderElms = [];
              currentColWidth = colWidth;
            }
            renderElms.push(compWrapper);

            if (hasChildren && (index !== (fieldOrderArr.length - 1))) {
              renderRows.push(<Row key={`row-${index}`}>{[...renderElms]}</Row>);
              renderElms.length = 0;
              currentColWidth = 0;
            }
          } else if (meta.form_type === 'hidden' || meta.form_type === 'popup') {
            hiddenFields.push(componentSection);
          }

            // If its the last elem
          if (index === (fieldOrderArr.length - 1)) {
            renderRows.push(<Row key={`row-${index}${1}`}>{[...renderElms]}</Row>);
          }
        }
      }
    });

    return { renderRows, hiddenFields };
  }

  render() {
    const fieldOrder = [...this.props.fieldOrder];
    this.clearFieldMap();
    const renderElms = this.renderComponent(fieldOrder);
    const renderRows = renderElms && renderElms.renderRows;
    const hiddenFields = renderElms && renderElms.hiddenFields;
    // TODO: move style to styleConfig
    return (
      <View>
        {renderRows}
        {hiddenFields}
      </View>
    );
  }
}

/*
Recieved props are:
formData
renderData
fieldOrder
screenId
validateAndUpdateRender - Validates the single element and update the valid state + error messages
updateFormData -  updates the formData for a particular key

This component will render the step's form elements in the order prescribed
-> Loop over fieldOrder
-> map the respective component to the form_type or meta.type
-> Render the unit component with the respective props for that elmId
->

*/
