/**
 *
 * RenderStep
 *
 */

import React from 'react';

import componentProps from './componentProps';
import * as renderService from 'utilities/renderDataService';
import { layoutConfig, behaveConfig } from 'configs/appConfig';

import Row from '../../components/Row/';
import Column from '../../components/Column/';
import RenderStepAbstract from './RenderStepAbstract';


export default class RenderStepContainer extends RenderStepAbstract { // eslint-disable-line react/prefer-stateless-function


  renderComponent = (fieldOrder) => {
    const renderData = { ...this.props.renderData },
      renderRows = [],
      hiddenFields = [];

    let currentColWidth = 0,
      renderElms = [];

    const fieldOrderArr = fieldOrder.filter((fieldId) => (renderData[fieldId] && !renderData[fieldId].isHidden));

    fieldOrder.forEach((elmId, index) => {
      if (!renderData[elmId]) { console.debug('fieldId wrong ', elmId); }
    });

    fieldOrderArr.forEach((elmId, index) => {
      if (renderData[elmId] && !renderData[elmId].isHidden && !this.fieldMap[elmId]) {
        const meta = renderData[elmId] && renderData[elmId].metaData;
        const isPrebuiltStaticComponent = renderService.getPrebuiltStaticFlag(elmId);
        let formType = meta && meta.form_type;
        if (meta.type === 'rest') {
          formType = meta.type;
        }
        if (isPrebuiltStaticComponent) {
          formType = 'prebuilt';
        }
        if (meta.expression) {
          formType = 'expression';
        }
        let UnitComponent = renderService.getComponentByFormType(formType);
        let colWidth = meta.colWidth || layoutConfig.defaultColumnWidth;
        const hasChildren = meta.children && meta.children.length;
        const offset = meta.offset || '';
        let colSize = colWidth * layoutConfig.defaultColumnSizeForMobile;
        const defaultValue = this.props.formData[elmId];

        if (hasChildren) {
          colWidth = 12;
          colSize = 90;
        }
        const compProps = componentProps[formType] && componentProps[formType](renderData[elmId], elmId, defaultValue, {...this.props.formData});
        this.fieldMap[elmId] = true;
        if (UnitComponent) {
          const childrenBlock = hasChildren && this.renderComponent(meta.children);
          let rowsBlock = childrenBlock && childrenBlock.renderRows ? childrenBlock.renderRows : null,
            hiddenBlock = childrenBlock && childrenBlock.hiddenFields ? childrenBlock.hiddenFields : null;

          const componentSection = (<UnitComponent
            compProps={compProps}
            key={elmId}
            elmId={elmId}
            renderData={renderData[elmId]}
            formData={this.props.formData}
            updateFormData={this.props.updateFormData}
            setRenderData={this.props.setRenderData}
            onChangeHandler={this.onChangeHandler}
            onBlurHandler={(e) => this.onBlurHandler(e, elmId)}
            toggleScroll={this.props.toggleScroll}
          >
            {rowsBlock}
            {hiddenBlock}
          </UnitComponent>);
          const compWrapper = (<Column colWidth={colWidth} offset={offset} key={`compwrapper-${elmId}`}>{componentSection}</Column>); // `

          // Console Logs commented below can help in debugging layout.
          if (meta.form_type !== 'hidden' && meta.form_type !== 'popup') {
            currentColWidth += colWidth;
            if (currentColWidth > 12 || meta.newRow) {
              if (renderElms.length) {
                renderRows.push(<Row key={`row-${index}`}>{[...renderElms]}</Row>); // `
              }
              renderElms = [];
              currentColWidth = colWidth;
            }
            renderElms.push(compWrapper);

            if (hasChildren && (index !== (fieldOrderArr.length - 1))) {
              if (renderElms.length) {
                renderRows.push(<Row key={`row-${index}${1}`}>{[...renderElms]}</Row>); // `
              }
              renderElms.length = 0;
              currentColWidth = 0;
            }
          } else if (meta.form_type === 'hidden' || meta.form_type === 'popup') {
            hiddenFields.push(componentSection);
          }

            // If its the last elem
          if (index === (fieldOrderArr.length - 1)) {
            if (renderElms.length) {
              renderRows.push(<Row key={`row-${index}${2}`}>{[...renderElms]}</Row>); // `
            }
          }
        }
      }
    });

    return { renderRows, hiddenFields };
  }

  render() {
    const fieldOrder = [...this.props.fieldOrder];
    this.clearFieldMap();
    const renderElms = this.renderComponent(fieldOrder);
    const renderRows = renderElms && renderElms.renderRows;
    const hiddenFields = renderElms && renderElms.hiddenFields;

    return (
      <div className="">
        {[...renderRows]}
        {hiddenFields}
      </div>
    );
  }
}

/*
Recieved props are:
formData
renderData
fieldOrder
screenId
validateAndUpdateRender - Validates the single element and update the valid state + error messages
updateFormData -  updates the formData for a particular key

This component will render the step's form elements in the order prescribed
-> Loop over fieldOrder
-> map the respective component to the form_type or meta.type
-> Render the unit component with the respective props for that elmId
->

*/
