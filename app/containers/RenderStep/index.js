/**
 *
 * RenderStep
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from '../../utils/injectSaga';
import injectReducer from '../../utils/injectReducer';
import makeSelectRenderStep from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import * as appActions from '../AppDetails/actions';
import RenderStepContainer from './RenderStepContainer';

export class RenderStep extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  render() {
    return (
      <RenderStepContainer {...this.props} />
    );
  }
}

/*
Recieved props are:
formData
renderData
fieldOrder
screenId
validateAndUpdateRender - Validates the single element and update the valid state + error messages
updateFormData -  updates the formData for a particular key

This component will render the step's form elements in the order prescribed
-> Loop over fieldOrder
-> map the respective component to the form_type or meta.type
-> Render the unit component with the respective props for that elmId
->

*/

RenderStep.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  renderstep: makeSelectRenderStep(),
});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'renderStep', reducer });
const withSaga = injectSaga({ key: 'renderStep', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(RenderStep);
