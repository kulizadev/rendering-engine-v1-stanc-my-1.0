/*
 * ConfiguratorPage
 *
 */
import React from 'react';
import TextField from 'material-ui/TextField';
import Row from '../../components/Row';
import Column from '../../components/Column';
import ConfiguratorWrapper from './ConfiguratorWrapper';
import RaisedButton from 'material-ui/RaisedButton';
import styleVars from 'configs/styleVars';
const fonts = styleVars.fonts,
  colors = styleVars.colors;


const inputComponentStyle = {
  style: { // This is the root element style
    width: '100%',
    color: colors.basicFontColor,
  },
  inputStyle: {
    marginBottom: 0,
  },
  floatingLabelStyle: {
    fontSize: fonts.fontSize,
  },
};
export class Configurator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clientName: '',
      journeyName: '',
    };
  }

  handleChange = (e, key) => {
    this.setState({
      [key]: e.target.value,
    });
  }

  triggerLogin = () => {
    const loginUrl = `/clientId/${this.state.clientName}_${this.state.journeyName}`;
    this.props.history.push(loginUrl);
  }

  render() {
    const clientCompProps = {
      id: 'ku-number-clientId',
      type: 'text',
      placeholder: 'e.g. sva',
      floatingLabelFixed: true,
      floatingLabelText: 'Client Name',
    };

    const journeyCompProps = {
      id: 'ku-number-journeyName',
      type: 'text',
      floatingLabelText: 'Journey Name',
      floatingLabelFixed: true,
      placeholder: 'e.g. accentureMVPDataEntry',
    };
    return (
      <Row>
        <Column colWidth={6} offset={3}>
          <ConfiguratorWrapper>
            <TextField
              {...inputComponentStyle}
              {...clientCompProps}
              value={this.state.clientName}
              onChange={(e) => this.handleChange(e, 'clientName')}
            />
            <TextField
              {...inputComponentStyle}
              {...journeyCompProps}
              value={this.state.journeyName}
              onChange={(e) => this.handleChange(e, 'journeyName')}
            />
            <RaisedButton
              type="primary"
              onClick={this.triggerLogin}
              label="START"
            />
          </ConfiguratorWrapper>
        </Column>
      </Row>);
  }
}


export default Configurator;
