/**
 *
 * Login Native
 *
 */


import React, { Component } from 'react';
import { ScrollView, Text } from 'react-native';
import { TextField as TextInputField } from 'react-native-material-textfield';
import { Button as RenderButton } from 'react-native-elements';

// import TextInputField from '../../components/TextInputField/';
// import RenderButton from '../../components/RenderButton';
import { wrapperStyle, headerStyle } from './styles.native';
import { strings } from '../../../locales/i18n';
import { colors } from '../../configs/styleVars';

export default class Configurator extends Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      clientName: 'ABC',
      journeyName: 'businessLoanV1',
    };
  }

  handleChange = (text, type) => {
    this.setState({
      [type]: text,
    });
  };

  handleSubmit = () => {
    const { clientName, journeyName } = this.state;
    this.props.setClientData({
      clientName,
      journeyName,
    });
  }

  // TODO: move width to styleconfig
  render() {
    return (
      <ScrollView
        keyboardShouldPersistTaps="always"
        contentContainerStyle={{ ...wrapperStyle }}
      >
        <Text style={headerStyle}>{'Configurator'}</Text>
        <TextInputField
          label={'Client Name'}
          placeholder={'e.g. sva'}
          onChangeText={(text) => this.handleChange(text, 'clientName')}
        />
        <TextInputField
          label={'Journey Name'}
          placeholder={'e.g. accentureMVPDataEntry'}
          onChangeText={(text) => this.handleChange(text, 'journeyName')}
        />
        <RenderButton
          text={strings('login.buttonText')}
          type={'primary'}
          onPress={this.handleSubmit}
          // disabled={!this.state.checked}
          style={{ backgroundColor: `${this.state.checked ? colors.primaryBGColor : colors.grey96}` }}
        />
      </ScrollView>
    );
  }
}

