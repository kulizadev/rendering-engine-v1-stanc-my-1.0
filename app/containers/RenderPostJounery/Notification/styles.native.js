import { colors, fonts, lineHeight, sizes } from '../../../configs/styleVars';

export const wrapperStyle = {
  backgroundColor: colors.white,
  flex: 1,
};

export const headerStyle = {
  paddingTop: 20,
  paddingHorizontal: sizes.appPaddingHorizontal,
  fontSize: fonts.h3,
  lineHeight: lineHeight.h3,
  color: colors.black,
  ...fonts.getFontFamilyWeight('bold'),
  paddingTop: sizes.headerPadding,
};

export const tabHeaderStyle = {
  backgroundColor: colors.white,
  paddingLeft: 10,
  flex: 0,
  shadowRadius: 0,
  elevation: 0,
  shadowOffset: {
    height: 0,
  },
};

export const tabWrap = { flex: 1 };

export const tabHeaderLabelFocused = { fontSize: fonts.description,
  lineHeight: lineHeight.description,
  color: colors.black,
  ...fonts.getFontFamilyWeight('bold'),
  paddingBottom: 7,
};

export const tabHeaderLabel = {
  fontSize: fonts.description,
  lineHeight: lineHeight.description,
  color: colors.tabHeaderColor,
  ...fonts.getFontFamilyWeight('bold'),
  paddingBottom: 7,
};

export const indicatorStyle = {
  backgroundColor: colors.primaryBGColor,
};

export const topSectionStyle = {
  paddingHorizontal: 20,
  paddingBottom: 50,
};

export const topInfoStyle = {
  flexDirection: 'row',
  paddingVertical: 20,
};

export const topHeaderStyle = {
  ...fonts.getFontFamilyWeight('bold'),
  fontSize: fonts.h2,
  lineHeight: lineHeight.h2,
  color: colors.white,
};

export const topHeaderAmt = {
  fontSize: fonts.h2,
  lineHeight: lineHeight.h2,
  color: colors.amountHeading,
  ...fonts.getFontFamilyWeight('bold'),
};
export const focusedLabelStyle = {
  borderWidth: 0,
  borderBottomWidth: 3,
  borderBottomColor: colors.primaryBGColor,
};

export const loanCardWrapper = {
  flex: 1,
  margin: 20,
};

