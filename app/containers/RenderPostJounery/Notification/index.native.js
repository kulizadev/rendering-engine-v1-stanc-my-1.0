import React from 'react';
import { View, Text, Platform, Dimensions, ScrollView } from 'react-native';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { TabViewAnimated, TabBar, TabViewPagerScroll, TabViewPagerPan } from 'react-native-tab-view';
import { NotificationList } from '../../../components/RenderStaticComponents';
import * as styles from './styles.native';
import { strings } from '../../../../locales/i18n';

import * as appActions from '../../AppDetails/actions';
import { makeSelectUserDetails } from '../../AppDetails/selectors';

const initialLayout = {
  height: 100,
  width: Dimensions.get('window').width,
};

const notifications = [
  {
    title: 'Hello',
    hint: '12-12-12 2323',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
    status: 'unread',
  },
  {
    title: 'Hello',
    hint: '12-12-12 2323',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
    status: 'read',
  },
  {
    title: 'Hello',
    hint: '12-12-12 2323',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
    status: 'read',
  },
  {
    title: 'Hello',
    hint: '12-12-12 2323',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
    status: 'read',
  },
  {
    title: 'Hello',
    hint: '12-12-12 2323',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
    status: 'read',
  },
  {
    title: 'Hello',
    hint: '12-12-12 2323',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
    status: 'read',
  },
  {
    title: 'Hello',
    hint: '12-12-12 2323',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
    status: 'read',
  },
  {
    title: 'Hello',
    hint: '12-12-12 2323',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
    status: 'read',
  },
];

const labelMap = {
  billing: 'billing',
  support: 'support',
  maintenance: 'maintenance',
};

export class Notification extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      billingNotifs: [],
      maintenanceNotifs: [],
      supportNotifs: [],
      allNotifs: [],
      routes: [
            { key: 'billing', title: strings('notification.billing') },
            { key: 'supportRequest', title: strings('notification.supportRequest') },
            { key: 'maintenance', title: strings('notification.maintenance') },
      ],
    };
  }

  componentDidMount() {
    this.getNotifications();
  }


  buildStateNotifs = (allNotifs) => {
    const billingNotifs = allNotifs.filter((notif, idx) => notif.category === labelMap.billing);
    const supportNotifs = allNotifs.filter((notif, idx) => notif.category === labelMap.support);
    const maintenanceNotifs = allNotifs.filter((notif, idx) => notif.category === labelMap.maintenance);

    this.setState({
      billingNotifs: [...billingNotifs],
      supportNotifs: [...supportNotifs],
      maintenanceNotifs: [...maintenanceNotifs],
      allNotifs: [...allNotifs],
    });
  }


  getNotifications = () => {
    const self = this;
    const reqObject = {
      params: {},
      userDetails: this.props.userDetails,
      successCb: (response) => {
        if (response && response.data) {
          console.debug('what is the response for notifications', response);
          const allNotifs = response.data;
          self.setState({ allNotifs: [...allNotifs] });
          // self.buildStateNotifs(allNotifs);
        }
      },
      errorCb: () => {

      },
    };

    this.props.getRequest({ key: 'getNotifications', data: reqObject });
  }

  handleIndexChange = (index) => this.setState({ index });

  renderTabPager = (props) => (Platform.OS === 'ios') ? <TabViewPagerScroll {...props} /> : <TabViewPagerPan {...props} />;

  renderTabHeader = (props) => (<TabBar
    {...props}
    style={styles.tabHeaderStyle}
    indicatorStyle={styles.indicatorStyle}
    tabStyle={{ flex: 0 }}
    renderLabel={(current) => {
      const style = current.focused ? styles.tabHeaderLabelFocused : styles.tabHeaderLabel;
      return (<View style={current.focused ? styles.focusedLabelStyle : {}}>
        <Text style={style}>{current.route.title}</Text>
      </View>);
    }}
    renderIndicator={() => null}
  />);

  renderTabScene = ({ route }) => {
    switch (route.key) {
      case 'billing':
        return <ScrollView><NotificationList notifications={this.state.billingNotifs} /></ScrollView>;
      case 'supportRequest':
        return <ScrollView><NotificationList notifications={this.state.supportNotifs} /></ScrollView>;
      case 'maintenance':
        return <ScrollView><NotificationList notifications={this.state.maintenanceNotifs} /></ScrollView>;
      default:
        return null;
    }
  }

  render() {
    return (<View style={styles.wrapperStyle}>
      {this.props.screenProps.header}
      <Text style={styles.headerStyle} >{strings('notification.title')}</Text>
      <View style={styles.tabWrap}>
        {/* <TabViewAnimated
          navigationState={this.state}
          renderScene={this.renderTabScene}
          renderHeader={this.renderTabHeader}
          renderPager={this.renderTabPager}
          onIndexChange={this.handleIndexChange}
          initialLayout={initialLayout}
        /> */}
        <NotificationList notifications={this.state.allNotifs} />
      </View>
    </View>);
  }
}

Notification.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  userDetails: makeSelectUserDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(Notification);
