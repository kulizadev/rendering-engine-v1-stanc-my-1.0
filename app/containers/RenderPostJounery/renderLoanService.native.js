import { strings } from '../../../locales/i18n';

export const statusMap = {
  REJECTED: 'REJECTED',
  CANCELLED: 'CANCELLED',
  NEW: 'NEW',
  EXPIRED: 'EXPIRED',
  DISBURSED: 'DISBURSED',
  CLOSED: 'CLOSED',
  APPROVED: 'APPROVED',
};

export const loanStatusMap = () => [{
  name: strings('loanCard.statusActive'),
  value: statusMap.DISBURSED,
}, {
  name: strings('loanCard.statusClosed'),
  value: statusMap.CLOSED,
}, {
  name: strings('loanCard.statusCancelled'),
  value: statusMap.CANCELLED,
}];

export const appStatusMap = () => [{
  name: strings('loanCard.statusInProgress'),
  value: statusMap.NEW,
}, {
  name: strings('loanCard.statusCompleted'),
  value: statusMap.APPROVED,
}, {
  name: strings('loanCard.statusExpired'),
  value: statusMap.EXPIRED,
}, {
  name: strings('loanCard.statusRejected'),
  value: statusMap.REJECTED,
}, {
  name: strings('loanCard.statusCancelled'),
  value: statusMap.CANCELLED,
}];

export const getStatusMap = (type) => (type === 'loan' ? loanStatusMap() : appStatusMap());

export const statusKey = 'applicationStatus';
