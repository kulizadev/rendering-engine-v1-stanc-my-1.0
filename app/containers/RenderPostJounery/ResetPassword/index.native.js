import React from 'react';
// import PropTypes from 'prop-types';
import { View, ScrollView, Platform, KeyboardAvoidingView } from 'react-native';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { makeSelectUserDetails } from '../../AppDetails/selectors';
import * as appActions from '../../AppDetails/actions';
import TextInputField from '../../../components/TextInputField';
import { strings } from '../../../../locales/i18n';
import SelfieConfirmation from '../../../components/SelfieConfirmation/';
import RenderPopup from '../../../components/RenderPopup';

import Loader from '../../../components/Loader';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { colors } from '../../../configs/styleVars';
import * as validationFns from '../../../utilities/validations/validationFns';
const FAILURE_STATUS = {
  INVALID_SELFIE: 1,
  INVALID_PASSWORD: 2,
  SAME_PASSWORD: 3,
};

import { sha256, sha224 } from 'js-sha256';

class ResetPassword extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      currentMpin: '',
      newMpin: '',
      confirmMpin: '',
      isMpinValid: false,
      isConfirmMpinValid: false,
      isCurrentMpinValid: false,
      invalidCurrentMpinError: '',
      invalidMpinError: '',
      invalidConfirmMpinError: '',
      showPassword: false,
      openCamera: false,
      loaderState: {},
    };
  }

  setMpinToStorage = () => {
    this.props.setUserDetails({
      ...this.props.userDetails,
      mpin: this.state.mpin,
      mpinConfirmed: 'true',
      mpin_setup: 'false',
    });
  }

  checkConfirmMpinValidation = (confirmMpin) => {
    const { newMpin, isMpinValid } = this.state;
    let isConfirmMpinValid = false;
    let invalidConfirmMpinError = strings('errors.confirmMpin');
    if (isMpinValid) {
      if (confirmMpin === newMpin) {
        isConfirmMpinValid = true;
        invalidConfirmMpinError = '';
      }
    } else {
      this.checkMpinValidation(this.state.mpin);
    }
    this.setState({ isConfirmMpinValid, invalidConfirmMpinError });
    return isConfirmMpinValid;
  }

  checkMpinValidation = (value) => {
    const isValidObj = validationFns.getValidationObj('mpin', value);
    this.setState({ isMpinValid: isValidObj.isValid });
    this.setState({ invalidMpinError: isValidObj.errorMessage });
  }

  checkCurrentMpinValidation = (value) => {
    const isValidObj = validationFns.getValidationObj('mpin', value);
    this.setState({ isCurrentMpinValid: isValidObj.isValid });
    this.setState({ invalidCurrentMpinError: isValidObj.errorMessage });
  }

  handleConfirmMpinChange = (confirmMpin) => {
    this.setState({ confirmMpin });
    this.checkConfirmMpinValidation(confirmMpin);
  }

  handleNewMpinChange = (newMpin) => {
    this.setState({ newMpin });
    this.checkMpinValidation(newMpin);
  }

  handleCurrentMpinChange = (currentMpin) => {
    this.setState({ currentMpin });
    this.checkCurrentMpinValidation(currentMpin);
  }

  buildRequestParams = (selfie) => {
    const fd = new FormData();
    const { currentMpin, newMpin } = this.state;
    fd.append('image', selfie);
    fd.append('oldMpin', sha256(currentMpin));
    fd.append('newMpin', sha256(newMpin));
    return fd;
  }

  showIncorrectMpinError = (type) => {
    let isValidObj = {};
    if (type === FAILURE_STATUS.INVALID_PASSWORD) {
      isValidObj = validationFns.getValidationObj('mpin', '');
    } else {
      isValidObj.errorMessage = strings('profileData.samePasswordError');
    }
    this.setState({
      currentMpin: '',
      newMpin: '',
      confirmMpin: '',
      isMpinValid: false,
      isConfirmMpinValid: false,
      isCurrentMpinValid: true,
      invalidCurrentMpinError: isValidObj.errorMessage,
      invalidMpinError: '',
      invalidConfirmMpinError: '',
    });
  }

  buildLoaderObj = (flag) => ({
    isVisible: flag,
    loader: true,
    isLongReq: true,
    loaderMessage: strings('subJourney.verifySelfieLoader'),
  })

  submitMpin = (selfie) => {
    const self = this;
    const reqData = {
      params: this.buildRequestParams(selfie),
      userDetails: this.props.userDetails,
      loader: true,
      loaderMessage: strings('subJourney.verifySelfieLoader'),
      contentType: 'multipart/form-data',
      successCb: (response) => {
        this.setState({ loaderState: this.buildLoaderObj(false) });
        if (response && response.data) {
          if (response.data.status) {
            self.setState({ showPopup: 'success' });
            self.setMpinToStorage();
          } else {
            // 1 - selfie mismatch
            // 2 - incorrect current MPIN
            // 3 - new password and old password same
            if (response.data.error === FAILURE_STATUS.INVALID_SELFIE) {
              self.setState({ showPopup: 'failure' });
            } else if (response.data.error === FAILURE_STATUS.INVALID_PASSWORD) {
              self.showIncorrectMpinError(FAILURE_STATUS.INVALID_PASSWORD);
            } else if (response.data.error === FAILURE_STATUS.SAME_PASSWORD) {
              self.showIncorrectMpinError(FAILURE_STATUS.SAME_PASSWORD);
            }
          }
        }
      },
      errorCb: () => {
        self.setState({ loaderState: self.buildLoaderObj(false) });
        self.setState({ showPopup: 'failure' });
      },
    };
    this.setState({ loaderState: this.buildLoaderObj(true) });
    this.props.postRequest({ key: 'updatePassword', data: reqData });
  }

  renderIcon = () => {
    const { showPassword } = this.state;

    const name = showPassword ?
      'visibility' :
      'visibility-off';

    return (
      <MaterialIcon
        size={24}
        name={name}
        color={showPassword ? colors.primaryBGColor : colors.labelColor}
        onPress={this.toggleShowPassword}
      />
    );
  }

  toggleShowPassword = () => {
    this.setState(({ showPassword }) => ({ showPassword: !showPassword }));
  }

  toggleRetry = (value) => {
    this.setState({ openCamera: value });
  }

  getSuccessPopUpdata = () => ({
    headerText: strings('profileData.successPopupHead'),
    bodyText: strings('profileData.successPopupBody'),
    actionButtons: [{
      label: strings('profileData.successPopupBtnText'),
      callback: this.successCb,
      action: 'callback',
      type: 'primary',
    }],
    type: 'success',
  });

  getErrorPopUpdata = () => {
    const self = this;
    return {
      headerText: strings('profileData.errorPopupHead'),
      bodyText: strings('profileData.errorPopupBody'),
      actionButtons: [{
        label: strings('profileData.errorPopupBtnText1'),
        action: 'call',
        type: 'secondary',
      },
      {
        label: strings('profileData.errorPopupBtnText2'),
        action: 'callback',
        callback: self.failureCb,
        type: 'primary',
      }],
      type: 'error',
    };
  }

  successCb = () => {
    this.props.hideModal();
    this.setState({ showPopup: '' });
  }

  failureCb = () => {
    this.toggleRetry(true);
    this.setState({ showPopup: '' });
  }

  getPopInfo = () => {
    let showPopup = false;
    let popData = {};
    switch (this.state.showPopup) {
      case 'success':
        showPopup = true;
        popData = this.getSuccessPopUpdata();
        break;
      case 'failure':
        showPopup = true;
        popData = this.getErrorPopUpdata();
        break;

      default:
        break;
    }
    return [showPopup, popData];
  }

  render() {
    const { showPassword, openCamera, invalidConfirmMpinError, invalidCurrentMpinError, invalidMpinError, isMpinValid, isConfirmMpinValid, isCurrentMpinValid, currentMpin, newMpin, confirmMpin } = this.state;
    const scrollBehave = {};
    const [showPopup, popData] = this.getPopInfo();
    if (Platform.OS === 'ios') {
      scrollBehave.behavior = 'padding';
    }
    return (
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        {...scrollBehave}
      >

        <View style={{ flex: 1, marginHorizontal: 20, marginBottom: 20 }}>
          <ScrollView
            keyboardShouldPersistTaps="always"
          >
            <TextInputField
              value={currentMpin}
              label={strings('login.currentPasscode')}
              error={invalidCurrentMpinError}
              onChangeText={this.handleCurrentMpinChange}
              maxLength={4}
              keyboardType={'numeric'}
              secureTextEntry
            />
            <TextInputField
              value={newMpin}
              label={strings('login.newPassword')}
              error={invalidMpinError}
              onChangeText={this.handleNewMpinChange}
              maxLength={4}
              keyboardType={'numeric'}
              secureTextEntry={!showPassword}
              renderAccessory={this.renderIcon}
            />
            <TextInputField
              value={confirmMpin}
              label={strings('login.confirmPassword')}
              error={invalidConfirmMpinError}
              onChangeText={this.handleConfirmMpinChange}
              maxLength={4}
              secureTextEntry
              keyboardType={'numeric'}
            />
          </ScrollView>
          <SelfieConfirmation
            openCamera={openCamera}
            onSubmit={this.submitMpin}
            toggleRetry={this.toggleRetry}
            successPopData={this.getSuccessPopUpdata()}
            errorPopData={this.getErrorPopUpdata()}
            showPopup={showPopup}
            isValid={isMpinValid && isConfirmMpinValid && isCurrentMpinValid}
          />
          <RenderPopup
            showPopup={showPopup}
            {...popData}
          />
          <Loader loaderStatus={this.state.loaderState} />
        </View>
      </KeyboardAvoidingView>
    );
  }
}


const mapStateToProps = createStructuredSelector({
  userDetails: makeSelectUserDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    setUserDetails: (data) => dispatch(appActions.setUserDetails(data)),
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(ResetPassword);
