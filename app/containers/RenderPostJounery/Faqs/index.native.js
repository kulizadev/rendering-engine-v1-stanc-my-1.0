import React from 'react';
// import PropTypes from 'prop-types';
import { View, Text, ScrollView, Image, TouchableOpacity, Switch } from 'react-native';
import { strings } from '../../../../locales/i18n';
import { ModalOverlay, FaqCategory } from '../../../components/RenderStaticComponents';
import * as styles from './styles.native';
import { sizes, fonts, lineHeight, colors } from '../../../configs/styleVars';

export default class Faqs extends React.Component { // eslint-disable-line react/prefer-stateless-function
  getFaqsList = () => {
    const allFaqs = [
      {
        name: 'Category 1',
        faqs: [
          {
            ques: 'Who can apply for a personal loan through the mobile app?',
            ans: 'Any Vietnamese citizen having a valid National ID and between the age of 20 and 60 years can apply for a personal loan.',
          },
          {
            ques: 'What is the minimum required income to be qualified for a loan?',
            ans: 'The minimum income required to apply for a loan is 3 mil VND. However, the required income for successful application will be dependent on the required loan amount.',
          },
        ],
      },
      {
        name: 'Category 2',
        faqs: [
          {
            ques: 'How long does it take to get loan approval?',
            ans: 'Normally, the entire approval process will be completed in 5 minutes but can vary due to mobile connectivity.',
          },
          {
            ques: 'What is the interest rate for my loan? Will it change during the tenure of the loan?',
            ans: 'The interest rate will be determined during the application process. The interest rate will remain fixed through the tenure of the loan',
          },
        ],
      },
      {
        name: 'Category 3',
        faqs: [
          {
            ques: 'Any specific purpose is required for applying for FE Credit loan?',
            ans: 'According to the rules specified by the State Bank of Vietnam (SBV), customers are required to specify the purpose of their loan requirement.',
          },
          {
            ques: 'What documents do I need to provide to apply for the loan?',
            ans: 'All customers are required to provide a valid National ID. Customers may be required to provide income proof in the form of Bank statement/ EVN Bill/ MRC/ Insurance Contract and Insurance bill depending on the category of customer',
          },
        ],
      },
      {
        name: 'Category 4',
        faqs: [
          {
            ques: 'How can I get the money if my loan is approved?',
            ans: ' We can either transfer the money to the Bank Account of your choice or you can collect the money in cash from our partner locations. Our partners include VNPost, Sacombank, BIDV, VPBank, Agribank.',
          },
          {
            ques: 'Where are the channels to repay the loan?',
            ans: 'You can either pay online through our mobile app or pay cash at our partner locations. Our partners include VNPOST, Sacombank, BIDV, VPBank, Agribank.',
          },
        ],
      },
      {
        name: 'Category 5',
        faqs: [
          {
            ques: 'I haven’t received Mpin yet, how can I get this code?',
            ans: 'The MPIN can be requested through the app using the “Forgot MPIN” option ',
          },
          {
            ques: 'If I terminate my loan early, how much will it cost?',
            ans: 'Customers are free to foreclose the loan by paying an early termination fee of 5% of the outstanding principal loan amount',
          },
        ],
      },
    ];
    return allFaqs.map((categ) => <FaqCategory title={categ.name} faqs={categ.faqs} />);
  }
  render() {
    return (
      <View style={{ flex: 1, paddingHorizontal: sizes.appPaddingHorizontal }}>
        {this.props.screenProps.header}
        <Text style={{ fontSize: fonts.h2, lineHeight: lineHeight.h2, color: colors.primaryBGColor, marginTop: sizes.headerPadding, paddingVertical: 10, ...fonts.getFontFamilyWeight('bold') }} >Faqs</Text>
        {this.getFaqsList()}
      </View>
    );
  }
  }
