import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native';
import NavigationService from '../../../utilities/NavigationService.native';
import { ProgressBar, BackgroundImage } from '../../../components/RenderStaticComponents';
import { colors } from '../../../configs/styleVars';
import notification from '../../../images/notification.png';
import notificationBig from '../../../images/notification-big.png';
import application from '../../../images/application.png';
import megaphone from '../../../images/megaphone.png';
import referral from '../../../images/referral.png';
import support from '../../../images/supportWhite.png';
import loans from '../../../images/loansWhite.png';
import dashboardBackground from '../../../images/dashboard-background.png';
import * as styles from './styles.native';
import * as common from '../../../utilities/commonUtils';
import { strings } from '../../../../locales/i18n';
import moment from 'moment';

export default class Dashboard extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      dashboardData: {},
    };
  }

  componentDidMount() {
    const reqObj = {
      key: 'getDashboardData',
      data: {
        successCb: (response) => {
          this.props.screenProps.setDashboardData(response.data);
          this.setState({ dashboardData: response.data });
        },
        userDetails: this.props.screenProps.userDetails,
      },
    };
    this.props.screenProps.getRequest(reqObj);
  }

  getTiles = () => {
    const tiles = [
      {
        title: strings('dashboard.tileLoan'),
        screenId: 'Loans',
        props: {
          defaultTab: 0,
        },
        notication: 5,
        desc: '',
        icon: loans,
      },
      {
        title: strings('dashboard.tileApplication'),
        screenId: 'Applications',
        props: {
          defaultTab: 1,
        },
        notication: 5,
        desc: '',
        icon: application,
      },
      {
        title: strings('dashboard.tileNotification'),
        screenId: 'Notification',
        notication: 5,
        desc: '',
        icon: notificationBig,
      },
      {
        title: strings('dashboard.tileSupport'),
        screenId: 'Support',
        notication: 5,
        desc: '',
        icon: support,
      },
    ];
    return tiles.map((tile, index) => {
      const tileMod = index % 2 === 0;
      return (
        <View style={styles.tileWrapper} key={tile.screenId}>
          <TouchableOpacity onPress={() => this.props.screenProps.navigateToScreen(tile.screenId, tile.props)}>
            <View style={[styles.tileBox, { marginLeft: tileMod ? 0 : 8, marginRight: tileMod ? 8 : 0 }]}>
              <Text style={styles.tileHeader}>{tile.title}</Text>
              <Text style={styles.tileDesc}>{tile.desc}</Text>
              <View style={styles.tileImageWrap}>
                <Image source={tile.icon} resizeMode={'contain'} />
              </View>
            </View>
          </TouchableOpacity>
        </View>
      );
    });
  }
  render() {
    let { remainingAmount, remainingTenure, totalAmount, date } = this.state.dashboardData;
    const repaidPercentage = remainingAmount !== '' ? parseInt(((totalAmount - remainingAmount) * 100) / totalAmount, 10) : 0;
    const formattedRemainingAmount = common.convertCurrencyFormat(remainingAmount);
    totalAmount = common.convertCurrencyFormat(totalAmount);
    date = moment(date).format('D MMMM YYYY');
    console.log('repaidPercentage', repaidPercentage);
    return (
      <View style={styles.wrapperStyle}>
        {this.props.screenProps.header}
        <BackgroundImage source={dashboardBackground}>
          <View style={styles.topWrapperStyle}>
            <View style={styles.headerWrap}>
              <Text style={styles.headerStyle}>{strings('dashboard.title')}</Text>
              <Text style={styles.headerStyle}>{strings('dashboard.date', { date })}</Text>
            </View>
            <View style={styles.payNowWraper}>
              <Text style={styles.amountStyle}>{formattedRemainingAmount || '-'}</Text>
              {/* {remainingAmount
              ? (<TouchableOpacity>
                <View style={styles.payNowBtnWrap}>
                  <Text style={styles.payNowBtn} >{strings('dashboard.payNow')}</Text>
                </View>
              </TouchableOpacity>)
              : null} */}
            </View>
            <View style={styles.midViewWrap}>
              <View style={styles.tileWrap}>
                <Text style={styles.tileDesc}>{strings('dashboard.tenure')}</Text>
                <Text style={styles.tileHeader}>{remainingTenure || '-'} {strings('dashboard.months')}</Text>
              </View>
              <View style={styles.tileWrap}>
                <Text style={styles.tileDesc}>{strings('dashboard.loan')}</Text>
                <Text style={styles.tileHeader}>{totalAmount || '-'}</Text>
              </View>
            </View>
            <View style={styles.progressWrap}>
              <ProgressBar filled={repaidPercentage} filledTrackColor={colors.amountHeading} unfilledTrackColor={colors.white} />
            </View>
          </View>
        </BackgroundImage>
        <View style={styles.bottomWrap}>
          <ScrollView>
            <View style={styles.bottomTilesWrap}>
              {this.getTiles()}
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
  }

