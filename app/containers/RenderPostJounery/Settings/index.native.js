import React from 'react';
// import PropTypes from 'prop-types';
import { View, Text, ScrollView, Image, TouchableOpacity, Switch } from 'react-native';
import DropDownModal from '../../../components/DropDownModal';
import { languages } from '../../../configs/appConstants';
import rightArrow from '../../../images/rightArrow.png';
import RenderButton from '../../../components/RenderButton';
import { strings } from '../../../../locales/i18n';
import { ModalOverlay } from '../../../components/RenderStaticComponents';
import * as userUtils from '../../../utilities/userUtils';
import RenderMpin from '../../RenderMpin';
import { checkSupport } from '../../../utilities/native/touchIDService.native';
import * as styles from './styles.native';
import ResetPassword from '../ResetPassword/';

export default class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showLanguageDropDown: false,
      notificationStatus: false,
      isTouchIdSupported: true,
      touchIdStatus: false,
      showPassModal: false,
    };
  }

  componentWillMount = () => {
    this.getSettingsData();
    this.checkTouchIDStatus();
  }

  getSettingsData = () => {
    const reqData = {
      userDetails: this.props.userDetails,
      successCb: (response) => {
        this.setState({ notificationStatus: response.data.notification_preference, touchIdStatus: response.data.touch_id_enabled });
      },
    };
    this.props.screenProps.getRequest({ key: 'getSettings', data: reqData });
  }

  onLogout = () => {
    userUtils.logout().then((userObj) => {
      const userDetails = userObj;
      this.props.screenProps.setUserDetails(userDetails);
      this.props.screenProps.clearJourneyData();
      this.props.screenProps.toggleMpin(false);
    });
  }

  checkTouchIDStatus = async () => {
    const supportStatus = await checkSupport();
    this.setState({ isTouchIdSupported: supportStatus });
  }

  handleLanguageDropDown = (flag) => {
    if (typeof flag === 'boolean') {
      this.setState({ showLanguageDropDown: flag });
    } else {
      this.setState(({ showLanguageDropDown }) => ({ showLanguageDropDown: !showLanguageDropDown }));
    }
  }

  handleLanguageChange = (selectedValue) => {
    const { userDetails } = this.props.screenProps;
    const reqData = {
      params: {
        lang: selectedValue.value,
      },
      userDetails,
      successCb: (res) => {
        console.log('res: ', res);
        this.props.screenProps.updateUserLanguage(selectedValue.value);
      },
    };
    this.props.screenProps.postRequest({ key: 'updatePreferedLang', data: reqData });
    this.handleLanguageDropDown(false);
  }

  handleTouchIdChange = (value) => {
    this.setState(({ touchIdStatus }) => ({ touchIdStatus: !touchIdStatus }));
    const reqData = {
      params: { enableTouchID: value },
      userDetails: this.props.userDetails,
      successCb: (response) => {
        this.props.screenProps.setUserDetails({
          ...this.props.screenProps.userDetails,
          useTouchID: value.toString(),
        });
      },
    };
    this.props.screenProps.postRequest({ key: 'updateTouchId', data: reqData });
  }

  handleNotificationStatus = (value) => {
    this.setState(({ notificationStatus: value }));
    const { userDetails } = this.props.screenProps;
    const reqData = {
      params: { notificationPreference: value },
      userDetails,
      successCb: (res) => {
      },
    };
    this.props.screenProps.postRequest({ key: 'updateNotificationPreference', data: reqData });
  }

  handlePassModal= (flag) => {
    this.setState({ showPassModal: flag });
  }

  render() {
    const { userLanguage } = this.props.screenProps;
    // console.log('useTouchID: ', useTouchID);
    const selectedLanguage = languages.find((language) => language.value === userLanguage);
    const { touchIdStatus } = this.state;
    const touchIdMarkUp = (
      <View style={styles.tileStye}>
        <View>
          <Text>{strings('settings.touchIdLabel')}</Text>
        </View>
        <Switch
          onValueChange={this.handleTouchIdChange}
          value={touchIdStatus}
        />
      </View>
    );
    return (
      <View style={styles.wrapperStyle}>
        {this.props.screenProps.header}
        <View>
          <View><Text style={styles.titleStyle}>{strings('settings.title')}</Text></View>
          <View style={styles.tileStye}>
            <Text>{strings('settings.languageLabel')}</Text>
            <TouchableOpacity onPress={this.handleLanguageDropDown}>
              <Text>{selectedLanguage.name}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.tileStye}>
            <View>
              <Text>{strings('settings.notificationLabel')}</Text>
            </View>
            <Switch
              onValueChange={this.handleNotificationStatus}
              value={this.state.notificationStatus}
            />
          </View>
          {touchIdMarkUp}
          <TouchableOpacity onPress={() => this.handlePassModal(true)} >
            <View style={styles.tileStye}>
              <Text>{strings('settings.changePassLabel')}</Text>
              <Image source={rightArrow} />
            </View>
          </TouchableOpacity>
          <DropDownModal
            options={languages}
            label={strings('login.languageLabel')}
            updateOption={this.handleLanguageChange}
            showList={this.state.showLanguageDropDown}
            handleBackPress={() => this.handleLanguageDropDown(false)}
            value={selectedLanguage ? selectedLanguage.value : ''}
          />
        </View>
        <View>
          <RenderButton text={strings('settings.logout')} type={'secondary'} onPress={this.onLogout} />
        </View>
        <ModalOverlay
          onBack={() => this.handlePassModal(false)}
          isVisible={this.state.showPassModal}
          headerType={'white'}
        >
          <ResetPassword
            hideModal={() => this.handlePassModal(false)}
          />
        </ModalOverlay>
      </View>
    );
  }
}
