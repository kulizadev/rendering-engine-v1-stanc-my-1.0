import React from 'react';
import PropTypes from 'prop-types';
import { createDrawerNavigator } from 'react-navigation';
import RenderMyLoan from '../RenderMyLoan';
import Support from './Support';
import Dashboard from './Dashboard';
import Profile from './Profile';
import Settings from './Settings';
import Faqs from './Faqs';
import Notification from './Notification';
import NavigationService from '../../utilities/NavigationService.native';
import { SideMenu, PostHeader } from '../../components/RenderStaticComponents';
import { strings } from '../../../locales/i18n';

export const getNavLocales = (nav) => {
  const nLocales = {
    Dashboard: strings('navigation.dashboard'),
    Loans: strings('dashboard.tileLoan'),
    Notification: strings('dashboard.tileNotification'),
    Support: strings('navigation.support'),
    Profile: strings('navigation.profile'),
    Settings: strings('navigation.settings'),
    Faqs: strings('navigation.faqs'),
  };
  return nLocales[nav];
};


const RenderPostJourneyRoutes = createDrawerNavigator(
  {
    Dashboard,
    Profile,
    Support,
    Applications: RenderMyLoan,
    Loans: RenderMyLoan,
    Settings,
    Faqs,
    Notification,
  },
  {
    initialRouteName: 'Dashboard',
    contentComponent: (props) => (
      <SideMenu {...props} />
      ),
  }
);

export default class RenderPostJourney extends React.PureComponent {

  render() {
    return (
      <RenderPostJourneyRoutes
        ref={(navigatorRef) => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
        screenProps={{
          ...this.props,
          header: <PostHeader {...this.props} />,
        }}
      />
    );
  }
}

RenderPostJourney.propTypes = {
  getLabel: PropTypes.func,
};
