import React from 'react';
// import PropTypes from 'prop-types';
import { View, Text, ScrollView, Image } from 'react-native';
import { ProfileDetail } from '../../../components/RenderStaticComponents';
import * as styles from './styles.native';
import profilePic from '../../../images/tempPhoto.png';
import dob from '../../../images/dob.png';
import nationalId from '../../../images/nationalId.png';
import callIcon from '../../../images/call-black.png';
import emailIcon from '../../../images/email.png';
import addressIcon from '../../../images/address.png';
import workInfoIcon from '../../../images/workInfo.png';
import categoryIcon from '../../../images/category.png';
import buildingIcon from '../../../images/building.png';
import RenderProfile from '../../RenderProfile/';
import { strings } from '../../../../locales/i18n';

export default class Profile extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showEditForm: false,
      formtype: '',
      profileData: {},
    };
  }

  componentDidMount() {
    this.getProfileData();
  }

  getProfileData = () => {
    const reqData = {
      userDetails: this.props.screenProps.userDetails,
      successCb: (response) => {
        if (response && response.data) {
          const { customerInfo, addressInfo, employmentInfo } = response.data;
          this.setState({ profileData: { ...customerInfo, ...addressInfo, ...employmentInfo } });
        }
      },
    };
    this.props.screenProps.getRequest({ key: 'getProfile', data: reqData });
  }

  groupProfileData = (data) => {
    const personalInfo = [
      {
        icon: dob,
        label: strings('profileData.dob'),
        value: data.dateOfBirth,
      },
      {
        icon: nationalId,
        label: strings('profileData.nationalId'),
        value: data.nationalId,
      },
    ];
    const communicationInfo = [
      {
        icon: callIcon,
        label: strings('profileData.contactNumber'),
        value: data.contactNumber,
      },
      {
        icon: emailIcon,
        label: strings('profileData.email'),
        value: data.email,
      },
      {
        icon: addressIcon,
        label: strings('profileData.address'),
        value: `${data.temporaryAddressLine1 || ''} ${data.temporaryAddressLine2 || ''} ${data.temporaryWard || ''} ${data.temporaryCityDistrict || ''} ${data.temporaryProvince || ''}`,
      },
    ];
    const jobInfo = [
      {
        icon: workInfoIcon,
        label: strings('profileData.employmentStatus'),
        value: data.employmentStatus,
      },
      {
        icon: categoryIcon,
        label: strings('profileData.companyType'),
        value: data.companyType,
      },
      {
        icon: buildingIcon,
        label: strings('profileData.companyName'),
        value: data.companyName,
      },
      {
        icon: workInfoIcon,
        label: strings('profileData.occupation'),
        value: data.occupation,
      },
      {
        icon: buildingIcon,
        label: strings('profileData.position'),
        value: data.position,
      },
    ];
    return { personalInfo, communicationInfo, jobInfo };
  }

  handleEdit = (type) => {
    this.setState({ showEditForm: true, formtype: type });
  }

  hideForm = () => {
    this.setState({ showEditForm: false });
  }

  refreshData = () => {
    this.hideForm();
    this.getProfileData();
  }

  render() {
    const { showEditForm, formtype, profileData } = this.state;
    const { personalInfo, communicationInfo, jobInfo } = this.groupProfileData(profileData);
    console.log('Render Post Journey', this.state);
    return (
      <View style={styles.wrapperStyle}>
        {this.props.screenProps.header}
        <View style={styles.topViewWrap}>
          {profileData.profilePhotoLink
          ? <View>
            <Image
              source={{ uri: profileData.profilePhotoLink }}
              style={styles.proImageStyle}
            />
          </View>
          : null
          }
          <View style={styles.topDetailWrap}>
            <Text style={styles.topDetailLabel}>{profileData.name}</Text>
            <Text style={styles.topDetailDesc}>{strings('profileData.customerSince')} {profileData.customerSince}</Text>
          </View>
        </View>
        <View style={styles.bottomViewWrap}>
          <ScrollView contentContainerStyle={styles.contentContainer}>
            <ProfileDetail
              title={strings('profileData.personalInfo')}
              details={personalInfo}
            />
            <ProfileDetail
              title={strings('profileData.communicationInfo')}
              details={communicationInfo}
              isEditable
              onEdit={() => this.handleEdit('communicationInfo')}
            />
            <ProfileDetail
              title={strings('profileData.jobInfo')}
              details={jobInfo}
              isEditable
              onEdit={() => this.handleEdit('jobInfo')}
            />
          </ScrollView>
        </View>
        <RenderProfile
          showForm={showEditForm}
          formType={formtype}
          hideForm={this.hideForm}
          formData={profileData}
          refreshData={this.refreshData}
        />
      </View>
    );
  }
}
