import React from 'react';
// import PropTypes from 'prop-types';
import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native';
import { BackgroundImage, SupportRequestCard, ModalOverlay, SupportChat, FilterList } from '../../../components/RenderStaticComponents';
import RenderButton from '../../../components/RenderButton';
import * as styles from './styles.native';
import { strings } from '../../../../locales/i18n';
import supportBackground from '../../../images/support-background.jpg';
import floatingButton from '../../../images/floatingButton.png';
import callIcon from '../../../images/call-black.png';
import CreateRequest from './CreateRequest';
import FilterRequest from './FilterRequest';
import moment from 'moment';
const today = moment();

import { customerSupportNumber } from '../../../configs/appConstants';
import Communications from 'react-native-communications';

export default class Support extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      supportInfo: {},
      supportRequests: [],
      showCreateForm: false,
      selectedFilters: {
        title: '',
        status: '',
        dateRange: {},
      },
      showFilters: false,
      popupData: {},
    };
  }

  componentDidMount() {
    this.getRequestList();
  }

  makeCall = () => {
    if (customerSupportNumber) {
      window.setTimeout(() => {
        Communications.phonecall(customerSupportNumber.toString(), true);
      }, 100);
    }
  }

  getRequestList = () => {
    const { selectedFilters } = this.state;
    console.log('selectedFilters', selectedFilters);
    const reqFilterObj = {
      title: selectedFilters.title && selectedFilters.title.value,
      status: selectedFilters.status && selectedFilters.status.value,
      to: (selectedFilters.dateRange && selectedFilters.dateRange.to) ? moment(selectedFilters.dateRange.to, 'DD-MM-YYYY').format('YYYY-MM-DD') : '',
      from: (selectedFilters.dateRange && selectedFilters.dateRange.from) ? moment(selectedFilters.dateRange.from, 'DD-MM-YYYY').format('YYYY-MM-DD') : '',
    };
    const self = this;
    const reqData = {
      params: {
        customerType: 'los_user',
        ...reqFilterObj,
      },
      userDetails: this.props.screenProps.userDetails,
      successCb: (response) => {
        console.log('get list response --->', response);
        if (response && response.data && response.data.serviceRequests) {
          self.setState({ supportRequests: response.data.serviceRequests });
        }
      },
    };
    this.props.screenProps.getRequest({ key: 'getSupportRequests', data: reqData });
  }

  createPopUpData = (requestCode) => {
    const self = this;
    return {
      headerText: strings('support.createSuccessTitle'),
      bodyText: strings('support.createSuccessInfo', { requestCode }),
      actionButtons: [{
        label: strings('support.createSuccessCta'),
        callback: () => {
          self.toggleCreateForm(false);
          self.getRequestList();
        },
        action: 'callback',
        type: 'primary',
      }],
      type: 'success',
      showPopup: true,
    };
  }

  createNewRequest = (data) => {
    const reqData = {
      params: {
        journeyName: 'customerServiceWorkflow',
        typeOfRequest: data.subject,
        describeYourIssue: data.body,
      },
      userDetails: this.props.screenProps.userDetails,
      successCb: (response) => {
        if (response && response.data) {
          const reqNumber = response.data.data && response.data.data.serviceRequestNumber;
          const popupData = this.createPopUpData(reqNumber);
          this.setState({ popupData });
        }
      },
    };
    this.props.screenProps.postRequest({ key: 'createSupportRequest', data: reqData });
  }

  applyFilter = (data) => {
    this.setState({ selectedFilters: data }, () => this.getRequestList());
    this.toggleFilters(false);
  }

  openSupportChatRequest = (data) => {
    this.setState({ supportInfo: data });
  }

  closeSupportChatRequest = () => {
    this.setState({ supportInfo: {} });
  }

  toggleCreateForm = (value) => {
    this.setState({ showCreateForm: value, popupData: {} });
  }

  toggleFilters = (value) => {
    this.setState({ showFilters: value });
  }

  getSubjectTranslation = (requestType) => {
    const subjectMap = {
      Payment: strings('support.subjectPayment'),
      Disbursement: strings('support.subjectDisbursement'),
      Others: strings('support.subjectOthers'),
    };
    return subjectMap[requestType];
  }

  render() {
    const { supportInfo, showCreateForm, supportRequests, selectedFilters, showFilters, popupData } = this.state;
    const showSupportChat = supportInfo && supportInfo.comments;
    const subjectOptions = {
      label: strings('support.filterSub'),
      options: [{
        value: 'Payment',
        name: strings('support.subjectPayment'),
      }, {
        value: 'Disbursement',
        name: strings('support.subjectDisbursement'),
      }, {
        value: 'Others',
        name: strings('support.subjectOthers'),
      }],
    };

    const statusOptions = {
      label: strings('support.filterStatus'),
      options: [{
        value: 'Closed',
        name: strings('support.statusClosed'),
      }, {
        value: 'In Progress',
        name: strings('support.statusInProgress'),
      }],
    };
    const requestList = supportRequests.map((request, index) => (
      <SupportRequestCard
        key={request.serviceRequestNumber}
        status={request.serviceRequestStatus}
        label={strings('support.cardLabel', { serviceRequestNumber: request.serviceRequestNumber, days: today.diff(moment(request.startTime), 'days') })}
        header={this.getSubjectTranslation(request.typeOfRequest)}
        description={request.comments && !!request.comments.length && request.comments[0].message}
        openSupportChatRequest={() => this.openSupportChatRequest(request)}
      />
    ));
    return (
      <View style={styles.wrapperStyle}>
        {this.props.screenProps.header}
        <ScrollView>
          <View style={styles.infoBanner}>
            <Text style={styles.bannerText}>{strings('support.title')}</Text>
          </View>
          <BackgroundImage source={supportBackground} contentContainerStyle={styles.backgroundStyle} >
            <View>
              <Text style={styles.bannerLabel}>{strings('support.supportText')}</Text>
              <Text style={styles.bannerHead} >{strings('support.fromTo')}</Text>
              <Text style={styles.bannerHead} >{strings('support.time')}</Text>
            </View>
            <View style={styles.bannerButtonWrap}>
              <TouchableOpacity>
                <Text onPress={this.makeCall}style={styles.bannerButton}>{strings('support.callNow')}</Text>
              </TouchableOpacity>
              {/* <Text style={styles.bannerButtonSplit}>{strings('support.or')}</Text>
                            <TouchableOpacity>
                              <Text style={[styles.bannerButton, styles.bannerCallButton]}>{strings('support.callLater')}</Text>
                            </TouchableOpacity>}*/}
            </View>
          </BackgroundImage>
          <View style={styles.lowerWrap}>
            <FilterList
              list={selectedFilters}
              applyFilter={this.applyFilter}
              showFilters={() => this.toggleFilters(true)}
            />
            <View>
              {requestList}
            </View>
          </View>
        </ScrollView>
        <TouchableOpacity
          style={styles.floatButtonWrap}
          onPress={() => this.toggleCreateForm(true)}
        >
          <View>
            <Image
              source={floatingButton}
            />
          </View>
        </TouchableOpacity>
        <CreateRequest
          showCreateForm={showCreateForm}
          toggleCreateForm={this.toggleCreateForm}
          submitForm={this.createNewRequest}
          popupData={popupData}
        />
        <FilterRequest
          subjectOptions={subjectOptions}
          statusOptions={statusOptions}
          hideFilters={() => this.toggleFilters(false)}
          selectedFilters={selectedFilters}
          applyFilter={this.applyFilter}
          showFilters={showFilters}
        />
        <ModalOverlay
          onBack={() => this.closeSupportChatRequest()}
          isVisible={showSupportChat}
          headerType={'white'}
        >
          <View style={styles.modalWrap}>
            <View style={styles.modalTop}>
              <View style={styles.modalHeader}>
                <Text style={styles.modalTitle}>{supportInfo.serviceRequestNumber}</Text>
                <Image
                  source={callIcon}
                />
              </View>
              <Text style={styles.modalDesc}>{strings('support.chatTime', { time: moment(supportInfo.startTime).format('hh:mma'), date: moment(supportInfo.startTime).format('DD/MM/YYYY') })}</Text>
            </View>
            <ScrollView>
              <View>
                <SupportChat chats={supportInfo.comments} />
              </View>
            </ScrollView>
            {supportInfo.serviceRequestStatus === 'Closed'
            ? <View>
              <Text style={styles.ctaHelper}>{strings('support.ctaHelper')}</Text>
              <View style={styles.ctaButtonWrap}>
                <RenderButton
                  type={'primary'}
                  text={strings('support.modalCta')}
                  onPress={() => {}}
                />
              </View>
            </View>
            : null
            }
          </View>
        </ModalOverlay>
      </View>
    );
  }
}
