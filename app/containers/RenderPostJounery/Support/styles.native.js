import { colors,
    fonts,
    lineHeight,
    sizes } from '../../../configs/styleVars';

export const wrapperStyle = {
  position: 'relative',
  overflow: 'visible',
  flex: 1,
  backgroundColor: colors.secondaryBGColor,
};

export const infoBanner = {
  paddingHorizontal: sizes.appPaddingHorizontal,
  paddingVertical: 15,
  backgroundColor: colors.white,
  paddingTop: sizes.headerPadding,
};

export const bannerText = {
  color: colors.primaryBGColor,
  fontSize: fonts.h2,
  lineHeight: lineHeight.h2,
  ...fonts.getFontFamilyWeight('bold'),
};

export const backgroundStyle = {
  paddingHorizontal: sizes.appPaddingHorizontal,
  paddingVertical: 20,
};

export const bannerLabel = {
  fontSize: fonts.label,
  lineHeight: lineHeight.label,
  color: colors.black,
  paddingVertical: 10,
  ...fonts.getFontFamilyWeight(),
};

export const bannerHead = {
  fontSize: fonts.body,
  lineHeight: lineHeight.body,
  color: colors.black,
  ...fonts.getFontFamilyWeight('bold'),
};

export const bannerButtonWrap = {
  flexDirection: 'row',
  alignItems: 'center',
  paddingVertical: 10,
};

export const bannerButton = {
  backgroundColor: colors.primaryBGColor,
  paddingHorizontal: 10,
  paddingVertical: 5,
  borderRadius: 6,
  color: colors.white,
  ...fonts.getFontFamilyWeight(),
  overflow: 'hidden',
};

export const bannerButtonSplit = {
  paddingHorizontal: 10,
};

export const bannerCallButton = {
  backgroundColor: colors.white,
  color: colors.primaryBGColor,
};

export const lowerWrap = {
  paddingHorizontal: sizes.appPaddingHorizontal,
  paddingVertical: 15,
};

export const floatButtonWrap = {
  position: 'absolute',
  bottom: 15,
  right: 20,
  zIndex: 2,
};

export const modalWrap = {
  flex: 1,
  backgroundColor: colors.secondaryBGColor,
};

export const formModalWrap = {
  flex: 1,
  backgroundColor: colors.white,
};

export const modalTop = { paddingHorizontal: sizes.appPaddingHorizontal,
  backgroundColor: colors.white,
};

export const modalHeader = { flexDirection: 'row',
  justifyContent: 'space-between',
};

export const modalTitle = { fontSize: fonts.h2,
  lineHeight: lineHeight.h2,
  color: colors.black,
  ...fonts.getFontFamilyWeight('bold'),
};

export const modalDesc = { fontSize: fonts.label,
  lineHeight: lineHeight.label,
  color: colors.descriptionColor,
  paddingBottom: 10,
  ...fonts.getFontFamilyWeight(),
};

export const ctaHelper = { fontSize: fonts.body,
  lineHeight: lineHeight.body,
  color: colors.labelColor,
  paddingVertical: 10,
  textAlign: 'center',
  ...fonts.getFontFamilyWeight(),
};

export const ctaButtonWrap = { paddingHorizontal: 25,
  paddingTop: 5,
  paddingBottom: 25,
};

export const formWrapper = {
  paddingHorizontal: sizes.appPaddingHorizontal,
};

export const formScrollableView = {
  backgroundColor: colors.white,
};
