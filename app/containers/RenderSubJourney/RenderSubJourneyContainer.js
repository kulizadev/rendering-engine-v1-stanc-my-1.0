/**
 *
 * RenderStep
 *
 */

import React from 'react';

import * as renderService from '../../utilities/renderDataService';
import RenderSubJourneyAbstract from './RenderSubJourneyAbstract';


export default class RenderSubJourneyContainer extends RenderSubJourneyAbstract { // eslint-disable-line react/prefer-stateless-function


  render() {
    return (
      <div>This is RenderSubJourneyContainer</div>
    );
  }
}

