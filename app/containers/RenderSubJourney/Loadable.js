/**
 *
 * Asynchronously loads the component for RenderSubJourney
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
