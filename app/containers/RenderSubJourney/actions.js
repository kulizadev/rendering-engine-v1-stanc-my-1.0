/*
 *
 * RenderSubJourney actions
 *
 */

import * as C from './constants';

export function defaultAction() {
  return {
    type: C.DEFAULT_ACTION,
  };
}


export function setSubJourneyId(data) {
  return {
    type: C.SET_SUBJOURNEY_ID,
    data,
  };
}
