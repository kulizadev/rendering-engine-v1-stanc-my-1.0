/**
 *
 * RenderSubJourney
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectReducer from '../../utils/injectReducer';
import makeSelectRenderSubJourney from './selectors';
import { makeSelectUserDetails } from '../AppDetails/selectors';
import reducer from './reducer';
import messages from './messages';

import makeSelectRenderJourney from '../RenderJourney/selectors';
import * as appActions from '../AppDetails/actions';
import * as renderActions from '../RenderJourney/actions';

import RenderSubJourneyContainer from './RenderSubJourneyContainer';

export class RenderSubJourney extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderSubJourneyContainer {...this.props} />
    );
  }
}

RenderSubJourney.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  rendersubjourney: makeSelectRenderSubJourney(),
  renderjourney: makeSelectRenderJourney(),
  userDetails: makeSelectUserDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    setSubJourneyId: (subJourneyId) => dispatch(renderActions.setSubJourneyId(subJourneyId)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'renderSubJourney', reducer });

export default compose(
  withReducer,
  withConnect,
)(RenderSubJourney);
