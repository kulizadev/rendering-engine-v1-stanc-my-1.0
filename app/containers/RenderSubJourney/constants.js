/*
 *
 * RenderSubJourney constants
 *
 */

export const DEFAULT_ACTION = 'app/RenderSubJourney/DEFAULT_ACTION';

// export const SET_SUBJOURNEY_ACTIVE = 'app/RenderSubJourney/SET_SUBJOURNEY_ACTIVE';
export const SET_SUBJOURNEY_ID = 'app/RenderSubJourney/SET_SUBJOURNEY_ID';
