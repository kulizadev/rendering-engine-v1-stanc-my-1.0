
import { fromJS } from 'immutable';
import renderSubJourneyReducer from '../reducer';

describe('renderSubJourneyReducer', () => {
  it('returns the initial state', () => {
    expect(renderSubJourneyReducer(undefined, {})).toEqual(fromJS({}));
  });
});
