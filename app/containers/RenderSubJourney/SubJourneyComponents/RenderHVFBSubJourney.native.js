/**
*
* RenderHvfbComponent
*
*/

import React from 'react';
import { View, NativeModules, Platform } from 'react-native';
import Text from '../../../components/RenderText';
import { clientKeys } from '../../../configs/appConstants';

import { permissionConst } from '../../../configs/appConstants';
import * as sessionUtils from '../../../utilities/sessionUtils';
import { getClientKey } from '../../../configs/appConstants';

export default class RenderHVFBSubJourney extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      hvFBResponse: null,
    };
  }

  componentDidMount() {
    this.loginWithFacebook();
  }

  loginWithFacebook = async () => {
    const self = this;
    const { appid, appkey } = getClientKey('hypervergeFb');
    const selfieURL = await sessionUtils.getSessionKey('selfie');
    NativeModules.LendingUtils.openHVFBActivity(selfieURL, '', appid, appkey, (response, error) => {
      if (response) {
        console.log('HVFB Activity --->', response, error);
        self.props.updateFormData(self.props.subJourneyId, response);
        self.props.validateAndUpdateRender(response, self.props.subJourneyId);
      } else {
        console.log('HVFB Activity --->', response, error);
        self.props.updateFormData(self.props.subJourneyId, false);
        self.props.validateAndUpdateRender(false, self.props.subJourneyId);
      }
      self.props.exit();
    });
  }

  render() {
    return null;
  }
}
