/**
*
* RenderUpload
*
*/

import React from 'react';
import { View, ScrollView, NativeModules, Platform } from 'react-native';
import Text from '../../../components/RenderText';

import RenderButton from '../../../components/RenderButton/';
import RenderPreview from '../../../components/RenderPreview/';

import { headingStyle, bodyText } from '../styles';
import { strings } from '../../../../locales/i18n';
import styleVars from '../../../configs/styleVars';
import * as styles from '../../../components/RenderUpload/RenderUploadStyles';
import { permissionConst, getClientKey } from '../../../configs/appConstants';
import * as NativeMethods from '../../../components/RenderUpload/RenderUploadHelper';
import * as sessionUtils from '../../../utilities/sessionUtils';

export default class RenderSelfieSubJourney extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      selfieConfig: this.props.config,
      isSelfieSubmitted: false,
      isSelfieUploaded: false,
      selfieURL: null,
      selfieLocal: null,
      retryUpload: false,
    };
  }

  componentDidMount() {
    if (this.props.currentValue) {
      const valueObj = JSON.parse(this.props.currentValue);
      this.setState({
        currentValue: { ...valueObj },
        isSelfieUploaded: true,
        isSelfieSubmitted: true,
        selfieURL: valueObj.selfieDMSURL,
      });
    } else {
      this.captureSelfie();
    }
  }

  submitStep = () => {

  }

  buildFileData = (targetFile) => {
    const uri = Platform.OS === 'ios' ? targetFile.uri : `file://${targetFile.path}`;
    return {
      uri,
      name: targetFile.fileName,
      size: targetFile.fileSize,
      type: targetFile.type,
    };
  }

  // Config here is respective file's object in the meta
  _buildRequestParams = (file, config) => {
    const fd = new FormData();
    fd.append('image', { ...this.buildFileData(file) });
    fd.append('live', file.live);
    fd.append('livenessScore', file.livenessScore);
    fd.append('toBeReviewed', file.toBeReviewed);
    fd.append('fileType', config.fileType);
    fd.append('docID', config.docID);
    fd.append('moduleID', config.moduleID);
    fd.append('renameFile', config.renameFile);
    fd.append('appID', config.appID || this.props.formData.applicationId);
    // processInstanceId
    fd.append('id', this.props.userDetails.processInstanceId);
    return fd;
  }

  selfieUploadSuccessCb = (data) => {
    const self = this;
    window.setTimeout(() => {
      self.setState({ selfieURL: data.dmsURL, retryUpload: false }, () => {
        self.verifySelfie();
      });
    });
  }


   // TODO: Change this functionality
  _buildRequestObject(file, config) {
    const self = this;
    const reqData = {
      url: config.api && config.api.url,
      params: this._buildRequestParams(file, config),
      userDetails: this.props.userDetails,
      contentType: 'multipart/form-data',
      successCb: (response) => {
        console.debug('got response from the uplaod', response);
        const data = response.data;
        if (data.dmsURL) {
          self.selfieUploadSuccessCb(data);
        } else if (data.error) {
          // Re-take selfie
          self.setState({ retryUpload: true });
        }
      },
      errorCb: () => {
        self.setState({ retryUpload: true });
        console.debug('something went wrong in selfie upload');
      },
    };
    return reqData;
  }

  setLocalImg = (file) => {
    this.setState({ selfieLocal: file });
  }

  triggerUpload = () => {
    const file = this.state.selfieLocal;

    const config = this.state.selfieConfig.selfie;
    console.debug('whta is the file from HV click', file, config);
    const method = config.api && config.api.method;
    const reqObj = this._buildRequestObject(file, config);

    console.debug('whta is reqObj', reqObj);
    if (method === 'POST') {
      this.props.postRequest({ key: null, data: reqObj });
    } else {
      this.props.getRequest({ key: null, data: reqObj });
    }
  }

  captureSelfie = async () => {
    const permissionStatus = await NativeMethods.getPermission();
    const self = this;
    let fileConfig;

    fileConfig = this.state.selfieConfig.selfie;
    // const docType = 'CARD';
    if (permissionStatus === permissionConst.GRANTED) {
      // this.uploadType = 'docScan';
      const { appid, appkey } = getClientKey('hypervergeSnap');
      NativeModules.CameraModule.initHyperverge(appid, appkey, 'AsiaPacific', 'FACEID');
      NativeModules.CameraModule.takeSelfie((res) => {
        console.debug('what is res yoooo', res);
        if (res) {
          const file = JSON.parse(res);
          const uri = Platform.OS === 'ios' ? file.uri : `file://${file.path}`;
          file.uri = uri;
          self.setLocalImg(file);
        }
      });
    } else {
      console.log('Camera permission denied');
    }
  }

  buildFormDataJsonString = (data) => {
    let obj,
      self = this;
    if (data.status) {
      obj = {
        verified: true,
        selfieDMSURL: self.state.selfieURL,
      };
      return JSON.stringify(obj);
    }
    return false;
  }

  updateSessionData = async () => {
    const uri = Platform.OS === 'ios' ? this.state.selfieLocal.uri : this.state.selfieLocal.path;
    await sessionUtils.setSessionKey('selfie', this.state.selfieLocal.uri);
  }

  buildVerifyReqObj = (config) => {
    const self = this;
    const reqData = {
      isLongReq: true,
      loaderMessage: strings('subJourney.verifySelfieLoader'),
      url: config.api && config.api.url,
      params: { id: this.props.userDetails.processInstanceId },
      userDetails: this.props.userDetails,
      successCb: (response) => {
        console.debug('got response from the verify', response);
        const data = response.data;
        // setFormData
        self.props.updateFormData(self.props.subJourneyId, self.buildFormDataJsonString(data));
        self.props.validateAndUpdateRender(self.buildFormDataJsonString(data), self.props.subJourneyId);
        if (data.error && data.error === 1) {
          // when the error-code is 1 then max retry for the verification has been exhausted
          // hence the loan has been rejected
          // hence the back end task has been closed so trigger the initiate again
          self.props.refreshJourney();
          // trigger initiate
        }
        self.updateSessionData();
        // exit
        self.props.exit();
      },
      errorCb: () => {
        console.debug('something went wrong in selfie verify');
      },
    };

    return reqData;
  }

  verifySelfie = () => {
    const config = this.state.selfieConfig.verify;
    const self = this;
    let method = config.api && config.api.method,
      reqObj = this.buildVerifyReqObj(config);

    if (method === 'POST') {
      this.props.postRequest({ key: null, data: reqObj });
    } else {
      this.props.getRequest({ key: null, data: reqObj });
    }
  }

  submitSelfie = () => {
    // trigger verify
    this.triggerUpload();
  }

  getActionBlock = () => {
    const renderActions = null;
    return (
      <View style={{ alignSelf: 'flex-end' }}>
        <RenderButton
          text={strings('subJourney.submitText')}
          onPress={() => this.submitSelfie()}
          type={'primary'}
          {...styles.btnProps}
        />
        <RenderButton
          text={strings('subJourney.reTakeText')}
          onPress={() => this.captureSelfie()}
          type={'secondary'}
          {...styles.btnProps}
        />
      </View>
    );
  }

  render() {
    let renderBlock = null;
    let actionBlock = null;
    const messageBlock = (<Text style={bodyText}>{strings('subJourney.instructionSelfieText')}</Text>);
    let previewBlock = null;

    const headingBlock = (<Text style={headingStyle}>{strings('subJourney.selfieHeading')}</Text>);

    let retryBlock = null;
    const previewStyles = { height: 200, marginTop: 25 };

    if (this.state.retryUpload) {
      retryBlock = (<Text style={bodyText}>{strings('subJourney.retryText')}</Text>);
    }

    if (!this.state.selfieLocal) {
        // Show button to click photo
      renderBlock = (
        <View>
          {retryBlock}
          <RenderButton
            text={strings('subJourney.selfieTopText')}
            onPress={() => this.captureSelfie()}
            type={'primary'}
            {...styles.btnProps}
          />
        </View>);
    } else {
      previewBlock = (
        <View style={previewStyles}>
          <RenderPreview
            circular
            key={'uploaded' + 'selfie'}
            uri={this.state.selfieLocal.uri}
          />
        </View>);
      actionBlock = this.getActionBlock();
    }

    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <ScrollView keyboardShouldPersistTaps="always">
            {headingBlock}
            {messageBlock}
            {previewBlock}
          </ScrollView>
        </View>
        {renderBlock}
        {actionBlock}
      </View>
    );
  }
}
