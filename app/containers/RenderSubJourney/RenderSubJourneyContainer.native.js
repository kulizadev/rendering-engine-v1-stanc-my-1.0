/**
 *
 * RenderStep
 *
 */

import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import subJourneyMap from './subJourneyMap';

import RenderPreHeader from '../../components/RenderPreHeader';
import RenderSubJourneyAbstract from './RenderSubJourneyAbstract';
import * as JourneyStyles from '../RenderJourney/JourneyStyles.native';

export default class RenderSubJourneyContainer extends RenderSubJourneyAbstract { // eslint-disable-line react/prefer-stateless-function


  render() {
    // map the respective subJourney component for the current subJourneyId
    const subJourneyId = this.props.subJourneyId;
    const UnitComponent = subJourneyMap[subJourneyId];
    const meta = this.props.renderData.metaData;
    return (
      <View {...JourneyStyles.mainWrapperStyle}>
        <RenderPreHeader title="" hideCallAction onBack={this.exit} />
        <View
          {...JourneyStyles.subStepWrapperStyle}
        >
          <View style={{ marginTop: 15, flex: 1 }}>
            <UnitComponent
              config={meta} {...this.props}
              exit={this.exit}
              refreshJourney={this.refreshJourney}
              currentValue={this.state.currentValue}
            />
          </View>
        </View>
      </View>
    );
  }
}

