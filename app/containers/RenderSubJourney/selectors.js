import { createSelector } from 'reselect';

/**
 * Direct selector to the renderSubJourney state domain
 */
const selectRenderSubJourneyDomain = (state) => state.get('renderSubJourney');

/**
 * Other specific selectors
 */


/**
 * Default selector used by RenderSubJourney
 */

const makeSelectRenderSubJourney = () => createSelector(
  selectRenderSubJourneyDomain,
  (substate) => substate.toJS()
);

export default makeSelectRenderSubJourney;
export {
  selectRenderSubJourneyDomain,
};
