/**
 *
 * RenderStep
 *
 */

import React from 'react';

import * as renderService from '../../utilities/renderDataService';

export default class RenderSubJourneyAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      currentValue: this.props.formData[this.props.subJourneyId],
    };
  }

  componentDidMount() {
    this.props.updateFormData(this.props.subJourneyId, '');
  }

  componentDidUnMount() {
    this.props.setSubJourneyId(null);
  }

  refreshJourney = () => {
    const self = this;
    this.props.triggerInitiate(true);
    window.setTimeout(() => { self.exit(); });
  }

  exit = () => {
    // set the subJourneyId in store to null
    this.props.setSubJourneyId(null);
  }


  render() {
    return null;
  }
}

