
import styled from 'styled-components';

const ActionWrapper = styled.div`
  width: 100%;
  display:flex;
  justify-content:space-between;
`;

export default ActionWrapper;
