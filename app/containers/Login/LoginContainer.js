/**
 *
 * Login Native
 *
 */

import React from "react";

import TextInputField from "../../components/TextInputField";
import RaisedButton from "material-ui/RaisedButton";
import RenderButton from "../../components/RenderButton";
import { LoginWrapper, Positioning, Extrapost } from "./LoginWrapper";
import LoginAbstract from "./LoginAbstract";
import RenderOtp from "../RenderOtp";
import ActionWrapper from "./ActionWrapper";
import Row from "../../components/Row";
import Column from "../../components/Column";
import { colors, brand, fonts } from "../../configs/styleVars";
import ScsImage from "../../images/banner.png";
import { relative } from "path";
import { strings } from "../../../locales/i18n";
import { white } from "material-ui/styles/colors";

export default class LoginContainer extends LoginAbstract {
  // eslint-disable-line react/prefer-stateless-function

  showOtpField = () => {
    this.setState({ otpGenerated: true });
  };

  handleGenerateOtp = () => {
    this.generateOtp(this.showOtpField);
  };

  render() {
    const firstp = {
      marginTop: "0px",
      marginBottom: "1px"
    };

    const inputField = {
      fontSize: fonts.inputField
    };

    const textfield = {
      fontSize: fonts.textField,
      color: colors.basicFontColor
    };

    const otpButton = {
      color: "red"
    };

    const mainHeader = {
      fontSize: fonts.h3,
      color: colors.darkGray
    };

    const secondp = {
      marginTop: "0px",
      marginBottom: "0px"
    };

    const divspacing = {
      marginTop: "10px"
    };

    const subHeading = {
      fontSize: fonts.subHeading,
      color: colors.gray
    };

    const inputspacing = {
      position: "absolute",
      top: "25%",
      left: "10%"
    };

    const textpostions = {
      position: "absolute",
      top: "15%",
      left: "12%",
      display: "flex",
      flexDirection: "column",
      textAlign: "center",
      color: colors.blueSideNavBG,
      fontSize: fonts.h2
    };

    const positioning = {
      position: "relative",
      height: "90vh"
    };

    const errorMessage = this.state.invalidMobileError
      ? this.state.invalidMobileError
      : null;
    const otpErrorMessage = this.state.invalidOtpError
      ? this.state.invalidOtpError
      : null;
    const componentProps = {
      id: "ku-number-" + "login",
      type: "text",
      placeholder: "Enter Your Mobile Number",
      floatingLabelFixed: true,
      floatingLabelText: "Mobile Number",
      maxLength:'8'
    };

    const otpComponentProps = {
      id: "ku-number-otp",
      type: "number",
      floatingLabelText: "OTP",
      floatingLabelFixed: true,
      placeholder: "Please enter OTP"
    };

    const errorBlock = null;
    let otpBlock = null;
    let sendOtpButton = (
      <div className="center-align">
        <RenderButton
          style={otpButton}
          type="primary"
          onClick={this.handleGenerateOtp}
          label="SEND OTP"
          disabled={!this.state.isMobileNumberValid}
        />
      </div>
    );

    // Check if OTP is generated and only then show Login button and OTP input
    if (this.state.otpGenerated) {
      otpBlock = (
        <div>
          <div className="right-align">
            <TextInputField
              componentProps={otpComponentProps}
              errorMessage={otpErrorMessage}
              onChangeHandler={this.handleOtpChange}
            />
          </div>
          <ActionWrapper>
            <Extrapost>
              <RenderButton
                type="resendOtp"
                onClick={this.generateOtp}
                label="Resend OTP"
                disabled={!this.state.isValidOtp}
              />
            </Extrapost>
            <Extrapost>
              <RenderButton
                type="primary"
                onClick={this.triggerLogin}
                label="SUBMIT"
                disabled={!this.state.isValidOtp}
              />  
            </Extrapost>
          </ActionWrapper>
        </div>
      );
      sendOtpButton = null;
    }
    return (
      <Row>
        <Column colWidth={6} style={{ padding: "0" }}>
          {/* <LoginWrapper>
            <div className="center-align">
              <TextInputField
                componentProps={componentProps}
                errorMessage={errorMessage}
                onChangeHandler={this.handleMobileChange}
              />
            </div>
            {otpBlock}
            {sendOtpButton}
          </LoginWrapper> */}

          <div>
            <img src={ScsImage} style={{ width: "100%", height: "90vh" }} />
            <div style={textpostions}>
              <ul style={{ listStyle: "none" }}>
                <li>{strings("login.simple")}</li>
                {strings("login.attractive")} <li> </li>
                <li>{strings("login.needs")}</li>
              </ul>
            </div>
          </div>
        </Column>
        <Column colWidth={6}>
          <Positioning>
            <div style={inputspacing}>
              <div>
                <img
                  src={brand.logo}
                  style={{
                    width: "156.9px",
                    height: "60px",
                    marginBottom: "15px"
                  }}
                />
                <div>
                  <div>
                    <p style={firstp} style={mainHeader}>
                      {strings("login.welcome")}
                    </p>
                    <p style={secondp} style={mainHeader}>
                      {strings("login.standard")}
                    </p>
                  </div>
                  <div style={divspacing}>
                    <p style={firstp} style={subHeading}>
                      {strings("login.loan")}
                    </p>
                    <p style={secondp} style={subHeading}>
                      {strings("login.singapore")}
                    </p>
                  </div>
                </div>
              </div>
              <div style={inputField}>
                <TextInputField
                  style={TextInputField}
                  componentProps={componentProps}
                  errorMessage={errorMessage}
                  onChangeHandler={this.handleMobileChange}
                />
              </div>
              {otpBlock}
              {sendOtpButton}
            </div>
          </Positioning>
        </Column>
      </Row>
    );
  }
}
