import appVars from "../../configs/styleVars";
import styled from "styled-components";
const colors = appVars.colors,
  sizes = appVars.sizes;

export const LoginWrapper = styled.div`
  background: #fff;
  margin: 30px auto;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
  padding: 50px;
  margin-top: 70px;
`;

export const Positioning = styled.div`
  position: relative;
  height: 90vh;
  @media (max-width: 768px) {
    overflow-x: hidden;
    overflow-y: hidden;
  }
`;

export const Extrapost = styled.div`
  @media (max-width: 375px) {
    overflow-x: hidden;
    overflow-y: hidden;
    position: relative;
    left: -10%;
  }
`;
