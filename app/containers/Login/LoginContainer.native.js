/**
 *
 * Login Native
 *
 */


import React from 'react';
import { ScrollView, Text, TouchableOpacity, View, Platform } from 'react-native';
import { CheckBox } from 'react-native-elements';
import TextInputField from '../../components/TextInputField/';
import LoginAbstract from './LoginAbstract';
import RenderButton from '../../components/RenderButton';
import * as LoginProps from './LoginProps';
import RenderError from '../../components/RenderError/';

import {
  headerStyle,
  hintStyle,
  wrapperStyle,
  checkboxWrapper,
  checkboxTextWrapper,
  tncLink,
} from './styles.native';
import { strings } from '../../../locales/i18n';
import { fonts, colors } from '../../configs/styleVars';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

const checkboxStyles = {
  containerStyle: {
    borderWidth: 0,
    borderRadius: 0,
    backgroundColor: colors.transparentColor,
    borderColor: colors.transparentColor,
    padding: 0,
    marginTop: 10,
    marginLeft: 0,
  },
  checkedColor: colors.primaryBGColor,
  textStyle: {
    ...fonts.getFontFamilyWeight(),
    padding: 0,
    margin: 0,
    fontSize: fonts.inputSize,
    flex: 1,
    flexWrap: 'wrap',
  },
  size: 28,
};

export default class LoginContainer extends LoginAbstract { // eslint-disable-line react/prefer-stateless-function
  // constructor(props) {
  //   super(props);
  // }
  componentDidMount() {
    this.setState({ showPassword: false });
  }

  showOtpField = () => {
    this.props.setUserDetails({
      ...this.props.userDetails,
      mobileNumber: this.state.mobileNo,
    });
    this.props.navigation.navigate('RenderOtp');
  }

  handleGenerateOtp = () => {
    this.generateOtp();
  }

  handleMobileChange = (text) => {
    this.setState({ mobileNo: text });
    const value = text;
    // Check the email validation and set the state
    window.setTimeout(() => this.checkLoginValidation(value));
  }

  handlePasswordChange = (text) => {
    this.setState({ password: text });
    const value = text;
    // Check the email validation and set the state
    window.setTimeout(() => this.checkLoginPasswordValidation(value));
  }

  toggleCheckState = () => {
    this.setState(({ checked }) => ({ checked: !checked }));
  }

  toggleTncModal = () => {
    this.setState(({ showTermsAndConditions }) => ({ showTermsAndConditions: !showTermsAndConditions }));
  }

  renderIcon = () => {
    const { showPassword } = this.state;

    const name = showPassword ?
      'visibility' :
      'visibility-off';

    return (
      <MaterialIcon
        size={24}
        name={name}
        color={showPassword ? colors.primaryBGColor : colors.labelColor}
        onPress={this.toggleShowPassword}
      />
    );
  }

  toggleShowPassword = () => {
    console.log(this.state.showPassword);
    this.setState(({ showPassword }) => ({ showPassword: !showPassword }));
  }

  // TODO: move width to styleconfig
  render() {
    const errorMessageEmail = this.state.invalidEmailError ? this.state.invalidEmailError : null;
    const errorMessagePassword = this.state.invalidPasswordError ? this.state.invalidPasswordError : null;
    const loginError = (<RenderError errorMessage={this.state.loginError} />);
    return (
      <ScrollView
        keyboardShouldPersistTaps="always"
        contentContainerStyle={{ ...wrapperStyle }}
      >
        {/* <Text>{strings('login.welcome', { name: 'Joe' })}</Text> */}
        <Text style={headerStyle}>{strings('login.welcome')}</Text>
        <TextInputField
          label={strings('login.enterMobileNo')}
          error={this.state.invalidMobileError}
          keyboardType={'numeric'}
          onChangeText={this.handleMobileChange}
          onSubmitEditing={(() => this.state.checked && this.triggerLogin())}
          returnKeyType={'done'}
        />
        {/* <TextInputField
          label={strings('login.enterPassword')}
          error={errorMessagePassword}
          onChangeText={this.handlePasswordChange}
          onSubmitEditing={this.state.checked && (() => this.triggerLogin())}
          returnKeyType={'done'}
          secureTextEntry={!this.state.showPassword}
          noCopy
          renderAccessory={this.renderIcon}
        /> */}
        {loginError}
        {/* <View style={checkboxWrapper}>
          <CheckBox
            {...checkboxStyles}
            onPress={this.toggleCheckState}
            checked={this.state.checked}
            iconType="material-community"
            checkedIcon="checkbox-marked"
            uncheckedIcon="checkbox-blank-outline" />
          <Text style={checkboxTextWrapper}>
                      <Text>{strings('login.tncText1')}</Text>
                      <Text style={tncLink} textDecorationLine={'underline'} onPress={this.toggleTncModal}>{strings('login.tncText2')}</Text>
                      <Text>{strings('login.tncText3')}</Text>
                    </Text>
        </View> */}
        <RenderButton
          text={strings('login.buttonText')}
          type={'primary'}
          onPress={this.handleGenerateOtp}
          // disabled={!this.state.checked}
          {...LoginProps.btnProps}
        />
      </ScrollView>
    );
  }
}

