/*
 *
 * RenderProfile actions
 *
 */

import * as C from './constants';

export function defaultAction() {
  return {
    type: C.DEFAULT_ACTION,
  };
}

export function initRenderProfileService(data) {
  return {
    type: C.INIT_RENDER_PROFILE_SERVICE,
    data,
  };
}
