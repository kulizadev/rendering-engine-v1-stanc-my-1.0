
import { fromJS } from 'immutable';
import renderProfileReducer from '../reducer';

describe('renderProfileReducer', () => {
  it('returns the initial state', () => {
    expect(renderProfileReducer(undefined, {})).toEqual(fromJS({}));
  });
});
