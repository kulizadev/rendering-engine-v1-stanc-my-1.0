// import { take, call, put, select } from 'redux-saga/effects';

import { call, put, select, take, takeLatest, takeEvery } from 'redux-saga/effects';

import * as userUtils from '../../utilities/userUtils';
import * as C from './constants';
import * as renderActions from '../RenderJourney/actions';
import * as renderService from '../../utilities/renderDataService';
import * as renderProfileService from './renderProfileService';
import * as appActions from '../AppDetails/actions';
import * as appDetailsSelector from '../AppDetails/selectors';
import { getTranslation } from '../../utilities/localization';

// Individual exports for testing
export default function* defaultSaga() {
  const watcherRenderService = yield takeEvery(C.INIT_RENDER_PROFILE_SERVICE, renderProfileServiceFn);
}

export function* renderProfileServiceFn(action) {
  // This saga is called when we get the screen details response from initiate or submit-form

  yield put(renderActions.setRefreshFlag(false));

  // set render data with metaData for the current screen
  // Contains all the fields' json with order to be rendered in
  yield put(renderActions.setStepperData({ fullScreen: true }));
  console.log('Saga --->', action.data);
  const stepData = renderProfileService.populateFieldValues(action.data.formData, action.data.renderData);
  yield put(renderActions.setStepData(stepData));

  const renderData = renderService.buildRenderData(stepData);
  yield put(renderActions.setRenderData(renderData));

  // set the ordering of the fields to be rendered in a step
  const fieldOrder = renderService.getFieldOrder(renderData);
  yield put(renderActions.setFieldOrder(fieldOrder));

  // set current screen name
  // yield put(renderActions.setCurrentScreenName(stepData.screenName));

  // set current screen Id
  yield put(renderActions.setCurrentScreenId('editProfile'));

  // set form data to submit
  if (renderData && fieldOrder) {
    const formData = renderService.buildFormData(renderData, fieldOrder);
    yield put(renderActions.setFormData(formData));
  }

  yield put(renderActions.setRefreshFlag(true));
}
