export const populateFieldValues = (formData, renderData) => {
  const dummyRenderData = { ...renderData };
  Object.keys(dummyRenderData).forEach((key) => {
    dummyRenderData[key].value = formData[key];
  });
  console.log('SAGA Service--->', dummyRenderData);
  return {
    screenInfo: 'editProfile',
    data: {
      editProfile: { ...dummyRenderData },
    },
  };
};
