/**
 *
 * RenderProfile
 *
 */

import React from 'react';

import * as renderService from '../../utilities/renderDataService';
import * as validationFns from '../../utilities/validations/validationFns';
import * as userUtils from '../../utilities/userUtils';
import { appConst } from '../../configs/appConfig';
import * as inactivityTracker from '../../utilities/inactivityTracker';
import { strings } from '../../../locales/i18n';

export default class RenderProfileAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.REQ_KEY = {
      get: {
        communicationInfo: 'getAddressProfile',
        jobInfo: 'getJobProfile',
      },
      update: {
        communicationInfo: 'updateAddressProfile',
        jobInfo: 'updateJobProfile',
      },
    };
  }


  getSuccessPopUpdata = () => ({
    headerText: strings('profileData.successPopupHead'),
    bodyText: strings('profileData.successPopupBody'),
    actionButtons: [{
      label: strings('profileData.successPopupBtnText'),
      callback: this.successCb,
      action: 'callback',
      type: 'primary',
    }],
    type: 'success',
  })

  getErrorPopUpdata = () => {
    const self = this;
    return {
      headerText: strings('profileData.errorPopupHead'),
      bodyText: strings('profileData.errorPopupBody'),
      actionButtons: [{
        label: strings('profileData.errorPopupBtnText1'),
        action: 'call',
        type: 'secondary',
      },
      {
        label: strings('profileData.errorPopupBtnText2'),
        action: 'callback',
        callback: this.failureCb,
        type: 'primary',
      }],
      type: 'error',
    };
  }

  successCb = () => {
    this.props.refreshData();
    this.setState({ showPopup: '' });
  }

  failureCb = () => {
    this.toggleRetry(true);
    this.setState({ showPopup: '' });
  }
  triggerInitiate = () => {
    const self = this;
    const reqData = {
      userDetails: this.props.userDetails,
      successCb: (response) => {
        // Setup the render Data in the app
        // Call the render saga service
        console.log('Render data --->', response);

        self.props.initRenderProfileService({ formData: self.props.formData, renderData: response.data });
      },
    };
    this.props.getRequest({ key: this.REQ_KEY.get[this.props.formType], data: reqData });
  }

  buildRequestParams = (selfie) => {
    const fd = new FormData();
    const { formData } = this.props.renderjourney;
    fd.append('image', selfie);
    Object.keys(formData).forEach((key) => {
      fd.append(key, formData[key]);
    });
    return fd;
  }

  buildLoaderObj = (flag) => ({
    isVisible: flag,
    loader: true,
    isLongReq: true,
    loaderMessage: strings('subJourney.verifySelfieLoader'),
  })

  submitData = (selfie) => {
    const self = this;
    const reqData = {
      params: this.buildRequestParams(selfie),
      userDetails: this.props.userDetails,
      loader: true,
      contentType: 'multipart/form-data',
      successCb: (response) => {
        self.setState({ loaderState: self.buildLoaderObj(false) });
        // Setup the render Data in the app
        // Call the render saga service
        if (response && response.data) {
          // self.props.setPopupData(this.getSuccessPopUpdata());
          if (response && response.data) {
            if (response.data.status) {
              self.setState({ showPopup: 'success' });
            } else if (response.data.error && response.data.error === 1) {
              self.setState({ showPopup: 'failure' });
            }
          }
        }
      },
      errorCb: () => {
        // self.props.setPopupData(this.getErrorPopUpdata());
        self.setState({ loaderState: self.buildLoaderObj(false) });
        self.setState({ showPopup: 'failure' });
      },
    };
    this.setState({ loaderState: self.buildLoaderObj(true) });
    this.props.postRequest({ key: this.REQ_KEY.update[this.props.formType], data: reqData });
  }
}
