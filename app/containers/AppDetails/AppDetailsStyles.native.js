import { Dimensions } from 'react-native';
import { colors } from '../../configs/styleVars';

export const wrapperStyles = {
  style: {
    flex: 1,
    flexDirection: 'column',
    width: Dimensions.get('window').width,
    backgroundColor: colors.white,
  },
};
