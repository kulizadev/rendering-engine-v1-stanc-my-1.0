import * as common from '../../utilities/commonUtils';

// Below code is Helper Functions to remove the duplicate calls
const inProcessQueriesHash = [];

const _convertObjectToHash = (obj) => {
  let resultString = '';
  Object.keys(obj)
        .sort()
        .forEach((paramKey) => {
          let partialString = typeof (obj[paramKey]) === 'object' ? _convertObjectToHash(obj[paramKey]) : obj[paramKey];
          resultString += paramKey + '-' + partialString;
        });
  return resultString;
}

// Get the unique hashkey against a query (combinatin of URL, METHOD, OPTIONS)
export const getQueryHash = (url, options) => {
  let hashString = url + '-' + options.method;
  if (options.body) {
    let body = options.body;
    if (typeof (body) === 'object') {
     	hashString += _convertObjectToHash(body);
    } else if (typeof body === 'string') {
    	let derivedStr = common.isJson(body) ? _convertObjectToHash(JSON.parse(body)) : body;
    	hashString += derivedStr;
    }
  }

  return hashString;
}

const getInProcessQuery = () => {
	return inProcessQueriesHash;
}

export const pushIntoInProcessArray = (url, options) => {
	const queryhash = getQueryHash(url, options);
	if (getInProcessQuery().indexOf(queryhash) < 0) {
		inProcessQueriesHash.push(queryhash);
	}
}

export const removeFromInProcessArray = (url, options) => {
	const queryhash = getQueryHash(url, options);

	let targetIndex = inProcessQueriesHash.indexOf(queryhash);
	if (targetIndex !== -1) {
		inProcessQueriesHash.splice(targetIndex, 1)
	}
}

export const isQueryInProcess = (url, options) => {
	const queryhash = getQueryHash(url, options);

	return (getInProcessQuery().indexOf(queryhash) > -1);
}
