import { NativeModules, Platform } from 'react-native';
import { Crashlytics } from 'react-native-fabric';
import { languages } from '../../configs/appConstants';
import * as sessionUtils from '../../utilities/sessionUtils';

export const setLanguageHelper = (userLanguage) => {
  sessionUtils.setSessionKey('userLanguage', userLanguage);
  const langObj = languages.find((language) => language.value === userLanguage);
  if (typeof NativeModules.Utilities.setLocalization === 'function') {
    NativeModules.Utilities.setLocalization(langObj.icon);
  }
};


export const reportLog = (log) => {
  const logString = JSON.stringify(log);
  if (Platform.OS === 'ios') {
// Record a non-fatal JS error only on iOS
    Crashlytics.recordError(logString);
  } else {
// Record a non-fatal JS error only on Android
    Crashlytics.logException(logString);
  }
};
