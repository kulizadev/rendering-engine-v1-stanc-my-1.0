import { createSelector } from 'reselect';

/**
 * Direct selector to the appDetails state domain
 */
const selectAppDetailsDomain = (state) => state.get('appDetails');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AppDetails
 */

const makeSelectAppDetails = () => createSelector(
  selectAppDetailsDomain,
  (substate) => substate.toJS()
);

// ===========
/*
SELECT USER INFO SAVED IN STORE
format: {
  bpm_token: ,
  django_token: ,
  taskId: ,
  processInstanceId:
}
*/
const makeSelectUserDetails = () => createSelector(
  selectAppDetailsDomain,
  (substate) => substate.get('userDetails')
);

const makeSelectLoaderState = () => createSelector(
  selectAppDetailsDomain,
  (substate) => substate.get('loadingState')
);

const makeSelectPopupData = () => createSelector(
  selectAppDetailsDomain,
  (substate) => substate.get('popupData')
);

const makeSelectShowPopupState = () => createSelector(
  selectAppDetailsDomain,
  (substate) => substate.get('showPopupState')
);

const makeSelectUserLanguage = () => createSelector(
  selectAppDetailsDomain,
  (substate) => substate.get('userLanguage')
);

const makeSelectDashboardData = () => createSelector(
  selectAppDetailsDomain,
  (substate) => substate.get('dashboardData')
);

const makeSelectAppErrorData = () => createSelector(
  selectAppDetailsDomain,
  (substate) => substate.get('showAppError')
);

const makeSelectCurrentScreen = () => createSelector(
  selectAppDetailsDomain,
  (substate) => substate.get('currentScreen')
);

const makeSelectAppConfig = () => createSelector(
  selectAppDetailsDomain,
  (substate) => substate.get('appConfig')
);

export default makeSelectAppDetails;
export {
  selectAppDetailsDomain,
  makeSelectAppDetails,
  makeSelectUserDetails,
  makeSelectLoaderState,
  makeSelectUserLanguage,
  makeSelectPopupData,
  makeSelectShowPopupState,
  makeSelectDashboardData,
  makeSelectAppErrorData,
  makeSelectCurrentScreen,
  makeSelectAppConfig,
};
