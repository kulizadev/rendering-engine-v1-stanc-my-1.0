/*
 *
 * AppDetails actions
 *
 */

import * as C from './constants';

export function defaultAction() {
  return {
    type: C.DEFAULT_ACTION,
  };
}

export function getRequest(data) {
  return {
    type: C.GET_REQUEST,
    data,
  };
}

export function postRequest(data) {
  return {
    type: C.POST_REQUEST,
    data,
  };
}

export function deleteRequest(data) {
  return {
    type: C.DELETE_REQUEST,
    data,
  };
}

export function setUserDetails(data) {
  return {
    type: C.SET_USER_DETAILS,
    data,
  };
}

export function setUserLanguage(data, callback) {
  return {
    type: C.SET_USER_LANGUAGE,
    data,
    callback,
  };
}


export function updateUserLanguage(data) {
  return {
    type: C.UPDATE_USER_LANGUAGE,
    data,
  };
}

export function clearJourneyData() {
  return {
    type: C.CLEAR_JOURNEY_DATA,
  };
}

export function updateLoadingState(data) {
  return {
    type: C.UPDATE_LOADING_STATE,
    data,
  };
}

export function setPopupData(data) {
  return {
    type: C.SET_POPUP_DATA,
    data,
  };
}

export function setShowPopupData(data) {
  return {
    type: C.SET_SHOW_POPUP_STATE,
    data,
  };
}

export function setDashboardData(data) {
  return {
    type: C.SET_DASHBOARD_DATA,
    data,
  };
}

export function setAppError(data) {
  return {
    type: C.SET_APP_ERROR,
    data,
  };
}

export function navigateToSCreen(screen, params) {
  return {
    type: C.NAVIGATE_TO_SCREEN,
    screen,
    params,
  };
}

export function clearCurrentScreen() {
  return {
    type: C.CLEAR_CURRENT_SCREEN,
  };
}

export function setAppConfig(data) {
  return {
    type: C.SET_APP_CONFIG,
    data,
  };
}

export function dumbLog(data) {
  return {
    type: C.DUMP_LOG,
    data,
  };
}

export function logout() {
  return {
    type: C.LOGOUT,
  };
}
