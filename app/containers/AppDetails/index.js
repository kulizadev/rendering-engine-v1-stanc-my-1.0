/**
 *
 * AppDetails
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from '../../utils/injectSaga';
import injectReducer from '../../utils/injectReducer';
import { makeSelectAppDetails,
   makeSelectUserDetails,
    makeSelectLoaderState,
    makeSelectPopupData,
    makeSelectShowPopupState,
    makeSelectUserLanguage,
    makeSelectDashboardData,
    makeSelectAppErrorData,
    makeSelectCurrentScreen,
    makeSelectAppConfig,
   } from './selectors';
import reducer from './reducer';
import saga from './saga';
import * as appActions from './actions';
import * as renderActions from '../RenderJourney/actions';

import AppDetailsContainer from './AppDetailsContainer';

export class AppDetails extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <AppDetailsContainer {...this.props} />
    );
  }
}

AppDetails.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  appDetails: makeSelectAppDetails(),
  dashboardData: makeSelectDashboardData(),
  userDetails: makeSelectUserDetails(),
  userLanguage: makeSelectUserLanguage(),
  loadingState: makeSelectLoaderState(),
  popupData: makeSelectPopupData(),
  showPopupState: makeSelectShowPopupState(),
  showAppError: makeSelectAppErrorData(),
  currentScreen: makeSelectCurrentScreen(),
  appConfig: makeSelectAppConfig(),
});

function mapDispatchToProps(dispatch) {
  return {
    setUserDetails: (data) => dispatch(appActions.setUserDetails(data)),
    setUserLanguage: (data, callback) => dispatch(appActions.setUserLanguage(data, callback)),
    clearJourneyData: () => dispatch(renderActions.clearJourneyData()),
    setAppError: (data) => dispatch(appActions.setAppError(data)),
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    navigateToScreen: (screen, params) => dispatch(appActions.navigateToSCreen(screen, params)),
    clearCurrentScreen: () => dispatch(appActions.clearCurrentScreen()),
    setAppConfig: (data) => dispatch(appActions.setAppConfig(data)),
    logout: () => dispatch(appActions.logout()),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'appDetails', reducer });
const withSaga = injectSaga({ key: 'appDetails', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AppDetails);
