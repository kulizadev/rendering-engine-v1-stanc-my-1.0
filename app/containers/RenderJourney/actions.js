/*
 *
 * RenderJourney actions
 *
 */

import * as C from './constants';

export function defaultAction() {
  return {
    type: C.DEFAULT_ACTION,
  };
}

export function initRenderService(data) {
  return {
    type: C.INIT_RENDER_SERVICE,
    data,
  };
}

// set step data for the current screen
export function setStepData(data) { return { type: C.SET_STEP_DATA, data }; }

// set render data
export function setRenderData(data) {
  return {
    type: C.SET_RENDER_DATA,
    data,
  };
}

// set fields order in a step
export function setFieldOrder(data) {
  return {
    type: C.SET_FIELD_ORDER,
    data,
  };
}

// set current screen name
export function setCurrentScreenName(data) {
  return {
    type: C.SET_SCREEN_NAME,
    data,
  };
}

// set current screen Id
export function setCurrentScreenId(data) {
  return {
    type: C.SET_SCREEN_ID,
    data,
  };
}

// set application Id
export function setApplicationId(data) {
  return {
    type: C.SET_APPLICATION_ID,
    data,
  };
}

// set form data to submit
export function setFormData(data) {
  return {
    type: C.SET_FORM_DATA,
    data,
  };
}

// set isStepValid flag
export function setIsStepValid(data) {
  return {
    type: C.SET_STEP_VALID,
    data,
  };
}

export function setCompletedTasks(data) {
  return {
    type: C.SET_COMPLETED_TASKS,
    data,
  };
}

export function setStepperData(data) {
  return {
    type: C.SET_STEPPER_DATA,
    data,
  };
}

export function clearJourneyData() {
  return {
    type: C.CLEAR_JOURNEY_DATA,
  };
}

// set data refresh flag
export function setRefreshFlag(data) {
  return {
    type: C.SET_REFRESH_FLAG,
    data,
  };
}

export function setSubJourneyId(data) {
  return {
    type: C.SET_SUBJOURNEY_ID,
    data,
  };
}

export function cleanFormData() {
  return {
    type: C.CLEAN_FORM_DATA,
  };
}

export function updateFormElmAndRender(formKey, value) {
  return {
    type: C.UPDATE_FORM_ELM_AND_RENDER,
    formKey,
    value,
  };
}

export function submitForm(successCb) {
  return {
    type: C.SUBMIT_FORM,
    successCb,
  };
}

export function validateAndUpdateRender() {
  return {
    type: C.VALIDATE_FORM_AND_UPDATE_RENDER,
  };
}

export function validateElmAndUpdateRender(value, elmId) {
  return {
    type: C.VALIDATE_ELEMENT_AND_UPDATE_RENDER,
    value,
    elmId,
  };
}

export function initializeAndUpdateRender(response) {
  return {
    type: C.INITIALIZE_AND_UPDATE_RENDER,
    response,
  };
}

export function checkHiddenAndRender() {
  return {
    type: C.CHECK_HIDDEN_AND_RENDER,
  };
}

export function initiateJourney(successCb, options) {
  return {
    type: C.INITIATE_JOURNEY,
    successCb,
    options,
  };
}

export function gotoBackTask(screenKey, successCb) {
  return {
    type: C.GOTO_BACK_TASK,
    screenKey,
    successCb,
  };
}
