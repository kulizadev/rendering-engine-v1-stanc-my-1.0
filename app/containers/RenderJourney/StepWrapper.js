import appVars from "../../configs/styleVars";
import styled from "styled-components";
const colors = appVars.colors;
sizes = appVars.sizes;

export const StepWrapper = styled.div`
  padding: 20px;
  background-color: ${colors.stepWrapperBGColor};
  border-radius: 5px;
  margin: 0 auto 20px;
`;

export const Hositioning = styled.div`
  background: ${colors.blueSideNavBG};
  @media (max-width: 600px) {
    height: 330px !important;
  }
`;
