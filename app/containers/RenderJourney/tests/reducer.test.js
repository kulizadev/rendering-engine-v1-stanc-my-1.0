
import { fromJS } from 'immutable';
import renderJourneyReducer from '../reducer';

describe('renderJourneyReducer', () => {
  it('returns the initial state', () => {
    expect(renderJourneyReducer(undefined, {})).toEqual(fromJS({}));
  });
});
