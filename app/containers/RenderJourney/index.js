/**
 *
 * RenderJourney
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from '../../utils/injectSaga';
import injectReducer from '../../utils/injectReducer';
import makeSelectRenderJourney from './selectors';
import { makeSelectUserDetails } from '../AppDetails/selectors';
import reducer from './reducer';
import saga from './saga';
import * as renderActions from './actions';
import * as appActions from '../AppDetails/actions';

import RenderJourneyContainer from './RenderJourneyContainer';

export class RenderJourney extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderJourneyContainer {...this.props} />
    );
  }
}

const mapStateToProps = createStructuredSelector({
  renderjourney: makeSelectRenderJourney(),
  userDetails: makeSelectUserDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    setUserDetails: (data) => dispatch(appActions.setUserDetails(data)),
    setPopupData: (data) => dispatch(appActions.setPopupData(data)),
    initRenderService: (renderObj) => dispatch(renderActions.initRenderService(renderObj)),
    initializeAndUpdateRender: (response) => dispatch(renderActions.initializeAndUpdateRender(response)),
    setRenderData: (renderData) => dispatch(renderActions.setRenderData(renderData)),
    updateFormData: (formData) => dispatch(renderActions.setFormData(formData)),
    updateFormElmAndRender: (formKey, value) => dispatch(renderActions.updateFormElmAndRender(formKey, value)),
    cleanFormData: () => dispatch(renderActions.cleanFormData()),
    validateAndUpdateRender: () => dispatch(renderActions.validateAndUpdateRender()),
    validateElmAndUpdateRender: (value, elmId) => dispatch(renderActions.validateElmAndUpdateRender(value, elmId)),
    submitForm: (cb) => dispatch(renderActions.submitForm(cb)),
    initiateJourney: (cb, options) => dispatch(renderActions.initiateJourney(cb, options)),
    gotoBackTask: (screenKey, cb) => dispatch(renderActions.gotoBackTask(screenKey, cb)),
    setIsStepValid: (isValid) => dispatch(renderActions.setIsStepValid(isValid)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'renderJourney', reducer });
const withSaga = injectSaga({ key: 'renderJourney', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(RenderJourney);
