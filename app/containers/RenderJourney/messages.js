/*
 * RenderJourney Messages
 *
 * This contains all the text for the RenderJourney component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.RenderJourney.header',
    defaultMessage: 'This is RenderJourney container !',
  },
});
