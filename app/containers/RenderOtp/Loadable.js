/**
 *
 * Asynchronously loads the component for RenderOtp
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
