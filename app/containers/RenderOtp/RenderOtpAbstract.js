/**
 *
 * RenderOtp
 *
 */

import React from 'react';

import * as validationFns from '../../utilities/validations/validationFns';
// import { Crashlytics } from 'react-native-fabric';

export default class RenderOtpAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.OTP_VALID_TIME = 60;
    this.state = {
      otp: '',
      isOtpValid: '',
      invalidOtpError: '',
      resendEnabled: true,
      resendCounter: 0,
      intervalCounter: 0,
      deviceId: '',
    };
    this.tracker = null;
  }

  componentDidMount = () => {
    this.setDeviceId();
    this.setIntervalTracker();
    // Crashlytics.crash();
    // Crashlytics.recordError();
  }

  componentWillUnmount() {
    this.tracker && window.clearTimeout(this.tracker);
    this.clearIntervalTracker();
  }
  setDeviceId = () => {

  };


  setIntervalTracker = () => {
    this.setState({ intervalCounter: this.OTP_VALID_TIME });
    this.intervalTracker = setInterval(() => {
      this.setState(({ intervalCounter }) => {
        if (intervalCounter) {
          return { intervalCounter: intervalCounter - 1 };
        }
        this.clearIntervalTracker();
        return { intervalCounter: 0 };
      });
    }, 1000);
  }
  clearIntervalTracker = () => {
    clearInterval(this.intervalTracker);
  }

  checkOtpValidation = (value) => {
    const isValidObj = validationFns.getValidationObj('otp', value);
    this.setState({ isOtpValid: isValidObj.isValid });
    this.setState({ invalidOtpError: isValidObj.errorMessage });
    return isValidObj.isValid;
  }

  handleOtpChange = (e) => {
    this.setState({ otp: e.target.value });
    const value = e.target.value;
    // Check the mobile validation and set the state
    window.setTimeout(() => this.checkOtpValidation(value));
  }

  submitOtp = (otp, deviceType) => {
    const otpReq = {
      params: {
        mobile: this.props.userDetails.mobileNumber,
        otp,
      },
      successCb: (response) => {
        const resObj = response.data;
        // Setup the basic User/Token/Session Details
        // Call the saga service
        this.props.setUserDetails({
          ...this.props.userDetails,
          bpm_token: resObj.access_token,
          refresh_token: resObj.refresh_token,
        });
        if (this.navigateToJourney) {
          this.navigateToJourney();
        }
      },
      errorCb: () => {
        this.checkOtpValidation();
      },
    };
    this.props.postRequest({ key: 'validateOtp', data: otpReq });
  }

  disableResend = () => {
    this.setState({ resendEnabled: false, resendCounter: this.state.resendCounter + 1 });
    this.tracker = window.setTimeout(() => {
      window.clearInterval(this.intervalTracker);
      (this.state.resendCounter < 3) && this.setState({ resendEnabled: true });
    }, this.OTP_VALID_TIME * 1000);
  }

  resendOtp = () => {
    // Issue a request if login details are valid
    if (this.state.resendEnabled) {
      this.disableResend();
      this.setIntervalTracker();
      const otpReq = {
        params: { mobile: this.props.userDetails.mobileNumber },
        successCb: (response) => {
          console.log('Resend OTP resp -->', response);
        },
      };

      this.props.postRequest({ key: 'generateOtp', data: otpReq });
    }
  }


  render() {
    return null;
  }
}
