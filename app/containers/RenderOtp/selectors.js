import { createSelector } from 'reselect';

/**
 * Direct selector to the renderOtp state domain
 */
const selectRenderOtpDomain = (state) => state.get('renderOtp');

/**
 * Other specific selectors
 */


/**
 * Default selector used by RenderOtp
 */

const makeSelectRenderOtp = () => createSelector(
  selectRenderOtpDomain,
  (substate) => substate.toJS()
);

export default makeSelectRenderOtp;
export {
  selectRenderOtpDomain,
};
