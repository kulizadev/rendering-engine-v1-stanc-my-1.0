/*
 * RenderOtp Messages
 *
 * This contains all the text for the RenderOtp component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.RenderOtp.header',
    defaultMessage: 'This is RenderOtp container !',
  },
});
