/**
 *
 * RenderMyLoan
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import { View, Text, TouchableOpacity, Dimensions, ScrollView, Platform } from 'react-native';
import { TabViewAnimated, TabBar, SceneMap, TabViewPagerScroll, TabViewPagerPan } from 'react-native-tab-view';
import RenderStaticComponents, { LoanCard, ModalOverlay, PostPayModal } from '../../components/RenderStaticComponents';
import RenderButton from '../../components/RenderButton';
import Accordion from '../../components/Accordion';
import RenderDetails from './LoanDetailsModal';
import { colors } from '../../configs/styleVars';
import * as styles from './style.native';
import RenderHeader from './MyLoanHeader';
import { statusMap, statusKey } from '../RenderPostJounery/renderLoanService';
import BackgroundImage from '../../components/RenderStaticComponents/BackgroundImage';
import backgroundMyloan from '../../images/background-myloan.png';
import { strings } from '../../../locales/i18n';
import LoanView from './LoanView';

const initialLayout = {
  height: 100,
  width: Dimensions.get('window').width,
};

export default class RenderMyLoan extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      index: props.navigation ? props.navigation.getParam('defaultTab', 0) : 0,
      routes: [
        { key: 'myLoans', title: strings('loanDetails.myLoansTitle') },
        { key: 'myApps', title: strings('loanDetails.myAppsTitle') },
      ],
      isLoanDetailVisible: false,
      showPayModal: false,
      recentAppData: {},
      applicationData: [],
      loansData: [],
      selectedApp: {},
    };
  }

  componentDidMount() {
    const reqObj = {
      key: 'getApplicationData',
      data: {
        successCb: (response) => {
          const { loanDetails: recentAppData, MyLoans: loansData, MyApplication: applicationData } = response.data;
          this.setState({
            recentAppData,
            applicationData,
            loansData,
          });
        },
        errorCb: () => {
          console.debug('getApplicationData is failed');
        },
        userDetails: this.props.screenProps.userDetails,
      },
    };
    this.props.screenProps.getRequest(reqObj);
  }

  componentWillReceiveProps(newProps) {
    const paymentState = newProps.navigation.getParam('paymentDone', false);
    if (paymentState === true && this.state.isLoanDetailVisible) {
      this.setState({ isLoanDetailVisible: false, showPayModal: false });
    }
  }

  handleIndexChange = (index) => this.setState({ index });

  handleLoanDetail = (flag) => {
    this.setState({ isLoanDetailVisible: flag });
  }

  showLoanDetails = (data, formattedDetail) => {
    this.setState({ selectedApp: { ...data, headerData: formattedDetail }, isLoanDetailVisible: true });
  }

  handlePayModal = (flag) => {
    this.setState({ showPayModal: flag });
  }

  renderTabPager = (props) => (Platform.OS === 'ios') ? <TabViewPagerScroll {...props} /> : <TabViewPagerPan {...props} />;

  renderTabHeader = (props) => (<TabBar
    {...props}
    style={styles.tabHeaderStyle}
    indicatorStyle={styles.indicatorStyle}
    tabStyle={{ flex: 0 }}
    renderLabel={(current) => {
      const style = current.focused ? styles.tabHeaderLabelFocused : styles.tabHeaderLabel;
      return (<View style={current.focused ? styles.focusedLabelStyle : {}}>
        <Text style={style}>{current.route.title}</Text>
      </View>);
    }}
    renderIndicator={() => null}
  />);

  renderTabScene = ({ route }) => {
    const { loansData, applicationData } = this.state;
    switch (route.key) {
      case 'myLoans':
        return <LoanView data={loansData} showLoanDetails={this.showLoanDetails} type={'loan'} />;
      case 'myApps':
        return <LoanView data={applicationData} type={'application'} />;
      default:
        return null;
    }
  }

  render() {
    const { recentAppData, selectedApp } = this.state;
    console.log('recentAppData: ', recentAppData);
    return (
      <View>
        {this.props.screenProps.header}
        <ScrollView contentContainerStyle={styles.wrapperStyle}>
          <BackgroundImage
            source={backgroundMyloan}
            imageContainerStyle={{ paddingBottom: 150 }}
          >
            <View style={styles.topSectionStyle} >
              <View>
                <RenderHeader data={recentAppData} />
                <View style={{ marginTop: 30 }}>
                  <Text style={styles.currentLoanTitle}>{strings('loanDetails.newLoan')}</Text>
                  <LoanCard
                    data={recentAppData}
                    showReminder={recentAppData[statusKey] === statusMap.DISBURSED}
                    onPress={this.showLoanDetails}
                  />
                </View>
              </View>
            </View>
          </BackgroundImage>
          <TabViewAnimated
            navigationState={this.state}
            renderScene={this.renderTabScene}
            renderHeader={this.renderTabHeader}
            renderPager={this.renderTabPager}
            onIndexChange={this.handleIndexChange}
            initialLayout={initialLayout}
          />
          <ModalOverlay
            onBack={() => this.handleLoanDetail(false)}
            isVisible={this.state.isLoanDetailVisible}
          >
            <RenderDetails
              data={selectedApp}
              getRequest={this.props.screenProps.getRequest}
              userDetails={this.props.screenProps.userDetails}
              handlePayModal={this.handlePayModal}
            />
            <ModalOverlay
              onBack={() => this.handlePayModal(false)}
              isVisible={this.state.showPayModal}
              headerType={'white'}
            >
              <PostPayModal data={selectedApp} />
            </ModalOverlay>
          </ModalOverlay>
        </ScrollView>
      </View>
    );
  }
}

RenderMyLoan.propTypes = {
//   dispatch: PropTypes.func.isRequired,
};


// function mapDispatchToProps(dispatch) {
//   return {
//     dispatch,
//   };
// }

// const withConnect = connect(null, mapDispatchToProps);

// export default compose(
//   withConnect,
// )(RenderMyLoan);
