import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import * as styles from './style';
import { statusMap, statusKey } from '../RenderPostJounery/renderLoanService';
import { strings } from '../../../locales/i18n';

const getHeaderData = (data) => {
  let heading = null;
  let bodyText = null;
  let linkText = null;
  switch (data[statusKey]) {
    case statusMap.REJECTED:
    case statusMap.CANCELLED:
    case statusMap.EXPIRED:
    case statusMap.NEW:
      heading = (<Text style={styles.topHeaderStyle}>{strings('loanDetails.mainHeadExpire')}</Text>);
      bodyText = strings('loanDetails.bodyExpired');
      linkText = strings('loanDetails.callService');
      break;
    case statusMap.DISBURSED:
      heading = (<Text style={styles.topHeaderStyle}>{strings('loanDetails.mainHeadPayment')}</Text>);
      bodyText = strings('loanDetails.bodyPayment', { paymentDate: data.nextRepaymentDate });
      linkText = strings('loanDetails.payNow');
      break;
    case statusMap.CLOSED:
      heading = (
        <View>
          <Text style={styles.topHeaderStyle}>{strings('loanDetails.mainHeadApprove')}</Text>
          <Text style={styles.topHeaderAmt}>{data.securityCode}</Text>
        </View>
      );
      bodyText = '';
      linkText = '';
      break;
    case statusMap.APPROVED:
      heading = (
        <View>
          <Text style={styles.topHeaderStyle}>{strings('loanDetails.mainHeadApprove')}</Text>
          <Text style={styles.topHeaderAmt}>{data.securityCode}</Text>
        </View>
      );
      bodyText = strings('loanDetails.bodyApprove');
      linkText = strings('loanDetailModal.nearPO');
      break;
    default:
      heading = (<View></View>);
      bodyText = '';
      linkText = '';
  }
  return {
    heading,
    bodyText,
    linkText,
  };
};

const RenderHeader = (props) => {
  const { data } = props;
  const { heading, bodyText, linkText } = getHeaderData(data);
  return (
    <View>
      <View style={styles.topInfoStyle}>
        {heading}
      </View>
      <View style={styles.headerBottomWrap}>
        <Text style={styles.headerBottomLabel}>
          {bodyText}
        </Text>
        <TouchableOpacity onPress={() => {}}>
          <Text style={styles.forwardStyle}>
            {linkText} ›
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default RenderHeader;
