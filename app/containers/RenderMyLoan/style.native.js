import { colors, fonts, lineHeight, sizes } from '../../configs/styleVars';

export const wrapperStyle = {
  backgroundColor: colors.secondaryBGColor,
};

export const topSectionStyle = {
  paddingHorizontal: 20,
  paddingBottom: 50,
  paddingTop: 50,
};

export const topInfoStyle = {
  flexDirection: 'row',
  paddingTop: 20,
  paddingBottom: 10,
};

export const topHeaderStyle = {
  ...fonts.getFontFamilyWeight('bold'),
  fontSize: fonts.h2,
  lineHeight: lineHeight.h2,
  color: colors.white,
};

export const topHeaderAmt = {
  fontSize: fonts.h2,
  lineHeight: lineHeight.h2,
  color: colors.amountHeading,
  ...fonts.getFontFamilyWeight('bold'),
};

export const tabHeaderStyle = {
  backgroundColor: colors.secondaryBGColor,
  flex: 0,
  shadowRadius: 0,
  elevation: 0,
  paddingHorizontal: sizes.appPaddingHorizontal,
  shadowOffset: {
    height: 0,
  },
};

export const tabHeaderLabel = {
  fontSize: fonts.description,
  lineHeight: lineHeight.description,
  color: colors.tabHeaderColor,
  ...fonts.getFontFamilyWeight('bold'),
  paddingBottom: 7,
};

export const tabHeaderLabelFocused = { fontSize: fonts.description,
  lineHeight: lineHeight.description,
  color: colors.black,
  ...fonts.getFontFamilyWeight('bold'),
  paddingBottom: 7,
};

export const loanCardWrapper = {
  flex: 1,
  margin: 20,
};

export const indicatorStyle = {
  backgroundColor: colors.primaryBGColor,
};

export const focusedLabelStyle = {
  borderWidth: 0,
  borderBottomWidth: 3,
  borderBottomColor: colors.primaryBGColor,
};

export const forwardStyle = {
  color: colors.white,
  paddingTop: 10,
  ...fonts.getFontFamilyWeight('bold'),
};

export const detailsForward = {
  ...forwardStyle,
  color: colors.primaryBGColor,
};

export const currentLoanTitle = {
  color: colors.white,
  fontSize: fonts.h3,
  lineHeight: lineHeight.h3,
  ...fonts.getFontFamilyWeight('bold'),
  marginBottom: 15,
};

export const topWrapperStyle = {
  backgroundColor: colors.primaryBGColor,
  paddingHorizontal: 20,
  paddingBottom: 20,
};

export const modalHeader = {
  ...fonts.getFontFamilyWeight('bold'),
  fontSize: fonts.h3,
  lineHeight: lineHeight.h3,
  color: colors.white,
};

export const infoCardWrapper = {
  flexDirection: 'row',
  justifyContent: 'space-around',
  marginVertical: 15,
};

export const infoCardStyle = {
  backgroundColor: 'rgba(255,255,255,0.14)',
  padding: 15,
  justifyContent: 'center',
};

export const infoHeadStyle = {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  marginVertical: 15,
};

export const infoLabelStyle = {
  ...fonts.getFontFamilyWeight(),
  fontSize: fonts.label,
  lineHeight: lineHeight.label,
  color: colors.white,
};

export const infoDescLabel = {
  fontSize: fonts.body,
  lineHeight: lineHeight.body,
  color: colors.white,
  ...fonts.getFontFamilyWeight('bold'),
  paddingTop: 10,
};

export const modalTopWrap = {
  backgroundColor: colors.white,
  paddingHorizontal: 20,
  paddingBottom: 30,
  flex: 1,
};

export const toPayWrap = {
  backgroundColor: '#f2f2f2',
  marginVertical: 15,
  padding: 30,
  alignItems: 'center',
  borderRadius: 6,
};

export const payInfoLabel = {
  fontSize: fonts.body,
  lineHeight: lineHeight.body,
  color: colors.labelColor,
  ...fonts.getFontFamilyWeight(),
};

export const payAmount = {
  fontSize: fonts.h1,
  lineHeight: lineHeight.h1,
  color: colors.fillColor,
  ...fonts.getFontFamilyWeight('bold'),
  paddingVertical: 10,
};

export const payBottomLabel = {
  fontSize: fonts.body,
  lineHeight: lineHeight.body,
  color: colors.black,
  ...fonts.getFontFamilyWeight('bold'),
};

export const bottomPayWrap = {
  flexDirection: 'row',
  justifyContent: 'space-between',
  marginVertical: 5,
};

export const bottomPayLabel = {
  fontSize: fonts.body,
  lineHeight: lineHeight.body,
  color: colors.labelColor,
  ...fonts.getFontFamilyWeight(),
};

export const bottomPayDesc = {
  fontSize: fonts.body,
  lineHeight: lineHeight.body,
  color: colors.black,
  ...fonts.getFontFamilyWeight('bold'),
};

export const popUpWrap = {
  flexDirection: 'row',
  justifyContent: 'flex-start',
  alignItems: 'center',
  marginVertical: 5,
};

export const popUpLabel = {
  paddingRight: 5,
};

export const listLabel = {
  ...fonts.getFontFamilyWeight('bold'),
};

export const gotoButton = {
  fontSize: fonts.body,
  lineHeight: lineHeight.body,
  color: colors.primaryBGColor,
  ...fonts.getFontFamilyWeight('bold'),
  marginVertical: 10,
};

export const ctaButton = {
  marginVertical: 20,
};

export const headerBottomWrap = {
  marginVertical: 15,
};

export const headerBottomLabel = {
  color: colors.white,
};

export const accordionStyles = {
  marginLeft: 0,
};

export const detailDesc = {
  color: colors.labelColor,
};

export const scrollContentWrapper = {
  backgroundColor: colors.white,
};

export const tabWrapper = {
  paddingHorizontal: sizes.appPaddingHorizontal,
};

export const downloadStyles = {
  paddingVertical: 5,
};

export const listStyles = {
  borderBottomWidth: 1,
  borderBottomColor: colors.underlineColor,
  paddingBottom: 15,
};
