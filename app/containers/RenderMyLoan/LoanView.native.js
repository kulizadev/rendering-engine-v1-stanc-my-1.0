import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import * as styles from './style';
import { getStatusMap, statusKey } from '../RenderPostJounery/renderLoanService.native';
import { strings } from '../../../locales/i18n';
import { LoanCard, FilterList } from '../../components/RenderStaticComponents';
import FilterRequest from '../RenderPostJounery/Support/FilterRequest.native';
import { unformatDateStr } from '../../utilities/commonUtils';


class LoanView extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      selectedFilters: {
        status: '',
        dateRange: {},
      },
      showFilters: false,
    };
  }

  checkLoanDate = (loanDate, selectedDateRange) => {
    const fromDate = selectedDateRange.from && new Date(unformatDateStr(selectedDateRange.from));
    const toDate = selectedDateRange.to && new Date(unformatDateStr(selectedDateRange.to));
    const startRange = new Date(loanDate) > fromDate;
    const endRange = toDate > new Date(loanDate);
    return startRange && endRange;
  }

  filterData(data) {
    let dataDummy = [...data];
    const { selectedFilters } = this.state;
    const { status, dateRange } = selectedFilters;
    if (status) {
      dataDummy = dataDummy.filter((loan) => loan[statusKey] === status.value);
    }
    if (dateRange.to || dateRange.from) {
      dataDummy = dataDummy.filter((loan) => this.checkLoanDate(loan.applicationDate, dateRange));
    }
    return dataDummy;
  }

  toggleFilters = (value) => {
    this.setState({ showFilters: value });
  }

  applyFilter = (data) => {
    this.setState({ selectedFilters: data });
    this.toggleFilters(false);
  }

  render() {
    const { data, showLoanDetails, type } = this.props;
    const statusOptions = {
      label: strings('support.filterStatus'),
      options: getStatusMap(type),
    };
    const { showFilters, selectedFilters } = this.state;
    const filteredData = this.filterData(data);
    let cardList = null;
    if (!filteredData.length) {
      cardList = (<Text>{strings('loanDetails.noData')}</Text>);
    } else {
      cardList = filteredData.map((loan) => (
        <LoanCard
          key={loan.loanContractNumber}
          data={loan}
          onPress={(loanData, formattedDetail) => showLoanDetails && showLoanDetails(loanData, formattedDetail)}
        />
      ));
    }
    return (
      <View style={styles.tabWrapper}>
        <FilterList
          list={selectedFilters}
          applyFilter={this.applyFilter}
          showFilters={() => this.toggleFilters(true)}
        />
        {cardList}
        <FilterRequest
          statusOptions={statusOptions}
          hideFilters={() => this.toggleFilters(false)}
          selectedFilters={selectedFilters}
          applyFilter={this.applyFilter}
          showFilters={showFilters}
        />
      </View>
    );
  }

}

export default LoanView;
