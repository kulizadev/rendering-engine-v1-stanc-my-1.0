import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import { View, Text, TouchableOpacity, Dimensions, ScrollView, Platform } from 'react-native';
import RenderStaticComponents, { LoanCard, ModalOverlay, PostPayModal, CreateList } from '../../components/RenderStaticComponents';
import RenderButton from '../../components/RenderButton';
import Accordion from '../../components/Accordion';
import { colors } from '../../configs/styleVars';
import * as styles from './style.native';
import { statusMap, statusKey } from '../RenderPostJounery/renderLoanService';
import * as common from '../../utilities/commonUtils';
import RenderInfo from '../../components/RenderInfo';
import { strings } from '../../../locales/i18n';
import RenderDownload from '../../components/RenderDownload';
import { download } from '../../utilities/native/downloadService.native';

const contractDetails = (data) => [
  {
    name: strings('loanDetailModal.contractNum'),
    value: data.loanContractNumber,
  },
  {
    name: strings('loanDetailModal.disbursementDate'),
    value: data.disbursementDate,
  },
  {
    name: strings('loanDetailModal.disbursementMethod'),
    value: data.disbursementMethod,
  },
  {
    name: strings('loanDetailModal.loanAmount'),
    value: data.loanAmount ? common.convertCurrencyFormat(data.loanAmount) : '-',
  },
  {
    name: strings('loanDetailModal.loanTenure'),
    value: `${data.tenure} ${strings('loanDetailModal.month')}`,
  },
  {
    name: strings('loanDetailModal.monthlyInstallment'),
    value: data.emi ? common.convertCurrencyFormat(data.emi) : '-',
  },
];

const billingInfo = [
  // {
  //   date: '07/01/18',
  //   label: 'VPBank Visa 1117',
  //   amount: '1.000.000',
  // },
  // {
  //   date: '07/01/18',
  //   label: 'VPBank Visa 1117',
  //   amount: '1.000.000',
  // },
  // {
  //   date: '07/01/18',
  //   label: 'VPBank Visa 1117',
  //   amount: '1.000.000',
  // },
];

const RenderHeader = ({ details }) => (
  <View style={styles.infoCardWrapper}>
    <View style={styles.infoCardStyle}>
      <Text style={styles.infoLabelStyle}>{details && details[0].label}</Text>
      <Text style={styles.infoDescLabel}>{details && details[0].value}</Text>
    </View>
    <View style={styles.infoCardStyle}>
      <Text style={styles.infoLabelStyle}>{details && details[1].label}</Text>
      <Text style={styles.infoDescLabel}>{ details && details[1].value}</Text>
    </View>
  </View>
);

const RenderProgressBar = ({ type, tenure, remainingTenure }) => {
  let progress = 0;
  let progressText = '';
  if (type === statusMap.DISBURSED) {
    progress = remainingTenure !== '' ? ((tenure - remainingTenure) * 100) / tenure : 0;
    const completedTenure = remainingTenure !== '' ? (tenure - remainingTenure) : 0;
    progressText = `${completedTenure} / ${tenure} ${strings('loanDetailModal.month')}`;
  } else if (type === statusMap.CLOSED) {
    progress = 100;
    progressText = strings('loanDetailModal.completed');
  } else if (type === statusMap.APPROVED) {
    progress = 0;
    progressText = `0 / ${tenure} ${strings('loanDetailModal.month')}`;
  }
  if (type !== statusMap.DISBURSED && type !== statusMap.CLOSED && type !== statusMap.APPROVED) {
    return null;
  }
  return (
    <View>
      <View style={styles.infoHeadStyle}>
        <Text style={styles.infoLabelStyle}>{strings('loanDetailModal.progress')}</Text>
        <Text style={styles.infoDescLabel}>{progressText}</Text>
      </View>
      <RenderStaticComponents.ProgressBar
        filledTrackColor={colors.white}
        unfilledTrackColor={colors.progressUnfilled}
        filled={progress}
      />
    </View>
  );
};

const PaymentBlock = ({ emi, totalOverdueLoanAmount, nextRepaymentDate, earlyTerminationAmount, totalPaymentAmount, handlePayModal }) => {
  const upcomingPayment = common.convertCurrencyFormat(totalPaymentAmount);
  emi = common.convertCurrencyFormat(emi);
  const formatTerminationAmount = earlyTerminationAmount !== 'null' && earlyTerminationAmount ? common.convertCurrencyFormat(earlyTerminationAmount) : '-';
  totalOverdueLoanAmount = common.convertCurrencyFormat(totalOverdueLoanAmount);
  return (
    <View>
      <View style={styles.toPayWrap}>
        <Text style={styles.payInfoLabel}>{strings('loanDetailModal.UpcomingPayment')}</Text>
        <Text style={styles.payAmount}>{upcomingPayment}</Text>
        <Text style={styles.payBottomLabel}>{nextRepaymentDate}</Text>
      </View>
      <View>
        <View style={styles.bottomPayWrap}>
          <Text style={styles.bottomPayLabel}>{strings('loanDetailModal.monthlyInstallment')}</Text>
          <Text style={styles.bottomPayDesc}>{emi}</Text>
        </View>
        <View style={styles.bottomPayWrap}>
          <Text style={styles.bottomPayLabel}>{strings('loanDetailModal.overdueAmount')}</Text>
          <Text style={styles.bottomPayDesc}>{totalOverdueLoanAmount}</Text>
        </View>
        <View style={styles.popUpWrap}>
          <Text style={styles.popUpLabel}>{strings('loanDetailModal.earlyTermination')}</Text>
          <RenderInfo
            headerText={strings('loanDetailModal.terminationPopupHead')}
            bodyText={strings('loanDetailModal.terminationPoupBody', { amount: formatTerminationAmount })}
            btnText={strings('loanDetailModal.terminationPopupBtn')}
            infoStyles={{ marginLeft: 5 }}
          />
        </View>
        <View style={styles.ctaButton}>
          <RenderButton
            text={strings('loanDetailModal.payNow')}
            type="primary"
            // onPress={() => handlePayModal(true)}
          />
        </View>
      </View>
    </View>
  );
};

const LoanSecureBlock = ({ secureCode }) => (
  <View>
    <View style={styles.toPayWrap}>
      <Text style={styles.payInfoLabel}>{strings('loanDetails.mainHeadApprove')}</Text>
      <Text style={styles.payAmount}>{secureCode}</Text>
    </View>
    <View>
      <View style={styles.headerBottomWrap}>
        <Text style={styles.detailDesc}>
          {strings('loanDetails.bodyApprove')}
        </Text>
        <TouchableOpacity onPress={() => {}}>
          <Text style={styles.detailsForward}>
            {strings('loanDetailModal.nearPO')} ›
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  </View>
);


export default class RenderDetails extends React.PureComponent {

  getRepaymentLink = (loanId) => {
    const { userDetails } = this.props;
    const reqObj = {
      key: 'getRepaymentLink',
      data: {
        params: { loanContractId: loanId },
        successCb: (response) => {
          download(response.data.repaymentLink, response.data.docType);
        },
        errorCb: () => {
          console.debug('getRepaymentLink is failed');
        },
        userDetails,
      },
    };
    this.props.getRequest(reqObj);
  }

  getPaymentInfoMap = (data) => data.map((item) => ({
    ...item,
    payment_status: strings(`loanDetailModal.payment${item.payment_status}`),
  }
  ));

  render() {
    const { data, handlePayModal } = this.props;

    const paymentDetails = [{
      label: 'Date',
      value: 'payment_date',
    }, {
      label: 'Mode',
      value: 'payment_mode',
    }, {
      label: 'Amount',
      value: 'payment_amount',
      isCurrency: true,
      isHeading: true,
    }];

    const repaymentDetails = [{
      label: 'Date',
      value: 'payment_date',
    }, {
      label: 'Amount',
      value: 'payment_amount',
      isCurrency: true,
    }, {
      label: 'Status',
      value: 'payment_status',
      isHeading: true,
    }];
    const paymentHistory = Array.isArray(data.paymentHistory) ? data.paymentHistory : [];
    const repaymentInfo = Array.isArray(data.repaymentInfo) ? this.getPaymentInfoMap(data.repaymentInfo) : [];
    return (
      <ScrollView style={styles.scrollContentWrapper}>
        <View style={styles.topWrapperStyle}>
          <View>
            <Text style={styles.modalHeader}>{strings('loanDetailModal.loanDetails')}</Text>
          </View>
          <RenderHeader details={data.headerData} />
          <RenderProgressBar
            type={data[statusKey]}
            tenure={data.tenure}
            remainingTenure={data.remainingTenure}
          />
        </View>
        <View style={styles.modalTopWrap}>
          { data[statusKey] === statusMap.DISBURSED
            ? <PaymentBlock
              emi={data.emi}
              totalOverdueLoanAmount={data.totalOverdueLoanAmount}
              nextRepaymentDate={data.nextRepaymentDate}
              earlyTerminationAmount={data.earlyTerminationAmount}
              totalPaymentAmount={data.totalPaymentAmount}
              handlePayModal={handlePayModal}
            />
            : null
          }
          { data[statusKey] === statusMap.APPROVED
            ? <LoanSecureBlock
              secureCode={data.securityCode}
            />
            : null
          }
          <View>
            <Accordion label={strings('loanDetailModal.contractDetails')} collapsedViewStyles={styles.accordionStyles}>
              <View style={styles.listStyles}>
                {
                  contractDetails(data).map((detail, index) => (
                    <View key={index} style={styles.bottomPayWrap}>
                      <Text>{detail.name}</Text>
                      <Text>{detail.value}</Text>
                    </View>
                  )
                  )}
                <RenderDownload style={styles.downloadStyles} inline compProps={{ label: strings('loanDetailModal.loanAgreement'), url: data.loanContractUrl, docType: 'pdf' }} />
                <RenderDownload style={styles.downloadStyles} inline compProps={{ label: strings('loanDetailModal.repaymentSchedule') }} onPress={() => this.getRepaymentLink(data.loanContractNumber)} />
              </View>
            </Accordion>
            <Accordion label={strings('loanDetailModal.paymentInfo')} collapsedViewStyles={styles.accordionStyles}>
              <View style={styles.listStyles}>
                <CreateList header={paymentDetails} data={paymentHistory} />
              </View>
            </Accordion>
            <Accordion label={strings('loanDetailModal.repaymentStatus')} collapsedViewStyles={styles.accordionStyles}>
              <View style={styles.listStyles}>
                <CreateList header={repaymentDetails} data={repaymentInfo} />
              </View>
            </Accordion>
          </View>
        </View>
      </ScrollView>
    );
  }
}
