
import { fromJS } from 'immutable';
import renderMpinReducer from '../reducer';

describe('renderMpinReducer', () => {
  it('returns the initial state', () => {
    expect(renderMpinReducer(undefined, {})).toEqual(fromJS({}));
  });
});
