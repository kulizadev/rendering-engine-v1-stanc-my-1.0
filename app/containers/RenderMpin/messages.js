/*
 * RenderMpin Messages
 *
 * This contains all the text for the RenderMpin component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.RenderMpin.header',
    defaultMessage: 'This is RenderMpin container !',
  },
});
