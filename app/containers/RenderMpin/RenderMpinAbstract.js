/**
 *
 * RenderMpin
 *
 */

import React from 'react';

import * as validationFns from '../../utilities/validations/validationFns';
import { strings } from '../../../locales/i18n';

import { sha256, sha224 } from 'js-sha256';
import { setClientKey } from '../../configs/appConstants';

export default class RenderMpinAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      mpin: '',
      confirmMpin: '',
      isMpinValid: '',
      isConfirmMpinValid: '',
      invalidMpinError: '',
      invalidConfirmMpinError: '',
      isTouchIDSupported: false,
      enableTouchID: false,
      showPassword: false,
    };
  }

  getPopUpdata = () => {
    const self = this;
    return {
      headerText: strings('login.touchIdPopupHead'),
      bodyText: strings('login.touchIdPopupBody'),
      actionButtons: [{
        label: strings('login.touchIdPopupSkip'),
        callback: () => self.shouldEnableTouchID(false),
        action: 'callback',
        type: 'secondary',
      },
      {
        label: strings('login.touchIdPopupUse'),
        callback: () => self.shouldEnableTouchID(true),
        action: 'callback',
        type: 'primary',
      }],
      type: 'touch',
      showPopup: true,
    };
  }

  checkMpinValidation = (value) => {
    const isValidObj = validationFns.getValidationObj('mpin', value);
    this.setState({ isMpinValid: isValidObj.isValid });
    this.setState({ invalidMpinError: isValidObj.errorMessage });
  }

  checkConfirmMpinValidation = (confirmMpin) => {
    const { mpin, isMpinValid } = this.state;
    let isConfirmMpinValid = false;
    let invalidConfirmMpinError = strings('errors.confirmMpin');
    if (isMpinValid) {
      if (confirmMpin === mpin) {
        isConfirmMpinValid = true;
        invalidConfirmMpinError = '';
      }
    } else {
      this.checkMpinValidation(this.state.mpin);
    }
    this.setState({ isConfirmMpinValid, invalidConfirmMpinError });
    return isConfirmMpinValid;
  }

  handleMpinChange = (mpin) => {
    this.setState({ mpin });
    this.checkMpinValidation(mpin);
  }

  handleConfirmMpinChange = (confirmMpin) => {
    this.setState({ confirmMpin });
    this.checkConfirmMpinValidation(confirmMpin);
  }

  setMpinToStorage = () => {
    this.props.setUserDetails({
      ...this.props.userDetails,
      mpin: this.state.mpin,
      mpinConfirmed: 'true',
      mpin_setup: 'false',
    });
    this.props.toggleMpin(true);
  }

  submitMpin = () => {
    const shouldSubmit = this.checkConfirmMpinValidation(this.state.confirmMpin);
    if (shouldSubmit) {
      const mpinObj = {
        params: { mPin: sha256(this.state.mpin) },
        userDetails: this.props.userDetails,
        successCb: (response) => {
          const responseData = response.data;
          setClientKey({
            hypervergeFb: {
              appid: responseData.hypervergeAppId,
              appkey: responseData.hypervergeAppSecret,
            },
          });
          if (this.state.isTouchIDSupported) {
            this.showTouchIDPopUp();
          } else {
            this.setMpinToStorage();
          }
        },
      };
      this.props.postRequest({ key: 'saveMpin', data: mpinObj });
    }
  }

  resetMpin = () => {
    this.setMpinToStorage();
    if (typeof this.props.onMpinSet === 'function') {
      this.props.onMpinSet();
    }
  }


  render() {
    return null;
  }
}
