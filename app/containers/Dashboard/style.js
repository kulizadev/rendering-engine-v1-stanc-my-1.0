// import { colors } from '../../configs/styleVars';

export const detailsWrapperStyle = {
  alignItems: 'center',
  paddingLeft: 15,
  paddingRight: 15,
};

export const loanInfoStyle = {
  alignItems: 'center',
  paddingTop: 5,
  paddingBottom: 5,
};
