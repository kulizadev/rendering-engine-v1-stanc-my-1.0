/*
 * LeadListView Messages
 *
 * This contains all the text for the LeadListView component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.LeadListView.header',
    defaultMessage: 'This is LeadListView container !',
  },
});
