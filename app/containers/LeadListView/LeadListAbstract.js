/**
 *
 * Lead List Abstract
 *
 */

import React from 'react';

import * as renderService from '../../utilities/renderDataService';
import { behaveConfig } from '../../configs/appConfig';
import { formatDateTime } from '../../utilities/commonUtils';
import { appConst } from '../../configs/appConfig';
export default class LeadListAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      createdLeads: [],
      assignedLeads: [],
      inProcessLeads: [],
      tabConfig: {
        index: 0,
        routes: [
          { key: 'createdLeads', title: 'Created Leads' },
          { key: 'assignedLeads', title: 'Assigned Leads' },
          { key: 'inProcessLeads', title: 'In Process PD' },
        ],
      },
      paginationOptions: {
        page: appConst.leadlistDefaults.page,
        size: appConst.leadlistDefaults.size,
        dataLength: null,
      },
      searchText: '',
    };
  }
  componentDidMount() {
    // ---------------('ApiEndpoint ', 'TableKey'    );
    this.getLeadsByKey('inProcessLeads');
  }
  getLeadsByKey = (tableKey, search, page = appConst.leadlistDefaults.page, size = appConst.leadlistDefaults.size) => {
    const self = this;
    const reqObj = {
      params: {
        search,
        userId: this.props.userDetails.emailId,
        page,
        size,
      },
      userDetails: this.props.userDetails,
      successCb: (response) => {
        self.setState({
          [tableKey]: response.data.content,
          paginationOptions: {
            page,
            size,
            dataLength: response.data.totalElements,
          },
        });
      },
    };
    this.props.getRequest({ key: tableKey, data: reqObj });
  }

  fetchNextPage = (key) => {
    this.getLeadsByKey(key, this.state.searchText, this.state.paginationOptions.page + 1);
  };
  fetchPrevPage = (key) => {
    this.getLeadsByKey(key, this.state.searchText, this.state.paginationOptions.page - 1);
  };
  fetchSearchResult = (key, searchText = '') => {
    this.setState({ searchText });
    this.getLeadsByKey(key, searchText, this.state.paginationOptions.page);
  };
  activateJourney = (data) => {
    if (data.mobile) {
      const userDetails = {
        ...this.props.userDetails,
        processInstanceId: data.processInstanceId,
        mobile: data.mobile,
        loanApplicationNumber: data.loanApplicationNumber,
        journeyName: data.journeyTobeInitiated,
        isJourneyCompleted: false,
      };
      this.props.setUserDetails(userDetails);
    }
  };

  updateScheduleDateTimePerTable = (processInstanceIds, dateTime, listName) => {
    const date = formatDateTime(dateTime);
    const that = this;
    const leadList = this.state[listName];
    const reqObj = {
      params: {
        processInstanceIds,
        scheduledDateTime: date,
      },
      userDetails: this.props.userDetails,
      successCb: () => {
        const newLeadList = leadList.map(((element) => {
          if (processInstanceIds.includes(element.processInstanceId)) {
            element.scheduledDateTime = date;
          }
          return element;
        }));
        that.setState({ [listName]: newLeadList });
      },
    };

    that.props.postRequest({ key: 'submitScheduleDateTime', data: reqObj });
  }

  render() {
    return null;
  }
}
