/**
 *
 * Lead List Abstract
 *
 */

import React from 'react';
import { View, Platform, Dimensions, ScrollView } from 'react-native';
import Text from '../../components/RenderText';

import LeadListAbstract from './LeadListAbstract';
import { TabViewAnimated, TabBar, SceneMap, TabView, TabViewPagerScroll, TabViewPagerPan } from 'react-native-tab-view';

import LeadTable from './LeadTable';
import * as styles from './tableStyles';

const initialLayout = {
  height: 400,
  flex: 1,
  backgroundColor: '#009090',
  width: Dimensions.get('window').width,
};

export default class LeadListContainer extends LeadListAbstract { // eslint-disable-line react/prefer-stateless-function

  renderCreatedLeads = () => (<View style={{ flex: 1 }}>
    <ScrollView >
      <LeadTable
        data={this.state.createdLeads}
        paginationOptions={this.state.paginationOptions}
        searchText={this.state.searchText}
        searchDisabled
        onSubmitSearchText={(searchText) => this.fetchSearchResult('createdLeads', searchText)}
        additionalKeyset={{}}
        onPress={this.activateJourney}
        updateScheduleDateTime={(leadIds, dateTime) => { this.updateScheduleDateTimePerTable(leadIds, dateTime, 'createdLeads'); }}
        onPressNext={() => { this.fetchNextPage('createdLeads'); }}
        onPressPrev={() => { this.fetchPrevPage('createdLeads'); }}
        disableActions
      />
    </ScrollView >
  </View>);

  renderAssignedLeads = () => (<View style={{ flex: 1 }}>
    <ScrollView >
      <LeadTable
        data={this.state.assignedLeads}
        paginationOptions={this.state.paginationOptions}
        searchText={this.state.searchText}
        searchDisabled
        onSubmitSearchText={(searchText) => this.fetchSearchResult('assignedLeads', searchText)}
        additionalKeyset={{ scheduledDatetime: 'Schedule' }}
        onPress={this.activateJourney}
        updateScheduleDateTime={(leadIds, dateTime) => { this.updateScheduleDateTimePerTable(leadIds, dateTime, 'assignedLeads'); }}
        onPressNext={() => { this.fetchNextPage('assignedLeads'); }}
        onPressPrev={() => { this.fetchPrevPage('assignedLeads'); }}
      />
    </ScrollView >
  </View>);

  renderInProcessLeads = () => (<View style={{ flex: 1 }}>
    <ScrollView >
      <LeadTable
        onRefresh={() => this.getLeadsByKey('inProcessLeads')}
        data={this.state.inProcessLeads}
        paginationOptions={this.state.paginationOptions}
        searchText={this.state.searchText}
        searchDisabled
        onSubmitSearchText={(searchText) => this.fetchSearchResult('inProcessLeads', searchText)}
        additionalKeyset={{ pd1Location: 'PD1 Location', scheduledDateTime: 'Schedule' }}
        onPress={this.activateJourney}
        updateScheduleDateTime={(leadIds, dateTime) => { this.updateScheduleDateTimePerTable(leadIds, dateTime, 'inProcessLeads'); }}
        onPressNext={() => { this.fetchNextPage('inProcessLeads'); }}
        onPressPrev={() => { this.fetchPrevPage('inProcessLeads'); }}
      />
    </ScrollView >
  </View>);

  handleIndexChange = (index) => {
    const updatedTabs = { ...this.state.tabConfig, index };
    this.setState({ tabConfig: updatedTabs });
    const { key: tableKey } = this.state.tabConfig.routes[index];
    this.getLeadsByKey(tableKey);
  }

  renderTabHeader = (props) => (<TabBar
    {...props}
    style={styles.tabHeaderStyle}
    indicatorStyle={styles.indicatorStyle}
    tabStyle={{}}
    renderLabel={(current) => {
      const style = current.focused ? styles.tabHeaderLabelFocused : styles.tabHeaderLabel;
      return (<View>
        <Text style={style}>{current.route.title}</Text>
      </View>);
    }}
    indicatorStyle={styles.indicatorStyle}
  />);

  renderTabPager = (props) => (Platform.OS === 'ios') ? <TabViewPagerScroll {...props} /> : <TabViewPagerPan animationEnabled={false} swipeEnabled={false} {...props} />;

  render() {
    return (<View style={{ flex: 1 }}>
      {/* Removing tab animation component here as we need only in process leads for now*/}
      {this.renderInProcessLeads()}
      {/* <TabViewAnimated*/}
      {/* navigationState={this.state.tabConfig}*/}
      {/* onIndexChange={this.handleIndexChange}*/}
      {/* renderScene={SceneMap({*/}
      {/* createdLeads: this.renderCreatedLeads,*/}
      {/* assignedLeads: this.renderAssignedLeads,*/}
      {/* inProcessLeads: this.renderInProcessLeads,*/}
      {/* })}*/}
      {/* renderHeader={this.renderTabHeader}*/}
      {/* renderPager={this.renderTabPager}*/}
      {/* initialLayout={initialLayout}*/}
      {/* />*/}
    </View>);
  }
}
