import { createSelector } from 'reselect';

/**
 * Direct selector to the leadListView state domain
 */
const selectLeadListViewDomain = (state) => state.get('leadListView');

/**
 * Other specific selectors
 */


/**
 * Default selector used by LeadListView
 */

const makeSelectLeadListView = () => createSelector(
  selectLeadListViewDomain,
  (substate) => substate.toJS()
);

export default makeSelectLeadListView;
export {
  selectLeadListViewDomain,
};
