/**
 *
 * Lead List Component
 *
 */

import React from 'react';
import { View, TouchableOpacity, Keyboard, TextInput, Button, Alert } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { Table, TableWrapper, Cell } from 'react-native-table-component';
import Text from '../../components/RenderText';
import styledComponents from '../../configs/styledComponents';
import TablePagination from '../../components/TablePagination';
import * as styles from './tableStyles.native';

export default class LeadTable extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      allowedColumns: [],
      rowData: [],
      headerData: [],
      headerLabels: {},
      isDateTimePickerVisible: false,
      currentActionTarget: [],
      searchText: props.searchText,
      actions: [
        {
          id: 'reschedule',
          headerLabel: 'Action',
          cellText: 'Schedule',
          onPress: this.toggleDateTimePicker,
        },
      ],
    };
    if (props.disableActions) {
      this.state.actions = [];
    }
    this.toggleDateTimePicker = this.toggleDateTimePicker.bind(this);
  }
  componentDidMount() {
    const additionalKeys = Object.keys(this.props.additionalKeyset);
    const allowedColumns = [
      'loanApplicationNumber',
      'fullName',
      'enterpriseName',
      'loanAmount',
      'mobile',
      'officeAddress',
      'residenceAddress',
    ].concat(additionalKeys);
    const headerLabels = {
      loanApplicationNumber: 'Loan no.',
      fullName: 'Name',
      enterpriseName: 'Enterprise Name',
      loanAmount: 'Loan Amount',
      mobile: 'Mobile Number',
      officeAddress: 'Office Address',
      residenceAddress: 'Residence Address',
      ...this.props.additionalKeyset,
    };
    this.setState({ allowedColumns, headerLabels });
  }

  renderTableHeader = () => {
    const headerData = [];
    this.state.allowedColumns.forEach((fieldId) => {
      headerData.push(this.state.headerLabels[fieldId]);
    });
    return headerData.map((headerLabel, index) => {
      const cellData = (<Text style={styles.tableHeaderStyle}>{headerLabel}</Text>);
      return (<Cell key={`cell-header${index}`} data={cellData} style={styles.text} />);
    }).concat(
      this.state.actions.map((action, index) => {
        const cellData = (<Text style={styles.tableHeaderStyle}>{action.headerLabel}</Text>);
        return (<Cell key={`cell-header${headerData.length}${index}`} data={cellData} style={styles.text} />);
      }));
  }

  renderSingleRow = (data, index) => {
    const renderCells = [];
    const renderConfig = this.state.allowedColumns;
    renderConfig.forEach((fieldId) => {
      const unitCell = (<TouchableOpacity onPress={() => this.props.onPress(data)} style={styles.cellWrapper}><Text>{data[fieldId]}</Text></TouchableOpacity>);
      renderCells.push(
        <Cell key={`cell${index}-${fieldId}`} data={unitCell} style={styles.text} />
      );
    });

    return renderCells.concat(this.getActionCells(renderConfig.length, index));
  }

  getActionCells = (keyOffset, rowIndex) => {
    const { actions } = this.state;
    return actions.map((action, index) => {
      const cellData = (<Text style={styles.tableHeaderStyle} onPress={() => { action.onPress(rowIndex); }}>{styledComponents.getLinkStyledText(action.cellText)}</Text>);
      return (<Cell key={`cell${keyOffset + index}`} data={cellData} style={styles.text} />);
    });
  }

  onDateTimePickerConfirm = (dateTime) => {
    const { currentActionTarget } = this.state;
    Alert.alert(
      'Confirmation Dialog',
      `Rescheduling to ${dateTime.toLocaleString()}`,
      [
        {
          text: 'Cancel',
          onPress: () => {
            this.toggleDateTimePicker(null);
          },
        },
        {
          text: 'OK',
          onPress: () => {
            this.props.updateScheduleDateTime([currentActionTarget], dateTime);
            this.toggleDateTimePicker(null);
          },
        },
      ],
      { cancelable: true }
    );
  };

  toggleDateTimePicker = (rowIndex) => {
    const processInstanceId = Number.isInteger(rowIndex) ? this.props.data[rowIndex].processInstanceId : null;
    this.setState({ currentActionTarget: processInstanceId });
    Keyboard.dismiss();
    this.setState({ isDateTimePickerVisible: !this.state.isDateTimePickerVisible });
  };

  renderTableRows = () => {
    const self = this;
    const tableRowData = this.props.data;
    return tableRowData.map((rowData, index) => {
      const leadData = rowData.leadData || rowData;
      return (<TableWrapper key={`tableRow-${index}`} style={styles.row}>
        {self.renderSingleRow(leadData, index)}
      </TableWrapper>);
    });
  }
  customPaginationLabel = (from, to, count) =>
    `${from} to ${to} of ${count}`;

  render() {
    const tableHeader = this.renderTableHeader();
    const tableRows = this.renderTableRows();
    const {
      onPressNext,
      onPressPrev,
      paginationOptions,
      onSubmitSearchText,
      searchDisabled,
      onRefresh,
    } = this.props;
    const { searchText } = this.state;
    return (
      <View style={{ marginBottom: 15 }}>
        <View style={styles.tableToolBar}>
          <View style={styles.toolbarSubSectionLeft}>
            {
              !searchDisabled ?
                <View style={styles.searchContainer}>
                  <TextInput
                    placeholder="Search by Name/Id"
                    label="Phone number"
                    {...styles.searchInputStyle}
                    onChangeText={(text) => {
                      this.setState({ searchText: text });
                    }}
                    value={searchText}
                  />
                  <Button
                    {...styles.searchButton}
                    title="search"
                    onPress={() => {
                      onSubmitSearchText(searchText);
                    }}
                    disabled={!searchText}
                  />
                </View>
              :
                null
            }
          </View>
          <View style={styles.toolbarSubSectionRight}>
            <View style={styles.paginationContainer} >
              <TouchableOpacity onPress={onRefresh} style={styles.refreshLinkContainer}>
                {styledComponents.getLinkStyledText('Refresh')}
              </TouchableOpacity>
              <TablePagination
                onPressNext={onPressNext}
                onPressPrev={onPressPrev}
                paginationOptions={paginationOptions}
              />
            </View>
          </View>
        </View>
        <View style={styles.container}>
          <Table borderStyle={styles.tableBorderStyle}>
            <TableWrapper key={'tableHeader'} style={styles.headerRow}>{tableHeader}</TableWrapper>
            {tableRows}
          </Table>
          <View>
            <DateTimePicker
              mode="datetime"
              minimumDate={new Date()}
              isVisible={this.state.isDateTimePickerVisible}
              onConfirm={this.onDateTimePickerConfirm}
              onCancel={this.toggleDateTimePicker}
            />
          </View>
        </View>
      </View>);
  }
}

// Props
// data
