import { colors, fonts, sizes, lineHeight } from '../../configs/styleVars';
import { inputComponentStyle } from '../../configs/componentStyles/commonStyles';

export const tableHeaderStyle = {
  margin: 10,
  color: colors.secondaryFontColor,
  fontSize: fonts.body,
  ...fonts.getFontFamilyWeight('bold'),
};

export const container = {
  flex: 1,
  backgroundColor: colors.tableRowBG,
  marginBottom: 10,
};
export const text = {
  margin: 0,
  flex: 1,
  backgroundColor: colors.tableRowBG,
  borderRightWidth: 5,
  borderColor: colors.journeyWrapperBGColor,
};
export const row = {
  flexDirection: 'row',
};

export const cellWrapper = {
  padding: 8,
  backgroundColor: colors.white,
};

export const transparentCellWrapper = {
  padding: 8,
};

export const headerRow = {
  ...row,
  backgroundColor: colors.white,
};

export const tableBorderStyle = {
  borderColor: colors.journeyWrapperBGColor,
  borderWidth: 2,
};

export const tabHeaderStyle = {
  backgroundColor: colors.secondaryBGColor,
  // flex: 0,
  shadowRadius: 0,
  elevation: 0,
  paddingHorizontal: sizes.appPaddingHorizontal,
  paddingTop: 20,
  shadowOffset: {
    height: 0,
  },
};

export const tabHeaderLabel = {
  fontSize: fonts.description,
  lineHeight: lineHeight.description,
  color: colors.tabHeaderColor,
  ...fonts.getFontFamilyWeight('bold'),
  paddingBottom: 7,
  width: '100%',
};

export const tabHeaderLabelFocused = { fontSize: fonts.description,
  lineHeight: lineHeight.description,
  color: colors.black,
  ...fonts.getFontFamilyWeight('bold'),
  paddingBottom: 7,
};

export const indicatorStyle = {
  backgroundColor: colors.primaryBGColor,
};

export const tableToolBar = {
  height: 60,
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
};

export const toolbarSubSectionLeft = {
  flex: 1,
  flexDirection: 'row',
  width: '50%',
};

export const toolbarSubSectionRight = {
  flex: 1,
  flexDirection: 'row',
  width: '50%',
  justifyContent: 'flex-end',
};

export const paginationContainer = {
  display: 'flex',
  flexDirection: 'row',
  alignSelf: 'flex-end',
  justifyContent: 'space-around',
  alignItems: 'center',
  width: 270,
};

export const refreshLinkContainer = {
  marginRight: 20,
  flex: 1,
};

export const searchContainer = {
  flex: 1,
  flexDirection: 'row',
  alignItems: 'center',
};

export const searchInputStyle = {
  style: {
    ...inputComponentStyle,
    width: 400,
    marginRight: 20,
  },
  // underlineColorAndroid: colors.primaryBGColor,
};

export const searchButton = {
  color: colors.primaryBGColor,
};
