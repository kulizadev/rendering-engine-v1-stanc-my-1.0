
import { fromJS } from 'immutable';
import leadListViewReducer from '../reducer';

describe('leadListViewReducer', () => {
  it('returns the initial state', () => {
    expect(leadListViewReducer(undefined, {})).toEqual(fromJS({}));
  });
});
