/**
 *
 * Lead List Component
 *
 */

import React from 'react';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import DateTimePicker from 'components/DateTimePicker';
import ClickableDiv from 'components/AppHeader/ClickableDiv';
import DatePickerDialog from 'material-ui/DatePicker/DatePickerDialog';
import TimePickerDialog from 'material-ui/TimePicker/TimePickerDialog';
import Text from '../../components/RenderText';
import * as styles from './tableStyles';
import TablePagination from '../../components/TablePagination';
// import RenderInputText from '../../components/RenderInputText';
import styledComponents from '../../configs/styledComponents';
import RenderButton from '../../components/RenderButton';

class LeadTable extends React.PureComponent {
  constructor(props) {
    super(props);
    this.toggleDateTimePicker = this.toggleDateTimePicker.bind(this);
    this.state = {
      allowedColumns: [],
      rowData: [],
      headerData: [],
      headerLabels: {},
      currentActionTarget: [],
      searchText: this.props.searchText,
      dialog: {
        actions: [],
        modal: false,
        open: false,
        onRequestClose: this.handleClose,
        content: '',
        title: '',
      },
      actions: this.props.disableActions ? [] : [
        {
          id: 'reschedule',
          headerLabel: 'Action',
          cellText: 'Reschedule',
          onPress: this.toggleDateTimePicker,
        },
      ],
      dateTime: null,
      triggerDatePicker: false,
    };
  }

  componentDidMount() {
    const { additionalKeyset } = this.props;
    const additionalKeys = Object.keys(additionalKeyset);
    const allowedColumns = [
      'loanApplicationNumber',
      'fullName',
      'enterpriseName',
      'loanAmount',
      'mobile',
      'officeAddress',
      ...additionalKeys,
    ];
    const headerLabels = {
      loanApplicationNumber: 'Loan no.',
      fullName: 'Name',
      enterpriseName: 'Enterprise Name',
      loanAmount: 'Loan Amount',
      mobile: 'Mobile Number',
      officeAddress: 'Office Address',
      ...additionalKeyset,
    };
    this.setState({ allowedColumns, headerLabels });
  }

  toggleDateTimePicker = (rowIndex) => {
    const { data } = this.props;
    const processInstanceId = Number.isInteger(rowIndex) ? data[rowIndex].processInstanceId : null;
    this.setState({ currentActionTarget: processInstanceId, triggerDatePicker: true }, () => {
      this.setState({ triggerDatePicker: false });
    });
  };

  handleClose = () => {
    const { dialog } = this.props;
    this.setState({ dialog: {
      ...dialog,
      open: false,
    } });
  };

  renderTableHeader = () => {
    const { allowedColumns, headerLabels, actions } = this.state;
    const headerData = allowedColumns.map((fieldId) => headerLabels[fieldId]);
    const headerRender = headerData.map((headerLabel, index) => {
      const cellData = <Text style={styles.tableHeaderStyle}>{headerLabel}</Text>;
      return (<TableHeaderColumn key={`cell-header${index}`} style={styles.text}>{cellData}</TableHeaderColumn>);
    });
    const actionsRender = actions.map(({ headerLabel }, index) => {
      const cellData = <Text style={styles.tableHeaderStyle}>{headerLabel}</Text>;
      return (<TableHeaderColumn key={`cell-header${headerData.length}${index}`} style={styles.text}>{cellData}</TableHeaderColumn>);
    });

    const renderArray = [
      ...headerRender,
      ...actionsRender,
    ];

    return (
      <TableRow>
        {renderArray}
      </TableRow>
    );
  };

  renderSingleRow = (data, index) => {
    const { allowedColumns } = this.state;
    const { onPress } = this.props;
    const renderCells = allowedColumns.map((fieldId) => {
      const unitCell = (<div onClick={() => onPress(data)} style={styles.transparentCellWrapper}>
        <Text>{data[fieldId]}</Text>
      </div>);
      return <TableRowColumn key={`cell${index}-${fieldId}`} style={styles.text}>{unitCell}</TableRowColumn>;
    });
    const actionCells = this.getActionCells(allowedColumns.length, index);
    return [
      ...renderCells,
      ...actionCells,
    ];
  };

  getActionCells = (keyOffset, rowIndex) => {
    const { actions } = this.state;
    return actions.map((action, index) => {
      const cellData = (<Text style={styles.tableHeaderStyle} onClick={() => action.onPress(rowIndex)}>{styledComponents.getLinkStyledText(action.cellText)}</Text>);
      return <TableRowColumn key={`cell${keyOffset + index}`} style={styles.text}>{cellData}</TableRowColumn>;
    });
  };


  onDateTimePickerConfirm = (dateTime) => {
    const { currentActionTarget } = this.state;
    const { updateScheduleDateTime } = this.props;
    this.setState({
      dialog: {
        actions: [
          <FlatButton
            label="Cancel"
            primary
            onClick={() => this.closeDialog()}
          />,
          <FlatButton
            label="OK"
            primary
            onClick={() => {
              updateScheduleDateTime([currentActionTarget], dateTime);
              this.closeDialog();
            }}
          />,
        ],
        modal: false,
        open: true,
        onRequestClose: this.handleClose,
        content: `Rescheduling to ${dateTime.toLocaleString()}`,
        title: 'Confirmation Dialog',
      },
    });
  };

  closeDialog = () => {
    this.setState({
      dialog: {
        actions: [],
        modal: false,
        open: false,
        onRequestClose: this.handleClose,
        content: '',
        title: '',
      },
    });
  };

  renderTableRows = () => {
    const self = this;
    const { data: tableRowData } = this.props;
    return tableRowData.map((rowData, index) => {
      const leadData = rowData.leadData || rowData;
      return (<TableRow key={`tableRow-${index}`} style={styles.row}>
        {self.renderSingleRow(leadData, index)}
      </TableRow>);
    });
  };

  render() {
    const { dialog, searchText, triggerDatePicker } = this.state;
    const tableHeader = this.renderTableHeader();
    const tableRows = this.renderTableRows();
    const {
        onPressNext,
        onPressPrev,
        paginationOptions,
        onSubmitSearchText,
        searchDisabled,
        onRefresh,
    } = this.props;
    return (
      <div>
        <div style={{ marginBottom: 15 }}>
          <div style={styles.tableToolBar}>
            <div style={styles.toolbarSubSectionLeft}>
              {
                        !searchDisabled ?
                          <div style={styles.searchContainer}>
                            {/* <RenderInputText */}
                            {/* placeholder="Search by Name/Id" */}
                            {/* label="Phone number" */}
                            {/* {...styles.searchInputStyle} */}
                            {/* onChangeText={(text) => { */}
                            {/* this.setState({ searchText: text }); */}
                            {/* }} */}
                            {/* value={searchText} */}
                            {/* /> */}
                            <RenderButton
                              {...styles.searchButton}
                              title="search"
                              onPress={() => {
                                onSubmitSearchText(searchText);
                              }}
                              disabled={!searchText}
                            />
                          </div>
                            :
                            null
                    }
            </div>
            <div style={styles.toolbarSubSectionRight}>
              <div style={styles.paginationContainer} >
                <ClickableDiv onClick={onRefresh} style={styles.refreshLinkContainer}>
                  {styledComponents.getLinkStyledText('Refresh')}
                </ClickableDiv>
                <TablePagination
                  onPressNext={onPressNext}
                  onPressPrev={onPressPrev}
                  paginationOptions={paginationOptions}
                />
              </div>
            </div>
          </div>
          <div style={styles.container}>
            <Table borderStyle={styles.tableBorderStyle} selectable={false}>
              <TableHeader style={styles.headerRow} displaySelectAll={false} adjustForCheckbox={false}>{tableHeader}</TableHeader>
              <TableBody displayRowCheckbox={false}>{tableRows}</TableBody>
            </Table>
            <DateTimePicker
              onChange={this.onDateTimePickerConfirm}
              DatePicker={DatePickerDialog}
              TimePicker={TimePickerDialog}
              triggerDatePicker={triggerDatePicker}
            />
          </div>
        </div>
        <Dialog
          actions={dialog.actions}
          modal={dialog.modal}
          open={dialog.open}
          onRequestClose={dialog.onRequestClose}
          title={dialog.title}
        >
          {dialog.content}
        </Dialog>
      </div>
    );
  }
}

export default LeadTable;
// Props
// data
