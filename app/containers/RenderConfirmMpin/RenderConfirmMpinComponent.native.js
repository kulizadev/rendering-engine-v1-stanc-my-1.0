/**
 *
 * mpin Native
 *
 */


import React from 'react';
import { View, Text, NativeModules } from 'react-native';
import CodeInput from 'react-native-confirmation-code-input';

import RenderConfirmMpinAbstract from './RenderConfirmMpinAbstract';
import RenderButton from '../../components/RenderButton';
import RenderError from '../../components/RenderError/';
import styleVars from '../../configs/styleVars';
import * as styles from './RenderConfirmMpinStyles';
import { strings } from '../../../locales/i18n';
import { checkSupport } from '../../utilities/native/touchIDService.native';

export default class RenderConfirmMpinComponent extends RenderConfirmMpinAbstract { // eslint-disable-line react/prefer-stateless-function

  componentWillMount() {
    this.checkTouchIDStatus();
  }

  checkTouchIDStatus = async () => {
    const supportStatus = await checkSupport();
    this.setState({ isTouchIDSupported: supportStatus });
  }

  handleMpinChange = (text) => {
    this.checkMpinValidation(text);
  }

  shouldEnableTouchID = (value) => {
    // show popup for permission to enable touch ID
    this.state.isTouchIDSupported && this.props.toggleTouchID(value);
    this.setMpinToStorage();
  }

  showTouchIDPopUp = () => {
    // show popup for permission to enable touch ID
    const popupData = this.getPopUpdata();
    this.props.setPopupData(popupData);
  }

  // onPressLearnMore = () => {
  //   NativeModules.PayooModule.openPayoo("en","raj@gmail.com", "9999999999", "dadas", (x, y) => {
  //     console.log(x, y);
  //   })
  // }

  // TODO: create separate component for Code input
  render() {
    const errorMessage = this.state.invalidMpinError ? (<RenderError errorMessage={this.state.invalidMpinError} />) : null;
    const { mpinConfirmed } = this.props.userDetails;
    // TODO: for testing purpose only - remove it
    const backBtn = (
      <View>
        <Text {...styles.linkStyles} onPress={this.logout}>{strings('settings.logout')}</Text>
      </View>
    );
    return (
      <View {...styles.wrapperStyles}>
        <View
          {...styles.topViewStyles}
        >
          <Text {...styles.headingStyles}>{strings('login.confirmMpinHead')}</Text>
        </View>
        {/* <TextField
          label='Enter MPIN'
          {...styles.inputProps}
          error={errorMessage}
          onChangeText={this.handleMpinChange}/>
          <Button
            text="Generate "
            onPress={this.submitMpin}
            {...styles.btnProps}
          /> */}
        <View
          style={{ marginBottom: 40 }}
        >
          <CodeInput
            secureTextEntry
            keyboardType="numeric"
            placeholder={'•'}
            placeholderTextColor={'rgba(221, 221, 221, 0.31)'}
            codeInputStyle={styles.inputStyles}
            codeLength={4}
            space={0}
            size={30}
            className={'border-b'}
            inputPosition="center"
            autoFocus
            activeColor={styleVars.colors.white}
            inactiveColor={styleVars.colors.white}
            onFulfill={(code) => this.checkMpinValidation(code)}
          />
        </View>
        <View style={{ alignItems: 'center' }}>
          {errorMessage}
        </View>
        {backBtn}
        <Text {...styles.paraStyles}>{strings('login.confirmMpinForgot')}</Text>
        {/* <RenderButton
          text="Submit "
          onPress={this.submitMpin}
          type={'primary'}
          {...styles.btnProps}
        /> */}
      </View>
    );
  }
}
