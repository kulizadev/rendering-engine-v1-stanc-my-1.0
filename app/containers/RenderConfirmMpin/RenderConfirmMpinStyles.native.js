import { colors, fonts, sizes, lineHeight } from '../../configs/styleVars';

export const wrapperStyles = {
  style: {
    backgroundColor: colors.primaryBGColor,
    paddingLeft: sizes.appPaddingLeft,
    paddingRight: sizes.appPaddingRight,
    alignContent: 'center',
    flex: 1,
    paddingTop: 100,
  },
};

export const inputProps = {
  keyboardType: 'numeric',
  errorColor: colors.errorColor,
  tintColor: colors.primaryBGColor,
};

export const btnProps = {
  containerStyle: {
    marginTop: 20,
  },
};

export const topViewStyles = {
  style: {
    paddingBottom: 10,
  },
};

export const headingStyles = {
  style: {
    fontSize: fonts.h1,
    lineHeight: lineHeight.h1,
    color: colors.white,
    textAlign: 'center',
    paddingBottom: 10,
    ...fonts.getFontFamilyWeight('bold'),
  },
};


export const paraStyles = {
  style: {
    ...fonts.getFontFamilyWeight(),
    fontSize: fonts.paraSize,
    color: colors.white,
    textAlign: 'center',
    marginTop: 10,
  },
};

export const linkStyles = {
  style: {
    color: colors.white,
    textAlign: 'center',
    marginTop: 10,
  },
};

export const bottomViewStyles = {
  style: {
    paddingTop: 80,
    paddingBottom: 30,
  },
};

export const inputStyles = {
  color: colors.white,
  ...fonts.getFontFamilyWeight('bold'),
  fontSize: 25,
  borderColor: colors.primaryBGColor,
};
