import { createSelector } from 'reselect';

/**
 * Direct selector to the renderConfirmMpin state domain
 */
const selectRenderConfirmMpinDomain = (state) => state.get('renderConfirmMpin');

/**
 * Other specific selectors
 */


/**
 * Default selector used by RenderConfirmMpin
 */

const makeSelectRenderConfirmMpin = () => createSelector(
  selectRenderConfirmMpinDomain,
  (substate) => substate.toJS()
);

export default makeSelectRenderConfirmMpin;
export {
  selectRenderConfirmMpinDomain,
};
