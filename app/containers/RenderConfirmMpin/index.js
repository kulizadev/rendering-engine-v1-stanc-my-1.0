/**
 *
 * RenderConfirmMpin
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from '../../utils/injectSaga';
import saga from './saga';
import messages from './messages';

import { makeSelectAppDetails, makeSelectUserDetails } from '../../containers/AppDetails/selectors';

import * as appActions from '../../containers/AppDetails/actions';
import * as requestUtils from '../../utilities/requestUtils';

import RenderConfirmMpinComponent from './RenderConfirmMpinComponent';

export class RenderConfirmMpin extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderConfirmMpinComponent {...this.props} />
    );
  }
}

RenderConfirmMpin.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  appDetails: makeSelectAppDetails(),
  userDetails: makeSelectUserDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    setUserDetails: (data) => dispatch(appActions.setUserDetails(data)),
    setPopupData: (data) => dispatch(appActions.setPopupData(data)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withSaga = injectSaga({ key: 'renderConfirmMpin', saga });

export default compose(
  withSaga,
  withConnect,
)(RenderConfirmMpin);
