/*
 * RenderConfirmMpin Messages
 *
 * This contains all the text for the RenderConfirmMpin component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.RenderConfirmMpin.header',
    defaultMessage: 'This is RenderConfirmMpin container !',
  },
});
