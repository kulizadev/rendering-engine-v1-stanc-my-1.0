
import { fromJS } from 'immutable';
import renderConfirmMpinReducer from '../reducer';

describe('renderConfirmMpinReducer', () => {
  it('returns the initial state', () => {
    expect(renderConfirmMpinReducer(undefined, {})).toEqual(fromJS({}));
  });
});
