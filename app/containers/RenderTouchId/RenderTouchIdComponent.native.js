/**
*
* RenderTouchIdComponent
*
*/

import React from 'react';
import { View, Text, Image } from 'react-native';
import RenderTouchIdAbstract from './RenderTouchIdAbstract';
import fingerIcon from '../../images/finger.png';
import {
  wrapperStyles,
  topWrapperStyles,
  headingStyles,
  subHeadStyles,
  imageStyles,
  imageWrapperStyles,
  descriptionStyles,
} from './styles';
import RenderButton from '../../components/RenderButton';
import { strings } from '../../../locales/i18n';
import { authenticate } from '../../utilities/native/touchIDService.native';

class RenderTouchIdComponent extends RenderTouchIdAbstract { // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {
    this.getTouchId();
  }

  getTouchId = async () => {
    const authStatus = await authenticate();
    if (authStatus) {
      this.submitMpin();
    }
  }

  render() {
    const { mobileNumber } = this.props.userDetails;
    return (
      <View style={wrapperStyles}>
        <View style={topWrapperStyles}>
          <Text style={headingStyles}>{strings('touchId.hi')}</Text>
          <Text style={subHeadStyles}>{strings('touchId.mobileNumber')}{mobileNumber}</Text>
          <View style={imageWrapperStyles}>
            <Image
              source={fingerIcon}
              style={imageStyles}
              resizeMode="contain"
            />
          </View>
          <Text style={descriptionStyles}>{strings('touchId.description')}</Text>
        </View>
        <View>
          <RenderButton
            text={strings('touchId.enterPasscode')}
            type={'primary'}
            onPress={this.toggleAuthMethod}
          />
        </View>
      </View>
    );
  }
}

RenderTouchIdComponent.propTypes = {

};

export default RenderTouchIdComponent;
