import { createSelector } from 'reselect';

/**
 * Direct selector to the RenderTouchId state domain
 */
const selectRenderTouchIdDomain = (state) => state.get('RenderTouchId');

/**
 * Other specific selectors
 */


/**
 * Default selector used by RenderTouchId
 */

const makeSelectRenderTouchId = () => createSelector(
  selectRenderTouchIdDomain,
  (substate) => substate.toJS()
);

export default makeSelectRenderTouchId;
export {
  selectRenderTouchIdDomain,
};
