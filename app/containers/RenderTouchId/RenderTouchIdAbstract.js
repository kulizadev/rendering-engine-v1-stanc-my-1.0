/**
 *
 * RenderTouchId
 *
 */

import React from 'react';

import * as validationFns from '../../utilities/validations/validationFns';

export default class RenderTouchIdAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  saveMpinToStorage = () => {
    // Issue a request if mpin details are valid
    this.props.setUserDetails({
      ...this.props.userDetails,
      mpinConfirmed: 'true',
      useTouchID: 'true',
    });
    this.props.toggleMpin(true);
    this.props.toggleTouchID(false);
  }

  submitMpin = () => {
    const reqData = {
      params: { enableTouchID: true },
      userDetails: this.props.userDetails,
      successCb: (response) => {
        this.saveMpinToStorage();
      },
    };
    this.props.postRequest({ key: 'updateTouchId', data: reqData });
  }

  toggleAuthMethod = () => {
    this.props.toggleAuthMethod(false);
  }

  render() {
    return null;
  }
}
