
import { fromJS } from 'immutable';
import RenderTouchIdReducer from '../reducer';

describe('RenderTouchIdReducer', () => {
  it('returns the initial state', () => {
    expect(RenderTouchIdReducer(undefined, {})).toEqual(fromJS({}));
  });
});
