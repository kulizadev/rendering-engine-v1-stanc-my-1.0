/**
*
* TextField
*
*/

import React from 'react';
// import styled from 'styled-components';
import { View, TouchableWithoutFeedback, Keyboard, Clipboard } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import * as styles from './styles';
import styledComponents from '../../configs/styledComponents';


class TextFieldComponent extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  onPress = () => {
    Keyboard.dismiss();
    if (typeof this.props.onPress === 'function') {
      this.props.onPress();
    }
  }

  avoidCopy = () => {
    Clipboard.setString('');
  }
  // TODO: find right approach for no copy paste
  render() {
    let inputStyle = this.props.multiline ? styles.multilineTextInput : styles.input;
    if (this.props.inline) {
      inputStyle = { ...inputStyle, ...styles.inlineInput };
    }
    const hintText = styledComponents.getStyledHintText(this.props.hintText);
    // textfield if need to perform other action than typing
    if (this.props.type === 'viewOnly') {
      return (
        <TouchableWithoutFeedback onPress={this.onPress}>
          <View>
            <TextField
              {...this.props}
              {...inputStyle}
              editable={false}
            />
            {hintText}
          </View>
        </TouchableWithoutFeedback>
      );
    }
    let noCopy = {};
    if (this.props.noCopy) {
      noCopy = {
        onBlur: (e) => {
          this.avoidCopy();
          this.props.onBlur && this.props.onBlur(e);
        },
        onFocus: (e) => {
          this.avoidCopy();
          this.props.onFocus && this.props.onFocus(e);
        },
      };
    } else {
      noCopy = {
        onBlur: (e) => {
          this.props.onBlur && this.props.onBlur(e);
        },
        onFocus: (e) => {
          this.props.onFocus && this.props.onFocus(e);
        },
      };
    }


    return (
      <React.Fragment>
        <TextField
          {...inputStyle}
          {...this.props}
          {...noCopy}
        />
        {hintText}
      </React.Fragment>
    );
  }
}

TextField.propTypes = {
};

export default TextFieldComponent;
