import { inputComponentStyle } from '../../configs/componentStyles/commonStyles';

export const input = {
  ...inputComponentStyle,
};

export const inlineInput = {
  ...input,
  labelHeight: 0,
  labelPadding: 0,
  lineWidth: 0,
  activeLineWidth: 0,
  disabledLineWidth: 0,
  label: '',
  inputContainerStyle: {
    padding: 1,
  },
};

export const multilineTextInput = {
  ...inputComponentStyle,
  style: {
    height: 60,
  },
  inputContainerStyle: {
    height: 100,
  },
};
