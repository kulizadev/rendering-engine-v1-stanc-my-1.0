/**
*
* TextField
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import * as styles from './TextStyles';
import TextField from 'material-ui/TextField';
import './spinbutton.css'
import RenderError from '../RenderError/';
import styledComponents from '../../configs/styledComponents';

export default class TextInputField extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  onChangeHandler = (e) => {
    this.props.onChangeHandler && this.props.onChangeHandler(e);
  }

  toggleFocus = (e) => {
    const focusBoolean = e.type === 'focus';
    this.props.toggleFocus && this.props.toggleFocus(focusBoolean);
  }

  render() {
        // props is expected to have a componentProps object:

    const { errorMessage, componentProps: { hintText: hint, ...componentProps }, className } = this.props;
    let error = null;
    if (errorMessage) {
      error = (<RenderError errorMessage={errorMessage} />);
    }
    const hintText = styledComponents.getStyledHintText(hint);
    return (
      <div>
      <div className={className} style={{display:'flex'}}>
        <TextField
          {...this.props.inputStyles}
          {...componentProps}
          value={this.props.value}
          onChange={(e) => this.onChangeHandler(e)}
          onFocus={(e) => this.toggleFocus(e)}
          onBlur={(e) => this.toggleFocus(e)}
        />
        {hintText}
        </div>
        {error}
        
      </div>
    );
  }
}

TextInputField.propTypes = {

};

TextInputField.defaultProps = {
  focusBoolean: true,
  inputStyles: styles.inputStyles,
};

