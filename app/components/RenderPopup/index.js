/**
*
* RenderPopup
*
*/

import React from 'react';
// import styled from 'styled-components';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { connect } from 'react-redux';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import { setSubJourneyId } from '../../containers/RenderJourney/actions';

import RenderPopupComponent from './RenderPopupComponent';

class RenderPopup extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderPopupComponent {...this.props} />
    );
  }
}

RenderPopup.propTypes = {

};

const mapStateToProps = createStructuredSelector({

});

function mapDispatchToProps(dispatch) {
  return {
    setSubJourneyId: (data) => dispatch(setSubJourneyId(data)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(RenderPopup);
