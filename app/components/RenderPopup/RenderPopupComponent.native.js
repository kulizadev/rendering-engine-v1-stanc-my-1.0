/**
*
* RenderPopup
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { View, Image, Platform } from 'react-native';
import Communications from 'react-native-communications';
import Text from '../RenderText';
// import styled from 'styled-components';

import RenderPopupAbstract from './RenderPopupAbstract';

import * as styles from './popupStyles';
import Modal from '../Modal/';
import RenderButton from '../RenderButton';
import { customerSupportNumber } from '../../configs/appConstants';

export default class RenderPopupComponent extends RenderPopupAbstract { // eslint-disable-line react/prefer-stateless-function

  triggerCall = (config) => {
    const self = this;
    const numberToCall = (config.number && config.number.toString()) || customerSupportNumber;
    console.debug('inside triggerCall', config, numberToCall, Communications);
    self.hidePopup();
    if (numberToCall) {
      window.setTimeout(() => {
        Communications.phonecall(numberToCall, true);
      }, 1000);
    }
  }


  render() {
    const self = this;
    const { headerText, bodyText, actionButtons, type } = this.props;
    const actionButtonArr = [];

    const iconWrapperStyle = { ...styles.iconWrapper };
    if (Platform.OS === 'ios') {
      iconWrapperStyle.zIndex = 100;
    }
    const iconStyle = { ...styles.iconStyles };
    if (actionButtons) {
      actionButtons.forEach((buttonConfig, index) => {
        const clickHandler = self.handlerMap(buttonConfig);
        const button = (<RenderButton
          type={buttonConfig.type || 'primary'}
          text={buttonConfig.label}
          {...styles.buttonStyle}
          onPress={() => {
            clickHandler && clickHandler(buttonConfig);
          }}
          key={`action_button_${index}`}
        />);
        actionButtonArr.push(button);
      });
      const iconType = this.getIcon(type);
      const headingStyle = (type === 'error') ? styles.errorHeading : styles.heading;

      return (<Modal isVisible={this.state.showPopup}>
        <View style={{ overflow: 'visible' }}>
          <View {...iconWrapperStyle}>
            <Image
              source={iconType}
              {...styles.iconStyles}
            />
          </View>
          <View {...styles.bodyWrapper}>
            <View {...styles.textWrapper}>
              <Text {...headingStyle}>{headerText}</Text>
              <Text {...styles.bodyText}>{bodyText}</Text>
            </View>
            <View {...styles.buttonWrapper}>
              {actionButtonArr}
            </View>
          </View>
        </View>
      </Modal>);
    }

    return null;
  }
}

RenderPopupComponent.propTypes = {
  headerText: PropTypes.string,
  bodyText: PropTypes.string,
  actionButtons: PropTypes.array,
  type: PropTypes.string,
};
