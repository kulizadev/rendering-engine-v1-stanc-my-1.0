/**
*
* RenderPopup
*
*/

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import Text from '../RenderText';


import RenderPopupAbstract from './RenderPopupAbstract';
import * as styles from './popupStyles';
import Modal from '../Modal/';
import RenderButton from '../RenderButton';

export default class RenderPopupComponent extends RenderPopupAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    const self = this;
    const { headerText, bodyText, actionButtons, type } = this.props;
    // const actionButtonArr = [];
    const iconWrapperStyle = { ...styles.iconWrapper };
    const iconStyle = { ...styles.iconStyles };
    if (actionButtons) {
      // In case we need in-Modal buttons we can use these and add them as Modal child
      // actionButtons.forEach((buttonConfig, index) => {
      //   const clickHandler = self.handlerMap(buttonConfig);
      //   const button = (<RenderButton
      //     type={buttonConfig.type || 'primary'}
      //     label={buttonConfig.label}
      //     {...styles.buttonStyle}
      //     onClick={() => {
      //       clickHandler && clickHandler(buttonConfig);
      //     }}
      //     key={`action_button_${index}`}
      //   />);
      //   actionButtonArr.push(button);
      // });
      const iconType = this.getIcon(type);
      const headingStyle = (type === 'error') ? styles.errorHeading : styles.heading;
      return (<Modal
        open={this.state.showPopup}
        actionButtons={actionButtons}
        headerText={headerText}
        bodyText={bodyText}
        triggerDialogClose={this.triggerDialogClose}
        actionButtonType={'raised'}
      >
        <div>
          <div {...iconWrapperStyle}>
            {
              iconType ?
                <img
                  alt={type}
                  src={iconType}
                  {...styles.iconStyles}
                />
                :
                null
            }
          </div>
          <Text>{bodyText}</Text>
          {/*<div {...styles.buttonWrapper}>*/}
            {/*{actionButtonArr}*/}
          {/*</div>*/}
        </div>
      </Modal>);
    }

    return null;
  }
}

RenderPopupComponent.propTypes = {
  headerText: PropTypes.string,
  bodyText: PropTypes.string,
  actionButtons: PropTypes.array,
  type: PropTypes.string,
};

