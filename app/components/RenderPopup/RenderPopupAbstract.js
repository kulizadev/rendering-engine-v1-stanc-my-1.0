/**
*
* RenderSlider
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import crossIcon from "../../images/popupError.png";
import tickIcon from "../../images/popupSuccess.png";
import touchIcon from "../../images/popupTouch.png";

const popupConst = {};

export default class RenderPopupAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      fieldUpdateKey: 'fieldUpdate',
      click: 'click',
      triggerOnFieldUpdate: false,
      relatedFieldIdsToCheck: [],
      isDataSame: false,
    };
    this.handlerMap = this.handlerMap.bind(this);
    this.triggerDialogClose = this.triggerDialogClose.bind(this);
  }

  triggerDialogClose = () => {
    this.setState({ showPopup: false });
  };

  componentWillMount() {

  }

  componentWillReceiveProps(newProps) {
    console.debug('newProps $$$$$', newProps);
    window.setTimeout(() => {
      if (newProps && newProps.showPopup) {
        this.showPopup();
      } else {
        this.hidePopup();
      }
    });
  }

  showPopup = () => {
    if (!this.state.showPopup) {
      this.setState({ showPopup: true });
    }
  }

  hidePopup = () => {
    if (this.state.showPopup) {
      this.setState({ showPopup: false });
    }
  }

  triggerCallback = (config) => {
    this.hidePopup();
    window.setTimeout(() => {
      config.callback && config.callback();
    }, 1000);
  }

  triggerSubJourney = (subJourneyId) => {
    const self = this;
    this.hidePopup();
    window.setTimeout(() => {
      self.props.setSubJourneyId(subJourneyId);
    }, 1000);
  };

  handlerMap = (buttonConfig) => {
    const self = this;
    switch (buttonConfig.action) {
      case 'hide':
        return self.hidePopup;
      case 'call':
        return self.triggerCall;
      case 'callback':
        return self.triggerCallback;
      case 'triggerSubJourney':
        return () => self.triggerSubJourney(buttonConfig.subJourneyId);
    }

    return null;
  }

  onChangeHandler(e, value) {
    this.props.onChangeHandler(value, this.props.elmId);
  }

  getIcon = (type) => {
    //TODO candidate for abstract
    if (type === 'error') {
      return crossIcon;
    } else if (type === 'success') {
      return tickIcon;
    } else if (type === 'touch') {
      return touchIcon;
    }
    return null;
  };

  render() {
    return null;
  }
}

