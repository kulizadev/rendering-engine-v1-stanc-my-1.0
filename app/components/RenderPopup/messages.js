/*
 * RenderPopup Messages
 *
 * This contains all the text for the RenderPopup component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderPopup.header',
    defaultMessage: 'This is the RenderPopup component !',
  },
});
