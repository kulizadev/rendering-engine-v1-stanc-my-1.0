/**
*
* RenderBlockRadioComponent
*
*/

import React from 'react';
import { View, Text, Image, ScrollView } from 'react-native';
import { Card } from 'react-native-elements';
// import styled from 'styled-components';
import { radioImageWrapperStyle, ctaButtonWrapper, imageStyle, cardStyle, cardBottomWrapperStyle, radioCardsContainerStyle, radioCardWrapperStyle } from './RadioCardStyles.native';
import RenderError from '../RenderError/';
import styledComponents from '../../configs/styledComponents';
import RenderButton from '../../components/RenderButton';

export default class RenderCardRadioComponent extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      optionCardStyle: {},
    };
  }
  onPress = (value) => {
    this.props.onPress(value);
  };

  // getImageSize = (imgWidth, imgHeight, desiredWidth) => {
  //   return { width: desiredWidth, height: (imgHeight / imgWidth) * desiredWidth };
  // };

  getRadioCard = (elmId, option, selectedValue) => {
    const { label, value, infoData: { imageUrl, desc } } = option;
    const selected = value === selectedValue;
    const buttonProps = {
      disabled: selected,
      type: selected ? 'disabled' : 'primary',
      text: selected ? 'Selected' : 'Select',
    };
    // const headerStyles = selected ? selectedCardHeaderStyles : {};
    // const titleStyle = selected ? selectedTextStyles : {};
    // if (!this.state.optionCardStyle[value]) {
    //   Image.getSize(imageUrl, (imgWidth, imgHeight) => {
    //     const ss = this.getImageSize(imgWidth, imgHeight, 150);
    //     this.setState({
    //       optionCardStyle: {
    //         [value]: ss,
    //       },
    //     });
    //   });
    // }
    // this.state.optionCardStyle[value]
    return (
      <View style={radioCardWrapperStyle} key={`radioCard-${elmId}-${value}`} >
        <Card
          title={label}
          selected={selected}
          style={cardStyle}
        >
          <View style={radioImageWrapperStyle}>
            <Image
              style={imageStyle}
              source={{ uri: imageUrl }}
              resizeMode="contain"
            />
          </View>
          <View style={cardBottomWrapperStyle} >
            <Text> {desc} </Text>
            <View style={ctaButtonWrapper}>
              <RenderButton
                onPress={() => { this.onPress(value); }}
                {...buttonProps}
              />
            </View>
          </View>
        </Card>
      </View>);
  };
  render() {
    const { errorMessage, value: valueSelected, options, elmId } = this.props;
    let error = null;
    let radioCardsMap = [];
    options.forEach((option) => {
      radioCardsMap.push(
        this.getRadioCard(elmId, option, valueSelected)
      );
    });
    if (errorMessage) {
      error = (<RenderError errorMessage={errorMessage} />);
    }
    const hintText = styledComponents.getStyledHintText(this.props.hintText);
    return (
      <View style={radioCardsContainerStyle}>
        {hintText}
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          { radioCardsMap }
        </ScrollView>
        {error}
      </View>
    );
  }
}
