/**
*
* RenderRadio
*
*/

import React from 'react';
// import styled from 'styled-components';
import RenderRadioComponent from './RenderRadioComponent';

class RenderRadio extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  render() {
    return (
      <RenderRadioComponent {...this.props} />
    );
  }
}

RenderRadio.propTypes = {

};

export default RenderRadio;
