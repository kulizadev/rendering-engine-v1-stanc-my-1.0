/*
 * RenderRadio Messages
 *
 * This contains all the text for the RenderRadio component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderRadio.header',
    defaultMessage: 'This is the RenderRadio component !',
  },
});
