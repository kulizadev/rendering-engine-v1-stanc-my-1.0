import styled from 'styled-components/native';
import appVars from '../../configs/styleVars';

const colors = appVars.colors,
  sizes = appVars.sizes;

export const radioImageWrapperStyle = {
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'center',
};

export const radioCardWrapperStyle = {
  width: 380,
  flex: 1,
  flexDirection: 'row',
  marginVertical: 10,
};

export const ctaButtonWrapper = {
  paddingHorizontal: 25,
  paddingTop: 15,
  paddingBottom: 25,
};

export const cardStyle = {
  flex: 1,
};

export const imageStyle = {
  height: 250,
  width: 300,
};

export const cardBottomWrapperStyle = {
  width: '100%',
};

export const radioCardsContainerStyle = {
  marginBottom: 20,
};
