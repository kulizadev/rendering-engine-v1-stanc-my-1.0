/**
*
* RenderRadio
*
*/

import React from 'react';
import { View } from 'react-native';
import Text from '../RenderText';

import * as styles from './RenderRadioStyles';
import RenderError from '../RenderError/';

import RenderRadioAbstract from './RenderRadioAbstract';
import styleVars from '../../configs/styleVars';
import RenderRadioForm from './RenderRadioForm';
import RenderImageRadio from './RenderImageRadio';
import RenderBlockRadio from './RenderBlockRadio';
import RenderCardRadio from './RenderCardRadio';

import styledComponents from '../../configs/styledComponents';
import RenderHeading from '../../components/RenderHeading';

export default class RenderRadioComponent extends RenderRadioAbstract { // eslint-disable-line react/prefer-stateless-function

  onChangeHandler = (value) => {
    this.props.onChangeHandler(value, this.props.elmId);
  }

  render() {
    const { compProps, renderData, elmId } = this.props;
    const enumValues = renderData && renderData.enumValues;
    const meta = renderData.metaData,
      isRequired = meta.required || meta.required === 'true',
      infoData = meta.infoData;
    const radioProps = enumValues.map((option) => ({
      label: (option.name.charAt(0).toUpperCase() + option.name.slice(1)),
      value: option.id,
      icon: option.imageUrl,
      infoData: infoData && infoData[option.id],
    }));

    const label = isRequired ? styledComponents.getStyledRequiredLabel(this.props.renderData.name) : this.props.renderData.name;

    let errorMessage = null;
    if (this.props.renderData.errorMessage) {
      errorMessage = (
        <View style={styles.radioErrorStyle}>
          <RenderError errorMessage={this.props.renderData.errorMessage} />
        </View>);
    }
    let RadioComponent = RenderRadioForm;
    let labelComponent = (<Text {...styles.radioTitleStyles}>{label}</Text>);
    if (compProps.type === 'radio-block') {
      RadioComponent = RenderBlockRadio;
      labelComponent = null;
    } else if (compProps.type === 'radio-image') {
      RadioComponent = RenderImageRadio;
      labelComponent = null;
    } else if (meta.type === 'radio-card') { // check for types to be moved here as UI is in picture
      RadioComponent = RenderCardRadio;
      labelComponent = (<RenderHeading
        compProps={{
          label,
          type: 'h3',
        }}
      />);
    }
    const hintText = styledComponents.getStyledHintText(this.props.hintText);
    return (
      <View>
        { labelComponent }
        {hintText}
        <RadioComponent
          {...compProps}
          elmId={elmId}
          onPress={this.onChangeHandler}
          options={radioProps}
        />
        {errorMessage}
      </View>
    );
  }
}
