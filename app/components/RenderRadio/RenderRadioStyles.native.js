import { fonts, colors, lineHeight } from '../../configs/styleVars';

export const radioTitleStyles = {
  style: {
    marginTop: 15,
    fontSize: fonts.body,
    ...fonts.getFontFamilyWeight(),
    lineHeight: lineHeight.body,
    color: colors.basicFontColor,
  },
};

export const radioGroupStyles = {
  style: {
    marginTop: 10,
    alignItems: 'flex-start',
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    flexWrap: 'wrap',
  },
  buttonColor: colors.primaryBGColor,
  selectedButtonColor: colors.primaryBGColor,
};

export const buttonStyles = {
  wrapStyle: {
    marginBottom: 5,
    marginRight: 5,
  },
  labelHorizontal: true,
};

export const buttonIconStyles = {
  buttonInnerColor: colors.transparentColor,
  buttonSize: 12,
  buttonWrapStyle: {
    marginRight: 10,
  },
};

export const labelStyles = {
  labelHorizontal: false,
  labelWrapStyle: {
    marginRight: 20,
    marginBottom: 5,
  },
  labelStyle: {
    color: colors.inputColor,
    fontSize: fonts.body,
    ...fonts.getFontFamilyWeight(),
    lineHeight: lineHeight.body,
  },
};

export const radioErrorStyle = {
  borderStyle: 'solid',
  borderTopColor: colors.errorColor,
  borderTopWidth: 1,
};


// Image Radio Styles
export const imageRadioWrapperStyles = {
  flexDirection: 'row',
  flex: 1,
  alignItems: 'center',
};

export const optionStyles = {
  flexDirection: 'row',
  alignItems: 'center',
  borderRadius: 6,
  borderWidth: 1,
  borderColor: colors.underlineColor,
  padding: 10,
  marginTop: 12,
  marginBottom: 6,
  flex: 1,
};

export const selectedOptionStyles = {
  ...optionStyles,
  borderColor: colors.primaryBGColor,
};

export const textStyles = {
  color: colors.inputColor,
  fontSize: fonts.body,
  ...fonts.getFontFamilyWeight(),
  lineHeight: lineHeight.body,
  marginLeft: 12,
};

export const imageStyles = {
  width: 38,
  height: 38,
};
