/**
*
* RenderBlockRadioComponent
*
*/

import React from 'react';
import { View } from 'react-native';
// import styled from 'styled-components';
import radioSelect from '../../images/radioSelect.png';
import radioDeselect from '../../images/radioDeselect.png';
import ListLabel from '../ListLabel';

export default class RenderBlockRadioComponent extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  onPress = (value) => {
    this.props.onPress(value);
  }

  render() {
    const { options, value } = this.props;
    const radioOptions = options.map((option, index) => {
      const rightIcon = (value === option.value) ? 'radioSelect' : 'radioDeselect';
      return (
        <ListLabel
          key={option.label}
          onPress={() => this.onPress(option.value)}
          label={option.label}
          leftIcon={option.icon}
          leftIconType={'url'}
          rightIcon={rightIcon}
        >
        </ListLabel>
      );
    });

    return radioOptions;
  }
}
