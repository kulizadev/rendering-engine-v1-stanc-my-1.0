/**
*
* RenderRadio
*
*/

import React from 'react';
import { View, Text } from 'react-native';

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import * as styles from './RenderRadioStyles';

import { colors } from '../../configs/styleVars';

export default class RenderRadioComponent extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  onPress= (value) => {
    this.props.onPress(value);
  }

  renderRadioItem = (obj, i) => {
    const { value } = this.props;
    const isSelected = (value === obj.value);
    return (
      <RadioButton
        key={`${obj.value + i}-radio-key`}
        {...styles.buttonStyles}
      >
        <RadioButtonInput
          obj={obj}
          index={i}
          isSelected={isSelected}
          borderWidth={isSelected ? 7 : 1}
          buttonOuterColor={isSelected ? colors.primaryBGColor : colors.labelColor}
          onPress={this.onPress}
          {...styles.buttonIconStyles}
        />
        <RadioButtonLabel
          obj={obj}
          index={i}
          isSelected={isSelected}
          onPress={this.onPress}
          {...styles.labelStyles}
        />
      </RadioButton>
    );
  }

  render() {
    const { options, initial, value } = this.props;
    const radioGroupProp = {
      initial,
      formHorizontal: true,
    };
    return (
      <RadioForm
        {...radioGroupProp}
        {...styles.radioGroupStyles}
        radio_props={[]}
        onPress={this.onPress}
      >
        {options.map((obj, i) => this.renderRadioItem(obj, i))}
      </RadioForm>
    );
  }
}
