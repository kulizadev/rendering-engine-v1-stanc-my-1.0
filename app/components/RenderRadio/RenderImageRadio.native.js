/**
*
* RenderImageRadioComponent
*
*/

import React from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';
// import styled from 'styled-components';
import {
  imageRadioWrapperStyles,
  optionStyles,
  selectedOptionStyles,
  textStyles,
  imageStyles,
} from './RenderRadioStyles';
import RenderInfo from '../RenderInfo';

export default class RenderImageRadioComponent extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  onPress = (value) => {
    this.props.onPress(value);
  }

  render() {
    const { options, value } = this.props;
    const radioOptions = options.map((option, index) => {
      const { label, icon, value: optionValue, infoData } = option;
      const buttonStyles = (optionValue === value) ? { ...selectedOptionStyles } : { ...optionStyles };
      return (
        <View style={imageRadioWrapperStyles}>
          <TouchableOpacity style={{ flex: 1 }} onPress={() => this.onPress(optionValue)}>
            <View style={buttonStyles}>
              <Image source={{ uri: icon }} style={imageStyles} />
              <Text style={textStyles}>{label}</Text>
            </View>
          </TouchableOpacity>
          { infoData ? (
            <RenderInfo headerText={infoData.headerText} bodyText={infoData.bodyText} />
            ) : null
          }
        </View>
      );
    });
    return radioOptions;
  }
}
