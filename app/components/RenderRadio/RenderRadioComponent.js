/**
*
* RenderRadio
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
// import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';

import RenderRadioAbstract from './RenderRadioAbstract';
import RenderError from '../RenderError/';
import * as styles from './RenderRadioStyles';
import RadioField from '../Radio';
import styledComponents from '../../configs/styledComponents';
import RadioCardField from '../RadioCard';

export default class RenderRadioComponent extends RenderRadioAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    const compProps = { ...this.props.compProps };
    const renderData = this.props.renderData
    let options = renderData && renderData.enumValues;
    const meta = renderData.metaData;
    const { type, infoData } = meta;
    if (infoData) {
      options = options.map(
        (option) => ({
          ...option,
          ...infoData[option.id],
        })
      );
    }
    const isRequired = meta.required || meta.required === 'true';
    const label = isRequired ? styledComponents.getStyledRequiredLabel(this.props.renderData.name) : this.props.renderData.name;
    const errorMessage = renderData.errorMessage;

    compProps.radioBtnConfig = { ...compProps.radioButtonProp, ...styles.radioButtonStyles };
    compProps.radioGroupConfig = { ...compProps.radioGroupProp, ...styles.radioGroupStyles };
    let radioSet = (
      <RadioField
        componentProps={compProps}
        errorMessage={errorMessage}
        options={options}
        label={label}
        labelClassName=""
        elemId={this.props.elmId}
        onChangeHandler={this.onChangeHandler}
        onBlurHandler={this.onBlurHandler}
      />);
    if (type === 'radio-card') {
      radioSet = (
        <RadioCardField
          componentProps={compProps}
          errorMessage={errorMessage}
          options={options}
          label={label}
          labelClassName=""
          elemId={this.props.elmId}
          onChangeHandler={this.onChangeHandler}
          onBlurHandler={this.onBlurHandler}
        />);
    }
    return radioSet;
  }
}
