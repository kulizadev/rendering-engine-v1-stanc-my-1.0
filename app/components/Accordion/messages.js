/*
 * Accordion Messages
 *
 * This contains all the text for the Accordion component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Accordion.header',
    defaultMessage: 'This is the Accordion component !',
  },
});
