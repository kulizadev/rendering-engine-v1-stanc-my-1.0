/**
*
* Accordion
*
*/

import React from 'react';
import { View } from 'react-native';
import Collapsible from 'react-native-collapsible';
// import styled from 'styled-components';
import ListLabel from '../ListLabel/';
import {
  labelStyles,
  viewWithIconStyles,
  viewWithoutIconStyles,
} from './styles';

class Accordion extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      isCollapsed: true,
    };
  }

  onPress = () => {
    this.setState(({ isCollapsed }) => ({ isCollapsed: !isCollapsed }));
    if (typeof this.props.onPress === 'function') {
      this.props.onPress();
    }
  }

  render() {
    const { label, collapseIcon, expandIcon, leftIcon, leftIconType, shouldOpen, type } = this.props;
    let { isCollapsed } = this.state;
    if (type === 'radio') {
      isCollapsed = !shouldOpen;
    }
    const rightIcon = isCollapsed ? expandIcon : collapseIcon;
    const collapisbleStyles = leftIcon ? { ...viewWithIconStyles } : { ...viewWithoutIconStyles };
    if (!isCollapsed) {
      collapisbleStyles.borderBottomWidth = 0;
    }
    const childrenBlock = !isCollapsed ? (<View style={[collapisbleStyles, this.props.collapsedViewStyles]}>
      {this.props.children}
    </View>) : null;
    const headerNode = this.props.headerNode && React.cloneElement(this.props.headerNode, { onPress: this.onPress, rightIcon });
    return (
      <View>
        {
          this.props.headerNode ?

          headerNode
          : <ListLabel
            leftIcon={leftIcon}
            leftIconType={leftIconType}
            rightIcon={rightIcon}
            label={label}
            onPress={this.onPress}
            labelStyles={labelStyles}
            isExpanded={!isCollapsed}
          />
        }

        {childrenBlock}
      </View>
    );
  }
}

Accordion.propTypes = {

};

Accordion.defaultProps = {
  collapseIcon: 'minus',
  expandIcon: 'plus',
};

export default Accordion;
