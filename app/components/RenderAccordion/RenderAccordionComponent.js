/**
*
* RenderAccordionComponent
*
*/

import React from 'react';
import Accordion from '../Accordion';
import RenderAccordionAbstract from './RenderAccordionAbstract';
// import styled from 'styled-components';


class RenderAccordionComponent extends RenderAccordionAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { label, imageUrl } = this.props.compProps;
    // console.debug('what is propss', this.props);
    return (
      <Accordion
        label={label}
        leftIcon={'rightArrow'}
        leftIconType={'rightArrow'}
      >
        {this.props.children}
      </Accordion>
    );
  }
}

RenderAccordionComponent.propTypes = {

};

export default RenderAccordionComponent;
