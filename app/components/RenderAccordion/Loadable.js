/**
 *
 * Asynchronously loads the component for RenderAccordion
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
