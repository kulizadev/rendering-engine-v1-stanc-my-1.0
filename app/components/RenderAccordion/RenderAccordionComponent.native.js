/**
*
* RenderAccordionComponent
*
*/

import React from 'react';
import { View } from 'react-native';
import Accordion from '../Accordion';
import RenderAccordionAbstract from './RenderAccordionAbstract';
// import styled from 'styled-components';


class RenderAccordionComponent extends RenderAccordionAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    const { label, imageUrl } = this.props.compProps;
    return (
      <Accordion
        label={label}
        leftIcon={imageUrl}
        leftIconType={'url'}
      >
        {this.props.children}
      </Accordion>
    );
  }
}

RenderAccordionComponent.propTypes = {

};

export default RenderAccordionComponent;
