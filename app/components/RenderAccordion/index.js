/**
*
* RenderAccordion
*
*/

import React from 'react';
import RenderAccordionComponent from './RenderAccordionComponent';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class RenderAccordion extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderAccordionComponent {...this.props} />
    );
  }
}

RenderAccordion.propTypes = {

};

export default RenderAccordion;
