/**
*
* RenderBackButtonComponent
*
*/

import React from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import backIcon from '../../images/back-black.png';
import whiteIcon from '../../images/back-white.png';
import RenderBackButtonAbstract from './RenderBackButtonAbstract';
import {
  backButtonProps,
  backIconStyleProps,
} from './style.native';
// import styled from 'styled-components';


class RenderBackButtonComponent extends RenderBackButtonAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    const iconSrc = this.props.type === 'black' ? backIcon : whiteIcon;
    return (
      <View {...backButtonProps}>
        <TouchableOpacity
          onPress={this.props.onBack}
        >
          <Image
            source={iconSrc}
            {...backIconStyleProps}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

RenderBackButtonComponent.propTypes = {

};

export default RenderBackButtonComponent;
