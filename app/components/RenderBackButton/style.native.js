import { colors } from '../../configs/styleVars';

export const backIconStyleProps = {
  style: { height: 25, width: 25 },
  resizeMode: 'contain',
};

export const backButtonProps = {
  style: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    flex: 0,
    alignItems: 'center',
  },
};
