/**
*
* RenderBackButton
*
*/

import React from 'react';
import RenderBackButtonComponent from './RenderBackButtonComponent';
// import styled from 'styled-components';


class RenderBackButton extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderBackButtonComponent {...this.props} />
    );
  }
}

RenderBackButton.propTypes = {

};

export default RenderBackButton;
