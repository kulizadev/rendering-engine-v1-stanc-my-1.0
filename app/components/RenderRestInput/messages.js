/*
 * RenderRestInput Messages
 *
 * This contains all the text for the RenderRestInput component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderRestInput.header',
    defaultMessage: 'This is the RenderRestInput component !',
  },
});
