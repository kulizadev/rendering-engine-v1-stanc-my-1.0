/**
*
* RenderRestInput
*
*/

import React from 'react';
// import styled from 'styled-components';
import TextField from 'material-ui/TextField';

import RenderRestInputAbstract from './RenderRestInputAbstract';
import RenderError from '../RenderError/';
import * as styles from './RenderRestInputStyles';
import styledComponents from '../../configs/styledComponents';

export default class RenderRestInputComponent extends RenderRestInputAbstract { // eslint-disable-line react/prefer-stateless-function

  // TODO:
  // This rest component just support the Input text for now
  // Make is supported for all other form input types
  // Use form_type for choosing the right type
  render() {
    let errorMessage = null;
    if (this.props.renderData.errorMessage) {
      errorMessage = (<RenderError errorMessage={this.props.renderData.errorMessage} />);
    }
    const hintText = styledComponents.getStyledHintText(this.props.hintText);

    return (
      <div className="field-container">
        <TextField
          {...this.props.compProps}
          {...styles.inputStyles}
          onChange={this.onChangeHandler}
          onBlur={this.onBlurHandler}
        />
        {errorMessage}
        {hintText}
      </div>
    );
  }
}
