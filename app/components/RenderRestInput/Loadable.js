/**
 *
 * Asynchronously loads the component for RenderRestInput
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
