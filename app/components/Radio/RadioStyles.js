import styleVars from '../../configs/styleVars';
const fonts = styleVars.fonts,
  colors = styleVars.colors;

export const radioGroupStyles = {
  style: { // This is the root element style
    width: '100%',
  },
};

export const radioButtonStyles = {
  iconStyle: {
    color: colors.primaryBGColor,
    fill: colors.primaryBGColor,
  },
  labelStyle: {
    fontSize: fonts.fontSize,
    color: colors.basicFontColor,
  },
  style: {
    marginBottom: 4,
  },
};
