export const wrapper = {
  style: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    padding: 10,
    minHeight: 400,
  },
};
