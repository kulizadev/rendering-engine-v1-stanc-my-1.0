/**
*
* RenderSocialLogin
*
*/

import React from 'react';
// import styled from 'styled-components';
import RenderSocialLoginAbstract from './RenderSocialLoginAbstract';

export default class RenderSocialLoginComponent extends RenderSocialLoginAbstract { // eslint-disable-line react/prefer-stateless-function


  render() {
    return (
      <div className="field-container">
        <p>This is web social Login component.</p>
      </div>
    );
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
onBlur Fn
*/
