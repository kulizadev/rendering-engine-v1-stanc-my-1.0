/**
*
* RenderSocialLogin
*
*/

import React from 'react';
import RenderSocialLoginComponent from './RenderSocialLoginComponent';

class RenderSocialLogin extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderSocialLoginComponent {...this.props} />
    );
  }
}

RenderSocialLogin.propTypes = {

};

export default RenderSocialLogin;
