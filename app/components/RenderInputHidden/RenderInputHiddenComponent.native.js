/**
*
* RenderInputHidden
*
*/

import React from 'react';
import { View, TextInput } from 'react-native';
import * as styles from './RenderInputHiddenStyles';

import RenderInputHiddenAbstract from './RenderInputHiddenAbstract';

export default class RenderInputHiddenComponent extends RenderInputHiddenAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <View {...styles.hiddenFieldStyles}>
        <TextInput
          {...this.props.compProps}
          {...styles.hiddenFieldStyles}
        />
      </View>
    );
  }
}

