/**
*
* RenderInputHidden
*
*/

import React from 'react';
// import styled from 'styled-components';

import RenderInputHiddenComponent from './RenderInputHiddenComponent';

class RenderInputHidden extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderInputHiddenComponent {...this.props} />
    );
  }
}

RenderInputHidden.propTypes = {

};

export default RenderInputHidden;
