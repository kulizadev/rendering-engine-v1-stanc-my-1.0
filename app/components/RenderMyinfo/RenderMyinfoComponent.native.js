/**
*
* RenderInputText
*
*/

import React from 'react';
// import styled from 'styled-components';
import { View, Text } from 'react-native';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

import RenderMyinfoAbstract from './RenderMyinfoAbstract';
import RenderButton from '../RenderButton/';

export default class RenderMyinfoComponent extends RenderMyinfoAbstract { // eslint-disable-line react/prefer-stateless-function


  render() {
    return (
      <View>
        <Text>This is native RenderMyinfoComponent</Text>
      </View>
    );
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
onBlur Fn
*/
