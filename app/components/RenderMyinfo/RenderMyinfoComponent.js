/**
*
* RenderInputText
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import RenderMyinfoAbstract from './RenderMyinfoAbstract';
import RenderButton from '../RenderButton/';

export default class RenderMyinfoComponent extends RenderMyinfoAbstract { // eslint-disable-line react/prefer-stateless-function


  render() {
    return (
      <div>
        <div>This is web RenderMyinfoComponent</div>
        <RenderButton label="Verify Myinfo" onClick={this.initiateAuthReq} />
      </div>
    );
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
onBlur Fn
*/
