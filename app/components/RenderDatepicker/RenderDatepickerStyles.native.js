import styleVars from '../../configs/styleVars';
const colors = styleVars.colors,
  fonts = styleVars.fonts;

export const iconStyles = {
  color: styleVars.colors.primaryBGColor,
  size: 24,
};

export const datepickerStyles = {
  container: 'inline',
};

export const iosTextStyle = {
  textAlign: 'center',
  fontSize: 20,
  paddingTop: 13,
  ...fonts.getFontFamilyWeight(),
};
