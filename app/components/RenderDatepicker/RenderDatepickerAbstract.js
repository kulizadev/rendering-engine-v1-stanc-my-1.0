/**
*
* RenderDatepicker
*
*/

import React from 'react';
// import styled from 'styled-components';
import * as common from '../../utilities/commonUtils';

export default class RenderDatepickerAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {

    };
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onBlurHandler = this.onBlurHandler.bind(this);
  }

  componentWillMount() {

  }

  onChangeHandler(e, date) {
    this.props.onChangeHandler(date ? common.formatDefaultDate(date) : null, this.props.elmId);
  }

  onBlurHandler(e, date) {
    // this.props.onBlurHandler(common.formatDefaultDate(date), this.props.elmId);
  }

  render() {
    return null;
  }
}
