/**
*
* RenderDatepicker
*
*/

import React from 'react';
// import styled from 'styled-components';
import RenderDatepickerComponent from './RenderDatepickerComponent';

class RenderDatepicker extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  render() {
    return (
      <RenderDatepickerComponent {...this.props} />
    );
  }
}

RenderDatepicker.propTypes = {

};

export default RenderDatepicker;
