/**
*
* RenderDatepicker
*
*/

import React from 'react';
// import styled from 'styled-components';

import RenderDatepickerAbstract from './RenderDatepickerAbstract';
import * as styles from './RenderDatepickerStyles';
import DatepickerComponent from '../Datepicker';


export default class RenderDatepicker extends RenderDatepickerAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    const errorMessage = this.props.renderData.errorMessage;
    const componentProps = { ...this.props.compProps, ...styles.datepickerStyles };

    return (

      <DatepickerComponent
        componentProps={componentProps}
        errorMessage={errorMessage}
        className=""
        onChangeHandler={this.onChangeHandler}
        onBlurHandler={this.onBlurHandler}
      />
    );
  }
}
