import styleVars from '../../configs/styleVars';
const fonts = styleVars.fonts,
  colors = styleVars.colors;

export const datepickerStyles = {
  style: { // This is the root element style
    width: '100%',
  },
  textFieldStyle: {
    marginBottom: 0,
    color: colors.basicFontColor,
    width: '100%',
  },
  floatingLabelFocusStyle: {

  },
  floatingLabelStyle: {
    fontSize: fonts.fontSize,
    ...fonts.getFontFamilyWeight(),
  },
};
