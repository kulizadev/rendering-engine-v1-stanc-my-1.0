/**
*
* RenderDatepicker
*
*/

import React from 'react';
import { View, Keyboard, Image, Platform } from 'react-native';
import Text from '../RenderText';
// import styled from 'styled-components';
import RenderDatepickerAbstract from './RenderDatepickerAbstract';
import Datepicker from '../Datepicker/';
import * as common from '../../utilities/commonUtils';

export default class RenderDatepicker extends RenderDatepickerAbstract { // eslint-disable-line react/prefer-stateless-function

  onChangeHandler(date) {
    console.log('A date has been picked: ', date);
    this.props.onChangeHandler(date, this.props.elmId);
  }

  render() {
    // TODO: move size to styleConfig
    const compProps = { ...this.props.compProps };
    return (
      <Datepicker
        {...compProps}
        onChangeHandler={this.onChangeHandler}
      />
    );
  }
}
