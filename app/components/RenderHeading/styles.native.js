import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const getStyles = (type = 'body') => ({
  fontSize: fonts[type] || fonts.body,
  lineHeight: lineHeight[type] || lineHeight.body,
  color: colors.black,
  marginTop: 20,
  marginBottom: 5,
  ...fonts.getFontFamilyWeight('bold'),
});
