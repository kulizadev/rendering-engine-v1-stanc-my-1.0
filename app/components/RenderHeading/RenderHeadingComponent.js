/**
*
* RenderHeadingComponent
*
*/

import React from 'react';
import RenderHeadingAbstract from './RenderHeadingAbstract';
import Text from '../RenderText';
import { getStyles } from './styles';
// import styled from 'styled-components';


class RenderHeadingComponent extends RenderHeadingAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { label, type } = this.props.compProps;
    const headingStyles = getStyles(type);
    return (
      <div>
        <div style={headingStyles}>{label}</div>
      </div>
    );
  }
}

RenderHeadingComponent.propTypes = {

};

export default RenderHeadingComponent;
