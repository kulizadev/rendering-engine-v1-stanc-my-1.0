/*
 * RenderHeading Messages
 *
 * This contains all the text for the RenderHeading component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderHeading.header',
    defaultMessage: 'This is the RenderHeading component !',
  },
});
