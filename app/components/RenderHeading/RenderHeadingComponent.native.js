/**
*
* RenderHeadingComponent
*
*/

import React from 'react';
import { View } from 'react-native';
import Text from '../RenderText';
import RenderHeadingAbstract from './RenderHeadingAbstract';
// import styled from 'styled-components';
import { getStyles } from './styles.native';


class RenderHeadingComponent extends RenderHeadingAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { label, type } = this.props.compProps;
    const headingStyles = getStyles(type);
    return (
      <View>
        <Text style={headingStyles}>{label}</Text>
      </View>
    );
  }
}

RenderHeadingComponent.propTypes = {

};

export default RenderHeadingComponent;
