import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const getStyles = (type = 'body') => ({
  fontSize: `${fonts[type] || fonts.body}px`,
  lineHeight: `${lineHeight[type] || lineHeight.body}px`,
  color: colors.blueText,
  marginTop: '20px',
  marginBottom: '5px',
  fontSize:fonts.headingSize
  // ...fonts.getFontFamilyWeight('bold'),
});
