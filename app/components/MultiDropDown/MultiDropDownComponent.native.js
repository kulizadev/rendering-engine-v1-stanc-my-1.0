/**
*
* DropDownComponent Native
*
*/

import React from 'react';
import { View, Image, Text } from 'react-native';
import MultiDropDownAbstract from './MultiDropDownAbstract';
import MultiDropDownModal from '../MultiDropDownModal/';
import TextInputField from '../TextInputField/';
import dropDownIcon from '../../images/down.png';

export default class MultiDropDownComponent extends MultiDropDownAbstract { // eslint-disable-line react/prefer-stateless-function


  setListFlag = (value) => {
    this.setState({ showList: value });
  }

  renderIcon = () => (
    <Image
      style={{ marginBottom: 10 }}
      source={dropDownIcon}
    />
  );

  render() {
    const { label, value, disabled, options, error, displayNames } = this.props;
    const { markAll, showList } = this.state;
    const parsedValue = JSON.parse(value);
    const selectedValues = displayNames.join(', ');
    const allMarked = options.map(({ value: optionValue }) => optionValue);
    if ((allMarked.length === parsedValue.length) && !markAll) {
      this.setState({ markAll: true });
    } else if ((allMarked.length !== parsedValue.length) && markAll) {
      this.setState({ markAll: false });
    }
    return (
      <View>
        <MultiDropDownModal
          value={parsedValue}
          label={label}
          onChange={this.onChange}
          options={options}
          updateOption={this.onChangeHandler}
          showList={showList}
          markAll={markAll}
          handleBackPress={() => this.setListFlag(false)}
          toggleAll={this.toggleAll}
        />
        <TextInputField
          type={'viewOnly'}
          renderAccessory={this.renderIcon}
          label={label}
          error={error}
          disabled={disabled}
          value={selectedValues}
          onPress={() => this.setListFlag(true)}
        />
        {
          displayNames.map((val) => <Text> {val} </Text>)
        }
      </View>
    );
  }
}

MultiDropDownComponent.defaultProps = {
  disabled: false,
};
