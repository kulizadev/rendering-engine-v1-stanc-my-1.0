/**
*
* MultiDropDownAbstract
*
*/

import React from 'react';

export default class MultiDropDownAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      showList: false,
      markAll: false,
    };
  }

  onChangeHandler = (option) => {
    const { value } = this.props;
    const selectedArray = JSON.parse(value);
    const valuePosition = selectedArray.indexOf(option.value);
    const isValueMarked = valuePosition !== -1;
    const changedArray = !isValueMarked ? [
      ...selectedArray,
      option.value,
    ] : selectedArray.filter((iterator) => iterator !== option.value);
    this.props.onChangeHandler(JSON.stringify(changedArray));
  }

  toggleAll = () => {
    const { options, value, onChangeHandler } = this.props;
    const allMarked = options.map(({ value: optionValue }) => optionValue);
    const currentValue = JSON.parse(value);
    const changedArray = allMarked.length === currentValue.length ? [] : allMarked;
    onChangeHandler(JSON.stringify(changedArray));
  }

  render() {
    return null;
  }
}
