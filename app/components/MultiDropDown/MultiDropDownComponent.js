/**
*
* MultiDropDownComponent
*
*/

import React from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import MultiDropDownAbstract from './MultiDropDownAbstract';
import RenderError from '../RenderError/';
import * as styles from './styles';

export default class MultiDropDownComponent extends MultiDropDownAbstract { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      values: props.compProps.value || [],
    };
  }


  onChangeHandler = (e, index, option) => {
    this.setState({ values: option });
    this.props.onChangeHandler(option);
  }


  componentWillReceiveProps = (newProps) => {
    const value = newProps.compProps.value || [];
    if (JSON.stringify(value) !== JSON.stringify(this.state.values)) {
      this.setState({
        values: value,
      });
    }
  }

  render() {
    const { options: enums, error, className, compProps } = this.props;
    const { values } = this.state;

    const menuItems = enums.map(({ value, name }) => (
      <MenuItem
        value={value}
        primaryText={name}
        key={value}
        insetChildren
        checked={values && values.indexOf(value) > -1}
      />
    ));

    const errorMessage = error ? <RenderError errorMessage={error} /> : null;

    return (
      <div className={className}>
        <SelectField
          {...styles.multiDropdownStyles}
          {...compProps}
          value={values}
          multiple
          onChange={this.onChangeHandler}
        >
          {menuItems}
        </SelectField>
        {errorMessage}
      </div>
    );
  }
}
