/*
 * RenderAppError Messages
 *
 * This contains all the text for the RenderAppError component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderAppError.header',
    defaultMessage: 'This is the RenderAppError component !',
  },
});
