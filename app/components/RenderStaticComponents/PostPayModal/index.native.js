import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import RenderButton from '../../RenderButton';
import * as styles from './styles.native';
import { colors, fonts, lineHeight, sizes } from '../../../configs/styleVars';
import { strings } from '../../../../locales/i18n';
import * as common from '../../../utilities/commonUtils';

import RenderStaticComponents, { ModalOverlay, PaymentOptionsModal } from '../../RenderStaticComponents';


const getPayData = (appData) => {
  const payData = [
    {
      name: strings('postpayDetails.overdueMonth'),
      value: appData.noOfMonthsOverdue || '',
    },
    {
      name: strings('postpayDetails.loanAmount'),
      value: appData.overdueLoanAmount ? common.convertCurrencyFormat(appData.overdueLoanAmount) : '-',
    },
    {
      name: strings('postpayDetails.overdueInterest'),
      value: appData.overdueFeeAmount ? common.convertCurrencyFormat(appData.overdueFeeAmount) : '-',
    },
    {
      name: strings('postpayDetails.nextPayment'),
      description: appData.nextRepaymentDate,
      value: appData.emi ? common.convertCurrencyFormat(appData.emi) : '-',
    },
  ];
  return payData.map((data, index) => (
    <View
      key={`payData_${index}`}
      style={styles.detailWrap}
    >
      <View>
        <Text style={styles.labelStyle}>{data.name}</Text>
        {
            data.description
            ? <Text style={styles.descStyle}>
              {data.description}
            </Text>
            : null
        }
      </View>
      <View>
        <Text style={styles.valueStyle} >
          {data.value || '-'}</Text>
      </View>
    </View>
  ));
};

export default class PostPayModal extends React.Component { // eslint-disable-line react/prefer-stateless-functionconst PostPayModal = ({ status,  }) => (
  constructor(props) {
    super(props);
    this.state = {
      showPayOptionsModal: false,
    };
  }

  togglePayOptionsModal = (flag) => {
    this.setState({ showPayOptionsModal: flag });
  }
  render() {
    const totalPaymentAmount = this.props.data.totalPaymentAmount ? common.convertCurrencyFormat(this.props.data.totalPaymentAmount) : '-';
    const isOverDue = !!parseInt(this.props.data.overdueLoanAmount, 10);
    return (<View style={styles.wrapperStyle}>
      <View>
        <Text style={styles.headerStyle} >{strings('postpayDetails.heading')}</Text>
        {getPayData(this.props.data)}
        <View style={styles.payInfoWrap}>
          <Text style={styles.labelStyle}>{strings('postpayDetails.finalAmount')}</Text>
          <Text style={[styles.amountStyle, { color: isOverDue ? colors.errorColor : colors.amountHeading }]}>{totalPaymentAmount}</Text>
        </View>
      </View>
      <ModalOverlay
        onBack={() => this.togglePayOptionsModal(false)}
        isVisible={this.state.showPayOptionsModal}
        headerType={'white'}
      >
        <PaymentOptionsModal data={this.props.data} />
      </ModalOverlay>
      <View style={{ marginBottom: 30 }}>
        <RenderButton
          text={strings('postpayDetails.buttonCta')}
          type="primary"
          onPress={() => this.togglePayOptionsModal(true)}
        />
      </View>
    </View>);
  }
}

PostPayModal.propTypes = {
  status: PropTypes.string,
};

// export default PostPayModal;
