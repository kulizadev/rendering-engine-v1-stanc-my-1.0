import { colors, fonts, lineHeight, sizes } from '../../../configs/styleVars';

export const wrapperStyle = {
  paddingHorizontal: sizes.appPaddingHorizontal,
  flex: 1,
  justifyContent: 'space-between',
};

export const headerStyle = {
  fontSize: fonts.h3,
  ...fonts.getFontFamilyWeight('bold'),
  lineHeight: lineHeight.h3,
  color: colors.black,
};

export const payInfoWrap = {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  paddingTop: 20,
};

export const amountStyle = {
  fontSize: fonts.h2,
  ...fonts.getFontFamilyWeight('bold'),
  lineHeight: lineHeight.h2,
};

export const detailWrap = {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'flex-start',
  paddingVertical: 15,
  borderBottomWidth: 1,
  borderBottomColor: colors.underlineColor,
};

export const labelStyle = {
  fontSize: fonts.description,
  ...fonts.getFontFamilyWeight(),
  lineHeight: lineHeight.description,
  color: colors.labelColor,
};

export const descStyle = {
  fontSize: fonts.label,
  ...fonts.getFontFamilyWeight(),
  lineHeight: lineHeight.label,
  color: colors.labelColor,
};

export const valueStyle = {
  fontSize: fonts.body,
  ...fonts.getFontFamilyWeight('bold'),
  lineHeight: lineHeight.body,
  color: colors.black,
};
