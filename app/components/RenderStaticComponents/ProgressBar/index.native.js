import React from 'react';
import { View } from 'react-native';
import { getWrapperStyle, getFilledTrackStyle, getUnfilledTrackStyle } from './styles.native';

const ProgressBar = ({ trackWidth, filledTrackColor, unfilledTrackColor, filled, style }) => {
  const filledTrackStyles = getFilledTrackStyle(trackWidth, filledTrackColor, filled);
  const unfilledTrackStyles = getUnfilledTrackStyle(trackWidth, unfilledTrackColor);
  const wrapperStyle = getWrapperStyle(trackWidth);
  return (
    <View style={{ ...wrapperStyle, ...style }}>
      <View style={unfilledTrackStyles} />
      <View style={filledTrackStyles} />
    </View>);
};

export default ProgressBar;
