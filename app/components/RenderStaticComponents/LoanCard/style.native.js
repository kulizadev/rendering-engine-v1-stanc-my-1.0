import { colors, fonts, lineHeight } from '../../../configs/styleVars';

export const loanCardWrapStyle = {
  backgroundColor: colors.white,
  borderRadius: 6,
  marginVertical: 5,
  overflow: 'hidden',
};

export const contentWrapper = {
  padding: 20,
};

export const loanStatusStyle = {
  color: colors.white,
  fontSize: fonts.label,
  ...fonts.getFontFamilyWeight(),
  lineHeight: lineHeight.label,
  paddingHorizontal: 5,
  paddingVertical: 2,
  marginRight: 5,
  flex: 0,
};

export const statusWrapStyle = {
  flexDirection: 'row',
  alignItems: 'center',
  paddingVertical: 6,
};


export const detailsWrapStyle = {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'flex-start',
  marginTop: 10,
};

export const detailStyle = {
  flex: 1,
};

export const detailLabelStyle = {
  color: colors.labelColor,
  fontSize: fonts.label,
  ...fonts.getFontFamilyWeight(),
  lineHeight: lineHeight.label,
};


export const detailDescStyle = {
  color: colors.black,
  fontSize: fonts.body,
  ...fonts.getFontFamilyWeight(),
  lineHeight: lineHeight.body,
};

export const loanAmountStyle = {
  color: colors.black,
  fontSize: fonts.h2,
  lineHeight: lineHeight.h2,
  ...fonts.getFontFamilyWeight('bold'),
};

export const reminderStyles = {
  color: colors.black,
  fontSize: fonts.description,
  lineHeight: lineHeight.description,
  backgroundColor: colors.reminderColor,
  paddingHorizontal: 20,
  paddingVertical: 8,
  ...fonts.getFontFamilyWeight(),
};
