import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import ProgressBar from '../ProgressBar';
import { colors } from '../../../configs/styleVars';
import {
  loanStatusStyle, statusWrapStyle, detailsWrapStyle, detailStyle, detailLabelStyle, loanCardWrapStyle, loanAmountStyle, detailDescStyle, reminderStyles, contentWrapper,
} from './style.native';
import * as common from '../../../utilities/commonUtils';
import { statusMap, statusKey } from '../../../containers/RenderPostJounery/renderLoanService';
import { strings } from '../../../../locales/i18n';

const LoanStatus = ({ status }) => {
  const getStatusInfo = () => {
    let style = {};
    let label = '';
    switch (status) {
      case statusMap.APPROVED:
        style = { backgroundColor: colors.amountHeading, ...loanStatusStyle };
        label = strings('loanCard.statusApproved');
        break;
      case statusMap.DISBURSED:
        style = { backgroundColor: colors.inProgressColor, ...loanStatusStyle };
        label = strings('loanCard.statusInProgress');
        break;
      case statusMap.CLOSED:
        style = { backgroundColor: colors.closedColor, ...loanStatusStyle };
        label = strings('loanCard.statusClosed');
        break;
      case statusMap.REJECTED:
        style = { backgroundColor: colors.errorColor, ...loanStatusStyle };
        label = strings('loanCard.statusRejected');
        break;
      case statusMap.CANCELLED:
        style = { backgroundColor: colors.errorColor, ...loanStatusStyle };
        label = strings('loanCard.statusCancelled');
        break;
      case statusMap.EXPIRED:
        style = { backgroundColor: colors.errorColor, ...loanStatusStyle };
        label = strings('loanCard.statusExpired');
        break;
      default:
        break;
    }
    return [label, style];
  };
  const [label, style] = getStatusInfo();
  if (status) {
    return <Text style={style}>{label}</Text>;
  }
  return null;
};


const getModifiedList = (data) => {
  let details = [];
  let showProgressBar = false;
  let repaidPercent = 0;
  switch (data[statusKey]) {
    case statusMap.REJECTED:
    case statusMap.CANCELLED:
    case statusMap.NEW:
    case statusMap.EXPIRED:
      details = [{
        label: strings('loanCard.requestedAmount'),
        value: common.convertCurrencyFormat(data.loanAmount),
      }, {
        label: strings('loanCard.requestedtenure'),
        value: `${data.tenure} ${strings('loanDetailModal.month')}`,
      }];
      break;
    case statusMap.DISBURSED:
      details = [{
        label: strings('loanCard.outstandingAmount'),
        value: common.convertCurrencyFormat(data.outstandingLoanAmount),
      }, {
        label: strings('loanCard.remainingMonths'),
        value: `${data.remainingTenure || '-'} ${strings('loanDetailModal.month')}`,
      }];
      showProgressBar = true;
      repaidPercent = data.outstandingLoanAmount !== '' ? parseInt(((data.loanAmount - data.outstandingLoanAmount) * 100) / data.loanAmount, 10) : 0;
      break;
    case statusMap.CLOSED:
      details = [{
        label: strings('loanCard.totalPaid'),
        value: common.convertCurrencyFormat(data.loanAmount),
      }, {
        label: strings('loanCard.totalTenure'),
        value: `${data.tenure} ${strings('loanDetailModal.month')}`,
      }];
      showProgressBar = true;
      repaidPercent = 100;
      break;
    case statusMap.APPROVED:
      details = [{
        label: strings('loanCard.monthlyInstallment'),
        value: common.convertCurrencyFormat(data.emi),
      }, {
        label: strings('loanCard.loanTenure'),
        value: `${data.tenure} ${strings('loanDetailModal.month')}`,
      }];
      break;
    default:
      details = [];
  }
  return {
    details,
    showProgressBar,
    repaidPercent,
  };
};

const Detail = ({ label, value, style }) => (
  <View style={{ ...style, ...detailStyle }}>
    <Text style={detailLabelStyle}>{label}</Text>
    <Text style={detailDescStyle}>{value}</Text>
  </View>
);

const LoanCard = (props) => {
  const { data, showReminder } = props;
  const { details, showProgressBar, repaidPercent } = getModifiedList(data);
  const detailList = details.map((detail, index) => (
    <Detail
      key={detail.id}
      label={detail.label}
      value={detail.value}
      style={index % 2 ? {} : { marginRight: 15 }}
    />
    ));
  return (
    <TouchableOpacity onPress={() => props.onPress(data, details)}>
      <View style={loanCardWrapStyle} >
        <View style={contentWrapper}>
          <View style={statusWrapStyle} >
            <LoanStatus status={data[statusKey]} />
            <Text style={loanAmountStyle} >{common.convertCurrencyFormat(data.loanAmount)}</Text>
          </View>
          <View style={detailsWrapStyle}>
            {detailList}
          </View>
          {showProgressBar ? <ProgressBar filled={repaidPercent} style={{ marginTop: 20, marginBottom: 6 }} /> : null}
        </View>
        {showReminder
          ? <Text style={reminderStyles}>{strings('loanCard.reminderText')} {data.nextRepaymentDate}</Text>
          : null}
      </View>
    </TouchableOpacity>
  )
 ;
};

export default LoanCard;
