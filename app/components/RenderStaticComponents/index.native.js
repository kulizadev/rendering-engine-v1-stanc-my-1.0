/**
*
* RenderStaticComponentsComponent
*
*/

import React from 'react';
// import RenderStaticComponentsAbstract from './RenderStaticComponentsAbstract';
import RenderLoanCard from './LoanCard';
import RenderModalOverlay from './ModalOverlay';
import RenderProgressBar from './ProgressBar';
import RenderPostHeader from './PostHeader';
import RenderBottomNavBar from './BottomNavBar';
import RenderBackgroundImage from './BackgroundImage';
import RenderSupportRequestCard from './SupportRequestCard';
import RenderSupportChat from './SupportChat';
import RenderProfileDetail from './ProfileDetail';
import RenderPostPayModal from './PostPayModal';
import RenderWalkThroughConfirm from './WalkThroughConfirm';
import RenderFilterList from './FilterList';
import RenderPaymentOptionsModal from './PaymentOptionsModal';
import RenderPostPayOptions from './PostPayOptions';
import RenderFaqCategory from './FaqCategory';
import RenderNotificationList from './NotificationList';
import RenderSideMenu from './SideMenu';
import RenderCreateList from './CreateList';
import RenderAppLogo from './AppLogo';

export const LoanCard = (props) => <RenderLoanCard {...props} />;

export const ModalOverlay = (props) => <RenderModalOverlay {...props} />;

export const ProgressBar = (props) => <RenderProgressBar {...props} />;

export const PostHeader = (props) => <RenderPostHeader {...props} />;

export const BottomNavBar = (props) => <RenderBottomNavBar {...props} />;

export const BackgroundImage = (props) => <RenderBackgroundImage {...props} />;

export const SupportRequestCard = (props) => <RenderSupportRequestCard {...props} />;

export const SupportChat = (props) => <RenderSupportChat {...props} />;

export const ProfileDetail = (props) => <RenderProfileDetail {...props} />;

export const PostPayModal = (props) => <RenderPostPayModal {...props} />;

export const WalkThroughConfirm = (props) => <RenderWalkThroughConfirm {...props} />;

export const FilterList = (props) => <RenderFilterList {...props} />;

export const PaymentOptionsModal = (props) => <RenderPaymentOptionsModal {...props} />;

export const PostPayOptions = (props) => <RenderPostPayOptions {...props} />;

export const FaqCategory = (props) => <RenderFaqCategory {...props} />;

export const NotificationList = (props) => <RenderNotificationList {...props} />;

export const SideMenu = (props) => <RenderSideMenu {...props} />;

export const CreateList = (props) => <RenderCreateList {...props} />;

export const AppLogo = (props) => <RenderAppLogo {...props} />;

const RenderStaticComponentsComponent = {
  LoanCard,
  ModalOverlay,
  ProgressBar,
  PostHeader,
  BottomNavBar,
  BackgroundImage,
  SupportRequestCard,
  SupportChat,
  ProfileDetail,
  PostPayModal,
  WalkThroughConfirm,
  CreateList,
  AppLogo,
};

// TextField.propTypes = {

// };


export default RenderStaticComponentsComponent;
