import { Platform } from 'react-native';
import { colors } from '../../../configs/styleVars';


export const modalWrapStyle = {
  backgroundColor: colors.primaryBGColor,
  padding: 0,
  margin: 0,
  paddingTop: (Platform.OS === 'ios') ? 20 : 0,
  justifyContent: 'flex-start',
};


export const whiteModal = {
  backgroundColor: colors.white,
};
