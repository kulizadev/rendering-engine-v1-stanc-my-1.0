import React from 'react';
import { View, Platform, ScrollView } from 'react-native';
import Modal from '../../Modal';
import RenderBackButton from '../../RenderBackButton';
import { modalWrapStyle, whiteModal } from './styles.native';

export default class ModalOverlay extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
    };
  }
  render() {
    const { isVisible, style, children, headerType } = this.props;
    const newStyle = [modalWrapStyle, style];
    let backButtonType = 'white';
    if (headerType === 'white') {
      newStyle.push(whiteModal);
      backButtonType = 'black';
    }
    return (
      <Modal
        animationInTiming={500}
        animationOutTiming={500}
        backdropTransitionInTiming={500}
        backdropTransitionOutTiming={500}
        {...this.props}
        onRequestClose={this.props.onBack}
        onModalHide={this.props.onModalHide}
        onModalShow={this.props.onModalShow}
        isVisible={isVisible}
        style={newStyle}
      >
        <View style={{ flex: 1 }}>
          <View style={{ paddingHorizontal: 20, paddingVertical: 15 }}>
            <RenderBackButton onBack={this.props.onBack} type={backButtonType} />
          </View>
          {React.Children.toArray(children)}
        </View>
      </Modal>
    );
  }
  }
