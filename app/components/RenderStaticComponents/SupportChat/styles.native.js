import { Dimensions } from 'react-native';
import { sizes } from '../../../configs/styleVars';
const deviceWidth = Dimensions.get('window').width;

export const wrapperStyle = {
  paddingHorizontal: sizes.appPaddingHorizontal,
};

export const borderStyleUser = {
  borderTopRightRadius: 0,
};

export const borderStyleFec = {
  borderTopLeftRadius: 0,
};

export const chatWrap = {
  flexDirection: 'row',
  marginVertical: 10,
};

export const chatBubble = {
  width: deviceWidth * 0.65,
  padding: 15,
  marginLeft: 5,
  borderRadius: 14,
};
