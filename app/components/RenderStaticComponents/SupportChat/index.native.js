import React from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text } from 'react-native';
import * as styles from './styles.native';
import fecChat from '../../../images/fec-chat.png';

import { colors } from '../../../configs/styleVars';


const SupportChat = ({ chats }) => {
  if (chats && chats.length) {
    return (
      <View style={styles.wrapperStyle}>
        {chats.map((chat, index) => {
          let chatAlign = 'flex-end';
          let chatBackground = colors.primaryBGColor;
          let chatTextColor = colors.white;
          let chatIcon = null;
          let borderStyle = styles.borderStyleUser;
          if (chat.userName === 'Test Administrator') {
            chatAlign = 'flex-start';
            chatIcon = (<Image
              source={fecChat}
            />);
            chatBackground = colors.white;
            chatTextColor = colors.black;
            borderStyle = styles.borderStyleFec;
          }
          return (
            <View style={[styles.chatWrap, { justifyContent: chatAlign }]} key={`chat_${index}`}>
              {chatIcon}
              <View style={[styles.chatBubble, borderStyle, { backgroundColor: chatBackground }]}>
                <Text style={{ color: chatTextColor }}>{chat.message}</Text>
              </View>
            </View>
          );
        }
     )}
      </View>
    );
  }
  return null;
};

SupportChat.propTypes = {
  chats: PropTypes.object,
};

export default SupportChat;
