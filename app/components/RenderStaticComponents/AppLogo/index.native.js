import React, { Component } from 'react';
import { Image } from 'react-native';
import { brand } from '../../../configs/styleVars';
import * as styles from './styles.native';

export default class AppLogo extends Component {

  constructor(props) {
    super(props);
    this.state = {
      logoWidth: 0,
      logoHeight: 0,
    };
  }

  getImageSize = (imgWidth, imgHeight) => {
    const { height, width, style } = this.props;
    if (height && width) {
      return { logoWidth: width, logoHeight: height };
    } else if (height) {
      return { logoWidth: (imgWidth / imgHeight) * height, logoHeight: height };
    }
    return { logoWidth: width, logoHeight: (imgHeight / imgWidth) * width };
  }

  render() {
    const { height, width, style } = this.props;
    let logoImage = null;
    if (brand.logo && typeof brand.logo === 'string') {
      Image.getSize(brand.logo, (imgWidth, imgHeight) => {
        this.setState(this.getImageSize(imgWidth, imgHeight));
      });
      logoImage = (
        <Image
          source={{ uri: brand.logo }}
          {...styles.logoStyles}
          style={[style, { width: this.state.logoWidth, height: this.state.logoHeight }]}
        />
      );
    } else {
      logoImage = (
        <Image
          source={brand.logo}
          {...styles.logoStyles}
          style={style}
        />
      );
    }
    return logoImage;
  }
}
