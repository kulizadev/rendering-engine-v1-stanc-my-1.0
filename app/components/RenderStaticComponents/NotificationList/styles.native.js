import { fonts, colors, lineHeight } from '../../../configs/styleVars';

export const wrapperStyle = { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginVertical: 10 };

export const wrappedTextWrap = { flexDirection: 'row' };

export const wrapTextStyle = { flex: 1, flexWrap: 'wrap' };

export const titleSyle = {
  fontSize: fonts.body,
  lineHeight: lineHeight.body,
  color: colors.black,
};

export const hintStyle = {
  color: colors.labelColor,
  fontSize: fonts.label,
  lineHeight: lineHeight.label,
  ...fonts.getFontFamilyWeight(),
};


export const descStyle = {
  color: colors.black,
  fontSize: fonts.label,
  lineHeight: lineHeight.label,
  ...fonts.getFontFamilyWeight(),
};

export const notificationWrap = { flex: 1, marginRight: 30 };
