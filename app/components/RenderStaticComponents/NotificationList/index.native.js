import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import iconMap from '../../../utilities/iconMap';
import { sizes, fonts } from '../../../configs/styleVars';
import * as styles from './styles.native';

import { strings } from '../../../../locales/i18n';

const WrappedText = ({ text, textStyle }) => <View style={styles.wrappedTextWrap}><Text style={[styles.wrappedTextWrap, textStyle]}>{text}</Text></View>;

export default class NotificationList extends React.Component {

  getNotificationList = () =>
    // TODO: change key from index
     this.props.notifications.map((notification, index) => {
       const titleSyle = notification.status === 'unread' ? { ...fonts.getFontFamilyWeight('bold') } : {};
       return (
         <TouchableOpacity>
           <View key={`notification_${index}`} style={styles.wrapperStyle}>
             <View style={styles.notificationWrap}>
               <WrappedText textStyle={{ ...styles.titleSyle, ...titleSyle }} text={notification.title} />
               <WrappedText textStyle={styles.hintStyle} text={notification.hint} />
               <WrappedText textStyle={styles.descStyle} text={notification.message} />
             </View>
             {/* <Image
                            source={iconMap.rightArrow}
                          />*/}
           </View>
         </TouchableOpacity>
       );
     })
  render() {
    let renderNotifs = null;
    if (this.props.notifications.length) {
      renderNotifs = this.getNotificationList();
    } else {
      renderNotifs = (<Text style={styles.descStyle}>{strings('loanDetails.noData')}</Text>);
    }
    return (<View style={{ paddingHorizontal: sizes.appPaddingHorizontal }}>
      {renderNotifs}
    </View>);
  }
}
