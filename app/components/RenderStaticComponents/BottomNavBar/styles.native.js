import { colors } from '../../../configs/styleVars';

export const wrapperStyles = {
  backgroundColor: colors.white,
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-around',
  paddingVertical: 10,
};

export const drawerMenuStyle = { alignItems: 'center' };

export const tabText = {
  paddingTop: 5,
  fontSize: 12,
};

export const selectedTabText = {
  ...tabText,
  color: colors.primaryBGColor,
};
