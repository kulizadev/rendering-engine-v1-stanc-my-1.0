import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image } from 'react-native';
import filterIcon from '../../../images/filter.png';
import { wrapperStyles, listWrapperStyles } from './styles.native';
import RenderChip from '../../RenderChip/';
import { strings } from '../../../../locales/i18n';


class FilterList extends React.PureComponent {

  onRemoveHandler = (key) => {
    const { list, applyFilter } = this.props;
    const updatedList = {
      ...list,
      [key]: {},
    };
    applyFilter(updatedList);
  }

  getChipLabel = (value) => {
    let label = '';
    if (value && value.name) {
      return value.name;
    }
    Object.keys(value).forEach((key) => {
      label = label ? `${label} - ` : label;
      label += value[key];
    });
    return label;
  }

  render() {
    const { list, showFilters } = this.props;
    let filterChips = Object.keys(list).map((key) => this.getChipLabel(list[key])
                            ? <RenderChip style={{ marginRight: 10 }} label={this.getChipLabel(list[key])} onRemoveHandler={() => this.onRemoveHandler(key)} />
                            : null);
    const isFilterSelected = filterChips.some((comp) => comp !== null);
    if (!isFilterSelected) {
      filterChips = (<RenderChip style={{ marginRight: 10 }} label={strings('support.filterAll')} isDefault />);
    }
    return (
      <View style={wrapperStyles}>
        <View style={listWrapperStyles}>
          { filterChips }
        </View>
        <TouchableOpacity
          onPress={showFilters}
        >
          <Image
            source={filterIcon}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

FilterList.propTypes = {
  showFilters: PropTypes.func,
};

export default FilterList;
