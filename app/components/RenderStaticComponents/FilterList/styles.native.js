export const wrapperStyles = {
  flexDirection: 'row',
  justifyContent: 'space-between',
  paddingVertical: 10,
};

export const listWrapperStyles = {
  flex: 1,
  flexDirection: 'row',
  flexWrap: 'wrap',
};
