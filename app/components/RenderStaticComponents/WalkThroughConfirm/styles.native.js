import { colors, fonts, lineHeight, sizes } from '../../../configs/styleVars';


export const wrapperStyle = {
  paddingHorizontal: sizes.appPaddingHorizontal,
  flex: 1,
  justifyContent: 'space-between',
};

export const topViewStyle = {
  flex: 1,
};

export const headerStyle = {
  fontSize: fonts.h2,
  ...fonts.getFontFamilyWeight('bold'),
  lineHeight: lineHeight.h2,
  paddingBottom: 20,
  color: colors.black,
};

export const webViewStyle = {
  height: 400,
};

export const buttonWrapStyle = {
  paddingBottom: 20,
};
