import React from 'react';
import PropTypes from 'prop-types';
import {
  Image,
  View,
} from 'react-native';
import * as styles from './styles.native';

const BackgroundImage = ({
    source,
    children,
    style,
    imageContainerStyle,
    imageStyle,
    contentContainerStyle,
 }) => (
   <View
     style={[styles.wrapperStyle, style]}
   >
     <View
       style={[styles.imageContainerStyle, imageContainerStyle]}
     >
       <Image
         style={[styles.imageStyle, imageStyle]}
         source={source}
         resizeMode={'stretch'}
       />
     </View>
     <View
       style={[styles.contentContainerStyle, contentContainerStyle]}
     >
       {children}
     </View>
   </View>
  );

BackgroundImage.propTypes = {
  source: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  children: PropTypes.node,
  style: PropTypes.object,
  imageContainerStyle: PropTypes.object,
  imageStyle: PropTypes.object,
  contentContainerStyle: PropTypes.object,
};


export default BackgroundImage;
