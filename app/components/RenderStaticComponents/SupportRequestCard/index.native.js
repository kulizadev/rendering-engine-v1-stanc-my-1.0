import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Text, View, TouchableOpacity } from 'react-native';
import * as styles from './styles.native';
import { colors, fonts } from '../../../configs/styleVars';
import { strings } from '../../../../locales/i18n';

const getStatusMapping = () => (
  {
    'In Progress': {
      color: colors.amountHeading,
      label: strings('support.statusInProgress'),
    },
    Closed: {
      color: colors.labelColor,
      label: strings('support.statusClosed'),
    },
  }
);

const getStatusIndicator = (status) => {
  const statusMapping = getStatusMapping();
  return (
    <View style={styles.indicatorWrap}>
      <Icon name="brightness-1" style={{ color: statusMapping[status].color, fontSize: 8, ...fonts.getFontFamilyWeight() }} />
      <Text style={[styles.indicatorLabel, { color: statusMapping[status].color }]}>
        {statusMapping[status].label}
      </Text>
    </View>
  )
;
};

const SupportRequestCard = ({
    status,
    label,
    header,
    description,
    openSupportChatRequest,
 }) => {
  const statusIndicator = getStatusIndicator(status);
  return (
    <TouchableOpacity onPress={openSupportChatRequest} >
      <View style={styles.wrapperStyles}>
        <View style={styles.statusWrap}>
          <Text style={styles.labelStyle}>{label}</Text>
          {statusIndicator}
        </View>
        <View>
          <Text style={styles.headerStyle} >{header}</Text>
          <Text style={styles.descriptionStyle} >{description}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

SupportRequestCard.propTypes = {
  status: PropTypes.oneOf(Object.keys(getStatusMapping())),
  label: PropTypes.string,
  header: PropTypes.string,
  description: PropTypes.string,
};


export default SupportRequestCard;
