import { sizes, fonts, colors, lineHeight } from '../../../configs/styleVars';

export const linkTextStyle = {
  ...fonts.getFontFamilyWeight(),
  fontSize: fonts.body,
  lineHeight: lineHeight.body,
};

export const linkWrap = {
  marginVertical: 10,
};

export const container = {
};

export const categoryLabel = {
  ...fonts.getFontFamilyWeight(),
  fontSize: fonts.label,
  lineHeight: lineHeight.label,
  color: colors.labelColor,
  marginTop: 15,
  marginBottom: 5,
};

export const categoryWrap = {
  borderTopWidth: 1,
  borderTopColor: colors.underlineColor,
};


export const profileWrap = {
  paddingLeft: 30,
  paddingTop: 40,
  paddingBottom: 30,
  flexDirection: 'row',
  alignItems: 'center',
  backgroundColor: colors.primaryBGColor,
};

export const profilePic = {
  width: 60,
  height: 60,
  borderRadius: 30,
  marginRight: 20,
};


export const profileName = {
  ...fonts.getFontFamilyWeight('bold'),
  fontSize: fonts.body,
  lineHeight: lineHeight.body,
  color: colors.white,
  marginVertical: 5,
};

export const custSince = {
  ...fonts.getFontFamilyWeight(),
  fontSize: fonts.label,
  lineHeight: lineHeight.label,
  color: colors.white,
};


export const menuWrap = {
  paddingLeft: 30,
};

export const profileNameWrapper = {
  flex: 1,
};
