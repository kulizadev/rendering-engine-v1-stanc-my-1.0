import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import { ScrollView, Text, View, TouchableOpacity, Image } from 'react-native';
import { drawerNavListWithCategory } from '../../../configs/appConstants';
import { colors } from '../../../configs/styleVars';
import iconMap from '../../../utilities/iconMap';
import * as styles from './styles.native';
import { strings } from '../../../../locales/i18n';


class SideMenu extends Component {

  getCategoryRoutes = (categories) =>
     categories.map((category) => (<View style={styles.categoryWrap}>
       <Text style={styles.categoryLabel}>
         {strings(`navigation.${category.name}`)}
       </Text>
       {this.getSubRoutes(category)}
     </View>)
);

  getSubRoutes = (category) =>
     (<View>
       {category.routes.map((route) => (
         <TouchableOpacity onPress={() => this.props.screenProps.navigateToScreen(route)} >
           <View style={styles.linkWrap}>
             <Text style={[styles.linkTextStyle, { color: this.props.screenProps.currentScreen === route ? colors.primaryBGColor : colors.black }]}>
               {strings(`navigation.${route.toLowerCase()}`)}
             </Text>
           </View>
         </TouchableOpacity>))}
     </View>)

  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route,
    });
    this.props.navigation.dispatch(navigateAction);
  }
  render() {
    const { dashboardData } = this.props.screenProps;
    const name = dashboardData.borrowerFullName ? dashboardData.borrowerFullName.split(' ').splice(-1).join(' ') : '';
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => this.props.screenProps.navigateToScreen('Profile')}>
          <View style={styles.profileWrap} >
            <Image
              source={dashboardData.selfieLink ? { uri: dashboardData.selfieLink } : iconMap.selfie}
              style={styles.profilePic}
            />
            <View style={styles.profileNameWrapper}>
              <Text style={styles.profileName}>{name || '-'}</Text>
              <Text style={styles.custSince}>{`${strings('navigation.custSince')} ${dashboardData.customerSince}`}</Text>
            </View>
          </View>
        </TouchableOpacity>
        <ScrollView>
          <View style={styles.menuWrap}>
            {this.getSubRoutes(drawerNavListWithCategory)}
            {this.getCategoryRoutes(drawerNavListWithCategory.categories)}
          </View>
        </ScrollView>
      </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object,
};

export default SideMenu;
