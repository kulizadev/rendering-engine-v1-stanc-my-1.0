import { colors, fonts, lineHeight } from '../../../configs/styleVars';

export const wrapperStyle = {
  paddingTop: 20,
  borderBottomWidth: 1,
  borderBottomColor: colors.underlineColor,
};

export const titleWrap = {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  paddingVertical: 10,
};

export const titleStyle = {
  fontSize: fonts.body,
  ...fonts.getFontFamilyWeight('bold'),
  lineHeight: lineHeight.body,
  color: colors.black,
};

export const detailWrap = {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignContent: 'center',
  paddingVertical: 10,
};

export const detailLabelWrap = {
  flexDirection: 'row',
  alignContent: 'center',
};

export const detailLabelStyle = {
  paddingTop: 4,
  paddingLeft: 10,
  color: colors.labelColor,
};

export const detailValueWrap = {
  paddingLeft: 20,
  flexDirection: 'row',
  flex: 1,
  flexWrap: 'wrap',
  justifyContent: 'flex-end',
};

export const detailValueStyle = {
  textAlign: 'right',
};

