/**
*
* TextField
*
*/

import React from 'react';
// import styled from 'styled-components';

class RenderStaticComponentsComponent extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div> This is web RenderStaticComponentsComponent component.</div>
    );
  }
}

RenderStaticComponentsComponent.propTypes = {

};

export default RenderStaticComponentsComponent;
