import { colors, fonts, lineHeight, sizes } from '../../../configs/styleVars';

export const wrapperStyle = {
  paddingHorizontal: sizes.appPaddingHorizontal,
  flex: 1,
  justifyContent: 'space-between',
};

export const headerStyle = {
  fontSize: fonts.h3,
  ...fonts.getFontFamilyWeight('bold'),
  lineHeight: lineHeight.h3,
  color: colors.black,
};

export const detailWrap = {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'flex-start',
  paddingVertical: 15,
  borderBottomWidth: 1,
  borderBottomColor: colors.underlineColor,
};

export const listWrap = {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  paddingVertical: 15,
};

export const labelWrap = {
  flex: 1,
  flexDirection: 'row',
  alignItems: 'center',
  paddingLeft: 10,
};

export const textStyle = {
  paddingLeft: 10,
  fontSize: fonts.label,
  ...fonts.getFontFamilyWeight('bold'),
  lineHeight: lineHeight.label,
  flex: 1,
  flexWrap: 'wrap',
};

export const listTouchWrap = {
  borderBottomWidth: 1,
  borderBottomColor: colors.underlineColor,
};

export const textWrapStyle = {
  flexDirection: 'row',
};
