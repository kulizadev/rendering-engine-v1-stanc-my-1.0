import React from 'react';
// import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import Accordion from '../../Accordion';
import ModalOverlay from '../ModalOverlay';
import { ListItem } from '../PaymentOptionsModal/index';
import * as styles from './styles.native';

export default class FaqCategory extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      showPayOptionsModal: false,
    };
  }

  getAccordion = () => this.props.faqs && this.props.faqs.map((faq) => (
    <View
      style={styles.accordionStyle}
    >
      <Accordion
        headerNode={<ListItem
          label={faq.ques}
          key={faq.ques}
          listWrapStyle={styles.listWrapStyle}
          onPress={() => {}}
          textStyle={styles.accrodionTextStyle}
        />}
      >
        <Text
          style={styles.accordionAnsStyle}
        >{faq.ans}</Text>

      </Accordion>
    </View>
  ))

  handlePayMethodModal = (flag) => {
    this.setState({ showPayOptionsModal: flag });
  }
  render() {
    return (
      <View>
        <ListItem
          label={this.props.title}
          leftIcon={'faq'}
          rightIcon={'rightArrow'}
          key={this.props.title}
          onPress={() => this.handlePayMethodModal(true)}
          style={styles.listItemStyle}
          textStyle={styles.listTextStyle}
        />
        <ModalOverlay
          onBack={() => this.handlePayMethodModal(false)}
          isVisible={this.state.showPayOptionsModal}
          headerType={'white'}
        >
          <View style={styles.modalWrapStyle}>
            <View>
              <Text
                style={styles.modalTextStyle}
              >{this.props.title}</Text>
            </View>
            <View
              style={styles.accordionWrapStyle
            }
            >
              {this.getAccordion()}
            </View>
          </View>
        </ModalOverlay>
      </View>
    );
  }
}
