import { fonts, lineHeight, sizes, colors } from '../../../configs/styleVars';

export const listWrapStyle = {
  alignItems: 'flex-start',
};

export const accrodionTextStyle = {
  ...fonts.getFontFamilyWeight('bold'),
  fontSize: fonts.body,
  lineHeight: lineHeight.body,
}
;
export const accordionAnsStyle = {
  paddingLeft: 20,
  ...fonts.getFontFamilyWeight(),
  fontSize: fonts.body,
  lineHeight: lineHeight.body,
};

export const modalWrapStyle = {
  flex: 1,
};
export const modalTextStyle = {
  ...fonts.getFontFamilyWeight('bold'),
  fontSize: fonts.h3,
  paddingHorizontal: sizes.appPaddingHorizontal,
  lineHeight: lineHeight.h3,
  paddingBottom: 20,
};

export const accordionWrapStyle = {
  flex: 1,
  paddingRight: 20,
};

export const accordionStyle = {
  borderBottomWidth: 1,
  borderBottomColor: colors.underlineColor,
};


export const listItemStyle = {
  borderBottomWidth: 1,
  borderBottomColor: colors.underlineColor,
};

export const listTextStyle = { ...fonts.getFontFamilyWeight(),
  fontSize: fonts.body,
  lineHeight: lineHeight.body,
};
