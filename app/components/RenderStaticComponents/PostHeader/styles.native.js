import { Dimensions } from 'react-native';
import { sizes, fonts, lineHeight,
    colors } from '../../../configs/styleVars';

export const wrapperStyles = {
  flexDirection: 'row',
  justifyContent: 'space-between',
  paddingHorizontal: sizes.appPaddingHorizontal,
  paddingVertical: 20,
  backgroundColor: colors.transparentColor,
  top: 0,
  left: 0,
  width: Dimensions.get('window').width,
  zIndex: 1,
  position: 'absolute',
};

export const userDataWrap = { flexDirection: 'row', alignItems: 'center' };

export const imageStyle = { width: 24, height: 24, borderRadius: 12, marginRight: 5 };

export const textStyle = {
  ...fonts.getFontFamilyWeight('bold'),
  fontSize: fonts.label,
  lineHeight: lineHeight.label,
  color: colors.white,
  marginRight: 5,
};

export const whiteHeader = {
  backgroundColor: colors.white,
};
