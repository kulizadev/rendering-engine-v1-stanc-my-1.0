import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import hamMenu from '../../../images/hamburger.png';
import hamMenuGreen from '../../../images/hamMenuGreen.png';
import profile from '../../../images/profile.png';
import notificationIcon from '../../../images/notification.png';
import { wrapperStyles, userDataWrap, imageStyle, textStyle } from './styles.native';
import { toggleDrawer } from '../../../utilities/NavigationService.native';
const whiteHeaderMap = ['Support', 'Settings', 'Faqs', 'Notification'];
const hideProfileMap = ['Profile'];

class PostHeader extends React.PureComponent {

  onMenuPress = () => {
    toggleDrawer();
  }

  onProfilePress = () => {
    this.props.navigateToScreen('Profile');
  }

  render() {
    let newWrapperStyles = { ...wrapperStyles };
    const { dashboardData, currentScreen } = this.props;
    const name = dashboardData.borrowerFullName ? dashboardData.borrowerFullName.split(' ').splice(-1).join(' ') : '';
    const imageSrc = dashboardData.selfieLink;
    const whiteHeader = currentScreen && whiteHeaderMap.includes(currentScreen);
    let hamMenuIcon = hamMenu;
    let userData = (
      <TouchableOpacity
        onPress={this.onProfilePress}
      >
        <View style={userDataWrap}>
          <Text style={textStyle}>{name}</Text>
          <Image
            source={imageSrc ? { uri: imageSrc } : profile}
            style={imageStyle}
          />
        </View>
      </TouchableOpacity>
    );
    console.log('HEqadder this.props  ->', this.props);
    if (whiteHeader) {
      newWrapperStyles = { ...wrapperStyles, ...whiteHeader };
      hamMenuIcon = hamMenuGreen;
      userData = null;
    }
    if (hideProfileMap.includes(currentScreen)) {
      userData = null;
    }
    return (
      <View style={newWrapperStyles}>
        <TouchableOpacity
          onPress={this.onMenuPress}
        >
          <Image
            source={hamMenuIcon}
          />
        </TouchableOpacity>
        {userData}
      </View>
    );
  }
}

PostHeader.propTypes = {
  onMenuPress: PropTypes.func,
};

export default PostHeader;
