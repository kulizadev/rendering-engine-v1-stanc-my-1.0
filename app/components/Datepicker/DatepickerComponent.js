/**
*
* DatepickerComponent
*
*/

import React from 'react';
import DatepickerAbstract from './DatepickerAbstract';
import DatePicker from 'material-ui/DatePicker';
import * as styles from './DatepickerStyles';
import RenderError from '../RenderError/';
import styledComponents from '../../configs/styledComponents';

// import styled from 'styled-components';


class DatepickerComponent extends DatepickerAbstract { // eslint-disable-line react/prefer-stateless-function
  clearDate = (e) => {
    e.stopPropagation();
    this.props.onChangeHandler(null, null);
  };

  render() {
    const { errorMessage, componentProps, className } = this.props;
    let error = null;
    if (errorMessage) {
      error = (<RenderError errorMessage={errorMessage} />);
    }
    const hintText = styledComponents.getStyledHintText(this.props.hintText);
    const isCrossValid = componentProps.isDateRemovable && componentProps.value;
    const {isDateRemovable, ...compProps} = {...componentProps}
    return (
      <div style={styles.datepickerStyles.container}>
        <DatePicker
          {...this.props.datepickerStyles}
          {...compProps}
          onChange={this.props.onChangeHandler}
          onBlur={this.props.onBlurHandler}
        />
        {error}
        {hintText}
        {isCrossValid ? <span onClick={this.clearDate} style={styles.removeDateStyles} title="click to clear date">X</span> : null}
      </div>
    );
  }
}

DatepickerComponent.propTypes = {

};

DatepickerComponent.defaultProps = {
  datepickerStyles: styles.datepickerStyles,
};

export default DatepickerComponent;
