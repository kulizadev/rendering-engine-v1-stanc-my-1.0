/**
*
* DatepickerComponent
*
*/

import React from 'react';
import { View, Text, Platform, Image, Keyboard, TouchableOpacity } from 'react-native';
import DatepickerAbstract from './DatepickerAbstract';
// import styled from 'styled-components';
import TextInputField from '../TextInputField/';
import DateTimePicker from 'react-native-modal-datetime-picker';
import * as styles from './styles';
import calendarIcon from '../../images/calendar.png';
import * as common from '../../utilities/commonUtils';
import { strings } from '../../../locales/i18n';

class DatepickerComponent extends DatepickerAbstract { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.clearedDate = false;
    this.state = {
      isDatePickerVisible: false,
    };
  }

  clearDate = (e) => {
    this.clearedDate = true;
    this.onChangeHandler(null);
  };

  toggleDatePicker = () => {
    if (!this.clearedDate) {
      Keyboard.dismiss();
      this.setState({ isDatePickerVisible: !this.state.isDatePickerVisible });
    } else {
      this.clearedDate = false;
    }
  };

  renderIcon = () => (
    <Image
      style={{ marginBottom: 10 }}
      source={calendarIcon}
    />
    )

  onChangeHandler = (date) => {
    this.props.onChangeHandler(date ? this.props.formatDate(date) : null);
    this.toggleDatePicker();
  }

  render() {
    const customProps = {};
    const { date, formatDate, disabled, error, minimumDate, maximumDate, label, isDateRemovable } = this.props;
    if (Platform.OS === 'ios') {
      customProps.cancelTextIOS = strings('datepicker.cancelText');
      customProps.confirmTextIOS = strings('datepicker.confirmText');
      customProps.customTitleContainerIOS = (<Text style={styles.iosTextStyle}>{strings('datepicker.titleText')}</Text>);
    }
    const value = date ? formatDate(date) : '';
    const selectedDate = date || new Date();
    const maxMinObj = {};
    if (minimumDate) {
      maxMinObj.minimumDate = minimumDate;
    }
    if (maximumDate) {
      maxMinObj.maximumDate = maximumDate;
    }
    const isCrossValid = isDateRemovable && value;
    return (
      <View style={styles.container}>
        <TextInputField
          value={value}
          label={label}
          disabled={disabled}
          error={error}
          inline={this.props.inline}
          onFocus={this.toggleDatePicker}
          renderAccessory={!disabled && this.renderIcon}
          accessibilityLabel={this.props.accessibilityLabel}
        />
        <DateTimePicker
          date={selectedDate}
          {...maxMinObj}
          {...styles.datepickerStyles}
          isVisible={this.state.isDatePickerVisible}
          onConfirm={this.onChangeHandler}
          onCancel={this.toggleDatePicker}
        />
        {isCrossValid ? <TouchableOpacity onPress={this.clearDate} style={styles.cross}><Text>X</Text></TouchableOpacity> : null}
      </View>
    );
  }
}

DatepickerComponent.propTypes = {

};

DatepickerComponent.defaultProps = {
  formatDate: common.formatDefaultDate,
};

export default DatepickerComponent;
