/**
*
* Datepicker
*
*/

import React from 'react';
import DatepickerComponent from './DatepickerComponent';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class Datepicker extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <DatepickerComponent {...this.props} />
    );
  }
}

Datepicker.propTypes = {

};

export default Datepicker;
