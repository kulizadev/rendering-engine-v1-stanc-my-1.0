import { colors, fonts } from '../../configs/styleVars';

import { inputComponentStyle } from '../../configs/componentStyles/commonStyles';

export const input = {
  ...inputComponentStyle,
};

export const container = {
  position: 'relative',
};

export const cross = {
  position: 'absolute',
  color: colors.basicFontColor,
  right: 30,
  bottom: 35,
  fontWeight: '600',
};

export const inlineInput = {
  ...input,
  labelHeight: 0,
  labelPadding: 0,
  lineWidth: 0,
  activeLineWidth: 0,
  disabledLineWidth: 0,
  label: '',
  inputContainerStyle: {
    padding: 1,
  },
};

export const datepickerStyles = {
  container: 'inline',
};

export const iosTextStyle = {
  textAlign: 'center',
  fontSize: 20,
  paddingTop: 13,
  ...fonts.getFontFamilyWeight(),
};
