/**
 *
 * Asynchronously loads the component for RenderAutoComplete
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
