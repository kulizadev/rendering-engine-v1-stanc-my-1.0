/**
*
* RenderAutoComplete
*
*/

import React from 'react';
import TextField from 'material-ui/TextField';
import AutoComplete from 'material-ui/AutoComplete';
import RenderAutoCompleteAbstract from './RenderAutoCompleteAbstract';
import * as styles from './RenderAutoCompleteStyles';

import RenderError from '../RenderError/';

export default class RenderAutoCompleteComponent extends RenderAutoCompleteAbstract { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.dataSourceConfig = {
      text: 'name',
      value: 'name',
    };
  }

  getSelectedOption = (name) => this.state.optionsList.find((option) => option.name === name)

  handleUpdateInput = (searchText, dataSource, param) => {
    if (param.source !== 'click') {
      this.onChangeHandler(searchText);
    } else {
      const option = this.getSelectedOption(searchText);
      option && this.updateOption(option);
    }
  };

  render() {
    const newCompProps = { ...this.props.compProps, value: this.state.inputVal };
    let errorMessage = null;
    if (this.props.renderData.errorMessage) {
      errorMessage = (<RenderError errorMessage={this.props.renderData.errorMessage} />);
    }
    return (
      <div className="field-container">
        <AutoComplete
          {...newCompProps}
          searchText={this.state.inputVal.toString()}
          onUpdateInput={this.handleUpdateInput}
          dataSource={this.state.optionsList}
          dataSourceConfig={this.dataSourceConfig}
          onFocus={() => this.setListFlag(true)}
          filter={AutoComplete.fuzzyFilter}
          menuStyle={{ maxHeight: '200px', overflowY: 'auto' }}
          textFieldStyle={{ ...styles.inputStyles.inputStyle }}
          {...styles.inputStyles}
          fullWidth
          openOnFocus
        />
        {errorMessage}
      </div>
    );
  }
}
