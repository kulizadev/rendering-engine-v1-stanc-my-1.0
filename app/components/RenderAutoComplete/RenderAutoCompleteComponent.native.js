/**
*
* RenderAutoComplete
*
*/

import React from 'react';
import { View, TouchableWithoutFeedback, Keyboard, ScrollView } from 'react-native';
import Text from '../RenderText';
import RenderAutoCompleteAbstract from './RenderAutoCompleteAbstract';
import AutoCompleteOptions from '../AutoCompleteOptions/';
import TextInputField from '../TextInputField/';
import * as styles from './RenderAutoCompleteStyles';
import AutoCompleteModal from '../AutoCompleteModal';

export default class RenderAutoCompleteComponent extends RenderAutoCompleteAbstract { // eslint-disable-line react/prefer-stateless-function

  onChange = (e) => {
    this.onChangeHandler(e.nativeEvent.text);
  }

  render() {
    return (
      <View>
        <AutoCompleteModal
          compProps={this.props.compProps}
          onChange={this.onChange}
          inputVal={this.state.inputVal.toString()}
          options={this.filterOptions()}
          updateOption={this.updateOption}
          showList={this.state.showList}
          handleBackPress={() => this.setListFlag(false)}
        />
        <TextInputField
          type={'viewOnly'}
          {...this.props.compProps}
          value={this.state.inputVal}
          onPress={() => this.setListFlag(true)}
        />
      </View>
    );
  }
}
