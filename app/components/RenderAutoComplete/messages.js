/*
 * RenderAutoComplete Messages
 *
 * This contains all the text for the RenderAutoComplete component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderAutoComplete.header',
    defaultMessage: 'This is the RenderAutoComplete component !',
  },
});
