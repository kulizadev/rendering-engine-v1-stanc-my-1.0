import appVars from '../../configs/styleVars';
import styled from 'styled-components';
const colors = appVars.colors;

export const ErrorMessage = styled.div`
  text-align: left;
  width: 100%;
  color: ${colors.errorColor};
  font-size: 13px;
`;

export const ErrorMessageWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;