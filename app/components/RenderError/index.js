/**
*
* RenderError
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import RenderErrorComponent from './RenderErrorComponent';

class RenderError extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderErrorComponent {...this.props} />
    );
  }
}
/*
this.props contains:

errorMessage
*/

RenderError.propTypes = {

};

export default RenderError;
