/**
*
* RenderError
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import RenderErrorAbstract from './RenderErrorAbstract';
import { ErrorMessage, ErrorMessageWrapper } from './ErrorMessage';

export default class RenderErrorComponent extends RenderErrorAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    let { errorMessage } = {...this.props};
    if(!Array.isArray(errorMessage)){
      errorMessage = [errorMessage];
    }

    return (<ErrorMessageWrapper>
      {
        errorMessage.map((error, index) => {
          return (<ErrorMessage key={index}>{error} </ErrorMessage>)
        })
      }
      </ErrorMessageWrapper>
    )

  }
}
