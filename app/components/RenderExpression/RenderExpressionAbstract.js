import React from 'react';
import _ from 'underscore';
import expr from 'expr-eval';
import moment from 'moment';
import { appConst } from './../../configs/appConfig';

const Parser = new expr.Parser({ logical: true, comparison: true, operators: { 'in': true } });
Parser.functions.getAgeFromDate = (date, unit) => moment().diff(moment(date, appConst.defaultDateFormat), unit);
Parser.functions.getDateDiff = (startDate, endDate, unit) => Math.abs(moment(startDate, appConst.defaultDateFormat).diff(moment(endDate, appConst.defaultDateFormat), unit));

export default class RenderExpressionAbstract extends React.PureComponent {
    constructor(props) {
        super(props);
        let { formData, elmId} = { ...props }
        this.state = {
            value: formData[elmId].value
        };

    }

    componentWillReceiveProps = (nextProps) => {
        //console.log('nextProps and props', nextProps, this.props, parsedValue);
        let { renderData, formData, elmId} = { ...nextProps };
        let parsedValue = this.parseExpression(renderData, formData);

        if(isFinite(parsedValue) && (parsedValue !== this.state.value)) {
            this.props.onChangeHandler && this.props.onChangeHandler(parsedValue, elmId);
            this.setState({
                value: parsedValue
            });
        }
    }

    onChangeHandler = (elmId, value) => {
    }

    onBlurHandler = (value, elmId) => {

    }


    parseExpression = (variable, formData) => {
        let { expression, dependentFields } = { ...variable.metaData };
        let obj = {};
        dependentFields.forEach(field => {
            obj[field] = formData[field] || 0;
        });
        // console.log('expression', dependentFields, obj, expression, Parser.evaluate(expression, obj));
        return Parser.evaluate(expression, obj).toString();
    }

    render() {
        return null;
    }
}

RenderExpressionAbstract.propTypes = {
};