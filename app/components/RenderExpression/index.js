import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import ExpressionComponent from './ExpressionComponent';

class RenderExpression extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <ExpressionComponent {...this.props} />
    );
  }
}

RenderExpression.defaultProps = {
};


RenderExpression.propTypes = {

};

export default RenderExpression;