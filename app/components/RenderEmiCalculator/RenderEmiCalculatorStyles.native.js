import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const emiText = {
  style: {
    textAlign: 'center',
    fontSize: fonts.body,
    ...fonts.getFontFamilyWeight(),
    marginTop: 10,
    marginBottom: 10,
  },
};

export const emiBlock = {
  style: {
    marginTop: 30,
    marginBottom: 20,
    paddingVertical: 24,
    borderRadius: 6,
    backgroundColor: '#f1f1f1',
  },
};

export const emiValue = {
  style: {
    textAlign: 'center',
    color: colors.amountHeading,
    fontSize: fonts.h2,
    ...fonts.getFontFamilyWeight('bold'),
  },
};

export const wrapperStyles = {
  style: {
    marginTop: 10,
  },
};

export const textStyles = {
  style: {
    fontSize: fonts.description,
    lineHeight: lineHeight.description,
    ...fonts.getFontFamilyWeight(),
  },
};

export const colWrapper = {
  paddingRight: 15,
  paddingLeft: 15,
};
