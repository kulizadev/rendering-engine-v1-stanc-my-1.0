/**
*
* RenderEmiCalculator
*
*/

import React from 'react';
import RenderEmiCalculatorComponent from './RenderEmiCalculatorComponent';

class RenderEmiCalculator extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      	<RenderEmiCalculatorComponent {...this.props} />
    );
  }
}

RenderEmiCalculator.propTypes = {

};

export default RenderEmiCalculator;
