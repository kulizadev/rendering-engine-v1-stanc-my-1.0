/**
 *
 * RenderStep
 *
 */

import React from 'react';

import { behaveConfig, appConst } from '../../configs/appConfig';
import componentProps from '../../containers/RenderStep/componentProps';
import * as C from './constants';

export default class RenderEmiCalculatorAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      customValidation: {},
    };
    this.onBlurHandler = this.onBlurHandler.bind(this);
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.customValidateConf = this.customValidateConf.bind(this);
    this.customOnChangeHandler = this.customOnChangeHandler.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if ((newProps.formData[C.loanAmountKey] && this.props.formData[C.loanAmountKey] !== newProps.formData[C.loanAmountKey])
      || (newProps.formData[C.loanTenureKey] && this.props.formData[C.loanTenureKey] !== newProps.formData[C.loanTenureKey])
      || (newProps.formData[C.roiKey] && this.props.formData[C.roiKey] !== newProps.formData[C.roiKey])
    ) {
      const insuranceAmount = 0;
      const amount = newProps.formData[C.loanAmountKey] + insuranceAmount;
      const tenure = newProps.formData[C.loanTenureKey];
      const rate = (newProps.formData[C.roiKey] || appConst.roi) / 12 / 100;
      const updatedEmi = ((amount * rate) / (1 - Math.pow(1 / (1 + rate), (tenure))));
      console.log(amount, tenure, rate, updatedEmi);
      this.onChangeHandler(Math.ceil(updatedEmi), 'emi');
    }
  }

  componentDidMount() {
    this.calculateEMI();
  }

  calculateEMI = () => {
    const insuranceAmount = 0;
    const amount = this.props.formData[C.loanAmountKey] + insuranceAmount;
    const tenure = this.props.formData[C.loanTenureKey];
    const rate = (this.props.formData[C.roiKey] || appConst.roi) / 12 / 100;
    const updatedEmi = ((amount * rate) / (1 - Math.pow(1 / (1 + rate), (tenure))));
    console.log(amount, tenure, rate, updatedEmi);
    this.onChangeHandler(Math.ceil(updatedEmi), 'emi');
  }

  // On blur handler for elements
  onBlurHandler(value, elmId) {
    // Trigger validation if trigger event is blurred for input elements
    if (behaveConfig.validationTrigger === 'blur') {
      this.props.validateAndUpdateRender(value, elmId);
    }
  }

  /*
  On change may do multiple things - depending on the config setting
    --> Set form data
    --> Validate the current value
  */
  onChangeHandler(value, elmId) {
    // updateFormData
    this.props.updateFormData(elmId, value);
    // Validate things over if validation trigger is change
    if (behaveConfig.validationTrigger === 'change') {
      this.props.validateAndUpdateRender(value, elmId);
    }
  }
  customOnChangeHandler(value, elmId) {
    /* This function curates values for loan amount and security selected.
    *  and pass it further for validation.
    * */
    switch (elmId) {
      case C.securityOfferedKey :
        this.customValidateConf(
          value,
          this.props.formData[C.loanAmountKey],
          this.props.formData[C.newToCreditCustomerKey],
        );
        break;
      case C.loanAmountKey :
        this.customValidateConf(
          this.props.formData[C.securityOfferedKey],
          value,
          this.props.formData[C.newToCreditCustomerKey],
        );
        break;
      case C.newToCreditCustomerKey :
        this.customValidateConf(
          this.props.formData[C.securityOfferedKey],
          this.props.formData[C.loanAmountKey],
          value
        );
        break;
      default :
        break;
    }
  }

  customValidateConf = (security, loanAmount, newToCreditCustomer) => {
    /* This function receives the curated current values and just works on validating.
    *  and is not concerned about the key
    * */
    if (security !== C.securityOfferedCustomTriggerKey) {
      console.log(' triggerCustom Valid! ');
      this.setState(
        {
          customValidation: {
            invalidateSecurityOfferedKey: false,
            invalidationMessage: '',
          },
        }
      );
    } else if (loanAmount <= C.loanAmountCustomTriggerValueSet[newToCreditCustomer].value) {
      console.log(' triggerCustom Valid! ');
      this.setState(
        {
          customValidation: {
            invalidateSecurityOfferedKey: false,
            invalidationMessage: '',
          },
        }
      );
      // this.setState({ invalidateSecurityOfferedKey: false });
    } else {
      console.log(' triggerCustom inValid! ');
      this.setState(
        {
          customValidation: {
            invalidateSecurityOfferedKey: true,
            invalidationMessage: C.loanAmountCustomTriggerValueSet[newToCreditCustomer].message,
          },
        }
      );
    }
  };


  getComponentProps = (formType, elmId) => {
    const defaultValue = this.props.formData[elmId],
      renderData = { ...this.props.renderData };
    return componentProps[formType] && componentProps[formType](renderData[elmId], elmId, defaultValue);
  }

  render() {
    return null;
  }
}

/*
Recieved props are:
formData
renderData
fieldOrder
screenId
validateAndUpdateRender - Validates the single element and update the valid state + error messages
updateFormData -  updates the formData for a particular key

This component will render the step's form elements in the order prescribed
-> Loop over fieldOrder
-> map the respective component to the form_type or meta.type
-> Render the unit component with the respective props for that elmId
->

*/
