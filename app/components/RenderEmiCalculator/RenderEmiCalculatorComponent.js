/**
*
* RenderEmiCalculator
*
*/

import React from 'react';
import RenderEmiCalculatorAbstract from './RenderEmiCalculatorAbstract';

import RenderSlider from '../RenderSlider';
import RenderCheckbox from '../RenderCheckbox';
import RenderDropdown from '../RenderDropdown';
import RenderInputText from '../RenderInputText';
import { layoutConfig, appConst } from '../../configs/appConfig';

import { strings } from '../../../locales/i18n';
import RenderPopupForm from '../RenderPopupForm/';
import * as common from '../../utilities/commonUtils';
import * as C from './constants';
import Row from '../Row';
import Column from '../Column';

import {
  EmiContainer,
  ValueLabel,
  DeclarationBlock,
} from './styles';

export default class RenderEmiCalculatorComponent extends RenderEmiCalculatorAbstract { // eslint-disable-line react/prefer-stateless-function
  // TODO : remove rangeLabel from loanAmount. move it compProps
  render() {
    const formattedEMI = common.convertCurrencyFormat(this.props.formData.emi);
    const renderData = { ...this.props.renderData };
    const emiComponent = (<EmiContainer>
      <span>EMI is :</span>
      <ValueLabel> {formattedEMI} </ValueLabel>
    </EmiContainer>);

    let loanAmountBlock = null,
      tenureBlock = null,
      loanPurposeBlock = null,
      othersBlock = null;

    if (renderData[C.loanAmountKey]) {
      const meta = renderData[C.loanAmountKey] && renderData[C.loanAmountKey].metaData;
      let colWidth = meta.colWidth || layoutConfig.defaultColumnWidth,
        offset = meta.offset || '';

      loanAmountBlock = (
        <Column colWidth={colWidth} offset={offset} key={`compwrapper-${C.loanAmountKey}`}>
          <RenderSlider
            compProps={this.getComponentProps('slider', C.loanAmountKey)}
            elmId={C.loanAmountKey}
            onChangeHandler={this.onChangeHandler}
            renderData={renderData[C.loanAmountKey]}
          />
        </Column>);
    }
    if (renderData[C.loanTenureKey]) {
      const meta = renderData[C.loanTenureKey] && renderData[C.loanTenureKey].metaData;
      let colWidth = meta.colWidth || layoutConfig.defaultColumnWidth,
        offset = meta.offset || '';
      tenureBlock = (
        <Column colWidth={colWidth} offset={offset} key={`compwrapper-${C.loanTenureKey}`}>
          <RenderSlider
            compProps={this.getComponentProps('slider', C.loanTenureKey)}
            elmId={C.loanTenureKey}
            rangeLabel={strings('renderEmiCal.monthsLabel')}
            onChangeHandler={this.onChangeHandler}
            renderData={renderData[C.loanTenureKey]}
          />
        </Column>);
    }
    if (renderData[C.loanPurposeKey] && !renderData[C.loanPurposeKey].isHidden) {
      const meta = renderData[C.loanPurposeKey] && renderData[C.loanPurposeKey].metaData;
      let colWidth = meta.colWidth || layoutConfig.defaultColumnWidth,
        offset = meta.offset || '';
      loanPurposeBlock = (
        <Column colWidth={colWidth} offset={offset} key={`compwrapper-${C.loanPurposeKey}`}>
          <RenderDropdown
            compProps={this.getComponentProps('dropdown', C.loanPurposeKey)}
            elmId={C.loanPurposeKey}
            onChangeHandler={this.onChangeHandler}
            renderData={renderData[C.loanPurposeKey]}
            formData={this.props.formData}
            updateFormData={this.props.updateFormData}
            toggleScroll={this.props.toggleScroll}
          />
        </Column>);
    }
    if (renderData[C.othersKey] && !renderData[C.othersKey].isHidden) {
      const meta = renderData[C.othersKey] && renderData[C.othersKey].metaData;
      let colWidth = meta.colWidth || layoutConfig.defaultColumnWidth,
        offset = meta.offset || '';
      othersBlock = (
        <Column colWidth={colWidth} offset={offset} key={`compwrapper-${C.othersKey}`}>
          <RenderInputText
            compProps={this.getComponentProps('text', C.othersKey)}
            elmId={C.othersKey}
            onChangeHandler={this.onChangeHandler}
            renderData={renderData[C.othersKey]}
            formData={this.props.formData}
            updateFormData={this.props.updateFormData}
            toggleScroll={this.props.toggleScroll}
          />
        </Column>);
    }

    if (renderData[C.loanAmountKey] && renderData[C.loanTenureKey] && renderData[C.roiKey]) {
      return (<div>
        <Row>
          {loanAmountBlock}
          {tenureBlock}
        </Row>
        <Row>
          {emiComponent}
        </Row>
        <Row>
          {loanPurposeBlock}
          {othersBlock}
        </Row>
      </div>);
    }


    return null;
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
onBlur Fn
*/
