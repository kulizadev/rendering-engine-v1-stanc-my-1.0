export const loanAmountKey = 'loanAmount';
export const loanTenureKey = 'tenure';
export const roiKey = 'roi';
export const loanPurposeKey = 'loanPurpose';
export const othersKey = 'pleaseSpecify2';
