/**
 *
 * Loader Web
 *
 */


import React from 'react';
import LoaderAbstract from './LoaderAbstract';
import * as styles from './LoaderStyles';
import loaderSrc from '../../images/loader.gif';
import { strings } from '../../../locales/i18n';
import CircularProgress from 'material-ui/CircularProgress';

export default class LoaderComponent extends LoaderAbstract { // eslint-disable-line react/prefer-stateless-function

  showPopup = () => {
    if (this.popupDialog) {
      this.popupDialog.show();
    }
  }

  hidePopup = () => {
    if (this.popupDialog) {
      this.popupDialog.dismiss();
    }
  }

  render() {
    const { message, isLongReq } = this.props.loaderStatus;

    let { isVisible } = this.props.loaderStatus;

    isVisible = !!isVisible;
    const defaultMessage = strings('loaders.processingRequest');
    let loader = (<CircularProgress {...styles.loaderIcon} />);
    if (isLongReq) {
      loader = (
        <div style={styles.snackbarWrapperStyles}>
          <div style={styles.snackbarContentStyles}>
            <p style={styles.messageStyles}>{message || defaultMessage}</p>
            <img src={loaderSrc} style={styles.snackbarLoaderStyles} alt={'loaderImg'} />
          </div>
        </div>
            );
    }
    return (
      <div>
        {
        isVisible ? (
          <div {...styles.containerStyles}>
            {loader}
          </div>
        ) : ''
        }
      </div>
    );
  }
}

