import { Dimensions, Platform } from 'react-native';
import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const containerStyles = {
  style: {
    backgroundColor: colors.overlayColor,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
};

export const hiddenStyles = {
  style: {
    width: 0,
    height: 0,
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: colors.transparent,
  },
};

export const loaderIcon = {
  size: Platform.OS === 'ios' ? 'large' : 55,
  color: colors.primaryBGColor,
};

export const snackbarWrapperStyles = {
  position: 'absolute',
  bottom: 35,
  paddingHorizontal: 20,
  width: '100%',
};

export const snackbarContentStyles = {
  flexDirection: 'row',
  flex: 1,
  paddingVertical: 10,
  paddingHorizontal: 15,
  backgroundColor: colors.white,
  alignItems: 'center',
  justifyContent: 'space-between',
};

export const snackbarLoaderStyles = {
  height: 40,
  width: 40,
};

export const messageStyles = {
  ...fonts.getFontFamilyWeight(),
  fontSize: fonts.description,
  lineHeight: lineHeight.description,
  shadowOffset: { width: 0, height: 2 },
  shadowOpacity: 0.5,
  shadowRadius: 0,
  elevation: 2,
  shadowColor: 'rgb(202, 202, 202)',
  flex: 1,
};
