/**
 *
 * Loader Native
 *
 */


import React from 'react';
import { View, ActivityIndicator, Modal, Text, Image } from 'react-native';
import LoaderAbstract from './LoaderAbstract';
import * as styles from './LoaderStyles';
import loaderSrc from '../../images/loader.gif';
import { strings } from '../../../locales/i18n';

export default class LoaderComponent extends LoaderAbstract { // eslint-disable-line react/prefer-stateless-function

  showPopup = () => {
    if (this.popupDialog) {
      this.popupDialog.show();
    }
  }

  hidePopup = () => {
    if (this.popupDialog) {
      this.popupDialog.dismiss();
    }
  }

  render() {
    const { message, isLongReq } = this.props.loaderStatus;
    let { isVisible } = this.props.loaderStatus;
    if (isVisible === undefined) {
      isVisible = false;
    }
    const defaultMessage = strings('loaders.processingRequest');
    let loader = (<ActivityIndicator {...styles.loaderIcon} />);
    if (isLongReq) {
      loader = (
        <View style={styles.snackbarWrapperStyles}>
          <View style={styles.snackbarContentStyles}>
            <Text style={styles.messageStyles}>{message || defaultMessage}</Text>
            <Image
              source={loaderSrc}
              style={styles.snackbarLoaderStyles}
            />
          </View>
        </View>
      );
    }
    return (
      <Modal
        animationType={'none'}
        onRequestClose={this.hidePopup}
        transparent
        visible={isVisible}
        supportedOrientations={['portrait', 'landscape']}
      >
        <View {...styles.containerStyles}>
          {loader}
        </View>
      </Modal>
    );
  }
}

