/**
*
* RenderSideNavComponent
*
*/

import React from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import RenderSideNavAbstract from './RenderSideNavAbstract';
import ListLabel from '../ListLabel';
import * as styles from './sideNavStyles';
// import styled from 'styled-components';


class RenderSideNavComponent extends RenderSideNavAbstract { // eslint-disable-line react/prefer-stateless-function

  _buildNavLIst = () => {
    const { labels } = this.props;
    const self = this;
    return labels.map((label, index) => {
      let rightIconType = 'rightArrow',
        isActiveStep = index === this.state.currentActiveIndex,
        isCompletedStep = index < this.state.currentActiveIndex;

      let labelStyles = styles.navItemLabel,
        wrapperStyles = styles.navItemWrapper;

      if (isActiveStep) {
        labelStyles = styles.navItemLabelActive;
        wrapperStyles = styles.navItemWrapperActive;
      }
      if (isCompletedStep) {
        labelStyles = styles.navItemLabelCompleted;
        wrapperStyles = styles.navItemWrapperCompleted;
      }

      return (<TouchableOpacity onPress={() => self.onPress(index)} key={`nav-link-${index}`}>
        <View style={wrapperStyles}>
          <Text style={labelStyles}>{label}</Text>
        </View>
      </TouchableOpacity>);
    });
  }

  render() {
    const stepBlock = this._buildNavLIst();
    return (
      <View style={{ flex: 1 }}>
        <ScrollView
          keyboardShouldPersistTaps="always"
          ref="_scrollView"
          onStartShouldSetResponderCapture={() => false}
          scrollEnabled
          automaticallyAdjustContentInsets={false}
        >
          {stepBlock}
        </ScrollView>
      </View>
    );
  }
}

RenderSideNavComponent.propTypes = {

};

export default RenderSideNavComponent;
