/**
*
* RenderSideNavComponent
*
*/

import React from 'react';
import RenderSideNavAbstract from './RenderSideNavAbstract';
// import styled from 'styled-components';
import * as styles from './sideNavStyles';

class RenderSideNavComponent extends RenderSideNavAbstract { // eslint-disable-line react/prefer-stateless-function

  _buildNavLIst = () => {
    const { labels } = this.props;
    const self = this;
    return labels.map((label, index) => {
      let rightIconType = 'rightArrow',
        isActiveStep = index === this.state.currentActiveIndex,
        isCompletedStep = index < this.state.currentActiveIndex;
      let labelStyles = styles.navItemLabel,
        wrapperStyles = styles.navItemWrapper;

      if (isActiveStep) {
        labelStyles = styles.navItemLabelActive;
        wrapperStyles = styles.navItemWrapperActive;
      }
      if (isCompletedStep) {
        labelStyles = styles.navItemLabelCompleted;
        wrapperStyles = styles.navItemWrapperCompleted;
      }

      return (<div style={wrapperStyles} onClick={() => self.onPress(index)} key={`nav-link-${index}`}>
        <div style={labelStyles}>{label}</div>
      </div>);
    });
  }

  render() {
    const stepBlock = this._buildNavLIst();
    return (
      <div style={styles.navContainer}>
        {stepBlock}
      </div>
    );
  }
}

RenderSideNavComponent.propTypes = {

};

export default RenderSideNavComponent;
