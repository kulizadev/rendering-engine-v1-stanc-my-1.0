import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const navItemWrapper = {
  padding: 15,
  backgroundColor: colors.white,
  borderBottomWidth: 1,
  borderBottomColor: colors.borderLight,
  opacity: 0.6,
};

export const navItemWrapperActive = {
  ...navItemWrapper,
  backgroundColor: colors.primaryBGColor,
  opacity: 1,
};

export const navItemWrapperCompleted = {
  ...navItemWrapper,
  opacity: 1,
};

export const navItemLabel = {
  fontSize: fonts.body,
  color: colors.basicFontColor,
};

export const navItemLabelActive = {
  ...navItemLabel,
  color: colors.white,
};

export const navItemLabelCompleted = {
  ...navItemLabel,
};
