/**
*
* RenderSideNav
*
*/

import React from 'react';
import RenderSideNavComponent from './RenderSideNavComponent';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class RenderSideNav extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderSideNavComponent {...this.props} />
    );
  }
}

RenderSideNav.propTypes = {

};

export default RenderSideNav;
