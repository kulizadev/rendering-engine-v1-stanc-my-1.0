/**
*
* RenderMultiDropdown Abstract
*
*/

import React from 'react';
import PropTypes from 'prop-types';

export default class RenderMultiDropdownAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.onChangeHandler = this.onChangeHandler.bind(this);
  }

  onChangeHandler(value) {
    const { onChangeHandler, elmId } = this.props;
    onChangeHandler(value, elmId);
  }

  render() {
    return null;
  }

}

RenderMultiDropdownAbstract.propTypes = {
  onChangeHandler: PropTypes.func,
  elmId: PropTypes.string,
};
