/**
*
* RenderMultiDropdown
*
*/

import React from 'react';
import RenderMultiDropdownComponent from './RenderMultiDropdownComponent';

export default class RenderMultiDropdown extends React.PureComponent {
  render() {
    return <RenderMultiDropdownComponent {...this.props} />;
  }
}
