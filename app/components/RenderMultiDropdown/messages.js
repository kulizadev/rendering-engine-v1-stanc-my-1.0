/*
 * RenderMultiDropdown Messages
 *
 * This contains all the text for the RenderDropdown component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderMultiDropdown.header',
    defaultMessage: 'This is the RenderMultiDropdown component !',
  },
});
