/**
 *
 * TextField
 *
 */

import React from 'react';
import RenderTextComponent from './RenderTextComponent';

const RenderText = (props) => <RenderTextComponent {...props} />;

export default RenderText;
