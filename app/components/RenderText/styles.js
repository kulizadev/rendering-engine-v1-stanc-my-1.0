import { inputComponentStyle, label } from '../../configs/componentStyles/commonStyles';
import { fonts, lineHeight } from '../../configs/styleVars';

const getTextStyle = (type) => ({
  fontSize: fonts[type],
  lineHeight: lineHeight[type],
  ...fonts.getFontFamilyWeight(),
});
export const h1 = {
  fontSize: fonts.h1,
  ...fonts.getFontFamilyWeight(),
  lineHeight: lineHeight.h1,
};

const textStyle = {
  h1: getTextStyle('h1'),
  h2: getTextStyle('h2'),
  h3: getTextStyle('h3'),
  h4: getTextStyle('h4'),
  description: getTextStyle('description'),
  body: getTextStyle('body'),
};
export default textStyle;
