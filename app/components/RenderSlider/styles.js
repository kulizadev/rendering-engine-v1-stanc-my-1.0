import styled from 'styled-components';
import { fonts } from '../../configs/styleVars';

export const SliderContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
`;

export const SliderWrapper = styled.div`
    display: flex;
    width: 100%;
`;

export const SliderLabel = styled.span`
    margin-top:  28px;
    white-space: nowrap;
`;

export const LeftLabel = styled(SliderLabel)`
    margin-right: 10px;
`;

export const RightLabel = styled(SliderLabel)`
    margin-left: 10px;
`;

export const ValueLabel = styled.span`
    font-weight: bold;
`;

export const LabelBlock = styled.div`
    text-align: center;
    font-size: ${fonts.fontSize};
`;
