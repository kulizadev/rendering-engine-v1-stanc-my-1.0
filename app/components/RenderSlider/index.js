/**
*
* RenderSlider
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import RenderSliderComponent from './RenderSliderComponent';

class RenderSlider extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderSliderComponent {...this.props} />
    );
  }
}

RenderSlider.propTypes = {

};

export default RenderSlider;
