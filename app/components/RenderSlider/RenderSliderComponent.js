/**
*
* RenderSlider
*
*/

import React from 'react';
// import styled from 'styled-components';

import Slider from 'material-ui/Slider';
import RenderSliderAbstract from './RenderSliderAbstract';
import RenderError from '../RenderError/';
import {
  SliderContainer,
  SliderWrapper,
  LeftLabel,
  RightLabel,
  ValueLabel,
  LabelBlock,
} from './styles';

import * as common from '../../utilities/commonUtils';

export default class RenderSliderComponent extends RenderSliderAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { compProps } = this.props;
    const { rangeLabel } = this.props;

    const meta = this.props.renderData && this.props.renderData.metaData,
      prefix = meta.prefix || null,
      suffix = meta.suffix || null,
      labelValue = compProps.value,
      formattedLabel = compProps.isCurrency ? common.getShortCurrency(labelValue) : labelValue,
      minValue = !compProps.isStepIrregluar ? compProps.min : compProps.valArr[0],
      maxValue = !compProps.isStepIrregluar ? compProps.max : compProps.valArr[compProps.valArr.length - 1];
    const label = this.props.renderData.name;
    const labelBlock = (
      <LabelBlock className="center-align">{label} is:
         <ValueLabel>{prefix} {formattedLabel} {suffix}</ValueLabel>
      </LabelBlock>);

    let errorMessage = null;
    if (this.props.renderData.errorMessage) {
      errorMessage = (<RenderError errorMessage={this.props.renderData.errorMessage} />);
    }

    if (compProps.isStepIrregluar) {
      compProps.value = compProps.sliderStepIndex;
    }
    const { isCurrency, valArr, isStepIrregluar, sliderStepIndex, ...componentProps } = compProps;

    return (
      <SliderContainer>
        <SliderWrapper>
          <LeftLabel>{common.convertNumber(minValue)}{rangeLabel}</LeftLabel>
          <Slider
            sliderStyle={{ marginBottom: '20px', marginTop: '30px' }}
            style={{ width: 'inherit' }}
            {...componentProps}
            onChange={this.onChangeHandler}
          >
          </Slider>
          <RightLabel>{common.convertNumber(maxValue)}{rangeLabel}</RightLabel>
        </SliderWrapper>
        {labelBlock}
        {errorMessage}
      </SliderContainer>
    );
  }
}

