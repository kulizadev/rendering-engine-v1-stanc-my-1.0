/**
*
* RenderSlider
*
*/

import React from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import Text from '../RenderText';
import Slider from 'react-native-slider';

import RenderSliderAbstract from './RenderSliderAbstract';
import styleVars from '../../configs/styleVars';
import * as styles from './styles';
import decreaseIcon from '../../images/decrease.png';
import increaseIcon from '../../images/increase.png';
import * as common from '../../utilities/commonUtils';

import { strings } from '../../../locales/i18n';

export default class RenderSliderComponent extends RenderSliderAbstract { // eslint-disable-line react/prefer-stateless-function

  onChangeHandler = (value) => {
    const meta = this.props.renderData && this.props.renderData.metaData;
    const val = this.props.compProps.isStepIrregluar ? this.props.compProps.valArr[value] : value;
    this.props.onChangeHandler(val, this.props.elmId);
  }

  render() {
    const { compProps } = this.props;
    let meta = this.props.renderData && this.props.renderData.metaData,
      prefix = meta.prefix || null,
      suffix = meta.suffix || null,
      labelValue = compProps.value,
      formattedLabel = compProps.isCurrency ? common.getShortCurrency(labelValue) : labelValue,
      minValue = !compProps.isStepIrregluar ? compProps.minimumValue : compProps.valArr[0],
      maxValue = !compProps.isStepIrregluar ? compProps.maximumValue : compProps.valArr[compProps.valArr.length - 1];
    const label = this.props.renderData.name;
    if (suffix === 'months') {
      suffix = strings('renderEmiCal.monthsLabel');
    }
    const labelBlock = (
      <View {...styles.mainWrapper}>
        <Text {...styles.label}>{label}</Text>
        <Text {...styles.amount}>{prefix} {formattedLabel} {suffix}</Text>
      </View>
        );

    const { colors } = styleVars;
    // TODO: change implementation

    if (compProps.isStepIrregluar) {
      compProps.value = compProps.sliderStepIndex;
    }
    const rangeLabel = this.props.rangeLabel ? ` ${this.props.rangeLabel}` : '';
    return (
      <View
        {...styles.container}
      >
        {labelBlock}
        <View
          {...styles.mainWrapper}
        >
          <TouchableOpacity
            onPress={this.decreaseSlider}
          >
            <Image
              source={decreaseIcon}
              {...styles.icon}
            />
          </TouchableOpacity>
          <View
            {...styles.sliderWrapper}
          >
            <Slider
              {...styles.slider}
              {...compProps}

              onValueChange={this.onChangeHandler}
            />
            <View
              {...styles.minMaxWrapper}
            >
              <Text {...styles.minMax}>{common.convertNumber(minValue)}{rangeLabel}</Text>
              <Text {...styles.minMax}>{common.convertNumber(maxValue)}{rangeLabel}</Text>
            </View>
          </View>
          <TouchableOpacity
            onPress={this.increaseSlider}
          >
            <Image
              source={increaseIcon}
              {...styles.icon}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

