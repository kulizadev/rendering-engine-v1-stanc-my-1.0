import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const iconWrapperStyles = {
  overflow: 'visible',
  alignItems: 'center',
  position: 'absolute',
  width: '100%',
  elevation: 101,
};

export const iconStyles = {
  height: 100,
  width: 100,
  zIndex: 100,
  elevation: 101,
};

export const contentWrapperStyles = {
  backgroundColor: colors.white,
  paddingBottom: 20,
  shadowColor: colors.inputColor,
  shadowOffset: { width: 0, height: 8 },
  shadowOpacity: 0.3,
  shadowRadius: 30,
  borderRadius: 6,
};

export const contentWrapperWithIcon = {
  ...contentWrapperStyles,
  marginTop: 50,
  elevation: 10,
  paddingTop: 10,
};

export const wrapperStyles = {
  marginTop: 10,
};

export const amountWrapper = {
  paddingTop: 30,
  paddingBottom: 20,
  paddingHorizontal: 36,
};

export const mainLabelStyles = {
  fontSize: fonts.body,
  ...fonts.getFontFamilyWeight(),
  lineHeight: lineHeight.body,
  textAlign: 'center',
  color: colors.labelColor,
};

export const amountStyles = {
  textAlign: 'center',
  fontSize: fonts.h2,
  ...fonts.getFontFamilyWeight('bold'),
  lineHeight: lineHeight.h2,
  color: colors.amountHeading,
};

export const listWrapperStyles = {
  paddingHorizontal: 36,
};

export const rowStyles = {
  flexDirection: 'row',
  paddingVertical: 5,
};

export const labelStyles = {
  fontSize: fonts.body,
  ...fonts.getFontFamilyWeight(),
  lineHeight: lineHeight.body,
  color: colors.labelColor,
  flex: 1,
};

export const valueStyles = {
  ...labelStyles,
  color: colors.inputColor,
};

export const dotStyles = {
  width: 10,
  height: 10,
  borderRadius: 10,
  backgroundColor: 'rgb(240,240,240)',
};

export const greenDotStyles = {
  ...dotStyles,
  backgroundColor: 'rgb(126, 201, 170)',
};

export const borderWrapper = {
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignContent: 'center',
  marginBottom: 5,
};

export const borderCornerLeft = {
  width: 0,
  height: 0,
  borderTopRightRadius: 10,
  borderBottomRightRadius: 10,
  borderTopWidth: 11,
  borderBottomWidth: 11,
  borderLeftWidth: 0,
  borderRightWidth: 11,
  borderColor: 'rgb(240,240,240)',
  alignSelf: 'flex-start',
};

export const borderCornerLeftGreen = {
  ...borderCornerLeft,
  borderColor: colors.primaryBGColor,
};

export const borderCornerRight = {
  width: 0,
  height: 0,
  backgroundColor: 'rgb(240,240,240)',
  borderTopLeftRadius: 10,
  borderBottomLeftRadius: 10,
  borderTopWidth: 11,
  borderBottomWidth: 11,
  borderLeftWidth: 0,
  borderRightWidth: 11,
  borderColor: 'rgb(240,240,240)',
  alignSelf: 'flex-end',
};

export const borderCornerRightGreen = {
  ...borderCornerRight,
  borderColor: colors.primaryBGColor,
};
