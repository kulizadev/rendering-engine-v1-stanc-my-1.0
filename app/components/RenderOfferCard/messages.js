/*
 * RenderOfferCard Messages
 *
 * This contains all the text for the RenderOfferCard component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderOfferCard.header',
    defaultMessage: 'This is the RenderOfferCard component !',
  },
});
