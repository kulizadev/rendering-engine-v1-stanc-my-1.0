/**
*
* RenderOfferCardComponent
*
*/

import React from 'react';
import RenderOfferCardAbstract from './RenderOfferCardAbstract';
// import styled from 'styled-components';


class RenderOfferCardComponent extends RenderOfferCardAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>This is RenderOfferCard Component</div>
    );
  }
}

RenderOfferCardComponent.propTypes = {

};

export default RenderOfferCardComponent;
