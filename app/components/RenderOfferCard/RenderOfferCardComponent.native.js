/**
*
* RenderOfferCardComponent
*
*/

import React from 'react';
import { View, Text, Image, Platform } from 'react-native';
import RenderOfferCardAbstract from './RenderOfferCardAbstract';
import iconSrc from '../../images/cashIcon.png';
import * as common from '../../utilities/commonUtils';
import {
  iconWrapperStyles,
  iconStyles,
  contentWrapperStyles,
  contentWrapperWithIcon,
  wrapperStyles,
  amountWrapper,
  mainLabelStyles,
  amountStyles,
  listWrapperStyles,
  rowStyles,
  labelStyles,
  valueStyles,
  dotStyles,
  greenDotStyles,
  borderWrapper,
  borderCornerLeft,
  borderCornerRight,
  borderCornerLeftGreen,
  borderCornerRightGreen,
} from './styles';

class RenderOfferCardComponent extends RenderOfferCardAbstract { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      cardWidth: 0,
    };
  }

  getList = () => {
    const { fieldOrder, headingField, renderData } = this.props;
    const list = [];
    if (fieldOrder) {
      fieldOrder.forEach((fieldId) => {
        const data = renderData[fieldId];
        if (fieldId !== headingField && renderData[fieldId].metaData && renderData[fieldId].metaData.form_type !== 'download' && renderData[fieldId].metaData.form_type !== 'hidden') {
          list.push({
            label: data.name,
            value: data.value || '',
            isCurrency: data.metaData && data.metaData.isCurrency,
            suffix: data.metaData.suffix || '',
          });
        }
      });
    }
    return list;
  }

  getRow = (data) => (
    <View style={rowStyles} key={data.label}>
      <Text style={labelStyles}>{data.label}</Text>
      <Text style={valueStyles}>{data.isCurrency ? common.convertCurrencyFormat(data.value) : `${data.value} ${data.suffix}`.trim() }</Text>
    </View>
  );

  getBorder = () => {
    const { showIcon } = this.props;
    const borderDots = [];
    const { cardWidth } = this.state;
    const dotViewStyles = showIcon ? dotStyles : greenDotStyles;
    for (let i = 0; i < cardWidth / 30; i++) {
      borderDots.push((
        <View style={dotViewStyles} />
      ));
    }
    return borderDots;
  }

  onLayout = (event) => {
    this.setState({ cardWidth: event.nativeEvent.layout.width });
  }

  render() {
    const { showIcon, formData, headingField, renderData } = this.props;
    const { cardWidth } = this.state;
    const list = this.getList();
    let renderIcon = null;
    let renderBorder = null;
    const formattedLoanAmount = common.convertCurrencyFormat(formData[headingField]);
    const iconWrapper = { ...iconWrapperStyles };
    if (Platform.OS === 'ios') {
      iconWrapper.zIndex = 100;
    }
    let contentWrap = contentWrapperStyles;
    if (showIcon) {
      renderIcon = (
        <View style={{ ...iconWrapper }}>
          <Image source={iconSrc} style={iconStyles} />
        </View>
      );
      contentWrap = contentWrapperWithIcon;
    }
    if (cardWidth) {
      const leftIconStyle = showIcon ? borderCornerLeft : borderCornerLeftGreen;
      const rightIconStyle = showIcon ? borderCornerRight : borderCornerRightGreen;
      renderBorder = (
        <View style={borderWrapper}>
          <View style={leftIconStyle}></View>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around', alignContent: 'center', marginTop: 5, marginHorizontal: 5 }}>
            {this.getBorder()}
          </View>
          <View style={rightIconStyle}></View>
        </View>
      );
    }
    return (
      <View style={wrapperStyles}>
        {renderIcon}
        <View style={contentWrap} onLayout={this.onLayout}>
          <View style={amountWrapper}>
            <Text style={mainLabelStyles}>{renderData[headingField].name}</Text>
            <Text style={amountStyles}>{formattedLoanAmount}</Text>
          </View>
          {renderBorder}
          <View style={listWrapperStyles}>
            {list.map((listItem) => this.getRow(listItem))}
          </View>
          { this.props.children }
        </View>
      </View>
    );
  }
}

RenderOfferCardComponent.propTypes = {

};

export default RenderOfferCardComponent;
