/**
 *
 * RenderPopupForm abstract
 * Ideally all the common functionality B/W web and Native apps comes here
 *
 */

import React from 'react';
import { getSubJourneyErrorMessage, subJourneyIds } from '../../containers/RenderSubJourney/SubJourneyComponents/subJourneyService';

export default class RenderPopupFormAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

    constructor(props) {
        super(props);
        this.state = {
            fieldUpdateKey: 'fieldUpdate',
            click: 'click',
            triggerOnFieldUpdate: false,
            relatedFieldIdsToCheck: [],
            isDataSame: false,
        };
    }
    // componentWillReceiveProps(newProps) {
    //   // Set the props in the outer popup container
    //   this.props.setPopupData({
    //     renderData: newProps.renderData,
    //     formData: newProps.formData,
    //   });
    // }

    componentDidMount() {
        console.debug('componentDidMount firing', this.props);
        this.setTriggers(this.props);
        window.setTimeout(() => {
            this.checkShowPopup(this.props);
        });
    }

    componentWillUnmount() {
        this.props.setPopupData({
            renderData: null,
            formData: null,
            showPopup: false,
        });
    }

    componentWillReceiveProps(newProps) {
        const self = this;
        this.setTriggers(newProps);
        console.debug('componentWillReceiveProps firing', newProps, this.isFormDataSame(newProps, this.props));
        this.setState({ isDataSame: this.isFormDataSame(newProps, this.props) }, () => {
            console.debug('before timeout things out ', self.state.isDataSame);
            if (!self.state.isDataSame) {
                // window.setTimeout(() => {
                console.debug('calling things out ', self.state.isDataSame);
                self.checkShowPopup(newProps);
                // })
            }
        });
    }

    isFormDataSame = (newData, oldData) => {
        const relatedFieldIdsToCheck = this.state.relatedFieldIdsToCheck;
        let isDataSame = !!this.state.relatedFieldIdsToCheck.length;
        if (this.isFormIdsExist(newData)) {
            this.state.relatedFieldIdsToCheck.forEach((fieldId) => {
                if (!oldData.formData || newData.formData[fieldId] !== oldData.formData[fieldId]) {
                    isDataSame = false;
                }
            });
        }
        return isDataSame;
    }

    setTriggers = (newProps) => {
        const self = this;
        const meta = newProps.renderData && newProps.renderData.metaData;
        if (meta) {
            if (meta.showtrigger && meta.showtrigger.indexOf(this.state.fieldUpdateKey) !== -1) {
                this.setState({ triggerOnFieldUpdate: true });
                this.setState({ relatedFieldIdsToCheck: Object.keys(meta.triggerConfig[self.state.fieldUpdateKey]) });
            } else {
                // disable the popup
                this.setState({ triggerOnFieldUpdate: false });
                this.setState({ relatedFieldIdsToCheck: [] });
            }
        } else {
            this.setState({ triggerOnFieldUpdate: false });
            this.setState({ relatedFieldIdsToCheck: [] });
        }
    }

    isFormIdsExist = (newProps) => {
        const meta = newProps.renderData && newProps.renderData.metaData;
        const newFormData = newProps.formData;
        let isExist = false;
        const formIds = newFormData && Object.keys(newFormData);
        const relatedFieldIdsToCheck = this.state.relatedFieldIdsToCheck;

        if (formIds) {
            relatedFieldIdsToCheck.forEach((id) => {
                if (formIds.indexOf(id) !== -1) {
                    isExist = true;
                }
            });
        }

        return isExist;
    }


    checkFieldUpdateValidation = (newProps) => {
        console.debug('calling checkFieldUpdateValidation', newProps);
        const self = this;
        const meta = newProps.renderData && newProps.renderData.metaData;
        const newFormData = newProps.formData;

        const triggerConfig = meta.triggerConfig;
        const relatedFieldIdsToCheck = this.state.relatedFieldIdsToCheck;

        // Below code is retained becuase of the legacy 'showTrigger' meta configuraiton
        // We can remove it once we abolish the use of showTrigger and triggerConfig in the meta for popup
        console.debug('what are relatedFieldIdsToCheck', relatedFieldIdsToCheck, triggerConfig, this.state.isDataSame);
        if (relatedFieldIdsToCheck && relatedFieldIdsToCheck.length && !this.state.isDataSame) {
            relatedFieldIdsToCheck.forEach((fieldId) => {
                const validValues = triggerConfig
                    && triggerConfig[self.state.fieldUpdateKey]
                    && triggerConfig[self.state.fieldUpdateKey][fieldId];

                let bodyText = meta && meta.bodyText;
                if (fieldId === subJourneyIds.nationalId) {
                    bodyText = getSubJourneyErrorMessage(fieldId);
                }
                if (validValues && Array.isArray(validValues)
                    && (validValues.indexOf(newFormData[fieldId]) !== -1)) {
                    console.debug('calling setPopupData in showPopup', validValues, newFormData, newFormData[fieldId]);
                    const meta = newProps.renderData && newProps.renderData.metaData;

                    self.props.setPopupData({
                        headerText: meta && meta.headerText,
                        bodyText,
                        actionButtons: meta && meta.actionButtons,
                        type: meta && meta.popupType,
                        renderData: newProps.renderData,
                        formData: newProps.formData,
                        showPopup: true,
                    });
                } else {
                    // console.debug('calling setPopupData in hide', validValues, newFormData, newFormData[fieldId]);
                    // self.props.setPopupData({
                    //   renderData: newProps.renderData,
                    //   formData: newProps.formData,
                    //   showPopup: false,
                    // });
                    // self.hidePopup();
                }
            });
        }
    }

    // Show popup using meta.conditionalMeta.hide property (if there is any expression)
    checkConditionalExpr = (newProps) => {
        const meta = newProps.renderData && newProps.renderData.metaData;

        // isHidden property of the popup is controlled through conditionalMeta 'hide' property
        const showThisPopup = newProps.renderData.isHidden === false;
        let bodyText = meta && meta.bodyText;
        if (showThisPopup) {
            this.props.setPopupData({
                headerText: meta && meta.headerText,
                bodyText,
                actionButtons: meta && meta.actionButtons,
                type: meta && meta.popupType,
                renderData: newProps.renderData,
                formData: newProps.formData,
                showPopup: true,
            });
        }
    }

    // Check whether we need to show the popup and trigger the show function
    checkShowPopup = (newProps) => {
        console.debug('calling checkShowPopup', newProps);
        const meta = newProps.renderData && newProps.renderData.metaData;
        this.checkConditionalExpr(newProps);
        // Below code is retained becuase of the legacy 'showTrigger' meta configuraiton
        // We can remove it once we abolish the use of showTrigger and triggerConfig in the meta for popup
        if (meta && this.state.triggerOnFieldUpdate) {
            this.checkFieldUpdateValidation(newProps);
        }
    }


    render() {
        return null;
    }
}