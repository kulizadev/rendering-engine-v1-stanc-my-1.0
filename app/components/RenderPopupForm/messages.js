/*
 * RenderPopupForm Messages
 *
 * This contains all the text for the RenderPopupForm component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderPopupForm.header',
    defaultMessage: 'This is the RenderPopupForm component !',
  },
});
