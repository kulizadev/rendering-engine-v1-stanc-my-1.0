/**
 *
 * RenderPopupForm Web
 *
 */

import React from 'react';

import RenderPopupFormAbstract from './RenderPopupFormAbstract';

export default class RenderPopupFormComponent extends RenderPopupFormAbstract { // eslint-disable-line react/prefer-stateless-function

    render() {
        return null;
    }
}
RenderPopupFormComponent.propTypes = { };
