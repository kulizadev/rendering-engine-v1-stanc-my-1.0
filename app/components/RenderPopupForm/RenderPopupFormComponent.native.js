/**
 *
 * RenderPopupForm Native
 *
 */

import React from 'react';

import RenderPopupFormAbstract from './RenderPopupFormAbstract';

export default class RenderPopupFormComponent extends RenderPopupFormAbstract {

    render() {
        return null;
    }
}

RenderPopupFormComponent.propTypes = { };