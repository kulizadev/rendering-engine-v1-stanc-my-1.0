/**
*
* RenderWalkThrough
*
*/

import React from 'react';

// import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import RenderWalkThroughComponent from './RenderWalkThroughComponent';

class RenderWalkThrough extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderWalkThroughComponent {...this.props} />
    );
  }
}

RenderWalkThrough.propTypes = {

};

export default RenderWalkThrough;

