import { Dimensions } from 'react-native';
import { colors, fonts, lineHeight } from '../../configs/styleVars';

const paginationDotStyles = {
  backgroundColor: colors.primaryBGColor,
  width: 8,
  height: 8,
  borderRadius: 9,
  marginLeft: 6,
  marginRight: 6,
};

export const styles = {
  wrapper: {
    flex: 1,
  },
  slide: {
    flex: 1,
    backgroundColor: colors.transparentColor,
  },
  container: {
    flex: 1,
  },
  buttonWrapper: {
    position: 'absolute',
    bottom: 30,
    zIndex: 100,
    paddingHorizontal: 20,
  },
  paginationDot: {
    ...paginationDotStyles,
  },
  currentDot: {
    ...paginationDotStyles,
    backgroundColor: colors.white,
  },
  paginationStyles: {
    bottom: 270,
    marginLeft: 20,
    justifyContent: 'flex-start',
  },
  imgBackground: {
    backgroundColor: colors.transparentColor,
    position: 'absolute',
  },
  slideView: {
    backgroundColor: colors.transparentColor,
    overflow: 'hidden',
  },
  sliderInnerView: {
    position: 'absolute',
    bottom: 140,
    marginLeft: 20,
    height: 140,
    zIndex: 1000,
  },
  sliderTextHead: {
    color: colors.white,
    width: 180,
    marginBottom: 16,
    fontSize: fonts.h2,
    ...fonts.getFontFamilyWeight('bold'),
    lineHeight: lineHeight.h2,
  },
  sliderTextDes: {
    color: colors.white,
    width: Dimensions.get('window').width / 2,
    fontSize: fonts.description,
    ...fonts.getFontFamilyWeight(),
    lineHeight: lineHeight.description,
  },
  buttonStyle: {
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: colors.white,
  },
  textStyle: {
    color: colors.primaryBGColor,
  },
  listIconStyles: {
    paddingRight: 10,
    fontSize: 18,
    ...fonts.getFontFamilyWeight('bold'),
    color: colors.white,
  },
  descriptionWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  slideImage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  skewedBlock: {
    width: Dimensions.get('window').width / 1.5,
    height: Dimensions.get('window').height,
    backgroundColor: 'rgba(0, 147, 86, 0.6)',
    bottom: 0,
    left: 0,
    transform: [{ rotate: '-45deg' }, { translateX: -100 }, { translateY: 100 }],
    position: 'absolute',
    zIndex: 0,
  },
};
  // export default styles;
