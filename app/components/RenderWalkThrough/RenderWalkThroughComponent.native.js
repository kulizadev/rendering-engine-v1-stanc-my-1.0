/**
*
* RenderWalkThroughComponent
*
*/

import React from 'react';
import { View, Platform } from 'react-native';
import PropTypes from 'prop-types';
// import { FormattedMessage } from 'react-intl';
// import messages from './messages';
import { styles } from './RenderWalkThroughStyles.native';
import RenderCarousel from './RenderCarousel.native';
import RenderButton from '../RenderButton/';
import RenderWalkThroughAbstract from './RenderWalkThroughAbstract';
import { ModalOverlay, WalkThroughConfirm } from '../../components/RenderStaticComponents';
import { strings } from '../../../locales/i18n';

class RenderWalkThroughComponent extends RenderWalkThroughAbstract { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      showMadeEasy: false,
      showTermsAndConditions: false,
    };
  }

  handleMadeEasy= (flag) => {
    this.setState({ showMadeEasy: flag });
  }

  handleTermsAndConditions= (flag) => {
    this.setState({ showTermsAndConditions: flag });
  }

  closeModals = () => {
    const self = this;
    this.handleTermsAndConditions(false);
    window.setTimeout(() => {
      this.handleMadeEasy(false);
      window.setTimeout(() => {
        self.props.startJourney();
      }, 800);
    });
  }

  modalClosed = () => {

  }

  showProductInfo = () => {
    this.handleMadeEasy(true);
  }

  getDocumentUrl = (type, language) => language === 'en-US'
          ? 'http://129.213.24.224:8002/images/overview_vi_en.html'
          : 'http://129.213.24.224:8002/images/overview_vi_vn.html'

  render() {
    const { userLanguage } = this.props;
    return (
      <View style={styles.wrapper}>
        <RenderCarousel {...this.props} showProductInfo={this.showProductInfo} />
        <View style={styles.buttonWrapper}>
          <RenderButton
            type={'primary'}
            text={strings('swiperScreen.getStarted')}
            style={styles.buttonStyle}
            textStyle={styles.textStyle}
            onPress={this.props.startJourney}
          />
        </View>
        <ModalOverlay
          onBack={() => this.handleMadeEasy(false)}
          isVisible={this.state.showMadeEasy}
          headerType={'white'}
        >
          <WalkThroughConfirm
            header={strings('swiperScreen.personalLoan')}
            docUrl={{ uri: this.getDocumentUrl('overview', userLanguage) }}
            onButtonClick={() => this.handleTermsAndConditions(true)}
            buttonText={strings('swiperScreen.makeEasyButton')}
          />
          {/* <ModalOverlay
            onBack={() => this.handleTermsAndConditions(false)}
            isVisible={this.state.showTermsAndConditions}
            headerType={'white'}
            onModalHide={this.modalClosed}
          >
            <WalkThroughConfirm
              header={strings('swiperScreen.tncTitle')}
              docUrl={{ uri: this.getDocumentUrl('tnc', userLanguage) }}
              onButtonClick={this.closeModals}
              buttonText={strings('swiperScreen.tncButton')}
            />
          </ModalOverlay> */}
        </ModalOverlay>
      </View>
    );
  }
}

RenderWalkThroughComponent.propTypes = {
  userLanguage: PropTypes.string,
};

export default RenderWalkThroughComponent;

