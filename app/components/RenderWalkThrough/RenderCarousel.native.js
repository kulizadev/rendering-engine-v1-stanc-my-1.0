/**
*
* RenderCarousel
*
*/

import React from 'react';
import { View } from 'react-native';
import Swiper from 'react-native-swiper';
// import styled from 'styled-components';
import {
  styles,
} from './RenderWalkThroughStyles.native';
import Slider from './Slider.native';
import { getSlideData } from './slidesData.native';

class RenderCarousel extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  render() {
    const dot = (
      <View style={styles.paginationDot} />
    );
    const slidesData = getSlideData();
    const slides = slidesData.map((slideData) => (
      <Slider
        key={slideData.textHead}
        {...slideData}
        showProductInfo={this.props.showProductInfo}
      />
    ));

    return (
      <View style={styles.container}>
        <Swiper
          dot={<View style={styles.paginationDot} />}
          activeDot={<View style={styles.currentDot} />}
          paginationStyle={styles.paginationStyles}
          loop
        >
          { slides }
        </Swiper>
      </View>
    );
  }
}

RenderCarousel.propTypes = {

};

export default RenderCarousel;
