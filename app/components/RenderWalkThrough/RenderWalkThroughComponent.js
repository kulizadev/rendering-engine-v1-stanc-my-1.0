/**
*
* RenderWalkThroughComponent
*
*/

import React from 'react';
import RenderWalkThroughAbstract from './RenderWalkThroughAbstract';
// import styled from 'styled-components';


class RenderWalkThroughComponent extends RenderWalkThroughAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>This is RenderWalkThrough Component</div>
    );
  }
}

RenderWalkThroughComponent.propTypes = {

};

export default RenderWalkThroughComponent;
