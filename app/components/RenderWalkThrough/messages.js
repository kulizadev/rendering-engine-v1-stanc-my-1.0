/*
 * RenderWalkThrough Messages
 *
 * This contains all the text for the RenderWalkThrough component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderWalkThrough.header',
    defaultMessage: 'This is the RenderWalkThrough component !',
  },
});
