/**
*
* Row
*
*/

import React from 'react';

import { Row as NativeRow } from 'react-native-responsive-grid';
// import styled from 'styled-components';


class Row extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <NativeRow {...this.props}>
        {this.props.children}
      </NativeRow>
    );
  }
}

Row.propTypes = {

};

export default Row;
