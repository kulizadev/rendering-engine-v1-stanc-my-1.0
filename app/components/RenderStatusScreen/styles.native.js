import { fonts, lineHeight } from '../../configs/styleVars';

export const wrapperStyle = { flex: 1, paddingBottom: 10 };

export const headerWrap = { position: 'relative', height: 200 };

export const coverImageStyle = { height: '100%', width: '100%', position: 'absolute', top: 0, left: 0, zIndex: 1 };

export const detailWrap = { height: '100%', width: '100%', position: 'absolute', top: 0, left: 0, zIndex: 2 };

export const imageStyle = { height: 100, marginTop: 15 };

export const imageWrapStyle = { flexDirection: 'row', justifyContent: 'center' };

export const textWrap = { alignItems: 'center' };

export const textStyle = { color: 'white', fontSize: fonts.h3, ...fonts.getFontFamilyWeight('bold'), lineHeight: lineHeight.h3, textAlign: 'center' };
