/**
*
* RenderStatusScreen
*
*/

import React from 'react';
import RenderStatusScreenComponent from './RenderStatusScreenComponent';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class RenderStatusScreen extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderStatusScreenComponent {...this.props} />
    );
  }
}

RenderStatusScreen.propTypes = {

};

export default RenderStatusScreen;
