/**
*
* RenderStatusScreenComponent
*
*/

import React from 'react';
import { View, Text, Image } from 'react-native';
import RenderStatusScreenAbstract from './RenderStatusScreenAbstract';
// import styled from 'styled-components';
import background from '../../images/success-background.png';
import success from '../../images/sucessful.png';
import error from '../../images/error.png';
import {
  wrapperStyle,
  headerWrap,
  coverImageStyle,
  detailWrap,
  imageStyle,
  imageWrapStyle,
  textWrap,
  textStyle,
} from './styles';

class RenderStatusScreenComponent extends RenderStatusScreenAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { heading, subHeading, type } = this.props;
    const imageSrc = type && type === 'reject' ? error : success;
    // TODO: make it scrollable
    return (
      <View style={wrapperStyle}>
        <View style={headerWrap}>
          <Image
            source={background}
            style={coverImageStyle}
            resizeMode={'cover'}
          />
          <View
            style={detailWrap}
          >
            <View style={imageWrapStyle}>
              <Image
                style={imageStyle}
                source={imageSrc}
                resizeMode={'center'}
              />
            </View>
            <View style={textWrap}>
              <Text style={textStyle}>{heading}</Text>
              <Text style={textStyle}>{subHeading}</Text>
            </View>
          </View>
        </View>
        {this.props.children}
      </View>
    );
  }
}

RenderStatusScreenComponent.propTypes = {

};

export default RenderStatusScreenComponent;
