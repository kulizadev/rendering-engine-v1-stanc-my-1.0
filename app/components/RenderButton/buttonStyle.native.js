import { colors, fonts } from '../../configs/styleVars';

export const baseStyle = {
  borderRadius: 5,
  paddingTop: 5,
  paddingBottom: 5,
  paddingLeft: 10,
  paddingRight: 10,
  justifyContent: 'center',
  alignItems: 'center',
  elevation: 0,
};

export const primaryStyle = {
  backgroundColor: colors.primaryBGColor,
};

export const secondaryStyle = {
  backgroundColor: colors.white,
};

export const disabledStyle = {
  backgroundColor: colors.primaryDisableColor,
};

export const containerViewStyle = {
  // width: '100%',
};

export const textStyle = { ...fonts.getFontFamilyWeight() };
export const secondaryTextStyle = { color: colors.primaryBGColor };
