/**
*
* RenderButton
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-native-elements';
import * as styles from './buttonStyle';
import RenderButtonAbstract from './RenderButtonAbstract';

export default class RenderButtonComponent extends RenderButtonAbstract { // eslint-disable-line react/prefer-stateless-function
  getButtonStyle = (type) => {
    let style = {};
    switch (type) {
      case 'primary':
        style = Object.assign(style, { ...styles.baseStyle }, { ...styles.primaryStyle });
        break;
      case 'secondary':
        style = Object.assign(style, { ...styles.baseStyle }, { ...styles.secondaryStyle });
        break;
      case 'disabled':
        style = Object.assign(style, { ...styles.baseStyle }, { ...styles.disabledStyle });
        break;
      default:
        break;
    }
    return style;
  };

  getTextStyles = (type) => {
    let style = {};
    switch (type) {
      case 'secondary':
        style = { ...styles.secondaryTextStyle };
        break;
      default:
        break;
    }
    return { ...styles.textStyle, ...style, ...this.props.style.textStyle };
    // return Object.assign({}, this.props.style.textStyle, styles.textStyle, style);
  }
  render() {
    const { style, type, ...otherProps } = this.props;
    const buttonStyle = { ...this.getButtonStyle(type), ...this.props.style.buttonStyle }; // Object.assign({}, this.getButtonStyle(type), this.props.style.buttonStyle);
    const containerStyle = { ...styles.containerViewStyle, ...this.props.style.containerStyle };
    // Object.assign({}, this.props.style.containerStyle, styles.containerViewStyle);
    const textStyle = this.getTextStyles(type);
    const newStyle = Object.assign({}, style, { buttonStyle }, { containerStyle }, { textStyle });
    return (
      <Button {...otherProps} {...newStyle} />
    );
  }
}

RenderButtonComponent.propTypes = {
  type: PropTypes.string,
  style: PropTypes.object,
};

RenderButtonComponent.defaultProps = {
  style: {},
  containerViewStyle: {},
  textStyle: {},
};
