import styleVars from '../../configs/styleVars';

const colors = styleVars.colors,
  fonts = styleVars.fonts;

const commonButtonStyleObj = {
  style: {
    margin: '15px',
  },
  labelStyle: {
    ...fonts.getFontFamilyWeight('bold'),
    fontSize: fonts.buttonFontSize,
    lineHeight: fonts.buttonLineHeight,
  },
  buttonStyle: {
    height: '50px',
    minWidth: '150px',
    backgroundColor: colors.secondaryBGColor,
  },
  labelColor: colors.secondaryFontColor,
};

const primaryStyle = {
  style: { ...commonButtonStyleObj.style },
  labelColor: colors.primaryFontColor,
  buttonStyle: {
    ...commonButtonStyleObj.buttonStyle,
    backgroundColor: colors.primaryBGColor,
  },
  labelStyle: { ...commonButtonStyleObj.labelStyle },
};

const secondaryStyle = {
  style: { ...commonButtonStyleObj.style },
  labelColor: colors.secondaryFontColor,
  buttonStyle: {
    ...commonButtonStyleObj.buttonStyle,
    backgroundColor: colors.secondaryBGColor,
  },
  labelStyle: { ...commonButtonStyleObj.labelStyle },
};

const disabledStyle = {
  style: { ...commonButtonStyleObj.style },
  labelColor: colors.secondaryFontColor,
  buttonStyle: {
    ...commonButtonStyleObj.buttonStyle,
    backgroundColor: colors.primaryDisableColor,
  },
  labelStyle: { ...commonButtonStyleObj.labelStyle },
};


export default function buttonStyle(type) {
  if (type === 'primary') {
    return { ...primaryStyle };
  } else if (type === 'secondary') {
    return { ...secondaryStyle };
  } else if (type === 'disabled') {
    return { ...disabledStyle };
  }
  return { ...secondaryStyle };
}
