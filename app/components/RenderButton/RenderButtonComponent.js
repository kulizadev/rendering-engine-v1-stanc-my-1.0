/**
*
* RenderButton
*
*/

import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

import buttonStyle from './buttonStyle';
import RenderButtonAbstract from './RenderButtonAbstract';

export default class RenderButtonComponent extends RenderButtonAbstract { // eslint-disable-line react/prefer-stateless-function

  componentWillMount() {

  }

  render() {
    const styleObj = buttonStyle(this.props.type);// possible value of type: primary/secondary
    styleObj.buttonStyle = { ...styleObj.buttonStyle, ...this.props.buttonStyle };
    styleObj.labelStyle = { ...styleObj.labelStyle, ...this.props.labelStyle };
    const button = (<RaisedButton
      {...styleObj}
      onClick={this.props.onClick}
      label={this.props.label}
      disabled={this.props.disabled}
    />);

    return (button);
  }
}

/*
Allowed props
type: primary/secondary
onClick
label
disabled
*/
