/**
*
* RenderCongratsComponent
*
*/

import React from 'react';
import { View, Text, ScrollView, TouchableOpacity, Platform } from 'react-native';
import RenderCongratsAbstract from './RenderCongratsAbstract';
import { ModalOverlay, WalkThroughConfirm } from '../../components/RenderStaticComponents';
import { strings } from '../../../locales/i18n';
// import styled from 'styled-components';
import RenderOfferCard from '../RenderOfferCard';
import RenderButton from '../RenderButton';
import RenderDownload from '../RenderDownload';
import componentProps from '../../containers/RenderStep/componentProps';
import {
  wrapperStyle,
  textWrapperStyles,
  headingStyle,
  descriptionStyle,
  buttonWrapper,
  downloadWrapper,
  tncButtonWrapper,
} from './styles.native';

class RenderCongratsComponent extends RenderCongratsAbstract { // eslint-disable-line react/prefer-stateless-function
  state = {
    showTermsAndConditions: false,
  };

  getComponentProps = (formType, elmId) => {
    const defaultValue = this.props.formData[elmId];
    const renderData = { ...this.props.renderData };
    return componentProps[formType] && componentProps[formType](renderData[elmId], elmId, defaultValue);
  }

  handleTermsAndConditions= (flag) => {
    this.setState({ showTermsAndConditions: flag });
  }

  render() {
    const { fieldOrder, formData, renderData, stepperData } = this.props;
    if (renderData && renderData.finalLoanAmountToDisburse) {
      const downloadComponent = renderData.signedLoanAgreement
        ? (<View style={downloadWrapper}>
          <RenderDownload
            inline
            compProps={this.getComponentProps('download', 'signedLoanAgreement')}
          />
        </View>)
        : null;
      return (
        <View style={wrapperStyle}>
          <ScrollView
            style={{ flex: 1 }}
            contentContainerStyle={{ paddingHorizontal: 20, paddingBottom: 20 }}
          >
            <View style={{ flex: 1 }}>
              <View style={textWrapperStyles}>
                <Text style={{ ...headingStyle, marginBottom: 20 }}>{stepperData.taskName} </Text>
                <Text style={{ ...descriptionStyle, marginBottom: 40 }}>{stepperData.description} </Text>
              </View>
              <RenderOfferCard
                fieldOrder={fieldOrder}
                renderData={renderData}
                formData={formData}
                headingField={'finalLoanAmountToDisburse'}
                showIcon={false}
              >
                {downloadComponent}
              </RenderOfferCard>
              <TouchableOpacity onPress={() => this.handleTermsAndConditions(true)}>
                <View><Text style={tncButtonWrapper}>{strings('swiperScreen.tncTitle')}</Text></View>
              </TouchableOpacity>
              <ModalOverlay
                onBack={() => this.handleTermsAndConditions(false)}
                isVisible={this.state.showTermsAndConditions}
                headerType={'white'}
              >
                <WalkThroughConfirm
                  header={strings('swiperScreen.tncTitle')}
                  docUrl={{ uri: 'http://129.213.24.224:8002/images/TCforContract.html' }}
                />
              </ModalOverlay>
            </View>
          </ScrollView>
        </View>
      );
    }
    return null;
  }
}

RenderCongratsComponent.propTypes = {

};

export default RenderCongratsComponent;
