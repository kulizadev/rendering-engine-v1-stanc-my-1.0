/**
*
* RenderCongratsComponent
*
*/

import React from 'react';
import RenderCongratsAbstract from './RenderCongratsAbstract';
// import styled from 'styled-components';


class RenderCongratsComponent extends RenderCongratsAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>This is RenderCongrats Component</div>
    );
  }
}

RenderCongratsComponent.propTypes = {

};

export default RenderCongratsComponent;
