import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const wrapperStyle = {
  backgroundColor: colors.primaryBGColor,
  flex: 1,
};

export const textWrapperStyles = {
  marginTop: 30,
  marginBottom: 10,
};

export const headingStyle = {
  ...fonts.getFontFamilyWeight('bold'),
  fontSize: fonts.h2,
  lineHeight: lineHeight.h2,
  color: colors.white,
  marginBottom: 8,
  textAlign: 'center',
};

export const descriptionStyle = {
  fontSize: fonts.description,
  lineHeight: lineHeight.description,
  color: colors.white,
  textAlign: 'center',
};

export const buttonWrapper = {
  marginVertical: 10,
  paddingHorizontal: 20,
};

export const downloadWrapper = {
  paddingHorizontal: 36,
};


export const tncButtonWrapper = {
  ...fonts.getFontFamilyWeight('bold'),
  marginTop: 15,
  fontSize: fonts.label,
  lineHeight: lineHeight.label,
  color: colors.white,
  textAlign: 'right',
};
