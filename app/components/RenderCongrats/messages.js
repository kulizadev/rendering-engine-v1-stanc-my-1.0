/*
 * RenderCongrats Messages
 *
 * This contains all the text for the RenderCongrats component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderCongrats.header',
    defaultMessage: 'This is the RenderCongrats component !',
  },
});
