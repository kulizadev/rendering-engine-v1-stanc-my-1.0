/**
*
* RenderElasticSearch
*
*/

import React from 'react';
import * as styles from './RenderElasticSearchStyles';
import RenderElasticSearchAbstract from './RenderElasticSearchAbstract';
import AutoComplete from 'material-ui/AutoComplete';
import { MenuItem } from 'material-ui/Menu';

export default class RenderElasticSearchComponent extends RenderElasticSearchAbstract { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.dataSourceConfig = {
      text: 'name',
      value: 'name',
    };
  }

  getSelectedOption = (name) => this.state.optionsList.find((option) => option.name === name)

  handleUpdateInput = (searchText, dataSource, param) => {
    if (param.source !== 'click') {
      this.setState({ inputVal: searchText }, this.onChangeHandler);
    } else {
      const option = this.getSelectedOption(searchText);
      option && this.updateOption(option);
    }
  };

  render() {
    const newCompProps = { ...this.props.compProps, value: this.state.inputVal };
    const optionsList = this.state.optionsList.map((option, index) => {
      const menuItem = (
        <MenuItem
          innerDivStyle={{ whiteSpace: 'normal' }}
          primaryText={option.name}
          key={index}
        />);
      return { ...option, menuItem };
    });
    return (
      <div className="field-container">
        <AutoComplete
          {...newCompProps}
          searchText={this.state.inputVal.toString()}
          onUpdateInput={this.handleUpdateInput}
          dataSource={optionsList}
          dataSourceConfig={this.dataSourceConfig}
          filter={AutoComplete.noFilter}
          menuStyle={{ maxHeight: '200px', overflowY: 'auto' }}
          listStyle={{ whiteSpace: 'normal' }}
          textFieldStyle={{ ...styles.inputStyles.inputStyle }}
          {...styles.inputStyles}
          openOnFocus
          fullWidth
        />
      </div>
    );
  }
}
