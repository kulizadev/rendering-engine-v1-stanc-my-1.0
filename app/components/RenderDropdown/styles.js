import { dropdownComponentStyle } from '../../configs/componentStyles/commonStyles';

export const dropdownStyles = {
  ...dropdownComponentStyle,
};
