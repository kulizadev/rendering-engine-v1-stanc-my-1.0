/**
*
* RenderInputText
*
*/

import React from 'react';
// import styled from 'styled-components';
import RenderDropdownAbstract from './RenderDropdownAbstract';
import DropDown from '../DropDown/';

export default class RenderDropdownComponent extends RenderDropdownAbstract { // eslint-disable-line react/prefer-stateless-function

  onChangeHandler = (option) => {
    this.props.onChangeHandler(option, this.props.elmId);
  }

  render() {
    const enums = this.props.renderData.enumValues;
    const options = enums.map((option) => ({ name: option.name, value: option.id }));
    const errorMessage = this.props.renderData.errorMessage;

    const componentProps = { ...this.props.compProps }; // needed for DropdownField

    return (
      <DropDown
        componentProps={componentProps}
        errorMessage={errorMessage}
        options={options}
        className="field-container"
        onChangeHandler={this.onChangeHandler}
      >
      </DropDown>
    );
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
onBlur Fn
*/
