/**
*
* RenderRestDropdown
*
*/

import React from 'react';
// import styled from 'styled-components';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import * as appActions from '../../containers/AppDetails/actions';
import { makeSelectUserDetails } from '../../containers/AppDetails/selectors';
import { makeSelectRenderData } from '../../containers/RenderJourney/selectors';

import RenderRestDropdownComponent from './RenderRestDropdownComponent';

class RenderRestDropdown extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderRestDropdownComponent {...this.props} />
    );
  }
}

RenderRestDropdown.propTypes = {

};

const mapStateToProps = createStructuredSelector({
  userDetails: makeSelectUserDetails(),
  journeyRenderData: makeSelectRenderData(),
});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(RenderRestDropdown);
