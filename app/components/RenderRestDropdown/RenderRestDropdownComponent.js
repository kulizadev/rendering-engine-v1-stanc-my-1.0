/**
*
* RenderRestDropdown
*
*/

import React from 'react';
// import styled from 'styled-components';
import RenderRestDropdownAbstract from './RenderRestDropdownAbstract';
import DropDown from '../DropDown/';

export default class RenderRestDropdownComponent extends RenderRestDropdownAbstract { // eslint-disable-line react/prefer-stateless-function

  onChangeHandler = (option) => {
    const self = this;
    this.props.onChangeHandler(option, this.props.elmId);
    const selectedOption = this.state.optionsList.filter((optionObj) => {
      return optionObj.id === option;
    })[0];
    if (selectedOption) {
      window.setTimeout(() => {
        if (selectedOption.dependentFields) {
          Object.keys(selectedOption.dependentFields).forEach((field) => {
            selectedOption.dependentFields[field] && self.props.updateFormData(field, selectedOption.dependentFields[field]);
          });
        }
      });
    }
  };

  render() {
    // if (this.state.optionsList.length) {
      const enums = this.state.optionsList;
      const options = enums.map((option) => ({ name: option.name, value: option.id }));
      const error = this.props.renderData.errorMessage;
      return (
        <div className="field-container">
          <DropDown
            options={options}
            error={error}
            componentProps={this.props.compProps}
            onChangeHandler={this.onChangeHandler}
          >
          </DropDown>
        </div>
      );
    // }

    return null;
  }
}

