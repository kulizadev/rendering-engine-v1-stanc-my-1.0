/**
*
* RenderRestDropdown
*
*/

import React from 'react';
import { View, Image } from 'react-native';

import RenderRestDropdownAbstract from './RenderRestDropdownAbstract';
import DropDownModal from '../DropDownModal/';
import TextInputField from '../TextInputField/';
import dropDownIcon from '../../images/down.png';
import RenderError from '../RenderError/';

export default class RenderRestDropdownComponent extends RenderRestDropdownAbstract { // eslint-disable-line react/prefer-stateless-function

  onChangeHandler = (option) => {
    const self = this;
    this.setListFlag(false);
    this.props.onChangeHandler(option.value, this.props.elmId);
    const selectedOption = this.state.optionsList.filter((optionObj) => {
      return optionObj.id === option.value;
    })[0];
    if (selectedOption) {
      window.setTimeout(() => {
        if (selectedOption.dependentFields) {
          Object.keys(selectedOption.dependentFields).forEach((field) => {
            selectedOption.dependentFields[field] && self.props.updateFormData(field, selectedOption.dependentFields[field]);
          });
        }
      });
    }
  }

  setListFlag = (value) => {
    this.setState({ showList: value });
  }

  renderIcon = () => (
    <Image
      style={{ marginBottom: 10 }}
      source={dropDownIcon}
    />
    )

  render() {
    // if (this.state.optionsList) {
      const enums = this.state.optionsList;
      const { compProps } = this.props;
      const options = enums.map((option) => ({ name: option.name, value: option.id }));
      const selectedOption = this.state.optionsList.filter((optionObj) => {
        return optionObj.id === compProps.value;
      })[0];
      let displayedName = '';
      if (selectedOption && typeof selectedOption === 'object') {
        displayedName = selectedOption.name;
      }
      return (
        <View>
          <DropDownModal
            {...compProps}
            onChange={this.onChange}
            options={options}
            updateOption={this.onChangeHandler}
            showList={this.state.showList}
            handleBackPress={() => this.setListFlag(false)}
          />
          <TextInputField
            type={'viewOnly'}
            renderAccessory={this.renderIcon}
            {...this.props.compProps}
            value={displayedName}
            onPress={() => this.setListFlag(true)}
          />
        </View>
      );
    // }

    return null;
  }
}

