/**
*
* Modal
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import RenderButton from '../RenderButton';
import ModalAbstract from './ModalAbstract';
import * as styles from './ModalStyles';

export default class ModalComponent extends ModalAbstract { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    state = {
      open: props.open,
    };
    this.actionHandler = this.actionHandler.bind(this);
  }

  handleClose = () => {
    this.setState({ open: false });
    this.props.triggerDialogClose && this.props.triggerDialogClose();   // to setState in parent function
  };

  actionHandler = (e, action) => {
    switch (action.action) {
      case 'hide':
        this.handleClose();

      case 'callback':
        this.props.triggerCallback && this.props.triggerCallback();

            // case 'triggerSubJourney':
            //     return () => self.triggerSubJourney(buttonConfig.subJourneyId);
    }

    this.handleClose();
  };

  getButtonFromType(type = 'flat', action, indexKey) {
    return type === 'raised' ? (<RenderButton
      type={action.type || 'primary'}
      label={action.label}
      key={`action_button_${indexKey}`}
      {...styles.raisedButtonStyle}
      onClick={(e) => this.actionHandler(e, action)}

    />) : (
      <FlatButton
        label={action.label}
        primary={action.primary}
        key={`action_button_${indexKey}`}
        onClick={(e) => this.actionHandler(e, action)}
      />
    );
  }
  render() {
        // props: {
        //     headerText: string,
        //     actionButtons: array,
        //     children: component,
        //     bodyText: string,
        //     open: boolean,
        //     triggerDialogClose: function,
        //     triggerCallback: function
        // }
    const { headerText, actionButtons, preventOverlayClose, children, bodyText, open, actionButtonType } = this.props;
    const actionBtns = actionButtons && actionButtons.map((action, index) => this.getButtonFromType(actionButtonType, action, index));

    return (
      <div>
        <Dialog
          title={headerText}
          actions={actionBtns}
          modal={false}
          open={this.state.open || open}
          onRequestClose={!preventOverlayClose && this.handleClose}
        >
          {children || bodyText}
        </Dialog>
      </div>
    );
  }
}
