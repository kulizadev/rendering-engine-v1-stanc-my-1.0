/**
*
* Modal
*
*/

import React from 'react';
import { View } from 'react-native';
import Text from '../RenderText';
import ModalAbstract from './ModalAbstract';
import Modal from 'react-native-modal';

export default class ModalComponent extends ModalAbstract { // eslint-disable-line react/prefer-stateless-function

  handleBackPress = () => {
    this.props.handleBackPress && this.props.handleBackPress();
  }

  render() {
    const { isVisible, style, children } = this.props;
    return (
      <Modal
        animationInTiming={500}
        animationOutTiming={500}
        backdropTransitionInTiming={500}
        backdropTransitionOutTiming={500}
        {...this.props}
        onRequestClose={this.handleBackPress}
        isVisible={isVisible}
        style={style}
        supportedOrientations={['portrait', 'landscape']}
      >
        {React.Children.toArray(children)}
      </Modal>
    );
  }
}
