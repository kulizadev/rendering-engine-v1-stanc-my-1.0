/**
*
* ListLabel
*
*/

import React from 'react';
import ListLabelComponent from './ListLabelComponent';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class ListLabel extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <ListLabelComponent {...this.props} />
    );
  }
}

ListLabel.propTypes = {

};

export default ListLabel;
