/**
*
* ListLabelComponent
*
*/

import React from 'react';
import { View, TouchableNativeFeedback, TouchableHighlight, Platform, Image } from 'react-native';
import Text from '../RenderText';
import ListLabelAbstract from './ListLabelAbstract';
import {
  containerStyles,
  labelWrapperStyles,
  collapsedLabelWrapperStyles,
  iconWrapperStyles,
  labelStyles,
  imageStyles,
} from './styles';
import { colors } from '../../configs/styleVars';
import iconMap from '../../utilities/iconMap';
// import styled from 'styled-components';


class ListLabelComponent extends ListLabelAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { leftIcon, leftIconType, rightIcon, rightIconType, label, onPress, isExpanded } = this.props;
    let renderItem = null;
    let leftIconSrc = null;
    let rightIconSrc = null;
    let leftIconStyles = {};
    let rightIconStyles = {};
    if (leftIcon) {
      leftIconSrc = leftIconType === 'url' ? { uri: leftIcon } : iconMap[leftIcon];
      leftIconStyles = leftIconType === 'url' && imageStyles;
    }
    if (rightIcon) {
      rightIconSrc = rightIconType === 'url' ? { uri: rightIcon } : iconMap[rightIcon];
      rightIconStyles = rightIconType === 'url' && imageStyles;
    }
    const renderLeftIcon = leftIcon ? (<View style={iconWrapperStyles}>
      <Image source={leftIconSrc} style={leftIconStyles} />
    </View>) : null;
    const renderRightIcon = rightIconSrc ? (<View style={iconWrapperStyles}>
      <Image source={rightIconSrc} style={rightIconStyles} />
    </View>) : null;
    const wrapperStyles = !isExpanded ? collapsedLabelWrapperStyles : labelWrapperStyles;
    const renderLabel = (
      <View style={containerStyles}>
        {renderLeftIcon}
        <View style={wrapperStyles}>
          <Text style={[labelStyles, this.props.labelStyles]}>{label}</Text>
          { renderRightIcon }
        </View>
      </View>
    );
    if (Platform.OS === 'android') {
      renderItem = (<TouchableNativeFeedback
        onPress={onPress}
        background={TouchableNativeFeedback.Ripple('#909090', false)}
      >
        {renderLabel}
      </TouchableNativeFeedback>);
    } else {
      renderItem = (
        <TouchableHighlight
          underlayColor={colors.white}
          onPress={onPress}
        >
          {renderLabel}
        </TouchableHighlight>);
    }
    return renderItem;
  }
}

ListLabelComponent.propTypes = {

};

export default ListLabelComponent;
