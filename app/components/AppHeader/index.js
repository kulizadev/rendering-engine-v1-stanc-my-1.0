/**
*
* RenderHeader
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import AppHeaderComponent from './AppHeaderComponent';

class AppHeader extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <AppHeaderComponent {...this.props} />
    );
  }
}

AppHeader.propTypes = {

};

export default AppHeader;
