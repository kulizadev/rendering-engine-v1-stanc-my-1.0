/**
*
* AppHeader
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import AppHeaderAbstract from './AppHeaderAbstract';
import { AppHeaderWrapper, AppHeaderInnerWrapper, LogoutDiv,QueryDiv, Positioning, HeadPhoneSize, Border,Call  } from './AppHeaderWrapper';

import StyledLogo from './StyledLogo';
import { brand,headphone } from '../../configs/styleVars';
import { strings } from '../../../locales/i18n';


export default class AppHeaderComponent extends AppHeaderAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    let logoutdiv = null;
    let qeuryDetails = null;
    let hello = null;
    let border=null;
    let headphones=null;
    let hi = "Hi,"
    if (this.props.isLoggedin) {
      logoutdiv = (<LogoutDiv onClick={this.logout}>Logout</LogoutDiv>);
      qeuryDetails=(<div><QueryDiv>{strings('login.Assistance')}</QueryDiv><Call>Call 1234567890</Call></div>);
      hello = (<Call>Call 1234567890</Call>);
      headphones=(<HeadPhoneSize><img src={headphone.headset} width="35px" height="35px"/></HeadPhoneSize>)
      border=(<Border/>)
    }
    const logoImg = (<StyledLogo src={brand.logo} />);
    return (<AppHeaderWrapper className="app-header" style={{zIndex:1000}}>
      <AppHeaderInnerWrapper>
        {logoImg}
          <Positioning>
            {headphones}
            {qeuryDetails}
            {border}
            {logoutdiv}
          </Positioning>
     
      </AppHeaderInnerWrapper>
     
    </AppHeaderWrapper>);
  }
}
