  import appVars from '../../configs/styleVars';
  import styled from 'styled-components';
  const colors = appVars.colors,
    sizes = appVars.sizes;

  export const AppHeaderWrapper = styled.div`
    padding: 5px 20px;
    height: 52px;
    display: flex;
    align-items: center;
    text-align: left;
    /*width: 100%;*/
    color: ${colors.basicFontColor};
    font-size: 18px;
    background-color: ${colors.appHeaderBG};
    box-shadow: 0px 0px 15px rgba(0,0,0,0.5);
    position:fixed;
    width:100%;
    top:0%;
  `;

  export const AppHeaderInnerWrapper = styled.div`
    width: ${sizes.containerWidth};
    margin: 0 auto;
    display: flex;
    justify-content: space-between;
    margin-right:4%
  `;

  export const HeaderRightSubsection = styled.div`
    display: flex;
    max-width: 200px;
    width: 100%;
    justify-content: space-between;
    flex-direction: row-reverse;
  `;

  export const FlexSection = styled.div`
    display: flex;
    justify-content: space-between;
  `;

  export const LogoutDiv = styled.div`
      align-self: center;
      padding-left:5%;
  `;

  export const QueryDiv = styled.div`
      display:flex;
      justify:space
      float:left;
      width: 112px;
      align-self:center;
      font-family: OpenSans;
      font-size: 11px;
      line-height: 1.46;
      color: ${colors.grey};
  `;

  export const Border = styled.div`
      width: 0px;
      height: 60px;
      border: solid 1px #e0ebf3;
      display: flex;
      justify-content: space-between;
  `;

  export const Positioning = styled.div`
      display: flex;
      justify-content: space-between;
      float:right;
      text-align: center;
      align-items: center;
      
  `;

 

  export const HeadPhoneSize = styled.div`
    align-self:center;  
    width: 25x;
    height: 38px;
    margin-right:4%;
  `;

  export const Call = styled.div`
    color:${colors.darkGrey};
    font-size: 13px;
    text-align:left;
  `;