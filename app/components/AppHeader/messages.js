/*
 * RenderHeader Messages
 *
 * This contains all the text for the RenderHeader component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.AppHeader.header',
    defaultMessage: 'This is the AppHeader component !',
  },
});
