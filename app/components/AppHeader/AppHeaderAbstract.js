/**
*
* RenderHeader
*
*/

import React from 'react';
// import styled from 'styled-components';

import * as userUtils from '../../utilities/userUtils';

export default class AppHeaderAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      showDropdown: false,
      logoHeight: 0,
      logoWidth: 0,
    };
    this.logout = this.logout.bind(this);
    this.createNewLead = this.createNewLead.bind(this);
  }

  logout() {
    this.props.logout();
  }

  createNewLead(journeyName) {
    const extraParams = {
      journeyName,
      isJourneyCompleted: false,
      processInstanceId: null,
    };
    this.props.setUserDetails({ ...this.props.userDetails, ...extraParams });
  }

  goToLeadList = () => {
    const self = this;
    const deleteKeyList = ['mobileNumber', 'leadId', 'journeyName'];
    // Clear the session/AsyncStorage Details for the user
    userUtils.removeUserKeys(deleteKeyList, { ...this.props.userDetails }).then((userObj) => {
      const userDetails = userObj;
      self.props.setUserDetails(userDetails);
    });
  };

  render() {
    return null;
  }
}
