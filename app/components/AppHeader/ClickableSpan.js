import styled from 'styled-components';

const ClickableSpan = styled.span`
  cursor: pointer;
`;

export default ClickableSpan;
