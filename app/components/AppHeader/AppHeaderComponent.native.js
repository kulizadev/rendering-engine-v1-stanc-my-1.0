/**
*
* RenderHeader
*
*/

import React from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import Text from '../RenderText';
import { toggleDrawer } from '../../utilities/NavigationService.native';
import AppHeaderAbstract from './AppHeaderAbstract';
import * as styles from './AppHeaderStyle.native';
import LogoSrc from '../../images/logo.png';
import whiteLogoSrc from '../../images/logoWhite.png';
import { colors, brand } from '../../configs/styleVars';
import RenderPreHeader from '../RenderPreHeader';
import { PostHeader, AppLogo } from '../../components/RenderStaticComponents';
import DropDownModal from '../DropDownModal';
import downArrow from '../../images/down-arrow.png';
import downWhiteArrow from '../../images/downWhiteArrow.png';
import { strings } from '../../../locales/i18n';
import { languages, journeyNames } from '../../configs/appConstants';

export default class AppHeaderComponent extends AppHeaderAbstract { // eslint-disable-line react/prefer-stateless-function
  onMenuPress = () => {
    toggleDrawer();
  }

  handleDropDown = (flag) => {
    if (typeof flag === 'boolean') {
      this.setState({ showDropdown: flag });
    } else {
      this.setState(({ showDropdown }) => ({ showDropdown: !showDropdown }));
    }
  }

  onChangeHandler = (selectedValue) => {
    this.props.updateUserLanguage(selectedValue.value);
    this.handleDropDown(false);
  }

  render() {
    const { type, title, theme, isFixed, hideLogo, dashboardData, currentScreen } = this.props;
    const selectedLanguage = languages.find((language) => language.value === this.props.userLanguage);
    const isWhiteTheme = (theme === 'white');
    const logo = isWhiteTheme ? whiteLogoSrc : LogoSrc;
    const downIcon = isWhiteTheme ? downWhiteArrow : downArrow;
    const textStyles = isWhiteTheme ? styles.whiteTextStyles : styles.textStyles;
    const fixedStyles = isFixed ? styles.fixedHeader : {};

    const langBlock = this.props.hideLangBlock ? null : (
      <View>
        <TouchableOpacity onPress={this.handleDropDown} style={{ flex: 1 }}>
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
            <Text style={textStyles} >{selectedLanguage ? selectedLanguage.label : ''}</Text>
            <Image
              source={downIcon}
            />
          </View>
        </TouchableOpacity>
        <DropDownModal
          options={languages}
          label={strings('login.languageLabel')}
          updateOption={this.onChangeHandler}
          showList={this.state.showDropdown}
          handleBackPress={() => this.handleDropDown(false)}
          value={selectedLanguage ? selectedLanguage.value : ''}
        />
      </View>);

    // const backButton = this.props.userDetails.journeyName ?
    //   (
    //       <Text onPress={this.goToLeadList}>{'< Back'}</Text>
    //   )
    //   : null;
    const rightBtnSet = this.props.isLoggedin ?
      (<View style={styles.headerButtonsStyles}>
        {/* <View>
          {
            !this.props.userDetails.journeyName ?
              (<React.Fragment>
                <Text style={styles.newLeadBtnStyle} onPress={() => this.createNewLead(journeyNames.newLeadJourney)}>New Lead</Text>
              </React.Fragment>)
              : null
          }
        </View> */}
        <View >
          <Text onPress={this.logout}>Logout</Text>
        </View>
      </View>)
      : null;

    switch (type) {
      case 'appHeader':
        return (
          <View style={[styles.wrapperStyles, fixedStyles]}>
            {/* <View {...styles.sideWrapper}>
              {
              this.props.showAppDrawer ?
                (
                  <TouchableOpacity
                    onPress={this.onPress}
                    underlayColor={colors.white}
                  >
                    <Icon
                      name="ios-menu"
                      type="ionicon"
                      {...styles.menuIconStyles}
                    />
                  </TouchableOpacity>
              )
              : null
            }
            </View> */}
            {/* {backButton} */}
            <AppLogo height={50} />
            {rightBtnSet}
          </View>
        );
      case 'preHeader':
        return <RenderPreHeader title={title} onBack={this.logout} />;
      // case 'postAppHeader':
      //   return (<PostHeader
      //     onMenuPress={this.onMenuPress}
      //     name={dashboardData.borrowerFullName || '-'}
      //     imageSrc={dashboardData.selfieLink}
      //     currentScreen={currentScreen}
      //   />);
      default:
        return null;
    }
  }
}
