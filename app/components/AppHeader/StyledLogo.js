import styled from 'styled-components';

const StyledLogo = styled.img`
  float: left;
  width: auto;
  cursor: pointer;
  align-self: center;
  max-height: 50px;
`;

export default StyledLogo;
