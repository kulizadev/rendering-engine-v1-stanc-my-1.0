import { Dimensions } from 'react-native';
import { colors, fonts, lineHeight, sizes } from '../../configs/styleVars';
import { scale, verticalScale, normalize } from '../../configs/scalingUtils';

export const logoHeight = 35;

export const wrapperStyles = {
  width: Dimensions.get('window').width,
  paddingLeft: sizes.appPaddingLeft,
  paddingRight: sizes.appPaddingRight,
  height: verticalScale(60),
  backgroundColor: colors.white,
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  // shadowColor: colors.basicFontColor,
  // shadowOffset: { width: 0, height: 3 },
  // shadowOpacity: 0.3,
  // shadowRadius: 2,
  zIndex: 100,
};

export const fixedHeader = {
  position: 'absolute',
};

export const logoStyles = {
  resizeMode: 'contain',
  alignContent: 'center',
};

export const menuIconStyles = {
  size: 30,
  color: colors.primaryBGColor,
  style: {
    textAlign: 'right',
  },
};

export const sideWrapper = {
  style: {
    width: 50,
  },
};

export const textStyles = {
  ...fonts.getFontFamilyWeight(),
  fontSize: fonts.description,
  lineHeight: lineHeight.description,
  marginRight: 5,
};

export const whiteTextStyles = {
  ...textStyles,
  color: colors.white,
};

export const headerButtonsStyles = {
  width: 150,
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'flex-end',
};

export const newLeadBtnStyle = {
  color: colors.errorColor,
};
