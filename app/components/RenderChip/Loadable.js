/**
 *
 * Asynchronously loads the component for RenderChip
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
