import { colors, fonts, lineHeight } from '../../configs/styleVars';


export const containerStyle = {
  containerStyle: {
    flexDirection: 'row',
    marginVertical: 5,
    backgroundColor: colors.white,
    paddingHorizontal: 20,
  },
};

export const chipTextStyle = {
  style: {
    color: colors.inputColor,
    paddingRight: 5,
    fontSize: fonts.description,
    lineHeight: lineHeight.description,
  },
};

export const iconStyle = {
  size: 15,
  color: colors.hintTextColor,
  fontWeight: 'bold',
};
