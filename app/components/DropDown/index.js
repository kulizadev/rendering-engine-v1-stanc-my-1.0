/**
*
* DropDown
*
*/

import React from 'react';
import DropDownComponent from './DropDownComponent';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class DropDown extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <DropDownComponent {...this.props} />
    );
  }
}

DropDown.propTypes = {

};

export default DropDown;
