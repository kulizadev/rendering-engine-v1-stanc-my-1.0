/**
*
* DropDownComponent
*
*/

import React from 'react';
import DropDownAbstract from './DropDownAbstract';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RenderError from '../RenderError/';
// import styled from 'styled-components';
import * as styles from './styles';
import styledComponents from '../../configs/styledComponents';


export default class DropDownComponent extends DropDownAbstract { // eslint-disable-line react/prefer-stateless-function

  onChangeHandler = (e, index, option) => {
    this.props.onChangeHandler && this.props.onChangeHandler(option);
  }

  render() {
    const { componentProps, errorMessage, options, className } = this.props;
    const menuItems = [];
    options.forEach((option) => {
      menuItems.push(<MenuItem value={option.value} primaryText={option.name} key={option.value} />);
    });

    let error = null;
    if (errorMessage) {
      error = (<RenderError errorMessage={errorMessage} />);
    }
    const hintText = styledComponents.getStyledHintText(this.props.hintText);

    return (
      <div className={className}>
        <SelectField
          {...this.props.dropdownStyles}
          {...componentProps}
          onChange={this.onChangeHandler}
        >
          {menuItems}
        </SelectField>
        {error}
        {hintText}
      </div>
    );
  }
}

DropDownComponent.propTypes = {

};

DropDownComponent.defaultProps = {
  focusBoolean: true,
  dropdownStyles: styles.dropdownStyles,
};
