/**
*
* DropDownComponent
*
*/

import React from 'react';
import { View, Text, Image } from 'react-native';
import DropDownAbstract from './DropDownAbstract';
// import styled from 'styled-components';
import DropDownModal from '../DropDownModal/';
import TextInputField from '../TextInputField/';
import dropDownIcon from '../../images/down.png';

class DropDownComponent extends DropDownAbstract { // eslint-disable-line react/prefer-stateless-function


  onChangeHandler = (option) => {
    this.setListFlag(false);
    this.props.onChangeHandler(option);
  }

  setListFlag = (value) => {
    this.setState({ showList: value });
  }

  renderIcon = () => (
    <Image
      style={{ marginBottom: 10 }}
      source={dropDownIcon}
    />
  );

  getSelectedValue = (options, value) => {
    const selectedOption = options.find((option) => option.value === value);
    return selectedOption ? (selectedOption.name).toString() : '';
  }

  render() {
    const { label, value, disabled, options, error } = this.props;
    const selectedValue = this.getSelectedValue(options, value);
    return (
      <View>
        <DropDownModal
          value={value}
          label={label}
          onChange={this.onChange}
          options={options}
          updateOption={this.onChangeHandler}
          showList={this.state.showList}
          handleBackPress={() => this.setListFlag(false)}
        />
        <TextInputField
          inline={this.props.inline}
          type={'viewOnly'}
          renderAccessory={this.renderIcon}
          label={label}
          error={error}
          disabled={disabled}
          value={selectedValue}
          onPress={() => this.setListFlag(true)}
          accessibilityLabel={this.props.accessibilityLabel}
        />
      </View>
    );
  }
}

DropDownComponent.propTypes = {

};

DropDownComponent.defaultProps = {
  disabled: false,
};

export default DropDownComponent;
