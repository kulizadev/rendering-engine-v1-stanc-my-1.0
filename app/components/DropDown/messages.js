/*
 * DropDown Messages
 *
 * This contains all the text for the DropDown component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.DropDown.header',
    defaultMessage: 'This is the DropDown component !',
  },
});
