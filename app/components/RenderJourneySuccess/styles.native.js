import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const wrapperStyles = {
  flex: 1,
  justifyContent: 'space-between',
  paddingBottom: 35,
};

export const suceessWrapperStyles = {
  flex: 1,
  paddingBottom: 0,
};

export const buttonStyle = { width: '90%' };

export const textStyles = {
  padding: 30,
  color: colors.black,
  lineHeight: lineHeight.body,
  fontSize: fonts.body,
  ...fonts.getFontFamilyWeight(),
  textAlign: 'center',
};
