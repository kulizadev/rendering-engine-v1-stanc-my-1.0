/**
*
* RenderJourneySuccessComponent
*
*/

import React from 'react';
import { View } from 'react-native';
import Text from '../RenderText';
import RenderJourneySuccessAbstract from './RenderJourneySuccessAbstract';
import RenderStepper from '../RenderStepper';
import RenderStatusScreen from '../RenderStatusScreen';
import RenderButton from '../RenderButton';
import {
  wrapperStyles,
  suceessWrapperStyles,
  textStyles,
  buttonStyle,
} from './styles';
// import messages from './messages';
// import styled from 'styled-components';

import { customerSupportNumber } from '../../configs/appConstants';
import Communications from 'react-native-communications';
import { strings } from '../../../locales/i18n';


class RenderJourneySuccessComponent extends RenderJourneySuccessAbstract { // eslint-disable-line react/prefer-stateless-function

  makeCall = () => {
    if (customerSupportNumber) {
      window.setTimeout(() => {
        Communications.phonecall(customerSupportNumber.toString(), true);
      }, 100);
    }
  }

  renderComponent = (type) => {
    let screenView = null;
    const { stepperData } = this.props;
    if (type === 'success') {
      screenView = (
        <View style={suceessWrapperStyles}>
          <RenderStepper labels={stepperData.category} currentActiveStep={stepperData.currentActiveCategory} type={'fullscreen'} />
          <RenderButton
            type={'primary'}
            text={strings('statusScreen.nextStepButton')}
            onPress={this.props.submitStep}
            style={{ buttonStyle }}
          />
        </View>
      );
    } else if (type === 'onHold' || type === 'reject') {
      screenView = (
        <View style={wrapperStyles}>
          <Text style={textStyles}>{stepperData.description}</Text>
          <RenderButton
            type={'secondary'}
            text={strings('statusScreen.callUsButton')}
            onPress={this.makeCall}
            style={{ buttonStyle }}
          />
        </View>
      );
    }
    return screenView;
  }

  render() {
    const { stepperData } = this.props;
    if (stepperData) {
      return (
        <RenderStatusScreen heading={stepperData.header} subHeading={stepperData.body} type={stepperData.type}>
          {this.renderComponent(stepperData.type)}
        </RenderStatusScreen>
      );
    }
    return null;
  }
}

RenderJourneySuccessComponent.propTypes = {

};

RenderJourneySuccessComponent.defaultProps = {
  labels: [],
};

export default RenderJourneySuccessComponent;
