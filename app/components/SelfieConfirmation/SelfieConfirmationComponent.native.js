/**
*
* SelfieConfirmationComponent
*
*/
import React from 'react';
import { View, NativeModules, Platform, ScrollView, Text, Keyboard } from 'react-native';
import SelfieConfirmationAbstract from './SelfieConfirmationAbstract';
// import styled from 'styled-components';
import { permissionConst, getClientKey } from '../../configs/appConstants';
import * as NativeMethods from '../RenderUpload/RenderUploadHelper';
import RenderPopup from '../RenderPopup';
import RenderButton from '../RenderButton/';
import * as styles from './styles';
import { ModalOverlay } from '../RenderStaticComponents';
import RenderPreview from '../RenderPreview';
import { strings } from '../../../locales/i18n';


class SelfieConfirmationComponent extends SelfieConfirmationAbstract { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      selfieImage: '',
    };
  }

  componentWillReceiveProps(newProps) {
    if (newProps && newProps.openCamera && !this.props.openCamera) {
      this.retakeSelfie();
    }
    if (newProps && !newProps.isValid && this.props.isValid) {
      this.clearPreview();
    }
  }

  buildFileData = (targetFile) => ({
    uri: targetFile.uri,
    name: targetFile.fileName,
    size: targetFile.fileSize,
    type: targetFile.type,
  });

  takeSelfie = () => {
    const self = this;
    this.uploadType = 'selfie';
    const { appid, appkey } = getClientKey('hypervergeSnap');
    NativeModules.CameraModule.initHyperverge(appid, appkey, 'AsiaPacific', 'FACEID');
    NativeModules.CameraModule.takeSelfie((res) => {
      if (res) {
        const file = JSON.parse(res);
        const uri = Platform.OS === 'ios' ? file.uri : `file://${file.path}`;
        file.uri = uri;
        self.setState({ selfieImage: this.buildFileData(file) });
      }
    });
  }

  clickHandler = async () => {
    Keyboard.dismiss();
    const isValid = this.props.isValid !== undefined ? this.props.isValid : true;
    const permissionStatus = await NativeMethods.getPermission();
    if (permissionStatus === permissionConst.GRANTED) {
      isValid && this.takeSelfie();
    } else {
      console.log('Camera permission denied');
    }
  }

  retakeSelfie = () => {
    this.clickHandler();
    this.clearPreview();
    this.props.toggleRetry(false);
  }

  clearPreview = () => {
    this.setState({ selfieImage: '' });
  }

  submitData = () => {
    const self = this;
    const selfieImage = { ...self.state.selfieImage };
    this.clearPreview();
    window.setTimeout(() => {
      self.props.onSubmit(selfieImage);
    }, 500);
  }
  getPopInfo = () => {
    let showPopup = false;
    let popData = {};
    switch (this.props.showPopup) {
      case 'success':
        showPopup = true;
        popData = this.props.successPopData;
        break;
      case 'failure':
        showPopup = true;
        popData = this.props.errorPopData;
        break;

      default:
        break;
    }
    return [showPopup, popData];
  }
  // TODO: refactor to have retry logic within itself
  render() {
    const { selfieImage } = this.state;
    const isValid = this.props.isValid !== undefined ? this.props.isValid : true;
    const [showPopup, popData] = this.getPopInfo();
    return (
      <View>
        {/* <Text style={styles.infoStyles}>{'Next, please verify by taking your selfie. We will verify your authentication by making a comparison between your selfie and the selfie that you provided at the time of registration.'}</Text> */}
        <RenderButton
          type={'primary'}
          text={strings('profileData.takeSelfie')}
          onPress={this.clickHandler}
        />
        <ModalOverlay
          onBack={this.clearPreview}
          isVisible={!!selfieImage && isValid}
          headerType={'white'}
        >
          <View style={styles.wrapper}>
            <ScrollView keyboardShouldPersistTaps="always">
              <View>
                <Text style={styles.bodyText}>{strings('subJourney.instructionSelfieText')}</Text>
                <Text style={styles.headingStyle}>{strings('subJourney.selfieHeading')}</Text>
              </View>
              <View style={styles.previewStyles}>
                <RenderPreview
                  circular
                  uri={this.state.selfieImage.uri}
                />
              </View>
            </ScrollView>

            <View style={{ alignSelf: 'flex-end' }}>
              <RenderButton
                text={strings('subJourney.submitText')}
                onPress={() => this.submitData()}
                type={'primary'}
              />
              <RenderButton
                text={strings('subJourney.reTakeText')}
                onPress={this.retakeSelfie}
                type={'secondary'}
              />
            </View>
          </View>
          {/* <RenderPopup showPopup={showPopup} {...popData} />*/}
        </ModalOverlay>
      </View>
    );
  }
}

SelfieConfirmationComponent.propTypes = {

};

export default SelfieConfirmationComponent;

