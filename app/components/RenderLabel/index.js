/**
*
* RenderLabel
*
*/

import React, { Children } from 'react';
import PropTypes from 'prop-types';
import Text from '../RenderText';
import {
  baseLabelStyle,
  valueLabel,
  titleLabel,
} from './style';

// import styled from 'styled-components';
const getLabelStyle = (type) => {
  let style = {};
  switch (type) {
    case 'value':
      style = valueLabel;
      break;
    case 'title':
      style = titleLabel;
      break;
    default:
      break;
  }
  return { ...baseLabelStyle, ...style };
};

class RenderLabel extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const labelStyle = getLabelStyle(this.props.type);
    const { style, ...otherProps } = this.props;
    const newStyle = { ...labelStyle, ...style };
    return (
      <Text style={newStyle} {...otherProps}>
        {Children.toArray(this.props.children)}
      </Text>
    );
  }
}

RenderLabel.propTypes = {
  style: PropTypes.object,
  type: PropTypes.string,
  children: PropTypes.node,
};

RenderLabel.defaultProps = {
  style: {},
};

export default RenderLabel;
