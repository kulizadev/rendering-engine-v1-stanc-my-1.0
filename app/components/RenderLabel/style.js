import { colors, fonts } from '../../configs/styleVars';

export const baseLabelStyle = {
  fontSize: fonts.paraSize,
  ...fonts.getFontFamilyWeight(),
};

export const valueLabel = {
  color: colors.basicFontColor,
};

export const titleLabel = {
  color: colors.black,
  ...fonts.getFontFamilyWeight('bold'),
};
