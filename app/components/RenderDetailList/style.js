export const detailsItemStyle = {
  flexDirection: 'row',
  alignSelf: 'stretch',
  paddingTop: 5,
  paddingBottom: 5,
};
