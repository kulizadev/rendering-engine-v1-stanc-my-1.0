/**
*
* RenderDetailList
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import RenderLabel from '../RenderLabel';
import { detailsItemStyle } from './style';
// import styled from 'styled-components';


class RenderDetailList extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { detailList } = this.props;
    if (detailList && detailList.length) {
      return detailList.map((detail) => (
        <View key={`RenderDetailList_${detail.id}`} style={detailsItemStyle}>
          <RenderLabel type={'title'} style={{ width: '60%' }}>{detail.label}</RenderLabel>
          <RenderLabel type={'value'} style={{ width: '40%' }}>{`: ${detail.value}`}</RenderLabel>
        </View>
      ));
    }
    return null
    ;
  }
}

RenderDetailList.propTypes = {
  detailList: PropTypes.array,
};

export default RenderDetailList;
