/*
 * AutoCompleteModal Messages
 *
 * This contains all the text for the AutoCompleteModal component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.AutoCompleteModal.header',
    defaultMessage: 'This is the AutoCompleteModal component !',
  },
});
