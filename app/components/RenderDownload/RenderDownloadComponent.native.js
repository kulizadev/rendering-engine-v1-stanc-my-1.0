/**
*
* RenderDownload
*
*/

import React from 'react';
import { View, Image, TouchableOpacity, Platform, Linking } from 'react-native';
import Text from '../RenderText';
import Icon from 'react-native-vector-icons/MaterialIcons';
import RNFetchBlob from 'react-native-fetch-blob';
import RenderDownloadAbstract from './RenderDownloadAbstract';
import * as NativeMethods from './RenderDownloadHelper';
import { permissionConst } from '../../configs/appConstants';
import agreementIcon from '../../images/agreement.png';
import arrowIcon from '../../images/rightWhiteArrow.png';
import * as styles from './RenderDownloadStyles';
import * as common from '../../utilities/commonUtils';

export default class RenderDownload extends RenderDownloadAbstract { // eslint-disable-line react/prefer-stateless-function

  toggleLoader = (value) => {
    this.props.toggleLoader({
      isVisible: value,
      isLongReq: false,
      message: '',
    });
  }

  // TODO: replace it with download service method
  download = async () => {
    const permissionStatus = await NativeMethods.getPermission();
    const { url, docType } = this.props.compProps;
    if (permissionStatus === permissionConst.GRANTED && url && docType) {
      if (Platform.OS === 'ios') {
        const DownloadDir = Platform.OS === 'ios' ? RNFetchBlob.fs.dirs.DocumentDir : RNFetchBlob.fs.dirs.DownloadDir;
        const options = {
          fileCache: true,
          overwrite: false,
          path: `${DownloadDir}/FEC-doc.${docType}`,
          addAndroidDownloads: {
            useDownloadManager: true,
            notification: true,
            path: `${DownloadDir}/FEC-doc.${docType}`,
            mime: common.getMimetype(docType),
          },
        };
        this.toggleLoader(true);
        RNFetchBlob
          .config(options)
          .fetch('GET', url)
          .then((res) => {
            if (Platform.OS === 'ios') {
              RNFetchBlob.ios.openDocument(res.path());
            } else {
              // TODO: need to handle the viewing part
              // RNFetchBlob.android.actionViewIntent(res.path(), common.getMimetype(docType));
              res.path();
            }
            this.toggleLoader(false);
          })
          .catch((error) => {
            this.toggleLoader(false);
            window.logger.error('Network', `Downloading ${url}`, { options, error });
          });
      } else {
        const { url, docType } = this.props.compProps;
        Linking.canOpenURL(url).then((supported) => {
          if (supported) {
            Linking.openURL(url);
          } else {
            console.log(`Don't know how to open URL: ${url}`);
          }
        });
      }
    }
  }

  extention = (filename) => (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;

  onPress = () => {
    if (this.props.onPress) {
      this.props.onPress();
    } else {
      this.download();
    }
  }

  render() {
    if (this.props.inline) {
      return (
        <TouchableOpacity
          onPress={this.onPress}
        >
          <View style={[styles.inlineWrapper, this.props.style]}>
            <Text {...styles.inlineLabelText}>{this.props.compProps.label}</Text>
          </View>
        </TouchableOpacity>
      );
    }
    return (
      <View>
        <TouchableOpacity
          onPress={this.download}
        >
          <View {...styles.wrapper}>
            <Image
              source={agreementIcon}
              {...styles.imageWrapper}
            />
            <Text {...styles.labelText}>{this.props.compProps.label}</Text>
            <Image
              source={arrowIcon}
              {...styles.imageWrapper}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
