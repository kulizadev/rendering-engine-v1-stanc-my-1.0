/**
 *
 * Asynchronously loads the component for RenderDownload
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
