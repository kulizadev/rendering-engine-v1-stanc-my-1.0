import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const wrapper = {
  style: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    backgroundColor: colors.primaryBGColor,
    borderRadius: 6,
    paddingVertical: 25,
    marginTop: 10,
    marginBottom: 15,
  },
};

export const labelText = {
  style: {
    fontSize: fonts.body,
    ...fonts.getFontFamilyWeight('bold'),
    lineHeight: lineHeight.body,
    color: colors.white,
    flex: 1,
  },
};

export const imageWrapper = {
  style: {
    marginHorizontal: 20,
  },
};

export const inlineLabelText = {
  style: {
    fontSize: fonts.body,
    ...fonts.getFontFamilyWeight('bold'),
    lineHeight: lineHeight.body,
    color: colors.primaryBGColor,
    flex: 1,
  },
};

export const inlineWrapper = {
  paddingVertical: 10,
};
