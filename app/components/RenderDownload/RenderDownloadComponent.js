/**
*
* RenderDownload
*
*/

import React from 'react';
import RenderDownloadAbstract from './RenderDownloadAbstract';
import RenderButton from '../../components/RenderButton/';

export default class RenderDownload extends RenderDownloadAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    let { url, label } = this.props.compProps;
    label = `Download ${label}`;
    return (
      <a href={url} target="_blank" download>
        <RenderButton
          buttonStyle={{ width: 'auto' }}
          type="primary"
          label={label}
        />
      </a>
    );
  }
}
