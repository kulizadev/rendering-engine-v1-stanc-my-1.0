import { Dimensions } from 'react-native';
import { colors } from '../../configs/styleVars';

export const modal = {
  style: {
    justifyContent: 'flex-end',
    margin: 0,
  },
};

export const wrapper = {
  style: {
    backgroundColor: colors.white,
    maxHeight: Dimensions.get('window').height / 1.5,
  },
};

export const headerWrapper = {
  style: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 20,
    marginBottom: 10,
    marginRight: 15,
  },
};

export const headerRightWrapper = {
  style: {
    flexDirection: 'row',
  },
};

export const heading = {
  style: {
    margin: 20,
    marginBottom: 10,
  },
};

export const markAll = {
  style: {
    marginLeft: 15,
  },
};
