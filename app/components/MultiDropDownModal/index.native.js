/**
*
* MultiDropDownModal Native
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, ScrollView, Image, TouchableHighlight } from 'react-native';
import CheckedSrc from '../../images/checked.png';
import UncheckedSrc from '../../images/unchecked.png';
import AutoCompleteOptions from '../AutoCompleteOptions/';
import * as styles from './styles';
import Modal from '../Modal';

export default class MultiDropDownModal extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { showList, handleBackPress, toggleAll, value, options, updateOption, label, markAll } = this.props;
    const optionsList = showList ? (
      <AutoCompleteOptions
        listType={'multiselect-dropdown'}
        optionsList={options}
        updateOption={updateOption}
        selectedValue={value}
        selectedIcon={'checkBoxChecked'}
        deselectedIcon={'checkBoxUnchecked'}
      />) : <ScrollView />;

    return (
      <Modal
        isVisible={showList}
        handleBackPress={handleBackPress}
        onBackdropPress={handleBackPress}
        onSwipe={handleBackPress}
        swipeDirection={'down'}
        {...styles.modal}
      >
        <View
          {...styles.wrapper}
        >
          <View {...styles.headerWrapper}>
            <Text>{label}</Text>
            <View {...styles.headerRightWrapper}>
              <Text onPress={handleBackPress}>Select All</Text>
              <TouchableHighlight {...styles.markAll} onPress={toggleAll} activeOpacity={1}>
                <Image style={{ padding: 0, margin: 0 }} source={markAll ? CheckedSrc : UncheckedSrc} />
              </TouchableHighlight>
            </View>
          </View>
          {optionsList}
        </View>
      </Modal>
    );
  }
}

MultiDropDownModal.propTypes = {
  showList: PropTypes.bool,
  handleBackPress: PropTypes.func,
  toggleAll: PropTypes.func,
  updateOption: PropTypes.func,
  options: PropTypes.array,
  markAll: PropTypes.bool,
  value: PropTypes.array,
  label: PropTypes.string,
};
