/**
 *
 * Asynchronously loads the component for MultiDropDownModal
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
