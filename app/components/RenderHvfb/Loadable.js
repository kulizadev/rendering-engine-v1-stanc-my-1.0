/**
 *
 * Asynchronously loads the component for RenderHvfb
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
