/**
*
* RenderHvfbComponent
*
*/

import React from 'react';
import { View, NativeModules } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import RenderHvfbAbstract from './RenderHvfbAbstract';
import * as styles from './RenderHvfbStyles';
import { clientKeys } from '../../configs/appConstants';
import RenderPreview from '../RenderPreview';
import * as sessionUtils from '../../utilities/sessionUtils';

export default class RenderHvfbComponent extends RenderHvfbAbstract { // eslint-disable-line react/prefer-stateless-function

  loginWithFacebook = async () => {
    const { appid, appkey } = clientKeys.hyperverge;
    let selfie = await sessionUtils.getSessionKey('selfie');
    selfie = JSON.parse(selfie);
    NativeModules.LendingUtils.openHVFBActivity(selfie.path || selfie.uri, '', appid, appkey, (response, error) => {
      if (response) {
        console.log('HVFB Activity --->', response, error);
        this.onChangeHandler(response);
      }
    });
  }

  render() {
    return (
      <View {...styles.wrapper}>
        <Icon.Button
          name="facebook"
          {...styles.fbIcon}
          onPress={this.loginWithFacebook}
        >
          Login with Facebook
        </Icon.Button>
      </View>
    );
  }
}
