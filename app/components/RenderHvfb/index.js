/**
*
* RenderHvfb
*
*/

import React from 'react';

import RenderHvfbComponent from './RenderHvfbComponent';

class RenderHvfb extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderHvfbComponent {...this.props} />
    );
  }
}

RenderHvfb.propTypes = {

};

export default RenderHvfb;
