import styleVars from '../../configs/styleVars';
const { colors, fonts } = styleVars;

export const wrapper = {
  style: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    padding: 10,
  },
};

export const fbIcon = {
  backgroundColor: colors.fbColor,
};
