/*
 * RenderHvfb Messages
 *
 * This contains all the text for the RenderHvfb component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderHvfb.header',
    defaultMessage: 'This is the RenderHvfb component !',
  },
});
