import { sizes, fonts, lineHeight } from '../../configs/styleVars';

export const wrapper = {
  paddingHorizontal: sizes.appPaddingHorizontal,
  flex: 1,
  paddingBottom: 20,
};

export const infoStyles = {
  ...fonts.getFontFamilyWeight(),
  fontSize: fonts.description,
  lineHeight: lineHeight.description,
  paddingVertical: 10,
};
