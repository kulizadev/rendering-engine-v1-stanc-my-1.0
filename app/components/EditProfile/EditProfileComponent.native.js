/**
*
* EditProfileComponent
*
*/

import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import EditProfileAbstract from './EditProfileAbstract';
// import styled from 'styled-components';
import RenderStep from '../../containers/RenderStep/';
import * as styles from './styles';
// import SelfieConfirmation from '../SelfieConfirmation/';
import { strings } from '../../../locales/i18n';

class EditProfileComponent extends EditProfileAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <View style={styles.wrapper}>
        <ScrollView
          keyboardShouldPersistTaps={'always'}
        >
          <RenderStep
            screenId={this.props.screenId}
            renderData={this.props.renderData}
            fieldOrder={this.props.fieldOrder}
            formData={this.props.formData}
            validateAndUpdateRender={this.props.validateAndUpdateRender}
            updateFormData={this.props.updateFormData}
            toggleScroll={this.props.toggleScroll}
          />
          <Text style={styles.infoStyles}>{strings('profileData.confirmationText')}</Text>
        </ScrollView>
      </View>
    );
  }
}

EditProfileComponent.propTypes = {

};

export default EditProfileComponent;
