/**
*
* EditProfile
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import EditProfileComponent from './EditProfileComponent';

class EditProfile extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <EditProfileComponent {...this.props} />
    );
  }
}

EditProfile.propTypes = {

};

const mapStateToProps = createStructuredSelector({

});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

// export default RenderRestInput;
export default compose(
  withConnect,
)(EditProfile);
