/**
*
* EditProfileComponent
*
*/

import React from 'react';
import EditProfileAbstract from './EditProfileAbstract';
// import styled from 'styled-components';


class EditProfileComponent extends EditProfileAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>This is EditProfile Component</div>
    );
  }
}

EditProfileComponent.propTypes = {

};

export default EditProfileComponent;
