/*
 * EditProfile Messages
 *
 * This contains all the text for the EditProfile component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.EditProfile.header',
    defaultMessage: 'This is the EditProfile component !',
  },
});
