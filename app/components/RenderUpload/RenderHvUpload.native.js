/**
*
* RenderUpload
*
*/

import React from 'react';
import { View, NativeModules } from 'react-native';
import Text from '../RenderText';
import RenderUploadAbstract from './RenderUploadAbstract';
import RenderButton from '../RenderButton/';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import styleVars from '../../configs/styleVars';
import * as DocumentMap from '../../utilities/documentMap';
import * as styles from './RenderUploadStyles';
import { permissionConst } from '../../configs/appConstants';
import * as NativeMethods from './RenderUploadHelper';
import * as sessionUtils from '../../utilities/sessionUtils';

export default class RenderHvUpload extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.uploadType = null;
  }

  // To enable hyperverge only for elmId with -true
  checkRequestStatus = () => this.props.elmId.indexOf('-true') !== -1

  getElmId = () => this.props.elmId.split('-true')[0]

  updateSessionData = async (fileObj) => {
    const elmId = this.getElmId();
    await sessionUtils.setSessionKey(elmId, JSON.stringify(fileObj));
  }

  // HyperVerge file Upload
  triggerHVUpload = async (fileObj, metaData) => {
    let compareFileObj;
    // save image data to async storage
    this.updateSessionData(fileObj);
    const fd = new FormData();
    if (this.uploadType === 'docScan') {
      const value = await fd.append('image', { ...fileObj });
      return fd;
    }
    const value = await sessionUtils.getSessionKey('vietnamNationalId-front');
    compareFileObj = JSON.parse(value);
    fd.append('image1', { ...fileObj });
    fd.append('image2', { ...compareFileObj });
    fd.append('type', 'id');
    return fd;
  }

  triggerUpload = (targetFile, uploadClient) => {
    const self = this;
    const { fileObj, metaData } = this.props.getFileObj(targetFile);

    this.triggerHVUpload(fileObj, metaData).then((fd) => {
      if (self.checkRequestStatus()) {
        self.props.uploadFiles(fd, metaData);
      } else {
        self.props.updateFileList(metaData);
      }
    });
  }

  takeSelfie = () => {
    this.uploadType = 'selfie';
    NativeModules.CameraModule.takeSelfie((res) => {
      this.triggerUpload(JSON.parse(res), this.props.compProps.docSource);
    });
  }

  scanDocument = () => {
    this.uploadType = 'docScan';
    NativeModules.CameraModule.openCamera(this.props.compProps.docType, (res) => {
      this.triggerUpload(JSON.parse(res), this.props.compProps.docSource);
    });
  }

  capturePhoto = async () => {
    const { docType } = this.props.compProps;
    const permissionStatus = await NativeMethods.getPermission();
    if (permissionStatus === permissionConst.GRANTED) {
      docType ? this.scanDocument() : this.takeSelfie();
    } else {
      console.log('Camera permission denied');
    }
  }

  clickHanldler = () => {
    if (this.props.isSingleUpload) {
      this.props.removeFileData();
    }
    this.capturePhoto();
  }

  render() {
    return (
      <RenderButton
        type={'primary'}
        text={this.props.buttonLabel}
        onPress={this.clickHanldler}
        {...styles.uploadBtnStyles}
      />
    );
  }
}
