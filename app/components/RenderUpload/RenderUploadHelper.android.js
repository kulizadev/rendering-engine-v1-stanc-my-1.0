import * as permissionService from '../../utilities/native/permissionService';

export const getPermission = async () => {
  try {
    const granted = await permissionService.getPermission(['camera', 'storageWrite', 'storageRead']);
    return granted;
  } catch (err) {
    console.warn(err);
  }
};
