/**
*
* RenderUpload
*
*/

import React from 'react';
// import styled from 'styled-components';

import RenderUploadAbstract from './RenderUploadAbstract';
import RaisedButton from 'material-ui/RaisedButton';
import Chip from 'material-ui/Chip';
import Dropzone from 'react-dropzone';
import RenderError from '../RenderError/';
import RenderChip from '../RenderChip/';

import * as styles from './RenderUploadStyles';

import styled from 'styled-components';

const ChipWrapper = styled.div`
  margin-top: 10px;
`;

export default class RenderUploadComponent extends RenderUploadAbstract { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.onDropHandler = this.onDropHandler.bind(this);
  }

  onDropHandler(acceptedFiles, rejectedFiles) {
    acceptedFiles.forEach((file) => {
      this.triggerSingleUpload(file);
    });
  }

  render() {
    let dropzoneRef,
      fileChips = [];
    const { docType, docSource, ...componentProps } = this.props.compProps;
    const { multiple } = componentProps;
    const btnDisabled = !multiple && !!this.state.files.length;
    this.state.files.forEach((file, index) => {
      const fileChip = (<RenderChip
        key={`uploaded-${this.props.elmId}${index}`}
        uri={file.uri || ''}
        label={file.label || this.props.renderData.name + (index + 1)}
        onRemoveHandler={() => this.removeFileData(index)}
      />);
      fileChips.push(fileChip);
    });
    let errorMessage = null;
    if (this.props.renderData.errorMessage) {
      errorMessage = (<RenderError errorMessage={this.props.renderData.errorMessage} />);
    }

    return (
      <div className="field-container">
        <Dropzone
          {...componentProps}
          ref={(node) => { dropzoneRef = node; }}
          onDrop={this.onDropHandler}
          style={{ ...styles.zoneStyle }}
          disabledStyle={{ ...styles.disabledStyle }}
          disablePreview
          disabled={btnDisabled}
        >
          <p>{this.props.renderData.name}</p>
        </Dropzone>

        <ChipWrapper>
          {fileChips}
        </ChipWrapper>

        {errorMessage}
      </div>
    );
  }
}
