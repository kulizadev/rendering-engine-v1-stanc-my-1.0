/**
*
* RenderUpload
*
*/

import React from 'react';
// import styled from 'styled-components';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import * as appActions from '../../containers/AppDetails/actions';
import { makeSelectUserDetails } from '../../containers/AppDetails/selectors';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import RenderUploadComponent from './RenderUploadComponent';

class RenderUpload extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderUploadComponent {...this.props} />
    );
  }
}

RenderUpload.propTypes = {

};

const mapStateToProps = createStructuredSelector({
  userDetails: makeSelectUserDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    deleteRequest: (reqObject) => dispatch(appActions.deleteRequest(reqObject)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

// export default RenderRestInput;
export default compose(
  withConnect,
)(RenderUpload);

