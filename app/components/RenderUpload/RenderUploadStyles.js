import styleVars from '../../configs/styleVars';
const colors = styleVars.colors;

export const zoneStyle = {
  width: '100%',
  height: 60,
  borderWidth: 2,
  borderColor: colors.primaryBGColor,
  borderStyle: 'solid',
  borderRadius: 5,
  textAlign: 'center',
};

export const disabledStyle = {
  opacity: 0.6,
};

export const uploadBtnStyles = {
  style: {
    buttonStyle: {
      width: 300,
      borderRadius: 0,
    },
  },
};
