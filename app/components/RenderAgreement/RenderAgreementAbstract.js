/**
*
* RenderAgreement
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import qs from 'query-string';

export default class RenderAgreementAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      showAgreement: false,
      docusignURL: null,
    };
    this.successStatus = 'signing_complete';
    this.handleRedirection = this.handleRedirection.bind(this);
  }

  getDocuSignUrl = () => {
    console.debug('calling getDocuSignUrl');
    const self = this;
    const targetURL = this.props.formData[this.props.compProps.docusignUrlKey];
    if (targetURL) {
      const reqData = {
        url: targetURL,
        params: {},
        userDetails: this.props.userDetails,
        successCb: (response) => {
          // Setup the render Data in the app
          // Call the render saga service
          if (response.response && response.response.url) {
            console.debug('response from docusignUrlKey', response.response);
            self.setState({ docusignURL: response.response.url, showAgreement: true });
          }
        },
        erroCb: (error) => {
          console.debug('error in getDocuSignUrl', error);
        },
      };

      this.props.getRequest({ key: null, data: reqData });
    }
  }

  handleRedirection = (url) => {
    const queryString = qs.extract(url);
    const { event, imageIndex } = qs.parse(queryString);
    (event === this.successStatus) && this.props.onChangeHandler({ event, imageIndex }, this.props.elmId);
  }

  render() {
    return null;
  }
}
