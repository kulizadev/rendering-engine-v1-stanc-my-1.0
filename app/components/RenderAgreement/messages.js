/*
 * RenderAgreement Messages
 *
 * This contains all the text for the RenderAgreement component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderAgreement.header',
    defaultMessage: 'This is the RenderAgreement component !',
  },
});
