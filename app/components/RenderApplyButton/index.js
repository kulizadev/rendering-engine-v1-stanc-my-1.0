/**
*
* RenderApplyButton
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import RenderApplyButtonComponent from './RenderApplyButtonComponent';

class RenderApplyButton extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderApplyButtonComponent {...this.props} />
    );
  }
}

RenderApplyButton.propTypes = {

};

export default RenderApplyButton;
