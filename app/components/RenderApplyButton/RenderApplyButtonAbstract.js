/**
 *
 * RenderApplyButton
 *
 */

import React from 'react';

import * as validationFns from '../../utilities/validations/validationFns';

export default class RenderApplyButtonpAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.applyLoan = this.applyLoan.bind(this);
  }

  applyLoan = () => {
    this.props.setUserDetails({
      ...this.props.userDetails,
      journeyStarted: 'true',
    });
  }

  render() {
    return null;
  }
}
