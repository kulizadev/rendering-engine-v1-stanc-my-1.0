/*
 * RenderApplyButton Messages
 *
 * This contains all the text for the RenderApplyButton component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderApplyButton.header',
    defaultMessage: 'This is the RenderApplyButton component !',
  },
});
