import styleVars from '../../configs/styleVars';
import { nativeWrapperStyle } from '../../configs/componentStyles/commonStyles';
const { fonts } = styleVars;

export const topViewStyles = {
  style: {
    paddingBottom: 30,
  },
};

export const wrapperStyles = {
  style: {
    ...nativeWrapperStyle,
  },
};

export const btnProps = {
  containerStyle: {
    marginTop: 10,
  },
};

export const headingStyles = {
  style: {
    ...fonts.getFontFamilyWeight(),
    fontSize: fonts.mainHeadingSize,
    textAlign: 'center',
    paddingBottom: 10,
  },
};


export const paraStyles = {
  style: {
    ...fonts.getFontFamilyWeight(),
    fontSize: fonts.paraSize,
    textAlign: 'center',
  },
};
