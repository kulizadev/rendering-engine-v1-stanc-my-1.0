/**
*
* RenderRadioAccordionAbstract
*
*/

import React from 'react';
// import styled from 'styled-components';

export default class RenderRadioAccordionAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  onChangeHandler = () => {
    const { compProps, elmId } = this.props;
    this.props.onChangeHandler(elmId, compProps.valueHolder);
  }

  render() {
    return null;
  }
}

RenderRadioAccordionAbstract.propTypes = {

};
