/**
*
* RenderGeotracking
*
*/

import React from 'react';
import RenderGeotrackingComponent from './RenderGeotrackingComponent';
import { createStructuredSelector } from 'reselect';
import * as appActions from '../../containers/AppDetails/actions';
import connect from 'react-redux/es/connect/connect';
import { compose } from 'redux';
// import styled from 'styled-components';


class RenderGeotracking extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderGeotrackingComponent {...this.props} />
    );
  }
}

RenderGeotracking.propTypes = {

};

const mapStateToProps = createStructuredSelector({
});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

// export default RenderRestInput;
export default compose(
  withConnect,
)(RenderGeotracking);
