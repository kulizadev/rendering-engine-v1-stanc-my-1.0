/**
*
* RenderGeotrackingComponent
*
*/

import React from 'react';
import { View, Text } from 'react-native';
import RenderGeotrackingAbstract from './RenderGeotrackingAbstract';
import RenderButton from '../RenderButton';
// import styled from 'styled-components';
import * as styles from './RenderGeotrackingStyles.native';
import RenderError from '../RenderError';

class RenderGeotrackingComponent extends RenderGeotrackingAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { compProps } = this.props;
    const renderData = this.props.renderData;
    const errorMessage = renderData.errorMessage;
    let error = null;
    if (errorMessage) {
      error = (<RenderError errorMessage={errorMessage} />);
    }
    return (
      <View style={styles.geoMainWrapper}>
        <View style={styles.geoSubsectionLeft}>
          <View style={compProps.value ? styles.disabledWrapper : {}}>
            <RenderButton
              type={'primary'}
              text={compProps.label || 'Geo Tag'}
              onPress={this.locate}
              {...styles.locateBtnStyle}
            />
          </View>
          {error}
        </View>
        <View style={styles.geoSubsectionRight}>
          {/* <Text>Latitude: {this.state.latitude && this.state.latitude.toFixed(4)}</Text>*/}
          {/* <Text>Longitude: {this.state.longitude && this.state.longitude.toFixed(4)}</Text>*/}
          {this.state.address ? <Text>Area : {this.state.address}</Text> : null}
          {this.state.error ? <Text>Error: {this.state.error}</Text> : null}
        </View>
      </View>
    );
  }
}

RenderGeotrackingComponent.propTypes = {

};

export default RenderGeotrackingComponent;
