/**
*
* RenderGeotrackingAbstract
*
*/

import React from 'react';
import { getThirdPartyAPIEndPoint } from '../../configs/appConfig';
import { getClientKey } from '../../configs/appConstants';
import { getLocation } from '../../utilities/locationService';
// import styled from 'styled-components';

export default class RenderGeotrackingAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      latitude: null,
      longitude: null,
      error: null,
      address: props.compProps.value,
    };
    this.locate = this.locate.bind(this);
    this.updateFormData = this.updateFormData.bind(this);
  }

  locate = async () => {
    if (!this.props.compProps.value) {
      try {
        const position = await getLocation();
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });
        const currentLocation = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        };
        this.getAddress(currentLocation);
      } catch (error) {
        console.log(error);
        this.setState({ error: error.message });
      }
    }
  }

  updateFormData(value) {
    this.props.onChangeHandler(value, this.props.elmId);
  }

  getAddress = ({ latitude, longitude }) => {
    const reqObj = {
      disableHeaders: true,
      params: {
        key: getClientKey('google'),
        latlng: `${latitude},${longitude}`,
      },
      url: getThirdPartyAPIEndPoint('geocode'),
      successCb: (response) => {
        const { results } = response;
        if (results.length) {
          const approximateResults = results.filter((element) => element.geometry.location_type === 'APPROXIMATE');
          if (approximateResults.length) {
            const address = approximateResults[0].formatted_address;
            this.setState(
              { address },
              () => this.updateFormData(address)
             );
          }
        }
      },
      errorCb: () => {
        this.setState({ error: 'Unable to fetch address.' });
      },
    };
    this.props.getRequest({ data: reqObj });
  }

  render() {
    return null;
  }
}

RenderGeotrackingAbstract.propTypes = {

};
