import styled from 'styled-components';
import { colors } from '../../configs/styleVars';

export const MultiInstanceContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  padding : 10px;
`;

export const InstanceContainer = styled.div`
  margin-bottom: 15px;
  margin-left: -10px;
  padding-left: 15px;
  background-color: ${colors.white};
  box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px;
  border-radius: 2px;
  z-index: 1;
`;

export const InstanceHeader = styled.div`
  width: 100%;
  height: 60px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const ImageWrapper = styled.div`
`;

export const MultiInstanceWrapper = styled.div`
`;

export const UnitInstanceWrapper = styled.div`
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin: 10px 0;
`;

export const colWrapper = {
  paddingLeft: 15,
  paddingRight: 15,
};

export const ImageStyle = {
  style: {
    maxHeight: 40,
    margin: 10,
    padding: 0,
    opacity: 0.7,
  },
};
