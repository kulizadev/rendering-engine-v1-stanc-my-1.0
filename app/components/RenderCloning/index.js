/**
 *
 * RenderCloning
 *
 */

import React from 'react';
import RenderCloningComponent from './RenderCloningComponent';
import { createStructuredSelector } from 'reselect';
import { makeSelectRenderData } from '../../containers/RenderJourney/selectors';
import { makeSelectUserDetails } from '../../containers/AppDetails/selectors';
import * as appActions from '../../containers/AppDetails/actions';
import { connect } from 'react-redux';
import { compose } from 'redux';

class RenderCloning extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderCloningComponent {...this.props} />
    );
  }
}

//      {/*<RenderCloningComponent {...this.props} />*/}
RenderCloning.propTypes = {

};

const mapStateToProps = createStructuredSelector({
  journeyRenderData: makeSelectRenderData(),
  userDetails: makeSelectUserDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(RenderCloning);
