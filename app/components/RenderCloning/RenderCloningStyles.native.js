import styled from 'styled-components/native';
import { colors, globalColors } from '../../configs/styleVars';

export const MultiInstanceContainer = styled.View`
  width: 100%;
  display: flex;
  flex-direction: column;
  padding : 10px;
`;

export const InstanceContainer = styled.View`
  margin-bottom: 15px;
  margin-left: -10px;
  padding-left: 15px;
  background-color: ${colors.white};
  border-radius: 2px;
  border-color: ${globalColors.gray};
  border-width: 1px;
  `;

export const InstanceHeader = styled.View`
  width: 100%;
  height: 60px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const ImageWrapper = styled.View`
`;

export const MultiInstanceWrapper = styled.View`
`;

export const UnitInstanceWrapper = styled.View`
`;

export const ButtonWrapper = styled.View`
  display: flex;
  justify-content: center;
  margin: 10px 0;
`;

export const colWrapper = {
  paddingLeft: 15,
  paddingRight: 15,
};

export const ImageStyle = {
  style: {
    maxHeight: 40,
    margin: 10,
    padding: 0,
    opacity: 0.7,
    maxWidth: 50,
  },
};
