/**
 *
 * MultiInstanceComponent
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import dottie from 'dottie';
import cloneDeep from 'lodash/cloneDeep';
import { layoutConfig } from '../../configs/appConfig';
import { MultiInstanceWrapper, MultiInstanceContainer, InstanceContainer, ButtonWrapper, colWrapper as colWrapperStyle, InstanceHeader, ImageWrapper , ImageStyle} from './RenderCloningStyles';
import * as componentMap from '../../utilities/componentMap';
import componentProps from '../../containers/RenderStep/componentProps';
import Column from '../../components/Column/';
import RenderHeading from '../RenderHeading';
import RenderButton from '../../components/RenderButton';
import Row from '../../components/Row';
import deleteIcon from '../../images/delete.png';
import Img from '../Img';
import MultiInstance from '../../components/MultiInstance';

// import styled from 'styled-components';

class RenderCloningComponent extends MultiInstance {

  renderUnitInstance = (instance, data, instanceIndex) => {
    const fieldOrder = [...this.state.fieldOrder];
    const componentRenderData = cloneDeep(this.state.instancesRenderData);
    let elmList = [],
      renderRows = [],
      hiddenFields = [];
    let currentColWidth = 0;

    const instanceKeys = fieldOrder.filter((fieldId) => (componentRenderData[instanceIndex][fieldId] && !componentRenderData[instanceIndex][fieldId].isHidden));
    
    instanceKeys.forEach((elm, index) => {
      const keyProps = componentRenderData[instanceIndex] && componentRenderData[instanceIndex][elm];
      if (keyProps.isHidden) { return ;}
      const meta = typeof keyProps.metaData === 'string' ? JSON.parse(keyProps.metaData) : keyProps.metaData;
      const colWidth = meta.colWidth || layoutConfig.defaultColumnWidth;
      const instanceValue = (instance[elm] || instance[elm] === '') ? instance[elm] : meta.default;
      const formType = meta.form_type;
      const isUploadValue = (formType === 'upload' && !!instanceValue && !!instanceValue.length);
      keyProps.value = isUploadValue && instanceValue? JSON.parse(instanceValue) : instanceValue;
      const elmId = keyProps.id;
      const UnitComponent = componentMap.componentMapByFormType[formType];
      if (UnitComponent) {
        let compProps = componentProps[formType] &&
          componentProps[formType](
            keyProps,
            keyProps.id,
            keyProps.value,
            {...this.props.formData, ...instance}
          );
        const { instancesErrorData, instancesData } = this.state;
        const unitErrorObject = dottie.get(instancesErrorData, `${instanceIndex}.${elmId}`);
        const unitErrorMessage = dottie.get(unitErrorObject, 'errorMessage');
        const componentSection = (<UnitComponent
          compProps={{ ...compProps, error: unitErrorMessage }}
          key={elmId}
          elmId={elmId}
          renderData={{ ...keyProps, ...unitErrorObject }}
          formData={{ ...instancesData[instanceIndex] }}
          onChangeHandler={(value, unitKey = elmId) => this.onChangeHandler(value, unitKey, instanceIndex)}
          // onBlurHandler={value => this.onBlurHandler(value, elmId)}
        >
        </UnitComponent>);
        const compWrapper = (<Column style={{ ...colWrapperStyle }} colWidth={colWidth} offset={0} key={`compwrapper-${elmId}`} /* ` */>
          {componentSection}
        </Column>);
        if (meta.form_type !== 'hidden') {
          currentColWidth += colWidth;
          if (currentColWidth >= 12 || meta.newRow) {
            if (elmList.length) {
              renderRows.push(<Row key={`row-${index}`}>{[...elmList]}</Row>); // `
            }
            elmList.length = 0;
            currentColWidth = 0;
          }

          elmList.push(compWrapper);
        } else if (meta.form_type === 'hidden') {
          hiddenFields.push(componentSection);
        }

        // If its the last elem
        if (index === (instanceKeys.length - 1)) {
          if (elmList.length) {
            renderRows.push(<Row key={`row-${index}${2}`}>{[...elmList]}</Row>);  // `
          }
        }
      }
    });
    return [...renderRows,...hiddenFields];
  };

  renderMultiInstance = () => {
    const { renderData: instance, journeyRenderData } = { ...this.props };
    const { instancesData } = { ...this.state };
    if (instancesData && instancesData.length) {
      const { metaData: instanceMeta } = instance;
      const { subheading: instanceSubheading } = instanceMeta;
      return (
        <MultiInstanceContainer>
          {
            instancesData.map((instanceRowData , index) => (
              <InstanceContainer key={'instanceContainer-'+index}>
                <InstanceHeader>
                  <RenderHeading
                    compProps={{
                      label: `${instanceSubheading} - ${index + 1}`,  // `
                      type: 'h3',
                    }}
                  />
                  {this.isDeleteAllowed() ?
                    (<ImageWrapper
                      onStartShouldSetResponder={() => this.deleteRow(index)} // for native
                      onClick={() => this.deleteRow(index)} // for web
                    >
                      <Img
                        {...ImageStyle}
                        alt={'Delete Instance'}
                        src={deleteIcon}
                      />
                    </ImageWrapper>)
                  : null
                  }
                </InstanceHeader>
                {
                  this.renderUnitInstance(instanceRowData, journeyRenderData, index)
                }
              </InstanceContainer>
            ))
          }
        </MultiInstanceContainer>
      );
    } else return null;
  };

  render() {
    const { renderData: { metaData: { subheading: instanceSubheading } } } = { ...this.props };
    const headingProps = {
      compProps: {
        label: instanceSubheading,
        type: 'h2',
      },
    };
    return (
      <MultiInstanceWrapper>
        <RenderHeading {...headingProps} />
        {this.renderMultiInstance()}
        <ButtonWrapper>
          {this.isAddAllowed()
              ? <RenderButton
                  type={this.isAddAllowed() ? 'primary' : 'disabled'}
                  onPress={this.addRow} // for native  // TODO : change this to one
                  onClick={this.addRow} // for web
                  label={'+ Add '+ instanceSubheading}
                  text={'+ Add '+ instanceSubheading}
                  disabled={!this.isAddAllowed()}
                />
              : null
          }
        </ButtonWrapper>
      </MultiInstanceWrapper>
    );
  }
}

RenderCloningComponent.propTypes = {
  onChangeHandler: PropTypes.func,
};

export default RenderCloningComponent;
