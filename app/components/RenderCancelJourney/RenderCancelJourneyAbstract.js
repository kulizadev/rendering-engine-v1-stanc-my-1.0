/**
*
* RenderCancelJourneyAbstract
*
*/

import React from 'react';
// import styled from 'styled-components';
import { strings } from '../../../locales/i18n';

export default class RenderCancelJourneyAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  getPopUpdata = () => {
    const self = this;
    return {
      headerText: strings('renderJourney.cancelPopupHead'),
      bodyText: strings('renderJourney.cancelPopupBody'),
      actionButtons: [{
        label: strings('renderJourney.cancelPopupCancel'),
        callback: this.props.cancelJourney,
        action: 'callback',
        type: 'secondary',
      },
      {
        label: strings('renderJourney.cancelPopupContinue'),
        action: 'hide',
        type: 'primary',
      }],
      type: 'info',
      showPopup: true,
    };
  }

  render() {
    return null;
  }
}

RenderCancelJourneyAbstract.propTypes = {

};
