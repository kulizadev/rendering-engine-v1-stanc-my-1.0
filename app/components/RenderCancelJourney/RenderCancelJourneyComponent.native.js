/**
*
* RenderCancelJourneyComponent
*
*/

import React from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import RenderCancelJourneyAbstract from './RenderCancelJourneyAbstract';
// import styled from 'styled-components';
import {
  cancelButtonProps,
  cancelIconStyleProps,
} from './style.native';
import iconSrc from '../../images/cancel.png';

class RenderCancelJourneyComponent extends RenderCancelJourneyAbstract { // eslint-disable-line react/prefer-stateless-function

  cancelApplication = () => {
    this.props.setPopupData(this.getPopUpdata());
  }

  render() {
    return (
      <View {...cancelButtonProps}>
        <TouchableOpacity
          onPress={this.cancelApplication}
        >
          <Image
            source={iconSrc}
            {...cancelIconStyleProps}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

RenderCancelJourneyComponent.propTypes = {

};

export default RenderCancelJourneyComponent;
