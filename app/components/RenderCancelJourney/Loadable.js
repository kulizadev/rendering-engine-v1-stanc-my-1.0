/**
 *
 * Asynchronously loads the component for RenderCancelJourney
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
