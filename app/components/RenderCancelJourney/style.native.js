import { sizes } from '../../configs/styleVars';

export const cancelIconStyleProps = {
  style: { height: 25, width: 25 },
  resizeMode: 'contain',
};

export const cancelButtonProps = {
  style: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    flex: 0,
    alignItems: 'center',
    paddingLeft: sizes.appPaddingLeft,
    paddingVertical: 5,
  },
};
