/**
*
* RenderCancelJourneyComponent
*
*/

import React from 'react';
import RenderCancelJourneyAbstract from './RenderCancelJourneyAbstract';
// import styled from 'styled-components';


class RenderCancelJourneyComponent extends RenderCancelJourneyAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>This is RenderCancelJourney Component</div>
    );
  }
}

RenderCancelJourneyComponent.propTypes = {

};

export default RenderCancelJourneyComponent;
