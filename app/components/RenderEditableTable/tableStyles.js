import { colors, fonts, globalColors } from '../../configs/styleVars';


export const headerColumnWrapperStyle = {
  padding: '10px',
  wordWrap: 'break-word',
  whiteSpace: 'normal',
  background: colors.journeyWrapperBGColor,
  color: colors.secondaryFontColor,
  fontSize: fonts.body,
  width: '200px',
  borderWidth: '4px',
  borderStyle: 'solid',
  borderColor: colors.journeyWrapperBGColor,
  ...fonts.getFontFamilyWeight('bold'),
};

export const dropdownStyle = {
  height: '54px',
};

export const underlineStyle = {
  display: 'none',
};

export const cellColumnWrapperStyle = {
  padding: '0 8px',
  borderWidth: '4px',
  borderStyle: 'solid',
  borderColor: globalColors.lightGray,
  height: '40px',
  width: '200px',
};

export const cellColumnErrorStyle = {
  backgroundColor: colors.tableCellErrorColor,
};

export const buttonWrapperStyle = {
  textAlign: 'center',
  margin: '10px 0',
};

export const colWidth = 200;
