/*
 * RenderEditableTable Messages
 *
 * This contains all the text for the RenderEditableTable component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderEditableTable.header',
    defaultMessage: 'This is the RenderEditableTable component !',
  },
});
