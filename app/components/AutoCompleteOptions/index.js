/**
*
* AutoCompleteOptions
*
*/

import React from 'react';
import AutoCompleteOptionsComponent from './AutoCompleteOptionsComponent';

class AutoCompleteOptions extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <AutoCompleteOptionsComponent {...this.props} />
    );
  }
}

AutoCompleteOptions.propTypes = {

};

export default AutoCompleteOptions;
