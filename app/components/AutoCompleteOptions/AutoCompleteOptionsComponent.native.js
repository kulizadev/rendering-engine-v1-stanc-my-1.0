/**
*
* AutoCompleteOptions
*
*/

import React from 'react';
import { VirtualizedList } from 'react-native';
import AutoCompleteOptionsAbstract from './AutoCompleteOptionsAbstract';
import * as styles from './AutoCompleteOptionsStyles';
import ListLabel from '../ListLabel';

export default class AutoCompleteOptionsComponent extends AutoCompleteOptionsAbstract { // eslint-disable-line react/prefer-stateless-function

  renderOptions = (option) => {
    const { listType, selectedValue, selectedIcon, deselectedIcon } = this.props;
    let rightIcon = {};
    if (listType === 'dropdown') {
      rightIcon = (selectedValue === option.value) ? selectedIcon : deselectedIcon;
    } else if (listType === 'multiselect-dropdown') {
      const isSelected = selectedValue.find((value) => value === option.value);
      rightIcon = isSelected ? selectedIcon : deselectedIcon;
    }
    return (
      <ListLabel
        onPress={() => this.props.updateOption(option)}
        label={option.name}
        leftIcon={option.icon}
        leftIconType={option.iconType}
        rightIcon={rightIcon}
      />
    );
  }

  render() {
    const { style } = this.props;
    return (
      <VirtualizedList
        style={[styles.listStyles, style]}
        keyboardShouldPersistTaps="always"
        data={this.props.optionsList}
        windowSize={10}
        getItem={(data, index) => data[index]}
        getItemCount={() => this.props.optionsList.length}
        renderItem={({ item }) => this.renderOptions(item)}
        keyExtractor={(item, index) => index.toString()}
      />
    );
  }
}
