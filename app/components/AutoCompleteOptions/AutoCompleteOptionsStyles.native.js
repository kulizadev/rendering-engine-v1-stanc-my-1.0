import { Dimensions } from 'react-native';
import { colors } from '../../configs/styleVars';

export const optionsListStyles = {
  style: {
    backgroundColor: colors.white,
  },
};

export const listStyles = {
  paddingLeft: 20,
};
