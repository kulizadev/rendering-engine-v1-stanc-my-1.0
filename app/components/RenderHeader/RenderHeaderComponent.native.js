/**
*
* RenderHeader
*
*/

import React from 'react';
import { View } from 'react-native';
import Text from '../RenderText';
import RenderHeaderAbstract from './RenderHeaderAbstract';

import * as common from '../../utilities/commonUtils';
import * as styles from './HeaderStyle';

export default class RenderHeaderComponent extends RenderHeaderAbstract { // eslint-disable-line react/prefer-stateless-function


  render() {
    const date = new Date();
    const formattedData = common.formatDefaultDate(date);
    if (this.props.stepData && this.props.stepData.taskName) {
      return (<View {...styles.wrapperStyles}>
        <Text {...styles.headingStyle}>{this.props.stepData.taskName} </Text>
        {/* <Text {...styles.descriptionStyle}>{this.props.stepData.description} </Text>*/}
        <View style={{ flexDirection: 'column', alignItems: 'flex-end' }}>
          {/* <Text {...styles.applicationStyle}>Loan Application No.: # {this.props.applicationId} </Text>
                                    <Text {...styles.descItalicStyle}>Date: {formattedData} </Text>*/}
        </View>
      </View>);
    }

    return null;
  }
}
