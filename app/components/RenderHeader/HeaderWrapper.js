import appVars from '../../configs/styleVars';
import styled from 'styled-components';
const colors = appVars.colors;

const HeaderWrapper = styled.div`
  padding: 10px 20px;
  text-align: left;
  width: 100%;
  color: ${colors.basicFontColor};
  font-size: 18px;
  background-color: white;
  margin-bottom: 6px;
  flex-direction: row;
  justify-content: space-between;
  display: flex,
`;

export default HeaderWrapper;
