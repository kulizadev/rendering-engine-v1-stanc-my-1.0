/*
 * RenderHeader Messages
 *
 * This contains all the text for the RenderHeader component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderHeader.header',
    defaultMessage: 'This is the RenderHeader component !',
  },
});
