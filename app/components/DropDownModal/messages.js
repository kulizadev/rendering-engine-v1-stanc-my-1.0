/*
 * DropDownModal Messages
 *
 * This contains all the text for the DropDownModal component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.DropDownModal.header',
    defaultMessage: 'This is the DropDownModal component !',
  },
});
