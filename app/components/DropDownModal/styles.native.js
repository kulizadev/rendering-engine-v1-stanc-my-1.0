import { Dimensions } from 'react-native';
import { colors } from '../../configs/styleVars';

export const modal = {
  style: {
    justifyContent: 'flex-end',
    margin: 0,
  },
};

export const wrapper = {
  style: {
    backgroundColor: colors.white,
    maxHeight: Dimensions.get('window').height / 1.5,
  },
};

export const heading = {
  style: {
    margin: 20,
    marginBottom: 10,
  },
};
