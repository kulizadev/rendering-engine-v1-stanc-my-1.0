/**
*
* DropDownModal
*
*/

import React from 'react';
import { View, Text, ScrollView } from 'react-native';

import AutoCompleteOptions from '../AutoCompleteOptions/';
import * as styles from './styles';
import Modal from '../Modal';

class DropDownModal extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  render() {
    const { showList, handleBackPress, value, options, updateOption, label } = this.props;
    let optionsList = (<ScrollView></ScrollView>);
    if (showList) {
      optionsList = (
        <AutoCompleteOptions
          listType={'dropdown'}
          optionsList={options}
          updateOption={updateOption}
          selectedValue={value}
          selectedIcon={'radioSelect'}
          deselectedIcon={'radioDeselect'}
        />
      );
    }
    return (
      <Modal
        isVisible={showList}
        handleBackPress={handleBackPress}
        onBackdropPress={handleBackPress}
        onSwipe={handleBackPress}
        swipeDirection={'down'}
        {...styles.modal}
      >
        <View
          {...styles.wrapper}
        >
          <Text {...styles.heading}>{label}</Text>
          {optionsList}
        </View>
      </Modal>
    );
  }
}

DropDownModal.propTypes = {

};

export default DropDownModal;
