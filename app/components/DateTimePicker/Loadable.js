/**
 *
 * Asynchronously loads the component for DateTimePicker
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
