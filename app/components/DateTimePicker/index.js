/**
*
* DateTimePicker
*
*/

import React from 'react';
import DateTimePickerComponent from './DateTimePickerComponent';
// import styled from 'styled-components';


class DateTimePicker extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <DateTimePickerComponent {...this.props} />
    );
  }
}

DateTimePicker.propTypes = {

};

export default DateTimePicker;
