/**
*
* DateTimePickerComponent
*
*/

import React from 'react';
import { View, Text } from 'react-native';
import DateTimePickerAbstract from './DateTimePickerAbstract';
// import styled from 'styled-components';


class DateTimePickerComponent extends DateTimePickerAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <View><Text>This is DateTimePicker Component</Text></View>
    );
  }
}

DateTimePickerComponent.propTypes = {

};

export default DateTimePickerComponent;
