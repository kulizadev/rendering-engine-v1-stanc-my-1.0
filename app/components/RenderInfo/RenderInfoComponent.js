/**
*
* RenderInfoComponent
*
*/

import React from 'react';
import RenderInfoAbstract from './RenderInfoAbstract';
// import styled from 'styled-components';


class RenderInfoComponent extends RenderInfoAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>This is RenderInfo Component</div>
    );
  }
}

RenderInfoComponent.propTypes = {

};

export default RenderInfoComponent;
