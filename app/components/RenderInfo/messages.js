/*
 * RenderInfo Messages
 *
 * This contains all the text for the RenderInfo component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderInfo.header',
    defaultMessage: 'This is the RenderInfo component !',
  },
});
