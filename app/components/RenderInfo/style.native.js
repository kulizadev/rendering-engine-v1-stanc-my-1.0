export const wrapperStyles = {
  marginLeft: 16,
};

export const iconStyles = {
  width: 24,
  height: 24,
};
