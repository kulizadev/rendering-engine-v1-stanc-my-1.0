/**
*
* RenderInfo
*
*/

import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import RenderInfoComponent from './RenderInfoComponent';
// import styled from 'styled-components';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

import * as appActions from '../../containers/AppDetails/actions';

class RenderInfo extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderInfoComponent {...this.props} />
    );
  }
}

RenderInfo.propTypes = {

};

const mapStateToProps = createStructuredSelector({

});

function mapDispatchToProps(dispatch) {
  return {
    setPopupData: (reqObject) => dispatch(appActions.setPopupData(reqObject)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(RenderInfo);
