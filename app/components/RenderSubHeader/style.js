import { colors, fonts } from '../../configs/styleVars';

export const subHeadStyle = {
  color: colors.primaryBGColor,
  fontSize: fonts.subHeadingSize,
  ...fonts.getFontFamilyWeight(),
  textAlign: 'center',
  paddingTop: 10,
  paddingBottom: 10,
};
