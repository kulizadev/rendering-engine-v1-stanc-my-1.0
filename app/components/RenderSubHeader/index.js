/**
*
* RenderSubHeader
*
*/

import React, { Children } from 'react';
import PropTypes from 'prop-types';
import Text from '../RenderText';
import { subHeadStyle } from './style';

// import styled from 'styled-components';


class RenderSubHeader extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Text style={subHeadStyle}>
        {Children.toArray(this.props.children)}
      </Text>
    );
  }
}

RenderSubHeader.propTypes = {
  children: PropTypes.node,
};

export default RenderSubHeader;
