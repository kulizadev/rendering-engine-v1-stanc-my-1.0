import appVars from '../../configs/styleVars';
import styled from 'styled-components';
const colors = appVars.colors,
  sizes = appVars.sizes;

const RadioCardLabel = styled.div`
  margin-bottom: 10px;
  margin-top: 15px;
`;

const RadioCardsWrapper = styled.div`
  margin-bottom: 10px;
`;

const RadioCardWrapper = styled.div`
  margin: 10px;
  cursor: pointer;
  transform:  ${(props) => props.selected ? 'scale(1.02)' : 'scale(1)'};
  transition: all 0.2s ease-in;
`;

const selectedCardHeaderStyles = {
  background: colors.primaryBGColor,
};

const selectedTextStyles = {
  color: colors.white,
};
const imageStyle = {
  maxWidth: '100%',
  maxHeight: '100%',
  minWidth: 'auto',
  height: 'auto',
  width: 'auto',
};
const cardMediaStyle = {
  textAlign: 'center',
  display: 'flex',
  justifyContent: 'center',
  padding: 5,
  height: 200,
};
export { RadioCardLabel, RadioCardWrapper, RadioCardsWrapper, selectedCardHeaderStyles, selectedTextStyles, cardMediaStyle, imageStyle };
