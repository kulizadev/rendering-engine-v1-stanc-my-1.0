/**
*
* RenderCheckbox
*
*/

import React from 'react';
import { View } from 'react-native';
import RenderError from '../RenderError';
import Text from '../RenderText';
import RenderCheckboxAbstract from './RenderCheckboxAbstract';
import { CheckBox } from 'react-native-elements';

import * as styles from './RenderCheckboxStyles';
import styledComponents from '../../configs/styledComponents';

export default class RenderCheckboxComponent extends RenderCheckboxAbstract { // eslint-disable-line react/prefer-stateless-function


  onChangeHandler = () => {
    this.props.onChangeHandler(this.state.checked, this.props.elmId);
  };

  toggleCheckState = (event, id) => {
    this.setState({ checked: !this.state.checked }, () => this.onChangeHandler());
  }

  render() {
    let errorMessage = null;
    if (this.props.renderData.errorMessage) {
      errorMessage = (<RenderError errorMessage={this.props.renderData.errorMessage} />);
    }
    const hintText = styledComponents.getStyledHintText(this.props.hintText);

    return (
      <View>
        <CheckBox
          {...this.props.compProps}
          {...styles.checkboxStyles}
          onPress={this.toggleCheckState}
          iconType="material-community"
          checkedIcon="checkbox-marked"
          uncheckedIcon="checkbox-blank-outline"
          fontFamily={styles.checkboxFontFamily}
        />
        {errorMessage}
        {hintText}
      </View>
    );
  }
}
