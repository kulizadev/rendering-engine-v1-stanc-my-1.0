/**
*
* RenderCheckbox
*
*/

import React from 'react';
// import styled from 'styled-components';
import Checkbox from 'material-ui/Checkbox';

import RenderCheckboxAbstract from './RenderCheckboxAbstract';
import * as styles from './RenderCheckboxStyles';
import CheckboxField from '../Checkbox';


export default class RenderCheckboxComponent extends RenderCheckboxAbstract { // eslint-disable-line react/prefer-stateless-function


  render() {
    const errorMessage = this.props.renderData.errorMessage;

    const componentProps = { ...this.props.compProps, ...styles.checkBoxStyles };

    return (
      <CheckboxField
        componentProps={componentProps}
        errorMessage={errorMessage}
        className="field-container"
        onChangeHandler={this.onChangeHandler}
        onBlurHandler={this.onBlurHandler}
      />
    );
  }
}
