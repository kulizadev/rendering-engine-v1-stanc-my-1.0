/**
*
* RenderSlider
*
*/

import React from 'react';
import { View, Image, Text } from 'react-native';
// import styled from 'styled-components';
import StepIndicator from 'react-native-step-indicator';

import * as styles from './StepperStyles';
import RenderStepperAbstract from './RenderStepperAbstract';
import current from '../../images/current.png';
import completed from '../../images/completed.png';

export default class RenderStepperComponent extends RenderStepperAbstract { // eslint-disable-line react/prefer-stateless-function

  getIndicatorComponent = (icon) => (
    <Image
      source={icon}
    />
    )

  renderStepIndicator = (status) => {
    const { stepStatus, position } = status;
    if (stepStatus === 'current') {
      return this.getIndicatorComponent(current);
    } else if (stepStatus === 'finished') {
      return this.getIndicatorComponent(completed);
    }
    return (<Text style={styles.indicatorStyles}>{position + 1}</Text>);
  }

  onPress = (index) => {
    if (index < this.state.currentActiveIndex) {
      const { defaultScreenList, backTrigger } = this.props;
      const backScreenKey = Array.isArray(defaultScreenList) && defaultScreenList[index];
      if (backScreenKey) {
        backTrigger(backScreenKey);
      }
    }
  }

  render() {
    const { type } = this.props;
    let direction = 'horizontal';
    let customStyles = styles.stepperConfig;
    let wrapperStyles = styles.wrapperStyle;
    if (type === 'fullscreen') {
      direction = 'vertical';
      customStyles = styles.fullscreenStepperConfig;
      wrapperStyles = styles.fullWrapperStyle;
    }
    return (
      <View style={wrapperStyles}>
        <StepIndicator
          labels={this.props.labels}
          currentPosition={this.state.currentActiveIndex}
          customStyles={customStyles}
          stepCount={this.state.stepCount}
          direction={direction}
          onPress={this.onPress}

        />
      </View>
    );
  }
}
