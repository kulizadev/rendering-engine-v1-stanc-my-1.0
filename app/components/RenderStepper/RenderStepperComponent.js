/**
 *
 * RenderSlider
 *
 */

import React from "react";
// import styled from 'styled-components';

import { FormattedMessage } from "react-intl";
import messages from "./messages";
import { Step, Stepper, StepLabel, StepButton } from "material-ui/Stepper";
import RenderStepperAbstract from "./RenderStepperAbstract";
import * as styles from "./StepperStyles";
import { colors, fonts } from "../../configs/styleVars";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import getMuiTheme from "material-ui/styles/getMuiTheme";

export default class RenderStepperComponent extends RenderStepperAbstract {
  // eslint-disable-line react/prefer-stateless-function

  onPress = (index, idx) => {
    console.debug("what is index", index, idx);
    if (index < this.state.currentActiveIndex) {
      const { defaultScreenList, backTrigger } = this.props;
      const backScreenKey =
        Array.isArray(defaultScreenList) && defaultScreenList[index];
      if (backScreenKey) {
        backTrigger(backScreenKey);
      }
    }
  };

  render() {
    const muiTheme = getMuiTheme({
      stepper: { iconColor: colors.white }
    });
    const { currentActiveIndex } = this.state,
      { labels } = this.props,
      maxWidth = `${100 / labels.length}%`;
    const stepperList = labels.map((label, index) => {
      if (currentActiveIndex === index) {
        return (
          <Step key={label} style={{ height: "40px" }}>
            <StepLabel
              icon={
                <div
                  style={{
                    width: 10,
                    display: "inline-block",
                    height: 10,
                    border: "1px solid",
                    borderColor: colors.white,
                    borderRadius: "100%",
                    marginLeft: "7px",
                    backgroundColor: colors.white
                  }}
                ></div>
              }
              onClick={() => this.onPress(index)}
            >
              <span style={{ color: colors.white, fontWeight: "bold" }}>
                {label}
              </span>
            </StepLabel>
          </Step>
        );
      }

      if (currentActiveIndex > index) {
        return (
          <Step key={label} style={{ height: "40px" }}>
            <StepLabel
              icon={
                <div
                  style={{
                    width: 10,
                    display: "inline-block",
                    height: 10,
                    border: "1px solid",
                    borderColor: colors.greenOvalColor,
                    borderRadius: "100%",
                    marginLeft: "7px",
                    backgroundColor: colors.greenOvalColor,
                    color: "black"
                  }}
                ></div>
              }
              onClick={() => this.onPress(index)}
            >
              <span style={{ color: colors.white }}>{label}</span>
            </StepLabel>
          </Step>
        );
      }

      if (true) {
        return (
          <Step key={label} style={{ height: "40px" }}>
            <StepLabel
              icon={
                <div
                  style={{
                    width: 10,
                    display: "inline-block",
                    height: 10,
                    border: "1px solid",
                    borderColor: colors.white,
                    borderRadius: "100%",
                    marginLeft: "7px"
                  }}
                ></div>
              }
              onClick={() => this.onPress(index)}
            >
              <span style={{ color: colors.white }}>{label}</span>
            </StepLabel>
          </Step>
        );
      }
    });
    return (
      <div style={{ background: colors.blueText }}>
        <MuiThemeProvider muiTheme={muiTheme}>
          <Stepper activeStep={currentActiveIndex} orientation="vertical">
            {stepperList}
          </Stepper>
        </MuiThemeProvider>
      </div>
    );
  }
}
