import { colors, fonts, lineHeight } from '../../configs/styleVars';

export const stepperConfig = {
  stepIndicatorSize: 20,
  currentStepIndicatorSize: 20,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 0,
  stepStrokeCurrentColor: colors.primaryBGColor,
  stepStrokeWidth: 1,
  stepStrokeFinishedColor: colors.primaryBGColor,
  stepStrokeUnFinishedColor: colors.lighterColor,
  separatorFinishedColor: colors.lighterColor,
  separatorUnFinishedColor: colors.lighterColor,
  stepIndicatorFinishedColor: colors.primaryBGColor,
  stepIndicatorUnFinishedColor: colors.white,
  stepIndicatorCurrentColor: colors.white,
  stepIndicatorLabelFontSize: 12,
  currentStepIndicatorLabelFontSize: 12,
  stepIndicatorLabelCurrentColor: colors.primaryBGColor,
  stepIndicatorLabelFinishedColor: colors.white,
  stepIndicatorLabelUnFinishedColor: colors.labelColor,
  labelColor: '#999999',
  labelSize: 10,
  currentStepLabelColor: colors.primaryBGColor,
};

export const fullscreenStepperConfig = {
  ...stepperConfig,
  stepIndicatorSize: 20,
  currentStepIndicatorSize: 20,
  separatorStrokeWidth: 2,
  stepStrokeWidth: 2,
  stepIndicatorLabelFontSize: fonts.body,
  currentStepIndicatorLabelFontSize: fonts.body,
  labelSize: fonts.body,
  currentStepLabelColor: colors.black,
};

export const fullWrapperStyle = { marginTop: 15, flex: 1, width: '100%', alignItems: 'center' };

export const wrapperStyle = { marginTop: 15 };

export const indicatorStyles = { fontSize: fonts.label, lineHeight: lineHeight.label, ...fonts.getFontFamilyWeight('bold'), paddingBottom: 2 };
