/**
*
* RenderStepper
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import RenderStepperComponent from './RenderStepperComponent';

class RenderStepper extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderStepperComponent {...this.props} />
    );
  }
}

RenderStepper.propTypes = {

};

export default RenderStepper;
