/**
*
* RenderPrebuiltStatic
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import RenderPrebuiltStaticComponent from './RenderPrebuiltStaticComponent';

class RenderPrebuiltStatic extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderPrebuiltStaticComponent {...this.props} />
    );
  }
}

RenderPrebuiltStatic.propTypes = {

};

export default RenderPrebuiltStatic;
