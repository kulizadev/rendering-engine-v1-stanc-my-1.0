import styleVars from '../../configs/styleVars';
const { colors, fonts } = styleVars;

export const classesStyles = {
  'loan-info': {
    textAlign: 'center',
    color: colors.basicFontColor,
    marginTop: 15,
    marginBottom: 0,
  },
  'thank-heading': {
    textAlign: 'center',
    color: colors.basicFontColor,
    marginTop: 20,
    marginBottom: 0,
    fontSize: 22,
  },
};

export const baseFontStyle = {
  fontSize: 20,
  ...fonts.getFontFamilyWeight('bold'),
};
