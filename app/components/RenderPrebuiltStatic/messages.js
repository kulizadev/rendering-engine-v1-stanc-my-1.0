/*
 * RenderPrebuiltStatic Messages
 *
 * This contains all the text for the RenderPrebuiltStatic component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderPrebuiltStatic.header',
    defaultMessage: 'This is the RenderPrebuiltStatic component !',
  },
});
