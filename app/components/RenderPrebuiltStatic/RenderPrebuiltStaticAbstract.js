/**
*
* RenderInputText
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

export default class RenderPrebuiltStaticAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      messageHTML: (this.props.renderData.message && this.props.renderData.message.value) || this.props.renderData.value,
    };
  }

  componentWillMount() {

  }

  render() {
    return null;
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
onBlur Fn
*/
