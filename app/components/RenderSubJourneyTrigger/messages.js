/*
 * RenderSubJourneyTrigger Messages
 *
 * This contains all the text for the RenderSubJourneyTrigger component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderSubJourneyTrigger.header',
    defaultMessage: 'This is the RenderSubJourneyTrigger component !',
  },
});
