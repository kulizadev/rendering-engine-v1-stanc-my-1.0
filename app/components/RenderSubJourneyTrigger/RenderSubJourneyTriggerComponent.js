/**
*
* RenderSubJourneyTriggerComponent
*
*/

import React from 'react';
import RenderSubJourneyTriggerAbstract from './RenderSubJourneyTriggerAbstract';
// import styled from 'styled-components';


class RenderSubJourneyTriggerComponent extends RenderSubJourneyTriggerAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>This is RenderSubJourneyTrigger Component</div>
    );
  }
}

RenderSubJourneyTriggerComponent.propTypes = {

};

export default RenderSubJourneyTriggerComponent;
