/**
*
* RenderSubJourneyTriggerComponent
*
*/

import React from 'react';
import { View, Text } from 'react-native';
import RenderSubJourneyTriggerAbstract from './RenderSubJourneyTriggerAbstract';
import ListLabel from '../ListLabel';
import {
  labelStyles,
} from '../Accordion/styles';
// import styled from 'styled-components';

import { subJourneyIds } from '../../containers/RenderSubJourney/SubJourneyComponents/subJourneyService';

const idToIconTypeMap = {
  [subJourneyIds.nationalId]: 'ID',
  [subJourneyIds.selfie]: 'selfie',
  [subJourneyIds.facebook]: 'fb',
};

class RenderSubJourneyTriggerComponent extends RenderSubJourneyTriggerAbstract { // eslint-disable-line react/prefer-stateless-function

  isAllowed = () => {
    const self = this;
    const meta = this.props.renderData.metaData;
    let bool = true;
    if (meta.dependsOn && Array.isArray(meta.dependsOn)) {
      console.debug('what is dependsOn', meta.dependsOn);
      meta.dependsOn.forEach((fieldId) => {
        console.debug('In dependsOn self.props.formData[fieldId]', self.props.formData[fieldId], !self.props.formData[fieldId]);
        // TODO: has to check agains the app's renderdata for this elmID isValid
        if (!self.props.formData[fieldId]) {
          bool = false;
        }
      });
    }
    // If things have been verified then they should not be able to go into journey again
    if (self.props.renderData.isValid) {
      bool = false;
    }
    return bool;
  }

  render() {
    let rightIcon = 'arrow';
    let style = {};
    if (this.props.renderData.isValid) {
      rightIcon = 'tick-mark';
    }
    if (!this.isAllowed()) {
      rightIcon = 'click-not-allowed';
      style = { opacity: 0.5 };
    }

    const leftIconType = idToIconTypeMap[this.props.elmId];
    const rightIconType = this.props.renderData.isValid ? 'completed' : 'rightArrow';

    return (
      <View style={style}>
        <ListLabel
          leftIcon={leftIconType}
          leftIconType={leftIconType}
          rightIcon={rightIconType}
          rightIconType={rightIconType}
          label={this.props.compProps.label}
          onPress={this.isAllowed() && this.activateSubJourney}
          labelStyles={labelStyles}
        />

      </View>
    );
  }
}

RenderSubJourneyTriggerComponent.propTypes = {

};

export default RenderSubJourneyTriggerComponent;
