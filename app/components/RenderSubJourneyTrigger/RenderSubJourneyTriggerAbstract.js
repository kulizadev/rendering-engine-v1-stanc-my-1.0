/**
*
* RenderSubJourneyTriggerAbstract
*
*/

import React from 'react';
// import styled from 'styled-components';

export default class RenderSubJourneyTriggerAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {

    };
  }


  activateSubJourney = () => {
    console.debug('activateSubJourney@@@@-- ', this.props.elmId);
    this.props.setSubJourneyId(this.props.elmId);
  }

  render() {
    return null;
  }
}

RenderSubJourneyTriggerAbstract.propTypes = {

};
