/**
*
* RenderLoanApprovalComponent
*
*/

import React from 'react';
import RenderLoanApprovalAbstract from './RenderLoanApprovalAbstract';
// import styled from 'styled-components';


class RenderLoanApprovalComponent extends RenderLoanApprovalAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>This is RenderLoanApproval Component</div>
    );
  }
}

RenderLoanApprovalComponent.propTypes = {

};

export default RenderLoanApprovalComponent;
