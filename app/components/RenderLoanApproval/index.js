/**
*
* RenderLoanApproval
*
*/

import React from 'react';
import RenderLoanApprovalComponent from './RenderLoanApprovalComponent';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class RenderLoanApproval extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderLoanApprovalComponent {...this.props} />
    );
  }
}

RenderLoanApproval.propTypes = {

};

export default RenderLoanApproval;
