/**
*
* TablePagination
*
*/

import React from 'react';
import TablePaginationComponent from './TablePaginationComponent';
// import styled from 'styled-components';


class TablePagination extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <TablePaginationComponent {...this.props} />
    );
  }
}

TablePagination.propTypes = {

};

export default TablePagination;
