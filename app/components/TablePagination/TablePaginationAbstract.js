/**
*
* TablePaginationAbstract
*
*/

import React from 'react';
// import styled from 'styled-components';

export default class TablePaginationAbstract extends React.Component { // eslint-disable-line react/prefer-stateless-function
  customPaginationLabel = (from, to, count) =>
  `${from} to ${to} of ${count}`;

  render() {
    return null;
  }
}

TablePaginationAbstract.propTypes = {

};
