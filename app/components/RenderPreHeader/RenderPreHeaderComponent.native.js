/**
*
* RenderPreHeaderComponent
*
*/

import React from 'react';
import { View, Image } from 'react-native';
import Text from '../RenderText';
import RenderBackButton from '../RenderBackButton';
import callIcon from '../../images/call.png';
import RenderPreHeaderAbstract from './RenderPreHeaderAbstract';
import {
  containerStyle,
  textWrapStyle,
  textStyle,
  callIconStyleProps,
} from './styles.native';
// import styled from 'styled-components';


class RenderPreHeaderComponent extends RenderPreHeaderAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    let callBlock = null;
    let titleBlock = null;
    if (!this.props.hideCallAction) {
      callBlock = (<View>
        <Image
          source={callIcon}
          {...callIconStyleProps}
        />
      </View>);
    }
    if (this.props.title) {
      titleBlock = (<View style={textWrapStyle}>
        <Text style={textStyle}>{this.props.title}</Text>
        {callBlock}
      </View>);
    }

    return (
      <View style={containerStyle}>
        <RenderBackButton onBack={this.props.onBack} />
        {titleBlock}
      </View>
    );
  }
}

RenderPreHeaderComponent.propTypes = {

};

export default RenderPreHeaderComponent;
