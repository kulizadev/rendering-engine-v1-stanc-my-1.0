/*
 * RenderPreview Messages
 *
 * This contains all the text for the RenderPreview component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderPreview.header',
    defaultMessage: 'This is the RenderPreview component !',
  },
});
