/**
*
* RenderPreview
*
*/

import React from 'react';
import { View, Image } from 'react-native';
import Text from '../RenderText';
import Icon from 'react-native-vector-icons/MaterialIcons';

import RenderPreviewAbstract from './RenderPreviewAbstract';
import * as styles from './RenderPreviewStyles';

export default class RenderPreview extends RenderPreviewAbstract { // eslint-disable-line react/prefer-stateless-function

  render() {
    const imgstyles = { ...styles.imageStyles };
    if (this.props.circular) {
      imgstyles.style = {
        ...imgstyles.style,
        width: 150,
        height: 150,
        borderRadius: 75,
      };
      imgstyles.resizeMode = 'cover';
    }
    return (
      <View {...styles.containerStyle}>
        {/* <Icon
                  name='close'
                  {...styles.iconStyle}
                  onPress={this.props.onRemoveHandler} />*/}
        <Image
          source={{ uri: this.props.uri }}
          {...imgstyles}
        />
        {/* <Text
          {...styles.chipTextStyle}>
          {this.props.label}
        </Text> */}
      </View>
    );
  }
}
