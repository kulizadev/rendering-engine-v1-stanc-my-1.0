/**
*
* RenderPreview
*
*/

import React from 'react';
import RenderPreviewComponent from './RenderPreviewComponent';

class RenderPreview extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderPreviewComponent {...this.props} />
    );
  }
}

RenderPreview.propTypes = {

};

export default RenderPreview;
