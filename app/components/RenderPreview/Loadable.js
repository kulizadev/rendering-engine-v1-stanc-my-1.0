/**
 *
 * Asynchronously loads the component for RenderPreview
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
