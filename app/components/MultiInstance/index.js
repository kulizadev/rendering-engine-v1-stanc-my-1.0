/**
 *
 * RenderCloningComponentAbstract later to be converted to a helper for editable table too
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import dottie from 'dottie';
import cloneDeep from 'lodash/cloneDeep';
import minBy from 'lodash/minBy';
import maxBy from 'lodash/maxBy';
import meanBy from 'lodash/meanBy';
import sumBy from 'lodash/sumBy';
import * as validationFns from '../../utilities/validations/validationFns';
import * as styles from "../RenderEditableTable/tableStyles";

import * as renderService from '../../utilities/renderDataService';


class MultiInstance extends Component {

    state = {
        instancesData: [],
        instancesRenderData: [],
        instancesErrorData: [],
        emptyInstance: {},
        defaultRowRenderData: {},
        actions: [],
        fieldOrder: [],
        isCalculationTable: false,
        resultRow: {}
    };

    componentDidMount() {
        this.buildInstanceRenderData();
    }

    componentDidUpdate = (prevProps) => {
        if (!this.props.renderData.isValid && prevProps.renderData.toggleInternalValidation !== this.props.renderData.toggleInternalValidation) {
          this.validateWholeInstance();
        }
    };

    addRow = () => {
        const { instancesData, emptyInstance, instancesRenderData, defaultRowRenderData } = {...this.state};
        instancesData.push(emptyInstance);
        instancesRenderData.push(cloneDeep(defaultRowRenderData));
        this.setState({ instancesData, instancesRenderData }, () => {
            this.updateFormData();
            // this.validateWholeInstance();
        });
    };

    deleteRow = (index) => {
        const { instancesData, instancesRenderData } = { ...this.state };
        instancesData.splice(index, 1);
        instancesRenderData.splice(index, 1);
        this.setState({ instancesData, instancesRenderData }, () => {
            this.updateFormData();
            // this.validateWholeInstance();
        });
    };

    convertValueToData = (dummyRenderData) => {
        const { formData, elmId } = this.props;
        const { emptyInstance } = this.state;
        let value = formData[elmId];
        value = !value ? [ emptyInstance] : value;
        const data = typeof value === "object" ? value : JSON.parse(value);
        // Set the renderData for the all the rows => To make the dependency inside a block/row work internally
        const instancesRenderData = [];

        data.forEach((record, index) => {
            // Setting renderData for the hidden/conditional/display-on-selection expressions
            const updatedData = renderService.updateHiddenFields(cloneDeep(dummyRenderData), { ...this.props.formData, ...record}, [...this.state.fieldOrder]);
            instancesRenderData[index] = updatedData.renderData;
        });
        // this is to ensure the initial visible error state is visible, even if onchange for each element have'nt been called.
        this.setState({ instancesData: data, instancesRenderData }, () => {
            // this.validateWholeInstance();
            this.updateFormData();
        });
    };
   
    onChangeHandler = (value, fieldId, index) => {
        // console.log('changing value',this.props)
        // this.props.setRenderData(currentRenderData);
        let instancesRenderData = cloneDeep(this.state.instancesRenderData);

        const { instancesData } = {...this.state};
        // this.keyDecisionMaker(value,instancesData,fieldId);
        if(fieldId==="iskeyDecisionMaker_user_bil"){
        let newIntac=this.filterDicissionTableData(value,fieldId,index,[...instancesData])
        }  
        const updatedRowData = {
            ...instancesData[index],
            [fieldId]: value,
        };
        const updatedData = renderService.updateHiddenFields(instancesRenderData[index], { ...this.props.formData, ...updatedRowData}, [...this.state.fieldOrder]);

        const modifiedFormData = {...instancesData[index]};
        this.state.fieldOrder.forEach((fieldId) => {
            modifiedFormData[fieldId] = updatedData.formData[fieldId];
        });
        instancesData[index] = modifiedFormData;
        instancesRenderData[index] = updatedData.renderData;

        const newInstanceErrorData = this.getUnitValidated(value, fieldId, index, instancesData[index]);
        this.setState({ instancesData, instancesRenderData, instancesErrorData: newInstanceErrorData }, () => {
            this.updateFormData();
            // this.updateInstanceErrorState();
        });
    };
    keyDecisionMaker(newvalue,oldInstancesData,fieldId) {
        let newvalues=Object.keys(newvalue)
            newArr=newvalues.map((oldInstancesData)=>{
                return newvalue[oldInstancesData]
            })
            // newArr.forEach(index => {
            // });
            }
    

    getUnitValidated = (value, fieldId, index, rowData, instancesErrorData = [...this.state.instancesErrorData]) => {
        const { journeyRenderData } = { ...this.props };
        const validationObj = validationFns.validateElm(journeyRenderData[fieldId], value, {...this.props.formData, ...rowData});
        if (!validationObj) return;
        let updateErrorUnitData = {
            ...instancesErrorData[index],
            [fieldId]: validationObj,
        };
        instancesErrorData[index] = updateErrorUnitData;
        return instancesErrorData;
    };

    buildInstanceRenderData = () => {
        const { renderData, journeyRenderData } = this.props;
        let { metaData: { fields, columns, maxCount, allowAdd, allowDelete}} = renderData;
        let dummyRenderData = {};
        let emptyValues = {};
        let fieldMap = {};
        let actions =[];
        let fieldOrder = [];
        allowDelete && actions.push({
            key: 'delete',
            headerLabel: '',
            name: 'Delete',
            onTrigger: this.deleteRow,
            shouldRender: this.isDeleteAllowed
        });
                if(!fields) fields = columns;
        fieldOrder = fields.map(filed => filed.fieldId);
        fields.forEach((field) => {
            const { fieldId } = field;
            if (journeyRenderData[fieldId].metaData.dummyField) {
                // isHidden true is set for all the dummyFields by default ( because they are used for reference, not for actual rendering) 
                // but here we are rendering all these fields so need to set to false for our instancesRenderData
                dummyRenderData = {
                    ...dummyRenderData,
                    [fieldId]: cloneDeep({...journeyRenderData[fieldId], isHidden: false}),
                };
                field['name'] = journeyRenderData[fieldId].name;
            }
        });
        Object.keys(dummyRenderData).forEach((key) => {
            emptyValues = {
                ...emptyValues,
                [key]: dummyRenderData[key].metaData.default || '',
            };
        });

        fields.forEach((field)=>{
            fieldMap[field.fieldId] = field.name;
        });

        // Setting default renderData for the hidden/conditional/display-on-selection expressions
        const updatedData = renderService.updateHiddenFields(cloneDeep(dummyRenderData), { ...this.props.formData, ...emptyValues}, [...fieldOrder]);
        

        this.setState({ 
            emptyInstance: emptyValues, 
            defaultRowRenderData: updatedData.renderData, 
            maxCount, 
            fieldMap, 
            fieldOrder, 
            allowAdd, 
            allowDelete, 
            actions,
            isCalculationTable: renderData.metaData.isCalculationTable }, () => {
            this.convertValueToData(dummyRenderData);
        });
    };

    buildCalculationTableRow = () => {
        const { fieldOrder, instancesData } = this.state;
        const { renderData, onChangeHandler } = this.props;
        let { metaData: { fields, isCalculationTable }} = renderData;
        const resultRow = {};
        // Check if its a Calculation Table
        if (isCalculationTable) {
            // calculate the relative column fields
            fields.forEach((field) => {
                if (!!field.operation) {
                    let fieldId = field.fieldId;
                    resultRow[fieldId] = this.getCalculationResult(instancesData, fieldId, field.operation);
                    // set the result to respective process Variable
                    // if "mapTo" property exists, then update the step form data with the value
                    if (!!field.mapTo) {
                        onChangeHandler(resultRow[fieldId], field.mapTo);
                    }
                } else {
                    resultRow[field.fieldId] = '';
                }
            });

            this.setState({ resultRow });
            console.debug('calculating result Row', resultRow);
        }
    }

    isDeleteAllowed = () => {
        const { instancesData } = this.state;
        return this.state.allowDelete && (instancesData.length > 1);
    }

    isAddAllowed = () => {
        const { instancesData, maxCount } = this.state;
        return  this.state.allowAdd ? ( maxCount ? instancesData.length < maxCount : true) : false;
    };

    validateWholeInstance = (callBack) => {
        let newInstanceErrorData = [];
        const data = this.state.instancesData;
        data.forEach((instanceData,index) => {
            const instance = instanceData;
            const instanceKeys = Object.keys(instance);
            instanceKeys.forEach((fieldKey) => {
                newInstanceErrorData = this.getUnitValidated(instanceData[fieldKey], fieldKey, index, instanceData, newInstanceErrorData);
            });
        });
        this.setState({ instancesErrorData: newInstanceErrorData },callBack);
    };
    

    // updateUnitInstanceErrorState = (value, fieldId, index) => {
    //     this.setState({ 
    //         instancesErrorData: this.getUnitValidated(value, fieldId, index) 
    //     },this.updateInstanceErrorState);
    // }

    updateInstanceErrorState = () => {
        const { elmId, journeyRenderData } = { ...this.props };
        let allValid = true;
        let allErrors = [];
        const { instancesErrorData } = this.state;
        Object.values(instancesErrorData).forEach((instanceErrorRow, instanceIndex) => {
            Object.keys(instanceErrorRow).forEach((instanceKey) => {
                let { isValid, errorMessage } = instanceErrorRow[instanceKey];
                allValid = allValid && isValid;
                !isValid && allErrors.push('Row - '+(instanceIndex+1)+': '+errorMessage);
            });
        });

        this.setState({allErrorMessage: allErrors});
        // this.updateFormData();
    };

    updateFormData = () => {
        const { instancesData, instancesRenderData } = this.state;
        const { onChangeHandler, elmId } = this.props;
<<<<<<< HEAD
        let value = [];
        Object.keys(instancesData).forEach((index) => {
            value[index] = instancesData[index];
        });
        onChangeHandler(JSON.stringify(value), elmId);
=======
        onChangeHandler(JSON.stringify(instancesData), elmId);
        this.buildCalculationTableRow();
>>>>>>> 02251194c54da9bfc3e60f5f8fe80f3ee71b4a3a
    };

makeDefaultFalseKeyDecissionValue=(instanceData)=>{
   let newIntanceFalse =  instanceData.filter((item)=>{
     item.iskeyDecisionMaker_user_bil=false
    //  item.isGuarantor_User_BIL=false
    return item
    })

    return newIntanceFalse
}

buildIntanceData=(value,fieldId,index,instaceFalse)=>{    
    instaceFalse[index].iskeyDecisionMaker_user_bil=value;
    instaceFalse[index].isGuarantor_User_BIL=value;
    return instaceFalse
}

    filterDicissionTableData=(value,fieldId,index,instancesData)=>{

        let instaceFalse=this.makeDefaultFalseKeyDecissionValue(instancesData)

        let updatedIntsnaceData=this.buildIntanceData(value,fieldId,index,instaceFalse)
       
return updatedIntsnaceData 
    }  
    onBlurHandler = (value, elmId) => {
        // this.props.updateFormData(elmId, value);
    };

    getCalculationResult = (source, columnKey, operation) => {
        let result = null;
        switch (operation) {
            case 'mean' : result = meanBy(source, function(source) { return Number(parseFloat(source[columnKey]).toFixed(2)) || 0 });
                break;
            case 'sum' : result = sumBy(source, function(source) { return Number(parseFloat(source[columnKey]).toFixed(2)) || 0 });
                break;
            case 'min' :  result = minBy(source, function(source) { return Number(parseFloat(source[columnKey]).toFixed(2)) })[columnKey];
                break;
            case 'max' : result = maxBy(source, function(source) { return Number(parseFloat(source[columnKey]).toFixed(2)) })[columnKey];
                break;
            case 'count' : result = source && source.length;
                break;
            default:
                result = operation + 'not supported!';
        }

        return result
    }

    render() {
        return null;
    }
}

MultiInstance.propTypes = {
    onChangeHandler: PropTypes.func,
    setRenderData: PropTypes.func,
};

export default MultiInstance;
