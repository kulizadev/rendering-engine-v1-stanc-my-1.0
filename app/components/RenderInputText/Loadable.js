/**
 *
 * Asynchronously loads the component for RenderInputText
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
