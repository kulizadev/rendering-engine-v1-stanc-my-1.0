/**
*
* RenderInputText
*
*/

import React from 'react';
// import styled from 'styled-components';

import RenderInputTextComponent from './RenderInputTextComponent';

class RenderInputText extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  render() {
    return (
      <RenderInputTextComponent {...this.props} />
    );
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
onBlur Fn
*/

export default RenderInputText;
