/**
*
* RenderInputText
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import TextField from 'material-ui/TextField';
import * as common from '../../utilities/commonUtils';
import { appConst } from '../../configs/appConfig';

export default class RenderInputTextAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      cursorPosition: {
        start: 0,
        end: 0,
      },
      showCurrency: props.renderData && props.renderData.metaData && props.renderData.metaData.isCurrency,
      value: (props.compProps && props.compProps.value) || '',
      formattedValue: (props.compProps && common.convertCurrency(props.compProps.value)) || '',
      changeHandlerFlag: false,
    };
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onBlurHandler = this.onBlurHandler.bind(this);
    this.updateCursorPosition = this.updateCursorPosition.bind(this);
    this.toggleFocus = this.toggleFocus.bind(this);
    this.debouncedOnChangeHandler = this.debouncedOnChangeHandler.bind(this);
  }

  updateCursorPosition = () => {
    this.setState({ cursorPosition: { start: 0, end: 1 } });
  }

  // componentWillReceiveProps(newProps) {
  //   if (this.state.changeHandlerFlag) {
  //     return;
  //   }
  //   const self = this;
  //   if (newProps && this.props !== newProps && newProps.compProps.value && newProps.compProps.disabled) {
  //     window.setTimeout(() => self.updateCursorPosition());
  //   }
  //   if (this.props.renderData.metaData.isCurrency) {
  //     const value = (newProps.compProps && newProps.compProps.value) || '';
  //     const formattedValue = (newProps.compProps && common.convertCurrency(newProps.compProps.value)) || '';
  //     const oldFormattedValue = (this.props.compProps && common.convertCurrency(this.props.compProps.value)) || '';
  //     if (formattedValue != oldFormattedValue) {
  //       this.setState({ value, formattedValue });
  //     }
  //   } else {
  //     this.setState({ value: (newProps.compProps && newProps.compProps.value) || '' });
  //   }
  // }

  componentWillMount() {

  }

  onBlurHandler(text) {
    const value = this.props.renderData.metaData.isCurrency ? common.revertCurrency(text, true) : text;
    this.props.onBlurHandler && this.props.onBlurHandler(value, this.props.elmId);
  }

  onChangeHandler = (input) => {
    if (this.props.compProps.textOnly && input && !common.restrictInput(input)) {
      e.preventDefault();
      input = this.props.compProps.value;
    }
    if (this.props.renderData.metaData.isCurrency && input) {
      const actualInput = common.revertCurrency(input);
      input = actualInput;
    }
    this.props.onChangeHandler(input, this.props.elmId);
  }

  debouncedOnChangeHandler = (text) => {
    // if (this.props.renderData.metaData.isCurrency) {
    //   const value = common.revertCurrency(text);
    //   // this.setState({
    //   //   value,
    //   //   formattedValue: text,
    //   //   changeHandlerFlag: true,
    //   // });
    //   this.onChangeHandler(value);
    // } else {
    // this.onChangeHandler(text);
    // }
  };

  toggleFocus = (value) => {
    this.setState({ isFocused: value });
    let input = this.props.compProps.value;
    let { autoCapitalize } = this.props.compProps;

    if (this.props.renderData.metaData.isCurrency) {
      const formattedValue = this.props.renderData.metaData.isCurrency && input ? common.revertCurrency(input, true) : input;
      this.props.onBlurHandler && this.props.onChangeHandler(formattedValue, this.props.elmId);
    } else if (typeof (input) === 'string' && autoCapitalize) {
      input = input.toLocaleUpperCase(appConst.localCurrencyString);
      this.props.onChangeHandler(input, this.props.elmId);
    }
  }

  render() {
    return null;
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
onBlur Fn
*/
