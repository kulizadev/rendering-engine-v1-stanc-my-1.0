/**
*
* RenderInputText
*
*/

import React from 'react';
import { View } from 'react-native';
import Text from '../RenderText';
import RenderInputTextAbstract from './RenderInputTextAbstract';
import TextInputField from '../TextInputField/';
import * as styles from './RenderInputTextStyles';
import * as common from '../../utilities/commonUtils';
import { appConst } from '../../configs/appConfig';

export default class RenderInputText extends RenderInputTextAbstract { // eslint-disable-line react/prefer-stateless-function

  onChangeHandler(e) {
    let input = e.nativeEvent.text;
    if (this.props.compProps.textOnly && e.nativeEvent.text && !common.restrictInput(e.nativeEvent.text)) {
      e.preventDefault();
      input = this.props.compProps.value;
    }
    if (this.props.renderData.metaData.isCurrency && e.nativeEvent.text) {
      const actualInput = common.unformatCurrencyFormat(input);
      input = typeof actualInput === 'number' ? actualInput : '';
    }
    this.props.onChangeHandler(input, this.props.elmId);
  }

  toggleFocus = (value) => {
    this.setState({ isFocused: value });
    const input = this.props.compProps.value;
    if (this.state.value == 0 && this.props.renderData.metaData.isCurrency) {
      this.setState({
        formattedValue: '',
      });
    }
    if (typeof (input) === 'string') {
      // input = input.toLocaleUpperCase(appConst.localCurrencyString);
      this.props.onChangeHandler(input, this.props.elmId);
    }
  }

  onBlur(e) {
    const value = this.props.renderData.metaData.isCurrency ? this.state.formattedValue : this.state.value;
    this.onBlurHandler(value);
  }

  render() {
    const meta = this.props.renderData.metaData;
    const compProps = this.props.compProps;
    const cursorPosition = {};
    // if (compProps.disabled) {
    //   cursorPosition.selection = this.state.cursorPosition;
    // }
    const numValue = compProps.value;
    let propsRe = this.props.compProps;
    if (meta.isCurrency) {
      const currencyHintText = common.getShortCurrency(numValue);
      propsRe = {
        ...compProps,
        hintText: currencyHintText,
      };
    }
    const newCompProps = propsRe;
    const value = meta.isCurrency && compProps.value ? common.convertCurrency(compProps.value, this.state.isFocused) : compProps.value;
    return (
      <View>
        <View>
          <TextInputField
            {...newCompProps}
            {...cursorPosition}
            value={value}
            onFocus={() => this.toggleFocus(true)}
            onBlur={() => this.toggleFocus(false)}
            onChange={(e) => this.onChangeHandler(e.nativeEvent.text)}
            onBlurHandler={(e) => this.onBlur(e)}
          />
        </View>
      </View>
    );
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
onBlur Fn
*/
