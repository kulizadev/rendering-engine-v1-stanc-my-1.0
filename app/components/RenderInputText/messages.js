/*
 * RenderInputText Messages
 *
 * This contains all the text for the RenderInputText component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderInputText.header',
    defaultMessage: 'This is the RenderInputText component !',
  },
});
