import { consoleConfig } from 'js-console-logger';
import store from '../utils/store';
import { dumbLog } from '../containers/AppDetails/actions';

const reportFn = (obj) => {
  store.dispatch(dumbLog(obj));
};

consoleConfig.setReportFn(reportFn); // eslint-disable-line no-undef
