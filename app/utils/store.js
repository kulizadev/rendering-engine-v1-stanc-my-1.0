import configureStore from '../../app/configureStore';

const initialState = {};

const store = configureStore(initialState);

export default store;
