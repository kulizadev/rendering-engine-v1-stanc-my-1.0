let baseUrl;
if (process.env.NODE_ENV === 'development') {
  baseUrl = 'http://3.217.123.186:8080/journey-0.0.1-SNAPSHOT/';
  // baseUrl = 'http://35.200.183.145:8080/accenturemvp-0.0.1/';
  // baseUrl = 'http://35.186.146.10/';
} else if (process.env.NODE_ENV === 'production') {
  // baseUrl = 'http://35.200.183.145:8080/accenturemvp-0.0.1/';
  baseUrl = 'http://3.217.123.186:8080/journey-0.0.1-SNAPSHOT/';
}

export const BASE_API_URL = baseUrl;
export const IS_SIDEBAR_SHOWN = true;
