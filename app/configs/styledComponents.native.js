/*
*
* styledComponents
* Serving as a styling pipe for native components.
*
*/

import React from 'react';
import { Text } from 'react-native';
import { colors, fonts } from '../configs/styleVars';

const getStyledRequiredLabel = (text) => {
  const errorStyle = {
    color: colors.requiredColor,
  };
  return <React.Fragment>{text}<Text style={errorStyle}> *</Text></React.Fragment>;
};

const getLinkStyledText = (text) => {
  const linkStyle = {
    color: colors.tableActionsLinkColor,
  };
  return <Text style={linkStyle}> {text}</Text>;
};

const getStyledHintText = (text) => {
  const style = {
    fontStyle: fonts.hintFontStyle,
    fontSize: fonts.hintFontSize,
    opacity: fonts.hintOpacity,
  };
  return <Text style={style}> {(text || '').replace(/\b\w/g, (l) => l.toUpperCase())}</Text>;
};

const styledComponents = {
  getStyledRequiredLabel,
  getStyledHintText,
  getLinkStyledText,
};

export default styledComponents;
