import styleVars from 'configs/styleVars';
const fonts = styleVars.fonts,
  colors = styleVars.colors;

export const checkBoxComponentStyle = {
  style: { // This is the root element style
    width: '100%',
  },
  labelStyle: {
    fontSize: fonts.fontSize,
    color: colors.basicFontColor,
  },
};
