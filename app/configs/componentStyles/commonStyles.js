import styleVars from 'configs/styleVars';
const fonts = styleVars.fonts,
  colors = styleVars.colors;


export const inputComponentStyle = {
  style: { // This is the root element style
    width: '100%',
    color: colors.basicFontColor,
  },
  inputStyle: {
    marginBottom: 0,
  },
  floatingLabelStyle: {
    fontSize: fonts.fontSize,
    whiteSpace: 'nowrap',
  },
  underlineFocusStyle: {
    borderColor: colors.blueSideNavBG,
  },
  floatingLabelFocusStyle: {
    color: colors.textLabel,
  },
};

export const dropdownComponentStyle = {
  style: { // This is the root element style
    width: '100%',
    color: colors.basicFontColor,
  },
  inputStyle: {
    marginBottom: 0,
  },
  floatingLabelFocusStyle: {

  },
  floatingLabelStyle: {
    fontSize: fonts.fontSize,
  },
  underlineFocusStyle: {
    borderColor: colors.blueSideNavBG,
  },
  floatingLabelFocusStyle: {
    color: colors.textLabel,
  },
};
