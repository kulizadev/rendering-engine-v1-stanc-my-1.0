import styleVars from 'configs/styleVars';
const fonts = styleVars.fonts,
  colors = styleVars.colors;


export const inputComponentStyle = {
  style: { // This is the root element style
    width: '100%',
    color: colors.basicFontColor,
  },
  inputStyle: {
    marginBottom: 0,
  },
  floatingLabelFocusStyle: {

  },
  floatingLabelStyle: {
    fontSize: fonts.fontSize,
  },
};
