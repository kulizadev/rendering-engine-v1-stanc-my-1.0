import { fonts, colors, lineHeight } from '../styleVars';

export const inputComponentStyle = {
  baseColor: colors.labelColor,
  tintColor: colors.primaryBGColor,
  errorColor: colors.errorColor,
  fontSize: fonts.body,
  labelFontSize: fonts.label,
  textColor: colors.inputColor,
  labelHeight: 35,
  labelPadding: 6,
  activeLineWidth: 1,
  disabledLineWidth: 0,
  inputContainerStyle: {
    paddingBottom: 6,
  },
  labelTextStyle: {
    ...fonts.getFontFamilyWeight(),
  },
  titleTextStyle: {
    ...fonts.getFontFamilyWeight(),
  },
  style: {
    ...fonts.getFontFamilyWeight(),
    color: colors.inputColor,
  },
};

export const nativeWrapperStyle = {
  flex: 1,
  justifyContent: 'flex-start',
  overflow: 'hidden',
};

export const formWrapper = {
  paddingLeft: 20,
  paddingRight: 20,
};
