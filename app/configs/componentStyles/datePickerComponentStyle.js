import styleVars from 'configs/styleVars';
const fonts = styleVars.fonts,
  colors = styleVars.colors;

export const datePickerComponentStyle = {
  style: { // This is the root element style
    width: '100%',
  },
  textFieldStyle: {
    marginBottom: 0,
    color: colors.basicFontColor,
  },
  floatingLabelFocusStyle: {

  },
  floatingLabelStyle: {
    fontSize: fonts.fontSize,
  },
};
