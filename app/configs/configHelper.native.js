import { Dimensions } from 'react-native';
import Config from 'react-native-config';
import { appBreakPoints } from './appConstants';

export const BASE_API_URL = Config.BASE_API_URL;
export const IS_SIDEBAR_SHOWN = Dimensions.get('window').width > appBreakPoints.MEDIUM_Width;
