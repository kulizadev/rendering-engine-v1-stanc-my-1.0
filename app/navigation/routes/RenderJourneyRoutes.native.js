import React from 'react';
import RenderJourney from '../../containers/RenderJourney/';
import AppHeader from '../../components/AppHeader';

// can move AppHeader within the component for better control
const RenderJourneyRoutes = {
  RenderJourney: {
    screen: RenderJourney,
    navigationOptions: {
      header: ({ screenProps }) => <AppHeader logout={screenProps.logout} isLoggedin={screenProps.isLoggedin} type={'appHeader'} />,
    },
  },
};

export default RenderJourneyRoutes;
