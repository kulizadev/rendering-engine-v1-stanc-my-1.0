import React from 'react';
import Login from '../../containers/Login/';
import RenderOtp from '../../containers/RenderOtp/';
import RenderMPin from '../../containers/RenderMpin/index';
import RenderConfirmMpin from '../../containers/RenderConfirmMpin/';
import RenderTouchId from '../../containers/RenderTouchId/';
import AppHeader from '../../components/AppHeader';
import { strings } from '../../../locales/i18n';

const AuthRoutes = {
  Login: {
    screen: Login,
    navigationOptions: {
      header: ({ screenProps }) => <AppHeader logout={screenProps.logout} type={'appHeader'} />,
    },
  },
  RenderOtp: {
    screen: RenderOtp,
    navigationOptions: {
      header: ({ screenProps }) => <AppHeader logout={screenProps.logout} type={'preHeader'} />,
    },
  },
  RenderMPin: {
    screen: RenderMPin,
    navigationOptions: {
      header: ({ screenProps }) => (
        <AppHeader
          type={'preHeader'}
          logout={screenProps.logout}
          title={strings('login.creatPassHeader')}
        />
      ),
    },
  },
  RenderConfirmMpin,
  RenderTouchId,
};

export default AuthRoutes;
