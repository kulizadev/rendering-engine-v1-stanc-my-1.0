import React from 'react';
import { createStackNavigator } from 'react-navigation';
import RenderJourneyRoutes from './routes/RenderJourneyRoutes.native';

const Navigator = createStackNavigator(RenderJourneyRoutes, {
  // initialRouteName: 'JourneyOption',
  navigationOptions: {
    header: null,
  },
});

export default class RenderJourneyNavigator extends React.Component {

  static router = Navigator.router;

  constructor(props) {
    super(props);
    this.state = {
      journeyName: '',
    };
  }

  setJourneyName = (journeyName) => {
    this.setState({ journeyName });
  }

  render() {
    const { navigation, screenProps, ...otherProps } = this.props;
    const isLoggedin = !!(screenProps.bpm_token);

    return (
      <Navigator
        screenProps={{
          ...otherProps,
          ...screenProps,
          isLoggedin,
          setJourneyName: this.setJourneyName,
          journeyName: this.state.journeyName,
        }}
        navigation={navigation}
      />
    );
  }
}
